/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : db_unchu

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2021-03-01 17:41:22
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `tb_data_konsumen`
-- ----------------------------
DROP TABLE IF EXISTS `tb_data_konsumen`;
CREATE TABLE `tb_data_konsumen` (
  `id_data_konsumen` int(11) NOT NULL AUTO_INCREMENT,
  `nama_lengkap` varchar(100) DEFAULT NULL,
  `tempat_lahir` varchar(50) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `jenis_identitas` enum('KTP','SIM','PASSPORT') DEFAULT NULL,
  `identitas` varchar(100) DEFAULT NULL,
  `alamat_rumah` varchar(200) DEFAULT NULL,
  `kelurahan` varchar(100) DEFAULT NULL,
  `kecamatan` varchar(100) DEFAULT NULL,
  `kota` varchar(100) DEFAULT NULL,
  `propinsi` varchar(100) DEFAULT NULL,
  `status_pernikahan` enum('MENIKAH','BELUM MENIKAH','JANDA','DUDA','') DEFAULT NULL,
  `pass_foto` varchar(100) DEFAULT NULL,
  `doc_identitas` varchar(100) DEFAULT NULL,
  `no_hp` varchar(20) DEFAULT NULL,
  `no_hp_2` varchar(20) DEFAULT NULL,
  `no_hp_3` varchar(20) DEFAULT NULL,
  `email` varchar(225) DEFAULT '',
  `npwp` varchar(25) DEFAULT NULL,
  `jml_anak` int(2) DEFAULT NULL,
  `alamat_penagihan` varchar(100) DEFAULT NULL,
  `kelurahan_penagihan` varchar(100) DEFAULT NULL,
  `kecamatan_penagihan` varchar(100) DEFAULT NULL,
  `kota_penagihan` varchar(100) DEFAULT NULL,
  `propinsi_penagihan` varchar(100) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `tgl_entry` datetime DEFAULT NULL,
  `tgl_update` datetime DEFAULT NULL,
  PRIMARY KEY (`id_data_konsumen`) USING BTREE,
  KEY `id_data_konsumen` (`id_data_konsumen`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of tb_data_konsumen
-- ----------------------------
INSERT INTO `tb_data_konsumen` VALUES ('1', 'ZAINAL', 'TANGERANG', '1998-06-06', 'KTP', '366763434343', 'KP RUMPAK SINANG', '3603180008', '3603180', '3603', '36', 'MENIKAH', null, null, '0812717821', null, null, 'asa@gmail.com', '7832839123', '2', 'KP RUMPAK SINANG', '3603180008', '3603180', '3603', '36', '1', '2020-10-11 08:20:35', '2020-10-11 08:20:38');
INSERT INTO `tb_data_konsumen` VALUES ('28', 'Sampurasun', 'Brebes', '1988-12-31', 'KTP', '055551111343444', 'Jln. Kehidupan', '3603180008', '3603180', '3603', '36', 'MENIKAH', null, null, '080912123', null, null, 'djibroet@gmail.com', '67779089122678', '10', 'Jln. Pertemuan', '3603180008', '3603180', '3603', '36', '2', '2021-01-09 11:39:08', '2021-01-09 11:39:08');
INSERT INTO `tb_data_konsumen` VALUES ('29', 'Sampurasun', 'Brebes', '1988-12-31', 'KTP', '0555511113545', 'Jln. Kehidupan', '3603180008', '3603180', '3603', '36', 'MENIKAH', null, null, '0809121233', null, null, 'djibroetz@gmail.com', '67779089123', '10', 'Jln. Pertemuan', '3603180008', '3603180', '3603', '36', '3', '2021-01-28 07:17:01', '2021-01-28 07:17:01');
INSERT INTO `tb_data_konsumen` VALUES ('30', 'Doni', 'Jakarta', '2021-02-05', 'SIM', '0555511113434445', 'Kepulauan ratu', '1501020001', '1501020', '1501', '15', 'MENIKAH', null, null, '0895327244444', null, null, 't1zhi2z5dnj@temporar', '677790891226784', '0', null, null, null, null, null, '2', '2021-02-05 21:52:55', '2021-02-05 21:52:55');
