$(document).ready(function() {
    app.vm = new Vue({
        el: '#app',
        data: {
            sub_title: app.data.sub_title,
            // nama_unor: app.data.nama_unor,
            config: {
                filter_open: false
            },
            filter_data: {
               jenis_pengguna: app.data.jenis_pengguna,
               status_pengguna : app.data.status_pengguna
            },
            pagination: app.data.pagination,
            pagination_per_pages: [10, 25, 50, 100],
            filter: {

                search_query: app.data.search_query,
                jenis_pengguna:'',
                status_pengguna:''
            },
            grid_data: [],
            pager: {
                input_page: 1,
                current_page:1,
                page_count:4,
                page_to_display:[],
                is_first_page:false,
                has_next:false,
                has_prev:false,
                prev_page:0,
                next_page:0,
                max_page_to_display:5
            },
            state:{
            	grid_shown:true,
            	detail_shown:false,
                form_shown:false
            },
            detail:null,
            form:{mode:'',is_valid:false},
            new_detail:null,
            dl:{
                enable:false,
                url: app.site_url(`appbkpp/cetak/pegawai_pensiun/excel`),
                src:''
            },
            yes_no : {'0':'No','1':'Yes'}
        },
        mounted:function(){
        	console.log('mounted');
        	this.gridPaging('');
        },
        methods: {
            goBack: function() {
                document.location.href = app.link.pegawai_aktif;
            },
            toggleFilter: function() {
                this.config.filter_open = !this.config.filter_open;
            },
            onFilterChanged: function() {
                console.log(this.filter);
                this.goPaging('');
            },
            onPaginationChanged: function() {
                console.log(this.pagination);
                this.goPaging('');

            },
            onGridSearch: function() {
                console.log(this.filter.search_query);
                this.goPaging('');
            },
            goPaging: function(page) {
                // console.log(app.vm.pager.input_page);
                // this.gridPaging(this.pager.input_page);
                this.pagination.page = page;
                this.gridPaging(page);
            },
            inputPagingBlur: function(event) {
                const el = event.target;
                if (el.value == '') {
                    el.value = '1';
                }
            },
            inputPagingFocus: function(event) {
                const el = event.target;
                if (el.value == '1') {
                    el.value = '';
                }
            },
            gridPaging: function(page) {
                console.log(page);
                var form_data = new FormData();
				for ( var key in this.filter ) {
				    form_data.append(key, this.filter[key] );
				}
				form_data.append('page',page);
				form_data.append('per_page',this.pagination.per_page);
                axios({
                	method:'post',
                	url:app.site_url('data/user/json'),
                	data:form_data,
                	headers: {'Content-Type': 'multipart/form-data' },

                })
				    .then((response) => {
				        // Success 🎉
				        let data = response.data;
				        this.grid_data = data.records;
				        this.buildPager(data);
				    })
				    .catch((error) => {
				        swal(error.response.data);
				    });

                // 
            },
            buildPager:function(data){
            	this.pager.page_count = parseInt(data.total_pages);
				this.pager.current_page = parseInt(data.current_page);
				this.pagination.page = parseInt(data.current_page);
				this.pager.input_page = parseInt(data.current_page);
				this.pager.page_to_display=[];
                    
                    this.pager.page_count = isNaN(this.pager.page_count) ?0 : this.current_page.page;
                    this.pager.current_page = isNaN(this.pager.current_page) ? 0 : this.pager.current_page ;
                    this.pagination.page = isNaN(this.pagination.page) ? 0 : this.pagination.page;
                    this.pager.input_page = isNaN(this.pager.input_page) ? 0 : this.pager.input_page;

				/*
					if x+(y-1)==total_pages
					 loop from hi to lo
					else
					 loop from current page until max_page_to_display
				*/
				this.pager.max_page_to_display = parseInt(this.pager.max_page_to_display);
				// this.pager.current_page = parseInt(this.pager.current_page);
				if((this.pager.current_page + (this.pager.max_page_to_display-1) >= data.total_pages)&&data.total_pages>this.pager.max_page_to_display){
					//                11             >   11-5=6    
					let i = 1;
					let last = data.total_pages;//11
					while(i<=this.pager.max_page_to_display){
                        console.log(last)
						this.pager.page_to_display.push(last--);
						i++;
					}   
					// for (var i = data.total_pages; i > l; i--) {
						
					// }
                    console.log(this.pager)
					this.pager.page_to_display.reverse();
				}else{

					if(data.total_pages > 1 ){
					    let brk = 1;
						for (var i = (this.pager.current_page == data.total_pages?1:this.pager.current_page); i < (this.pager.current_page+this.pager.max_page_to_display); i++) {
							this.pager.page_to_display.push(i);
							if(brk == data.total_pages){
                                break;
                            }
                            brk++;
						}
					}else{
						this.pager.page_to_display.push(1);
					}
				}

				if(this.pager.current_page == data.total_pages){
					this.pager.has_next = false;
				}else{
					this.pager.has_next = true;
					this.pager.next_page = this.pager.current_page + 1;
				}

				if(this.pager.current_page == 1){
					this.pager.has_prev = false;
				}else{
					this.pager.has_prev = true;
					this.pager.prev_page = this.pager.current_page - 1;
				}
            },
            // photoUrl: function(foto,width,height){
            // 	// if(typeof foto == undefined)return '';
            // 	let rand = (new Date()).getTime();
            // 	let w = 150;
            // 	let h = 200;
            // 	if(!isNaN(height)){
            // 		h = height;
            // 	}
            // 	if(!isNaN(width)){
            // 		w = width;
            // 	}
            // 	return app.site_url(`appbkpp/pegawai/foto_thumb/${foto}/${w}/${h}?nd=${rand}`);
            // },
            getRowNumber:function(index){
            	let number = 0;
            	let page = parseInt(this.pagination.page)+0;
            	let per_page = parseInt(this.pagination.per_page);

            	if(page === 1){
            		number = index+1;
            	}else{
            				// 1       +  (10 * 1)	
            		number = (index+1) + (per_page*(page-1));
            	}
            	return number;
            }, 
            backToGrid:function(isForm){
                if(typeof isForm != 'undefined'){
                    if(isForm == true){
                        this.state.form_shown = false;
                        this.new_detail = null;
                    }
                }
            	this.state.grid_shown = true;
            	this.state.detail_shown = false;
            },
            getTabRo:function(m,id){
            	
            },
            applyJsForm(){
                let self = this;
                this.$nextTick(function(){
                //     $('input[name=tanggal_pensiun],input[name=tanggal_sk]').datetimepicker();
                //     $('input[name=tanggal_pensiun]').on('dp.change',function(e){
                //         // console.log(e)
                //         self.form.tanggal_pensiun = e.target.value;
                //         self.validateForm();
                //     });
                //     $('input[name=tanggal_sk]').on('dp.change',function(e){
                //         self.form.tanggal_sk = e.target.value;
                //         self.validateForm();
                //     });;

                });
            },
            formEdit(item){
                console.log(item);
                this.detail = item;
                this.state.form_shown = true;
                this.state.grid_shown = false;
                this.form = {mode:'edit',submited:false};
                this.form.id = item.id;
                this.form.email = item.email;
                this.form.nama_lengkap = item.nama_lengkap;
                this.form.nik = item.nik;
                this.form.jenis = item.jenis;
                this.form.is_active = item.is_active.length > 0 ? item.is_active: 0;
                this.form.is_verified = item.is_verified.length > 0 ? item.is_verified : 0; 

                this.form.jenis = item.jenis;
                 

                this.applyJsForm();
                this.form.is_valid = this.validateForm();

            },
            formDelete(item){
                // console.log(item);
                this.detail = item;
                this.state.form_shown = true;
                this.state.grid_shown = false;
                this.form = {mode:'delete',submited:false};
                this.form.id = item.id;
                this.form.is_valid = this.validateForm();
                this.form.jenis = item.jenis;
                this.form.is_active = item.is_active.length > 0 ? item.is_active: 0;
                this.form.is_verified = item.is_verified.length > 0 ? item.is_verified : 0;

            },
            formAdd(){
                // console.log(item);
                // this.detail = item;
                this.state.form_shown = true;
                // this.state.grid_shown = false;
                // this.form = {mode:'add',submited:false,nip_baru:''};
                // // this.form.id = item.id;
                // this.form.id_pegawai = '';
                // this.form.tanggal_pensiun = '';
                // this.form.no_sk = '';
                // this.form.tanggal_sk = '';
                // this.form.jenis_pensiun = '';
                // this.applyJsForm();
                // this.form.is_valid = this.validateForm();

            },
            validateForm(){
                let isValid = true;

                let a = this.form.id_pegawai !== "" ; 
             
                // let b = this.form.tanggal_sk.match(/^(\d{1,2})-(\d{1,2})-(\d{4})$/) ; 
                // let c = this.form.tanggal_pensiun.match(/^(\d{1,2})-(\d{1,2})-(\d{4})$/) ;
           
                // let d = this.form.jenis_pensiun.length > 1 ;
                // let e = this.form.no_sk.length > 1;
                // isValid = a && d && e;
                this.form.is_valid = isValid;
            },
            formSubmit(form){
                console.log(this.form.mode)
                this.validateForm();
                if(!this.form.is_valid){
                    swal('Mohon Lengkapi data anda');
                    return;
                }
                if(this.form.submited === false){
                    this.form.submited = true;
                    // console.log(form);
                    let messages = {
                        'edit' : 'Sedang Mengupdate data',
                        'activate' : 'Sedang Mengirim Email',
                        'delete' : 'Sedang Menghapus data',

                    };
                    $.blockUI({message:messages[this.form.mode]})

                    var form_data = new FormData();
                    if(this.form.mode == 'edit' || this.form.mode == 'delete' || this.form.mode == 'activate'){
                        form_data.append('id',this.form.id);
                    }
                    if(form.mode == 'add'){
                        // form_data.append('nip_baru',this.form.nip_baru);
                        // form_data.append('nama_pegawai',this.form.nama_pegawai);
                    }
                    
                    form_data.append('nik',this.form.nik);
                    form_data.append('nama_lengkap',this.form.nama_lengkap);
                    form_data.append('email',this.form.email);
                    form_data.append('is_active',this.form.is_active);
                    form_data.append('is_verified',this.form.is_verified);
                    form_data.append('jenis',this.form.jenis);
                    axios({
                        method:'post',
                        url:app.site_url('manajemen/user/form/'+this.form.mode),
                        data:form_data,
                        headers: {'Content-Type': 'multipart/form-data' },

                    })
                    .then((response) => {
                        // Success 🎉
                        $.unblockUI();
                        let data = response.data;
                        console.log(data);
                        // $(`div[m=${m}]`).html(data);
                        // if(form.mode=='delete'){
                            if(data=='success'){
                                snackbar('Success !');
                                this.backToGrid(true); 
                                this.gridPaging('');
                            }
                            else{
                                snackbar(data);
                            }
                           
                        // }
                        this.form.submited = false;
                    })
                     .catch((error) => {
                        $.unblockUI();
                        swal(error.response.data);
                        this.form.submited = false;
                    });

                        
                }
            },
            activateUser(detail){
                this.form = detail;
                this.form.mode = 'activate';
                this.form.submited = false;
                console.log(this.form);
                this.formSubmit(this.form);
            },
            formatDate(str){
                try{
                    let dt = str.split('-');
                    if(dt.length==3){
                        return dt[2]+'-'+dt[1]+'-'+dt[0];
                    }
                }catch(e){
                    return '00-00-0000';
                }
            },
            mysqlDate(str){
                try{
                    let dt = str.split('-');
                    if(dt.length==3){
                        return dt[2]+'-'+dt[1]+'-'+dt[0];
                    }
                }catch(e){
                    return '0000-00-00';
                }
            },
            checkInvalidDate(str){
                if(str == '01-01-1970' || str == '00-00-0000' || str == '30-11--0001'){
                    return '-';
                }else{
                    return str;
                }
            },
            printExcel(){

                this.dl.enable = false;
                this.dl.src = app.site_url('docs/blank.txt');
                this.$nextTick(()=>{
                    let param64 = $.extend({},this.filter,this.pagination);
                     console.log(param64);    
                     param64 = btoa(JSON.stringify(param64));
                    // let self

                    setTimeout(()=>{
                        this.dl.enable = true;
                        this.dl.src = this.dl.url + '?param='+param64;
                        // $('#dl').get(0).contentWindow.document.location.href = app.site_url('docs/blank.txt');  

                    },500);
                });
                
            }
        }
    });
});