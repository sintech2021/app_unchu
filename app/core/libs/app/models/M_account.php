<?php


/**
 * 
 */
class M_account extends CI_Model
{
	
	public function get_list()
	{
		$SQL = "
			SELECT
			m.id user_id,
			(CASE 
				WHEN m.t = 2 THEN a.username  
				WHEN m.t = 1 THEN n.username
				WHEN m.t = 0 THEN d.username
			END) as username,
			(CASE 
				WHEN m.t = 2 THEN a.email  
				WHEN m.t = 1 THEN n.email
				WHEN m.t = 0 THEN d.email
			END) as email,
			(CASE 
				WHEN m.t = 2 THEN a.nama_lengkap  
				WHEN m.t = 1 THEN n.nama_lengkap
				WHEN m.t = 0 THEN d.nama_lengkap
			END) as nama_lengkap,
			(CASE 
				WHEN m.t = 2 THEN a.passwd  
				WHEN m.t = 1 THEN n.passwd
				WHEN m.t = 0 THEN d.passwd
			END) as passwd, 

			m.user_id parent_id, m.t 
			FROM user_map m
			LEFT JOIN account a ON m.user_id = a.id AND m.t = 2
			LEFT JOIN account_register n ON m.user_id = n.id AND m.t = 1 
			LEFt JOIN account_adm  d ON m.user_id = d.id AND m.t = 0

		"

		return $this->db->query($SQL)->result();
	}
}