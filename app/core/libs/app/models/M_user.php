<?php

class M_user extends CI_Model
{ 
	
	public function get_list($cari, $mulai, $batas, $filter)
	{
		if(!empty($cari)){
        	$this->db->like('am.nama_lengkap',$cari)
        			 ->or_like('am.nip_nik',$cari)
        			 ->or_like('am.email',$cari)
        			 ->or_like('am.username',$cari);

        }
        if(!empty($filter['id_unor'])){
        	$unor = $filter['id_unor'];
        	$this->db->like('unit_kerja',$unor,'after');
        }
        if(!empty($filter['status_marketing'])){
        	$this->db->where('jenis',$filter['status_marketing']);
        }
        $this->db->limit($batas)->offset($mulai);

		$results =   $this->db->select('am.user_id,am.role,am.roles,am.is_active,am.nip_nik,am.t,am.jenis,am.nama_lengkap ')
						  ->get('account_map am')
						  ->result();
		foreach ($results as $index => &$row) {
		  	$row->row_number = $index + 1;
		}				  
		return $results;
}

	public function get_count($cari, $filter)
	{
		if(!empty($cari)){
        	$this->db->like('am.nama_lengkap',$cari)
        			 ->or_like('am.nip_nik',$cari)
        			 ->or_like('am.email',$cari)
        			 ->or_like('am.username',$cari);

        }
        if(!empty($filter['id_unor'])){
        	// LIKE '%\"kode_unor\":\"{$unor}%' ";
        	$unor = $filter['id_unor'];
        	$this->db->like('unit_kerja',$unor,'after');
        }
        if(!empty($filter['status_marketing'])){
        	$this->db->where('jenis',$filter['status_marketing']);
        }
		$row =  $this->db->select("COUNT(am.user_id) _count")
		        		 ->from('account_map am')
						 ->get()->row();
		return $row->_count;
	}

	public function get_display_name($user_id)
	{
		$row = $this->db->where('user_id',$user_id)->get('account_map')->row();
		if(!empty($row)){
			return $row->nama_lengkap;
		}
		return '';
	}
	public function get_unit_kerja($id_user)
	{
		# code...
		return '';
	}
}