<?php

use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Exception\ParseException; 

class Acl
{
	public $_start_segment = 1;
	public $_ci;

	function __construct()
	{
		$this->_ci = get_instance();
	}

	public function get_config_from_module($module_name,$method)
	{
		
		$acl_files = ['acl'];
		foreach ($acl_files as $filename) {
		 	$path 	= BASE . '/modules/'.$module_name . '/config/'.$filename.'.yml';

			if(file_exists($path)){
				$acl = Yaml::parse(file_get_contents($path));
				return $acl[$method];
			}
			return [];
		} 
		
	}
	protected function _trim(&$vars){
		
		if(is_array($vars)){
			$new_vars = [];
			foreach ($vars as $key => $value) {
				$n_key = trim($key);
				$n_val = trim($value);
				$new_vars[$n_key] = $n_val;
			}
			$vars = $new_vars;
			return $vars;
		}
		return trim($vars);

	}
	public function start()
	{
		$uri_segments = $this->_ci->uri->segment_array();
		$path = '';
		$paths = [];
		foreach ($uri_segments as $segment) {
			$paths[]= underscorize($segment);
		}
		$module = $paths[0];
		$path 	= BASE . '/modules/'.$module . '/config/acl.yml';

		if(file_exists($path)){
			$acl = Yaml::parse(file_get_contents($path));
			$segment_keys = [];
			$segment_ptr = '';
			foreach ($paths as $idx => $segment_key) {
				if($idx<2){
					continue;
				}
				$segment_ptr .= ($idx > 2 ? '/' :'').$segment_key; 
				$segment_keys[] = $segment_ptr;
			}	
			
			

			if($segment_key_counts == 1 && empty($segment_keys[0])){
				$segment_keys[0] .= 'index';
			}else if(empty($segment_keys)){
				$segment_keys[] = 'index';
			}
			$segment_key_counts = count($segment_keys);
			// $segment_keys[0]
			 // print_r($segment_keys);

			if($segment_key_counts > 0){
				$method = $paths[1];
				// die($method);
				if(is_array($acl[$method])){
					$method_keys = array_keys($acl[$method]);
					$segment_key_max_index = $segment_key_counts -1;
					// print_r($method_keys);
					while ($segment_key_max_index >= 0) {
						$active_segment_key = $segment_keys[$segment_key_max_index];
						
						if(in_array($active_segment_key, $method_keys)){
							if(is_array($acl[$method][$active_segment_key])){
								$acl_conf = $acl[$method][$active_segment_key];
								$current_role = trim($this->_ci->cms_user_group());
								// echo $current_role . "\n";
								if(is_array($acl_conf['credentials'])){
									$this->_trim($acl_conf['credentials']);
									// echo $active_segment_key . "\n";
									// print_r($acl_conf['credentials']);
									if(!in_array($current_role, $acl_conf['credentials'])){
										// print_r($acl_conf['credentials']);	
										if($this->_ci->input->is_ajax_request()){
											die(json_encode(['ERROR_ACCESS_DENIED']));
										}else{
											$this->_ci->__site_layout ='access_denied';
											// $this->_ci->template->clear_body();
											$data = [
												'role' => $current_role,
												'method' => $method,
												'acl_conf' => $acl[$method],
												'active_segment' => $active_segment_key,
												'segment_key_max_index' => $segment_key_max_index,
												'filepath' => $path,
												'segment_keys'=>$segment_keys
											];
											echo '';
											return $this->_ci->load->view('errors/access_denied',$data);
											exit();
										}
										
										
									}else{
										break;
									}
								}
								
							}
							
						}

						$segment_key_max_index = $segment_key_max_index - 1;
					}
				}
			}

			
			
			
		}
		
	}

	public function uri_segment($index)
	{
		return $this->_ci->uri->sement($index + $this->_start_segment);
	}
}