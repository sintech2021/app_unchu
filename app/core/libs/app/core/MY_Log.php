<?php

/**
 * 
 */
class MY_Log extends CI_Log
{
	protected $_log_path_2;

	function __construct()
	{
		$this->_log_path_2 = BASE .'/logs/';
		parent::__construct();
	}
	public function write_log2( )
	{
		if ($this->_enabled === FALSE)
		{
			return FALSE;
		}

	 

		 
		$ci = get_instance();
		$user_id = $ci->cms_user_id(); 	

		$seg_1 = $ci->uri->segment(1); 


		$log_path = $seg_1 ;
		$uri = $seg_1;

		$waktu_akses = '';
		$menu = '';
		$aksi = '';
		$status = '-';

		$segments =[2,3,4,5,6,7,8,9,10];

		foreach ($segments as $segment) {
			$seg = $ci->uri->segment($segment);
			if(empty($seg)){
				break;
			}
			$log_path .= is_numeric($seg) ?  '' :  '/' .$seg;
			$uri .= '/' . $seg;
		}
		$save_to_log_user = false;
		
		if( isset( LOG_YML[$log_path] ) ){
			$row = LOG_YML[$log_path];

			$rows = explode('@', $row);	

			if(isset($rows[0])){
				$menu = $rows[0];
			}

			if(isset($rows[1])){
				$aksi = $rows[1];
			}
			$data_log = $uri;
			
			$save_to_log_user = true;

		}

		if($save_to_log_user){
			$log_data = [
				'user_id' => $user_id,
				'waktu_akses' => date('Y-m-d H:i:s'),
				'menu' => $menu,
				'aksi' => $aksi,
				'data_log' => $data_log,
				'aksi' => $aksi,
				'ip_klien' => $ci->input->ip_address(),
				'status' => $status								   
			];

			$ci->db->insert('m_log_user',$log_data);
		}
		 
	}


}