<!DOCTYPE html>


<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>UNCHU APP | Akses Ditolak</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="Preview page of Metronic Admin Theme #1 for 500 page option 2" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<?=site_url()?>themes/metronic/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=site_url()?>themes/metronic/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=site_url()?>themes/metronic/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=site_url()?>themes/metronic/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="<?=site_url()?>themes/metronic/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?=site_url()?>themes/metronic/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="<?=site_url()?>themes/metronic/assets/pages/css/error.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> </head>
    <!-- END HEAD -->

    <body class=" page-500-full-page">
        <div class="row">
            <div class="col-md-12 page-500">
                <div class=" number font-red"> 503 </div>
                <div class=" details">
                    <h3>Akses Ditolak</h3>
                    <p> Maaf, Anda tidak memiliki akses untuk halaman ini.
                        <br/> </p>
                    <p>
                        <a href="<?=site_url()?>" class="btn red btn-outline"> Kembali ke Beranda </a>
                        <br> </p>
                </div>
            </div>

        </div>
        <div class="row">
        	<div class="col-md-12" style="padding: 3em">
<?if(ENVIRONMENT=='development'):?>
<div class="panel-group accordion" id="accordion3">
    
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_4" aria-expanded="true"> Lihat Detail </a>
            </h4>
        </div>
        <div id="collapse_3_4" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
            <div class="panel-body">
            <table class="table">
				<tr>
					<th>Grup Pengguna</th><td><?=$role?></td>
				</tr>
				<tr>		
					<th>Method</th><td><?=$method?></td>
				</tr>
				<tr>		
					<th>File ACL</th><td><?=str_replace(BASE,'$BASE',$filepath)?></td>
				</tr>
			</table>    
            <h4>Isi ACL</h4>        
            <table class="table">
				<?foreach($acl_conf as $k => $acl):?>
				<?if(!is_array($acl['credentials']))$acl['credentials']=[];?>
				<tr class="<?=$k==$active_segment?'current':'segment'?>">
					<td><?=$k?></td><td><?=$acl['name']?> : <i><?=implode(',',$acl['credentials'])?></i></td>
				</tr>
				<?endforeach?>
			</table> 
			<h4>Kunci Segment</h4>        
            <table class="table">
				<?foreach($segment_keys as $segment_key):?>
				<tr>
					<td><?=$segment_key?> </td>
				</tr>
				<?endforeach?>
			</table> 
            </div>
        </div>
    </div>
</div>
<?endif?>        		
        	</div>
        </div>
        <!--[if lt IE 9]>
<script src="<?=site_url()?>themes/metronic/assets/global/plugins/respond.min.js"></script>
<script src="<?=site_url()?>themes/metronic/assets/global/plugins/excanvas.min.js"></script> 
<script src="<?=site_url()?>themes/metronic/assets/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="<?=site_url()?>themes/metronic/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="<?=site_url()?>themes/metronic/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?=site_url()?>themes/metronic/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="<?=site_url()?>themes/metronic/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="<?=site_url()?>themes/metronic/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="<?=site_url()?>themes/metronic/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="<?=site_url()?>themes/metronic/assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <!-- END THEME LAYOUT SCRIPTS -->
        <script>
            $(document).ready(function()
            {
                $('#clickmewow').click(function()
                {
                    $('#radio1003').attr('checked', 'checked');
                });
            })
        </script>
    </body>

</html>
<style type="text/css">
	tr.current > td{
		background: maroon;
		color: #fff;
	}
</style>