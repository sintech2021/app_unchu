<?php
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Exception\ParseException;

define('BASE', realpath(dirname(__FILE__).'/../../..')).'/';
define('APP',BASE.'/app/');

$ymls = [
	'config' => [
		'app','database','vars' ,'routes','smtp','assets','log'
	],
	'content' => [
		'menu' ,'account', 'breadcrumb','notification','controllers'
	]
];

foreach ($ymls as $dir => $items) {
	foreach ($items as $item) {
		$path = APP.$dir.'/'.$item.'.yml';
		if(!file_exists($path)){
			continue;
		}
		$yml = file_get_contents($path);
		$yml = Yaml::parse($yml);

		define(strtoupper($item).'_YML', $yml);
	}
}

