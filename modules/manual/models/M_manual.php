<?php
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Exception\ParseException;
require_once APPPATH . '/models/Grocery_CRUD_Model.php';
class M_manual  extends Grocery_CRUD_Model  {
	var $table = 'tb_m_manual'; 
    private function _get_datatables_query()
    {
        $this->db->select("a.*")
                 ->from($this->table . ' a');
        $i = 0;
        $search_queries = $_POST['search']['value'];
        $search_queries = explode(',', $search_queries);
        if(count($search_queries) > 0){
            foreach ($this->column_search as $item) // looping awal
            {
                if($_POST['search']['value']) // jika datatable mengirimkan pencarian dengan metode POST
                {
                    if($i===0) // looping awal
                    {
                        $this->db->group_start(); 
                        $this->db->like($item, $_POST['search']['value']);
                    }
                    else
                    {
                        $this->db->or_like($item, $_POST['search']['value']);
                    }
                    if(count($this->column_search) - 1 == $i) 
                        $this->db->group_end(); 
                }
                $i++;
            }
        }
        if(isset($_POST['order'])) 
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    function get_datatables()
    {
        // $this->_get_datatables_query();
        // if($_POST['length'] != -1)
        // $this->db->limit($_POST['length'], $_POST['start']);
        // $query = $this->db->get();
        // return $query->result();
        $yml = APP .'/form/tb_m_manual.yml';
        $manual = Yaml::parse(file_get_contents($yml));
        $tb = $manual['tb_m_manual'];
        $data = $tb['data'];

        $articles = $data['articles'];
        // print_r($articles);
        return $articles;
    }
    function count_filtered()
    {
        // $this->_get_datatables_query();
        // $query = $this->db->get();
        // return $query->num_rows();
        $articles = $this->get_datatables();
        return count($articles);
    }
    public function count_all()
    {
        return $this->count_filtered();
        // $this->db->from($this->table);
        // return $this->db->count_all_results();
    }

    public function list_table()
    {
        return $this->db->list_tables();
    }
}
