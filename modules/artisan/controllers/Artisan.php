<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 

use Foundationphp\Exporter\Xml;
use Foundationphp\Exporter\WordXml;
use Foundationphp\Exporter\OpenDoc;
use Foundationphp\Exporter\MsWord;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Exception\ParseException;


// require_once APPPATH . '/core/FCM.php';

class Artisan extends Theme_controller {
    function chk_imagick(){
        phpinfo();
    }
    function get_modules(){
        // $this->load->model()
    }
    function rupiah_to_int(){
        $rp = 'Rp. 1.300.000';
        echo rupiah_to_int($rp);
    }
    function get_um_date(){
        $start_date = '2021-12-30';
        
        $row = (object)['jadwal_uang_muka'=>4,'rtb_uang_muka'=>$start_date];
        
        

        print_r($row);
    }
    public function gc_call_protected($value='')
    {
        
        chdir(BASE);
        echo getcwd()."\n";
        $target_yaml = APP . '/form/tb_izin_proyek.yml';
        $buffer      = file_get_contents($target_yaml);
        $conf        = Yaml::parse($buffer);
        $crud        = $this->new_crud($conf);
        $state       = $crud->getState();
        $crud->set_crud_url_path(site_url('pengaturan/grup_proyek/izin_proyek'));
        // $crud->callback_before_insert([$this,'_cbBeforeInsertIzin']);
         
        // if (! $this->input->is_ajax_request()) {
        //     $crud->set_theme_config(['crud_paging' => true ]);
        // }

        $field_infos = $crud->call_protected_method('get_add_fields');

        $field_info_imb = $field_infos[5];
        $upload_input = $crud->call_protected_method('get_upload_file_input',$field_info_imb,'');
        print_r($upload_input);
    }
    public function gp()
    {
        $gp_json_path = BASE . '/pub/json/grup_proyek.json';

        if(file_exists($gp_json_path)){
            $gp_json = file_get_contents($gp_json_path);
            $gp = (array)json_decode($gp_json);
            // print_r($gp);
            $yml_json_path = BASE . '/modules/pengaturan/yml/gp.yml';

            // echo "WRITE: $yml_json_path\n"; 
            echo  Yaml::dump(array_keys($gp));
            file_put_contents($yml_json_path, Yaml::dump($gp));
        }
    }
    public function _encrypt_password($str,$simpeg=false){
		if($simpeg){
        	return md5(md5('$notimetodie$').$str);
		}
		return md5($this->config->item('encryption_key').$str.'manis-legi'.$str);
	}
	public function console($cmd = '', $a = '', $b = '', $c = '', $d = '', $e = '')
    {
        $method = str_replace(':', '_', $cmd);

        if (method_exists($this, $method)) {
            return $this->{$method}($a, $b, $c, $d, $e);
        }

        echo ('Unexistent command '."$cmd\n");
    }
    public function clear_konsumen_data($_0,$pk)
    {
        $list_tables = ['tb_data_aset','tb_data_keluarga_terdekat','tb_data_pasangan_konsumen','tb_data_pekerjaan_konsumen','tb_data_pekerjaan_pasangan','tb_data_penghasilan_pengeluaran','tb_data_pinjaman','tb_data_konsumen'];
        foreach ($list_tables as $tbl) {
            $this->db->where('id_data_konsumen',$pk)->delete($tbl);

            echo "DELETE record $tbl \n";
        }


    }
    public function loop_field($_0,$table)
    {
        $field_data = $this->db->field_data($table);
        $buffer = '';

        foreach ($field_data as $field) {
            $buffer .= "\t\t\$row[]=\$field->".$field->name.";\n";
        }

        return $buffer;
    }
    public function display_ases($_0,$table)
    {
        $field_data = $this->db->field_data($table);
        $buffer = '';

        foreach ($field_data as $field) {
            $buffer .= "\$crud->display_as('".$field->name."','".title_case(str_replace('_',' ',$field->name))."');\n";
        }

        echo  $buffer;
    }
    public function field_arr($_0,$table)
    {
        $field_data = $this->db->field_data($table);
        $buffer = '';

        foreach ($field_data as $field) {
            $buffer .= "'".$field->name."',";
        }

        
        echo  $buffer;
    }
    public function set_rules($_0,$table)
    {
        $field_data = $this->db->field_data($table);
        $buffer = '';

        foreach ($field_data as $field) {
            $buffer .= "\$crud->set_rules('".$field->name."','".title_case(str_replace('_',' ',$field->name))."','trim|required');\n";
        }

        echo  $buffer;
    }
    public function table_headers($_0,$table)
    {
        $field_data = $this->db->field_data($table);
        $headers = []; 
        $table_headers = '<tr>'."\n";
        foreach ($field_data as $field) {
            if($field->name == 'tgl_entry'){
                $caption = 'Tanggal Buat';
            }
            else if($field->name == 'tgl_update'){
                $caption = 'Tanggal Ubah';
            }
            else if($field->name == 'id_user'){
                $caption = 'Dibuat';
            }
            else $caption = title_case(str_replace('_',' ',$field->name));
       
      
            $table_headers .= "\t".'<th class="'.$field->name.'" >'. $caption .= '</th>'."\n";
        }
        // $table_headers .= '</tr>'."\n";

        return $table_headers;
    }
    public function reset_login()
    {
    	$data = [
    		'passwd'=>  $this->_encrypt_password('12345')
    	];
    	$this->db->where('id','1')
    			 ->update('account_adm',$data);
    }

    public function generate_breadcrumb()
    {
        $menu_yml = APP . 'content/menu.yml';
        $_menu = Yaml::parse(file_get_contents($menu_yml));
        $sidebar = $_menu['sidebar'];
        $breadcrumbs = [];
        foreach ($sidebar as $order => $menu_) {
            $brd_item = [
                'url' => $menu_['url'],
                'title' => $menu_['title']
            ];
            $brd_key = preg_replace('/\W/', '_', $menu_['url']);
            $brd_list = [
                ['title'=>'Dashboard','url'=>'dashboard']
            ];
            $brd_list_ch = [
                ['title'=>'Dashboard','url'=>'dashboard']
            ];

            $brd_list[] = $brd_item;
            $brd_list_ch[] = $brd_item;

            $breadcrumbs[$brd_key] = $brd_list;

            $items = is_array($menu_['items'])?$menu_['items']:[];

            foreach ($items as $order_item => $menu_child) {
                $brd_item_ch = [
                    'url' => $menu_child['url'],
                    'title' => $menu_child['title']
                ];
                $brd_list_ch[] = $brd_item_ch;
                $brd_key_ch = preg_replace('/\W/', '_', $menu_child['url']);
                $breadcrumbs[$brd_key_ch] = $brd_list_ch;
                $brd_list_ch = $brd_list; 
            }
        }
        file_put_contents(APP.'content/breadcrumb-generated.yml', Yaml::dump($breadcrumbs));
    }
    public function insert_menu()
    {
        $menu_yml = APP . 'content/menu.yml';
        $_menu = Yaml::parse(file_get_contents($menu_yml));
        $sidebar = $_menu['sidebar'];
        foreach ($sidebar as $order => $menu_) {
            
            $items = is_array($menu_['items'])?$menu_['items']:[];

            $menu = [
                'nama_menu' => $menu_['title'],
                'path' => $menu_['url'],
                'keterangan' => 'Menu '. $menu_['title'],
                'tgl_entry' => date('Y-m-d H:i:s'),
                'tgl_update' => date('Y-m-d H:i:s'),
                'order' => ($order+1),

                'icon' => $menu_['icon'],
            ];
            $this->db->insert('tb_menu',$menu);
            $id_menu = $this->db->insert_id();
            $menu['tree_path'] = $id_menu . '*';
            $this->db->where('id_menu',$id_menu)->update('tb_menu',$menu);
            echo "INSERT: tb_menu ".json_encode($menu)."\n";

            foreach ($items as $order_item => $menu_child) {
                $menu_ch = [
                    'nama_menu' => $menu_child['title'],
                    'path' => $menu_child['url'],
                    'keterangan' => 'Menu '. $menu_child['title'],
                    'tgl_entry' => date('Y-m-d H:i:s'),
                    'tgl_update' => date('Y-m-d H:i:s'),
                    'parent_id' => $id_menu,
                    'order'=> ($order_item+1),
                    'icon' => $menu_child['icon'],
                ];
                $this->db->insert('tb_menu',$menu_ch);
                $id_menu_child = $this->db->insert_id();
                $menu_ch['tree_path'] = $id_menu . '*' . $id_menu_child. '*';
                $this->db->where('id_menu',$id_menu_child)->update('tb_menu',$menu_ch);
                echo "INSERT: tb_menu ".json_encode($menu_ch)."\n";
                
            }
        }
    }

    public function db_list_field($_0,$table)
    {
        $field_data = $this->db->field_data($table);
        $fields = [$table=>[]];

        foreach ($field_data as $field) {
            $fields[$table][] = $field->name;
        }
        $target_yaml = APP . '/form/'.$table.'.yml';
        file_put_contents($target_yaml,Yaml::dump($fields));
    }
    public function generate_module($_me,$module_name)
    {
        $yml_path = APP . '/modules/'. $module_name.".yml";
        if(!file_exists($yml_path)){
            die("FILE NOT FOUND : $yml_path\n");
        }
        echo "READ : $yml_path \n";
        $module_config = Yaml::parse(file_get_contents($yml_path));
        $config = $module_config['config'];
        $vars   = $config['vars'];
        
        // print_r($var_keys);
        // print_r($config);
        // exit();
        // 1. PARSE vars
        $config_parsed = [
            'controllers' => [],
            'routes' => [],
            'views' => [],
            'models' => [],
            'forms' => [],
            'tabs'=>[],
            'breadcrumbs'=>[]
        ];
        $var_keys = array_keys($vars); 
        $config_parsed_keys = array_keys($config_parsed);

        foreach ($config_parsed as $config_item => $config_parsed_value) {
            foreach ($config[$config_item] as $config_key => $config_value) {
                $config_key_new     = $config_key;
                $config_value_new   = $config_value;
                // echo $config_key_new ."\n";
                foreach ($vars as $name => $value) {
                    $config_key_new = str_replace('$'.$name, $value, $config_key_new);
                    $config_value_new = str_replace('$'.$name, $value, $config_value_new);
                    ; 
                }
                $config_parsed[$config_item][$config_key_new]=$config_value_new;
            }
        }
        $breadcrumbs = [];
        foreach ($config_parsed['breadcrumbs'] as $breadcrumb_key => $v_value) {
            $breadcrumbs[$breadcrumb_key] = [];
               
            $breadcrumb_tmp = explode(',', $v_value);
            foreach ($breadcrumb_tmp as $b_value) {
                $breadcrumb_item_tmp = explode('|', $b_value);
                $breadcrumb_item = [
                    'title'=> ucfirst($breadcrumb_item_tmp[0]),
                    'url'=> $breadcrumb_item_tmp[1],
                ];
                $breadcrumbs[$breadcrumb_key][] = $breadcrumb_item;
            }
        }
        $config_parsed['breadcrumbs']  = $breadcrumbs;
        $vars['loop_field'] = $this->loop_field(null,$config_parsed['models']['table']);
        $vars['table_headers'] = $this->table_headers(null,$config_parsed['models']['table']);
        $config_parsed['vars'] = $vars;
        $config_parsed['vars']['tabs_json'] = json_encode($config_parsed['tabs']);

        $config_parsed_yml = APP.'/modules/'.$module_name.'_parsed.yml';
        file_put_contents($config_parsed_yml, Yaml::dump($config_parsed));
        echo "WRITE : $config_parsed_yml\n";
        // print_r($config_parsed);
        $ctl_tpl_path = APP . '/templates/ctl.txt';
        if(!file_exists($ctl_tpl_path)){
            die("FILE NOT FOUND : $ctl_tpl_path\n");
        }
        echo "GENERATE : $ctl_tpl_path\n"; 

        $buffer = file_get_contents($ctl_tpl_path);
        $buffer_arr = $this->splitNewLine($buffer);
        $buffer = '';

        foreach($buffer_arr as $line){
            $ck_line = trim($line);
            if(empty($ck_line)){
                continue;
            }
            // echo $line . "\n";
            if(preg_match_all("~\{\{\s*(.*?)\s*\}\}~", $line,$matches)){
                foreach($matches[0] as $index => $var){
                    $key_var = $matches[1][$index];
                    $line = $this->apply_template_var($var,$key_var,$line,$config_parsed,$vars);
                }
            }
            $buffer .= $line . "\n";

        }
        define('MODULEPATH',BASE.'/modules/'.$vars['path']);
        if(!is_dir(MODULEPATH)){
            echo "MKDIR : ".MODULEPATH."\n";
        }
        // echo $buffer;
         if(!is_dir( MODULEPATH .'/controllers')){
            mkdir(MODULEPATH .'/controllers');
        }
        $target_ctl = MODULEPATH .'/controllers/'. $config_parsed['controllers']['ctl']; 

        echo "WRITE : $target_ctl\n";
        file_put_contents($target_ctl, $buffer);
        // mdl
        $mdl_tpl_path = APP . '/templates/mdl.txt';
        if(!file_exists($mdl_tpl_path)){
            die("FILE NOT FOUND : $mdl_tpl_path\n");
        }
        echo "GENERATE : $mdl_tpl_path\n"; 

        $buffer = file_get_contents($mdl_tpl_path);
        $buffer_arr = $this->splitNewLine($buffer);
        $buffer = '';

        foreach($buffer_arr as $line){
            $ck_line = trim($line);
            if(empty($ck_line)){
                continue;
            }
            // echo $line . "\n";
            if(preg_match_all("~\{\{\s*(.*?)\s*\}\}~", $line,$matches)){
                foreach($matches[0] as $index => $var){
                    $key_var = $matches[1][$index];
                    $line = $this->apply_template_var($var,$key_var,$line,$config_parsed,$vars);
                }
            }
            $buffer .= $line . "\n";

        }
        // echo $buffer;
        if(!is_dir( MODULEPATH .'/models')){
            mkdir(MODULEPATH .'/models');
        }
        $target_mdl = MODULEPATH .'/models/'. $config_parsed['models']['model']; 

        echo "WRITE : $target_mdl\n";
        file_put_contents($target_mdl, $buffer);

        // dt
        $dt_tpl_path = APP . '/templates/dt.txt';
        if(!file_exists($dt_tpl_path)){
            die("FILE NOT FOUND : $dt_tpl_path\n");
        }
        echo "GENERATE : $dt_tpl_path\n"; 

        $buffer = file_get_contents($dt_tpl_path);
        $buffer_arr = $this->splitNewLine($buffer);
        $buffer = '';

        foreach($buffer_arr as $line){
            $ck_line = trim($line);
            if(empty($ck_line)){
                continue;
            }
            // echo $line . "\n";
            if(preg_match_all("~\{\{\s*(.*?)\s*\}\}~", $line,$matches)){
                foreach($matches[0] as $index => $var){
                    $key_var = $matches[1][$index];
                    $line = $this->apply_template_var($var,$key_var,$line,$config_parsed,$vars);
                }
            }
            $buffer .= $line . "\n";

        }
        if(!is_dir( MODULEPATH .'/views')){
            mkdir(MODULEPATH .'/views');
        }
        if(!is_dir(MODULEPATH .'/views/'. $config_parsed['views']['dir'])){
            mkdir(MODULEPATH .'/views/'. $config_parsed['views']['dir']);
        }
        // echo $buffer;
        $target_dt = MODULEPATH .'/views/'. $config_parsed['views']['dir'].'/'.$config_parsed['views']['dt']; 

        echo "WRITE : $target_dt\n";
        file_put_contents($target_dt, $buffer);

        // index
        $index_tpl_path = APP . '/templates/index.txt';
        if(!file_exists($index_tpl_path)){
            die("FILE NOT FOUND : $index_tpl_path\n");
        }
        echo "GENERATE : $index_tpl_path\n"; 

        $buffer = file_get_contents($index_tpl_path);
        $buffer_arr = $this->splitNewLine($buffer);
        $buffer = '';

        foreach($buffer_arr as $line){
            $ck_line = trim($line);
            if(empty($ck_line)){
                continue;
            }
            // echo $line . "\n";
            if(preg_match_all("~\{\{\s*(.*?)\s*\}\}~", $line,$matches)){
                foreach($matches[0] as $index => $var){
                    $key_var = $matches[1][$index];
                    $line = $this->apply_template_var($var,$key_var,$line,$config_parsed,$vars);
                }
            }
            $buffer .= $line . "\n";

        }
        // echo $buffer;

        $target_index = MODULEPATH .'/views/'. $config_parsed['views']['dir'].'/'.$config_parsed['views']['index']; 

        echo "WRITE : $target_index\n";
        file_put_contents($target_index, $buffer);

        // tab
        if(empty($config_parsed['vars']['tab_filename'])){
            exit();
        }
        $tab_tpl_path = APP . '/templates/tab.txt';
        if(!file_exists($tab_tpl_path)){
            die("FILE NOT FOUND : $tab_tpl_path\n");
        }
        echo "GENERATE : $tab_tpl_path\n"; 

        $buffer = file_get_contents($tab_tpl_path);
        $buffer_arr = $this->splitNewLine($buffer);
        $buffer = '';

        foreach($buffer_arr as $line){
            $ck_line = trim($line);
            if(empty($ck_line)){
                continue;
            }
            // echo $line . "\n";
            if(preg_match_all("~\{\{\s*(.*?)\s*\}\}~", $line,$matches)){
                foreach($matches[0] as $index => $var){
                    $key_var = $matches[1][$index];
                    $line = $this->apply_template_var($var,$key_var,$line,$config_parsed,$vars);
                }
            }
            $buffer .= $line . "\n";

        }
        // echo $buffer;
        $target_tab = MODULEPATH .'/views/'. $config_parsed['views']['dir'].'/'.$config_parsed['vars']['tab_filename'].'.php'; 
        // print_r($config_parsed);
        echo "WRITE : $target_tab\n";
        file_put_contents($target_tab, $buffer);
    }
    function splitNewLine($text) {
        $code=preg_replace('/\n$/','',preg_replace('/^\n/','',preg_replace('/[\r\n]+/',"\n",$text)));
        return explode("\n",$code);
    }
    function apply_template_var($var,$key,$line,$_config,$_var){
        $obj_props = explode('.', $key);
        if(count($obj_props) == 2){
            $config_item = $obj_props[0];
            $config_item_key = $obj_props[1];
            if(isset($_config[$config_item])){
                if(isset($_config[$config_item][$config_item_key])){
                    return str_replace($var, $_config[$config_item][$config_item_key], $line);
                }
            }
        }else{
            if(isset($_var[$key])){
                return str_replace($var, $_var[$key], $line);
            }
        }
        return $line;
    }
    public function check_password($_0,$username,$password)
    {
        $this->load->model('account/m_login');
        $login = $this->m_login->process_login($username,$password);
        print_r($login);
        if(empty($login)){
            echo "No user with U:$username, P:$password\n";
        }
    }
    public function gc_call()
    {   
        $crud = $this->new_crud();
        $crud->set_subject('Izin Proyek');
         $crud->set_table('tb_izin_proyek');
         $crud->set_theme('datatables');
         $result = $crud->__invoke_method($crud,'pre_render','tb_izin_proyek');
         $result = $crud->__invoke_method($crud,'get_columns','tb_izin_proyek');

         print_r($result);
         exit();
        echo 'hello';
    }
}