<?php
 
class Landing_page extends Theme_Controller
{
    public $_page_title = 'Landing Page';

	public function index($value='')
	{
		$user_id  = $this->cms_user_id();
		$group_id = $this->cms_user_group();
		
		$this->load->model('m_perumahan');

		$landing_pages = [
			''
		];
		$data = [
			'landing_pages' => $landing_pages,
			'user_id'=>$user_id,
			'group_id'=>$group_id,
			'daftar_perumahan' => $this->m_perumahan->get_list()
		];

		$data['daftar_perumahan_paged'] = array_chunk($data['daftar_perumahan'], 4, true);
		// print_r($daftar_perumahan_paged);
		// exit();
		$this->view('_landing_page/index',$data);
	}
}