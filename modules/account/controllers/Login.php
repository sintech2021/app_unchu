<?php
 
class Login extends Theme_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_login');
	}
	public function index($validate=false)
	{
		if($validate=='validate' && $this->input->get('mType') == 'json'){
			header("Content/Type:application/json");
			$response=new stdClass();
			$response->msg = '#json only for validate';
			echo json_encode($response);
			exit();
		}
 
		if($this->input->is_ajax_request()){
			die(json_encode(['LOGIN_REQUIRED']));
		}
		if(is_object($this->__session_data)){
			return $this->_login_success();
		}
		if($this->input->post()){ 
			$this->load->library('form_validation');
			$this->form_validation->set_rules('username','Username/Email  ','required');
			$this->form_validation->set_rules('password','Kata Sandi  ','required');
		
		 	if(APP_YML['app_enable_google_captcha']){
				$this->form_validation->set_rules('g-recaptcha-response','Rechaptcha','required');
            }

			if($this->form_validation->run() != false){

			 	if(APP_YML['app_enable_google_captcha']){

					if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response']))
					{
					    $secret = APP_YML['app_google_captcha_secret'];
					    $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
					    $responseData = json_decode($verifyResponse);
					    if($responseData->success)
					    {
					        $succMsg = 'Your login request have submitted successfully.';

					    }
					    else
					    {
					        $errMsg = 'Robot verification failed, please try again.';
					        $this->_login_failed($errMsg);
					    }
					}else{
						die('Robot verification failed, please try again.');
					}
				}

				$username = $this->input->post('username');
				$password = $this->input->post('password');

				if($account = $this->m_login->process_login($username,$password)){
					if($account->status == 1){
						return $this->_login_success($account);
					}else{
						return $this->_login_failed('Akun tidak aktif');

					}
				}else{
					return $this->_login_failed();
				}
			}else{
				return $this->_login_validation_error();
			}
	 
		}
		$this->__site_layout = 'login_layout';
		$this->view('login_page',['login_failed'=>false,'validation_error'=>'']);
	}
	public function _login_validation_error()
	{
		$validation_errors = validation_errors();
		$data=[

			'login_failed' => true
		];
		if(!empty(trim($validation_errors))){
			$data['message'] = $validation_errors;
		}
		$this->__site_layout = 'login_layout';
		$this->view('login_page',$data);
	}
	public function _login_failed($message='')
	{
		$data=[
			'login_failed' => true,
			'message' => $message
		];
		$this->__site_layout = 'login_layout';
		$this->view('login_page',$data);
	}

	public function _login_success($account='')
	{
		if(!empty($account)){
			$this->__session_data = $account;
			$this->session->set_userdata('account',$account);
		}
		redirect('/landing-page');
	}
	
}