<?php
 
class Recovery extends Theme_Controller
{
	public function changePassword()
	{
		$this->form_validation->set_rules('user_id','User ID','required');
        $this->form_validation->set_rules('old_password','Kata Sandi Lama','required');
        $this->form_validation->set_rules('new_password','Kata Sandi Baru','required|min_length[4]');
        $this->form_validation->set_rules('repeat_new_password','Ulang Kata Sandi Baru','required|min_length[4]');
                
        $user_id = $this->input->post('user_id');
        $old_passwd = $this->input->post('old_password');
        $new_passwd = $this->input->post('new_password');
        $repeat_new_passwd = $this->input->post('repeat_new_password');
        
        $msg = '';
        $msg_repeat_password_not_same = '';

        

        $success = false;
        $am = $this->db->where('user_id',$user_id)->get('account_map')->row();
        if($am->t == '2'){
            $encrypted_passwd = md5(md5('$notimetodie$').$old_passwd);
        }else{
            $encrypted_passwd =  md5($this->config->item('encryption_key').$old_passwd.'manis-legi'.$old_passwd);
        }
        

        if($this->form_validation->run()){
            $uppercase = preg_match('@[A-Z]@', $new_passwd);
            $lowercase = preg_match('@[a-z]@', $new_passwd);
            $number    = preg_match('@[0-9]@', $new_passwd);
            
            $valid_passwd = true;
            $valid_repeat_passwd = false;
            
            $valid_old_passwd = $am->passwd === $encrypted_passwd;
            if(!$valid_old_passwd){
                $msg .= 'Kata Sandi Lama salah' ."\n";
            }
            // if(!($uppercase&&$lowercase && $number)) {
            //   $msg .= 'Kata Sandi Baru tidak valid' . "\n";
            // }else{
            //     $valid_passwd = true;
            // }
            if($new_passwd !== $repeat_new_passwd){
              $msg .= 'Ulang Kata Sandi Baru tidak Sama'."\n";
            }else{
                $valid_repeat_passwd = true;
            }

            if($valid_passwd && $valid_repeat_passwd && $valid_old_passwd){
                $this->load->model('m_account_v2');
                $success = $this->m_account_v2->change_password($user_id, $encrypted_passwd, $new_passwd,$am);
            }
        }else{
            $validation_errors = validation_errors();
            $msg = str_replace(['<p>','</p>'],['',""],$validation_errors);
                        
        }
        
        
        $response = ['success'=>$success, 'message'=>$msg];

        echo json_encode($response);
	}
}