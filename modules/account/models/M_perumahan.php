<?php

class M_perumahan extends CI_Model{
	public function get_list()
	{
		// $this->load->model('pengaturan/m_grup_proyek');
		$this->db->select("a.*,
            b.no_rekening,b.nama_kop_surat,b.alamat_kantor b_alamat_kantor,b.logo,b.klausul_sppr,c.no_imb,c.no_izin_lokasi,c.no_pengesahan, c.no_shgb_induk,c.siteplan,p.nama_propinsi propinsi,k.nama_kota kota,kc.nama_kecamatan kecamatan,kl.nama_kelurahan kelurahan")
                 ->join('tb_klausul_sppr b','b.id_klausul_sppr=a.id_klausul_sppr','left')
                 ->join('tb_izin_proyek c','c.id_izin_proyek=a.id_izin_proyek','left')
                 ->join('tb_propinsi p','p.id_propinsi=a.propinsi','left')
                 ->join('tb_kota k','k.id_kota=a.kota','left')
                 ->join('tb_kecamatan kc','kc.id_kecamatan=a.kecamatan','left')
                 ->join('tb_kelurahan kl','kl.id_kelurahan=a.kelurahan','left')
                 ->from('tb_grup_proyek a');
		$results = $this->db->get()->result();

		foreach ($results as &$row) {
			$foto_kavling = $this->db->where('parent_id',$row->id_grup_proyek)
									->where('rules','foto_kavling')
									->get('uploads')
									->result();
			if(empty($foto_kavling)){
				$foto_kavling = [
					(object)['file_path'=>'uploads/foto_kavling/default.png']
				];
			}						
			$row->foto_kavling = $foto_kavling;
		}

		// print_r($results);
		return $results;
	}
}