  <!-- BEGIN LOGO -->

 <div class="container">
        
        <div class="logo" >
            <a href="javascript:;">
                <img src="{{ site_url }}pub/img/logo-white.jpg" style="height: auto;" alt="" /> </a>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content" style="margin-top: 0">

            <form class="login-form" action="{{ site_url }}login" id="login-form" method="post">
                <div class="form-title" style="text-align: center;">
                    <span class="form-title" style="display: none">Login Masuk</span>
                    <span class="form-subtitle"  style="display: none">Please login.</span>
                </div>
                 <?if($login_failed):?>
                <div class="alert alert-danger login_error">
                    <a href="javascript:;" class="close" data-close="alert"></a>
                    <span> <?=!empty($message)?$message:'Nama Pengguna atau Kata Sandi Salah.' ?></span>
                </div>
                <?endif?>
                <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">Nama Pengguna</label>
                    <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="Nama Pengguna" name="username" /> </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Kata Sandi</label>
                    <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Kata Sandi" name="password" /> 
                </div>
                <?if(APP_YML['app_enable_google_captcha']):?>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Verifikasi</label>
                    <div class="g-recaptcha " data-sitekey="<?=APP_YML['app_google_captcha_sitekey']?>"></div>
                </div>
                <?endif?>
                <div class="form-actions">
                    <button type="submit" class="btn red btn-block uppercase">Login</button>
                </div>
                
                <div class="form-actions">
                    <div class="pull-left">
                        <label class="rememberme mt-checkbox mt-checkbox-outline">
                            <input type="checkbox" name="remember" value="1"> Ingat Saya
                            <span></span>
                        </label>
                    </div>
                    <div class="pull-right forget-password-block">
                        <a href="{{ site_url }}lupa-password" id="forget-password" class="forget-password">Lupa Password?</a>
                    </div>
            </form>
            <!-- END LOGIN FORM -->
        </div>
    </div>
<script type="text/javascript">
    $(document).ready(()=>{
        setTimeout(()=>{
             $('#login-form').unbind('submit');
    $('#login-form').submit(()=>{
        console.log('login')
        return true;
    })
            // $('.videobg').get(0).play()
        },1000);
    
        // $(window).resize(()=>{
        //     var h = $(window).height();
        //     $('.container').height(h+30)
        // }).resize();

        $('a[data-close=alert]').click(()=>{
            $('.login_error').hide()
        });
    });
   
</script>

<style type="text/css">
    body{
        overflow: hidden;
    }
    .form-title{
        text-shadow: 1px 1px 0px #222, 
               1px 0px 0px maroon; 
    }
    .videobg{
        position: fixed;
        z-index: -1;
    }
    .login{
        background:#fff;
    }
    .login .content .mt-checkbox,
    .login .content .forget-password{
        color: #3598DC !important;
    }
</style>