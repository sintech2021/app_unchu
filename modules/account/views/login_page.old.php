<!-- BEGIN : LOGIN PAGE 5-1 -->
<div class="user-login-5">
    <div class="row bs-reset">
        <div class="col-md-6 bs-reset mt-login-5-bsfix">
            <div class="login-bg" style="background-image:url({{ site_url }}themes/metronic/assets/pages/img/login/bg1.jpg)">
            <!-- <img class="login-logo" src="{{ site_url }}themes/metronic/assets/pages/img/login/logo.png" /> -->
             </div>
        </div>
        <div class="col-md-6 login-container bs-reset mt-login-5-bsfix">
            <!--  -->
            
            <form data-toggle="validator" role="form" class="login-content" style="margin-top: 1em"  action="{{ site_url }}login" id="login-form" method="post">
                <h1>Tanahku</h1>
                <p>Sistem Informasi Pertanahan</p>
                <div class="form-group">
                    <input type="text" name="username" class="form-control" id="inputEmail" placeholder="Alamat Email" data-error="Alamat Email tidak valid" value="<?=set_value('username')?>" required>
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
                    <div class="form-group">
                        <input type="password" placeholder="Kata Sandi"  class="form-control" id="inputPassword" data-error="Kata Sandi harus di isi" name="password" required>
                        <div class="help-block with-errors"></div>
                        
                    </div>
                    
                </div>
                <?if(APP_YML['app_enable_google_captcha']):?>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group recaptcha-cnt">
                            <div class="g-recaptcha " data-sitekey="<?=APP_YML['app_google_captcha_sitekey']?>">
                            </div>
                        
                        <div class="help-block with-errors"></div>
                        </div>
                    </div>
                </div>
                <?endif?>
                
                <div class="form-group">
                    <button class="btn green" type="submit" style="width: 100%" id="login_btn">Masuk</button>
                </div>

                <div class="row">
                    <?if($login_failed):?>
                <div class="form-group alert alert-danger" style="margin: 1em">
                    <?=!empty($message)?$message:'Nama Pengguna atau Kata Sandi Salah.' ?>
                </div>
                <?endif?>
                    <div class="col-xs-12">
                        <!-- <p>Lupa Kata Sandi ? <a href="{{ base_url }}forget?ref=login" id="forget-password" class="forget-password" ><i class="fa fa-refresh"></i> Reset Password</a></p> -->
                        <!-- <p>Belum menjadi Pengguna ? <a href="{{ site_url }}register?ref=login"><i class="fa fa-envelope-o"></i> Daftar dengan <i class="fa fa-mail"></i> Email</a></p> -->
                        <!-- <p>Butuh bantuan? <a href="https://api.whatsapp.com/send?phone=<?=APP_YML['app_whatsapp_tel']?>"> Kirim pesan ke <i class="fa fa-whatsapp"></i> Whatsapp</a></p> -->
                    </div>
                </div>
            </form>
            <!--  -->
            <div class="google-play-badge">
                <div class="row">
                    <div class="col-xs-6" style="padding-left: 1em">
                        <!-- <h4 style="font-weight:bold;margin-left: 2.2em;">Download Aplikasi PPSL</h4> -->
                        <!-- <p style="color:#a9b5be;margin-left: 2.7em">Pendataan Potensi Sambungan Langganan</p> -->
                    </div>
                    <div class="col-xs-6">
                        <!-- <a href="<?=APP_YML['app_link_downlod_mobile']?>" target="_blank"><img style="width:200px" src="{{ site_url }}pub/img/google-play-badge.png"></a> -->
                        
                    </div>
                </div>
            </div>
            <div class="login-footer">
                <div class="row bs-reset">
                    <div class="col-xs-2 bs-reset">
                        <ul class="login-social">
                            <!-- <li> -->
                                <!-- <a href="https://facebook.com/psslapps"> -->
                                    <!-- <i class="icon-social-facebook"></i> -->
                                <!-- </a> -->
                            <!-- </li> -->
                            <!-- <li>
                                <a href="javascript:;">
                                    <i class="icon-social-twitter"></i>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <i class="icon-social-dribbble"></i>
                                </a>
                            </li> -->
                        </ul>
                       
                    </div>
                    <div class="col-xs-10 bs-reset">
                        <div class="login-copyright text-right">
                            <p>&copy; <?=date('Y')?> Lain Dunia | Pemerintah Kelurahan Desa Grinting Bulakamba Brebes</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="{{ site_url }}pub/js/validator.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $('.login-bg').backstretch([
            site_url() + "themes/metronic/assets/pages/img/login/bg1.jpg",
            ], {
              fade: 1000,
              duration: 8000
            }
        );
        <?if(APP_YML['app_enable_google_captcha']):?>
        $('#login-form').validator().on('submit', function (e) {
            let form = e.target;
          if (e.isDefaultPrevented()) {
            // handle the invalid form...
            console.log("validation failed");
            recaptcha_error('Silahkan konfirmasi anda bukan Robot');
          } else {
            // everything looks good!
            e.preventDefault();
            console.log("validation success");
            // grecaptcha.execute();
            if(typeof grecaptcha != 'undefined'){
                if(grecaptcha.getResponse() != ''){
                form.submit(); // form validation success, call ajax form submit

                }else{
                    recaptcha_error('Silahkan konfirmasi anda bukan Robot');
                }
            }else{
                recaptcha_error('Gagal memuat recaptcha , Silahkan refresh halaman ini');
            }
 
          }
        });
        <?endif?>
    }); 

    function recaptcha_error(message){
        console.log(message);
        $('.recaptcha-cnt').addClass('has-error');
        $('.recaptcha-cnt .help-block.with-errors').html(message);
    }
</script>

<style type="text/css">
    #login_btn[disabled],
    #login_btn.disabled{
        cursor: pointer !important;
    }
    .user-login-5 .login-container > .login-footer{
        /*position: relative !important;*/
    }
    body{
        overflow: hidden;
    }
</style>