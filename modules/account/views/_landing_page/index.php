<!-- user_id:<?=$user_id?>
group_id:<?=$group_id?> landing... -->
<h4>Pilih Perumahan</h4>
<div class="m-grid m-grid-demo">
<?foreach($daftar_perumahan_paged as $daftar_perumahan):?>

    <div class="m-grid-row">

	<?foreach($daftar_perumahan as $index => $perumahan):?>
	        <div class="m-grid-col m-grid-col-middle m-grid-col-center m-grid-col-md-3">
		<a href="{{ site_url }}dashboard?id_grup_proyek=<?=$perumahan->id_grup_proyek?>&grup_proyek=<?=preg_replace('/\W+/', '-', strtolower($perumahan->nama_proyek))?>">
			<h4><?=$perumahan->nama_proyek?></h4>
			<?if(is_array($perumahan->foto_kavling)):?>
			<!-- <ul> -->
				<?foreach($perumahan->foto_kavling as $foto_kavling):?>
				<!-- <li> -->
					<div style="width:100%;height:200px;background: #fff url('<?= site_url($foto_kavling->file_path)?>') no-repeat top left;background-size:cover;"></div>
				<!-- </li> -->
				<?break?>
				<?endforeach?>
			<!-- </ul> -->
			<?endif?>
			<small><?=$perumahan->kelurahan?> - <?=$perumahan->kota?></small>
		</a>
	</div>

	<?endforeach?>
    </div>

<?endforeach?>
</div>
<style type="text/css">
	.page-sidebar,
	#page_sidebar_wrapper{
		opacity: .1
	}
	.m-grid.m-grid-demo .m-grid-col{
		border: none;
		background: none;
	}
</style>

<script type="text/javascript">
	$(document).ready(()=>{
		setTimeout(()=>{
			// if(!$('body').hasClass('page-sidebar-closed')){
			// $('div.sidebar-toggler').click()
			$(document.body).addClass('page-sidebar-closed');
			// }
		},1000);
	});
</script>