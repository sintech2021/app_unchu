<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content" style="margin-left: 0">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN THEME PANEL -->
        
        <!-- END THEME PANEL -->
        
        <!-- BEGIN PAGE TITLE-->
        <?if(!isset($change_password_success)):?>
        
        <!-- END PAGE TITLE-->
        <!-- END PAGE HEADER-->
        <div id="konten_success" style="display: none;">
            <img style="width: 200px;margin: 0 auto;display: block" src="{{ theme_assets }}img/check-bullet.png">
            <h1 class="page-title" style="text-align: center;"> Ubah Kata Sandi Berhasil</h1>

        </div>
        <div id="konten">
                <img style="width: 200px;margin: 0 auto;display: block" src="{{ theme_assets }}img/logo-perumdam.png">

            <h1 class="page-title" style="text-align: center;"> Ubah Kata Sandi </h1>
            <div id="form">
            <div class="form-group">
                <a href="javascript:;" class="show-passwd hide"></a>
                <label>Kata Sandi Baru</label>
                <input type="password" name="passwd"  placeholder="Kata Sandi" class="form-control">
                <div class="help-block" style="color: #C4C4C4">
                    * Password minimal 6 karakter dan terdapat minimal 1 huruf dan angka.
                </div>
            </div>
            <div class="form-group">
                <a href="javascript:;" class="show-passwd hide"></a>
                <label>Ulangi Kata Sandi</label>
                <input type="password" name="repeat_passwd" placeholder="Ulangi Kata Sandi" class="form-control">
            </div>
            <div class="form-group form-error-msg">
                <div class="alert"></div>
            </div>
            <div class="form-group">
                <div class="button-group">
                    <button class="btn btn-success" style="display: block;width: 100%;font-weight: bold;" id="change_password">Ubah Kata Sandi</button>
                </div>
            </div>
        </div>
        </div>
        <?if($change_password_success == true):?>
        <h1 class="page-title"> Ubah Kata Sandi Berhasil
        <small></small>
        </h1>
        <div>
            Ubah kata sandi berhasil.
        </div>
        <?endif?>
        <?endif?>
    </div>
    <!-- END CONTENT BODY -->
</div>
<style type="text/css">
    #konten_success{
        margin-top: 10%;
    }
    .page-boxed{
        background: #fff !important;
    }
    .form-error-msg{
        display: none;
    }
    .container, .container-fluid{
        padding: 0 !important;
        width: 100% !important;
    }
    .page-container{
        margin-top: 0 !important;
    }
    #form{
        width: 400px;
        margin: 0 auto;
    }
    a.show-passwd{
        width: 20px;
        height: 20px;
        display: block !important;
        position: absolute;
        margin-left: 369px;
        margin-top: 31px;
    }
    a.show-passwd.show{
        background: transparent url({{ theme_assets }}img/eye-open.png) no-repeat center center;
    }
    a.show-passwd.hide{
        background: transparent url({{ theme_assets }}img/eye-close.png) no-repeat center center;
    }
    .page-content-wrapper{
        border-radius: 20px;
    }
</style>
<script type="text/javascript">
    $(document).ready(function(){
        // $('input').val('Test1234');

        $('a.show-passwd').click(function(){
            let hasShow = $(this).hasClass('show');
            let clsT = hasShow ? 'hide' : 'show'; 
            let clsNT = !hasShow ? 'hide' : 'show';

            $(this).removeClass(clsNT).addClass(clsT); 

            let input = $(this).parent().find('input');
            let type  = input.prop('type');

            let new_type = type =='text' ? 'password' : 'text';

            input.prop('type',new_type);
        });
        $('input[name=passwd]').focus();

        $('#change_password').click(function(){
             $('#change_password').prop('disabled',true);
            $password = $('input[name=passwd]').val();
            $repeat_new_password = $('input[name=repeat_passwd]').val();

            App.startPageLoading({animate:true});
            $('.form-error-msg').hide();

            let postData = {
                passwd:$password,
                repeat_passwd: $repeat_new_password,
                user_id: '<?=$user_id?>',
                parent_id: '<?=$parent_id?>',
                email:'<?=$email?>',
                owner: '<?=$owner?>'
            };
            $.post('{{ site_url }}forget_v2_post',postData,function(r){
                // $('#konten').html(r);
                if(r.success){
                     $('#konten').hide();
                     $('#konten_success').slideDown();
                }else{
                    $('.form-error-msg').show();
                    $('.form-error-msg .alert').addClass('alert-danger').html(r.msg);
                }
                App.stopPageLoading();

                $('#change_password').prop('disabled',false);

            },'json');
        });
        
    });
</script>