<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content" style="margin-left: 0">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN THEME PANEL -->
        
        <!-- END THEME PANEL -->
        
        <!-- BEGIN PAGE TITLE-->
        <?if(!isset($change_password_success)):?>
        <h1 class="page-title"> Reset Kata Sandi
        <small></small>
        </h1>
        <!-- END PAGE TITLE-->
        <!-- END PAGE HEADER-->
        <div id="konten">
            <div class="form-group">
                <label>Kata Sandi Baru</label>
                <input type="password" name="new_password"  class="form-control input-medium">
            </div>
            <div class="form-group">
                <label>Ulangi Kata Sandi</label>
                <input type="password" name="repeat_new_password" class="form-control input-medium">
            </div>
            <div class="form-group">
                <div class="button-group">
                    <a class="btn btn-success" id="change_password"><i class="fa fa-key"></i> Submit</a>
                </div>
            </div>
        </div>
        <?if($change_password_success == true):?>
        <h1 class="page-title"> Ganti Kata Sandi Berhasil
        <small></small>
        </h1>
        <div>
            Ganti kata sandi berhasil.
        </div>
        <?endif?>
        <?endif?>
    </div>
    <!-- END CONTENT BODY -->
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('input[name=new_password]').focus();


        

        // $('input[type=email]').change(function(){
        //     $('#send_email_forget').removeAttr('disabled')
        // });
        $('#change_password').click(function(){
            $password = $('input[name=new_password]').val();
            $repeat_new_password = $('input[name=repeat_new_password]').val();

            if($password.length <6){
                swal('Password minimal 6 karakter')
                return;
            }
            if($password != $repeat_new_password){
                swal('Password dan Ulangi Password tidak sama');
                return;
            }
            App.startPageLoading({animate:true});

            let postData = {
                password:$password,
                repeat_password: $repeat_new_password,
                user_id: '<?=$user_id?>',
                parent_id: '<?=$parent_id?>',
                email:'<?=$email?>'
            };
            $.post('{{ site_url }}forget?change_password=true',postData,function(r){
                $('#konten').html(r);
                App.stopPageLoading();


            });
        });
        
    });
</script>