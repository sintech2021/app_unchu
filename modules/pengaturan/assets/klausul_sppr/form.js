function fixFileUrl(url){
    return url;
}
window.message_prompt_delete_file="Apakah anda yakin ingin menghapus file ini";
window.string_delete_file ="Hapus File";
window.string_upload_file ="Upload File";
window.string_progress ="Mengupload";
const init_foto_kavling_form = (parent_id)=>{
	window.___F = new Vue({
		el:'#foto_kavling_form',
		data:{
			foto_kavling_rows:[],
			parent_id:parent_id
		},
		mounted(){
			this.fotoKavlingSessionCheck(this.parent_id);
			this.$nextTick(()=>{
				setTimeout(()=>{
					this.updateFancybox();
				},3000);
			});
		},
		 methods:{
		 	updateFancybox(){
		 		$('.image-thumbnail').fancybox({
	                'transitionIn'  :   'elastic',
	                'transitionOut' :   'elastic',
	                'speedIn'       :   600,
	                'speedOut'      :   200,
	                'overlayShow'   :   false
	            });
	            // console.log('fancybox updated')
		 	},
            toggleFormPhotoKavling(){
                
                let content = 'HELLO WORLD';
                $("#detail_foto_kavling").unbind('show.bs.modal');
                $("#detail_foto_kavling").on('show.bs.modal', function(){
                    // $('#lihat-foto_kavling').html(content);
                });
                $("#detail_foto_kavling").modal("show");

            },
            fotoKavlingSessionCheck(parent_id){
                let url = site_url()+'pengaturan/grup-proyek/foto_kavling_upload_session_check';
                if(typeof parent_id != 'undefined'){
                    url += `/${parent_id}`;
                }
                $.get(url,(res)=>{
                    if(res.success){
                        if(res.owners.length>0){
                            for(var i=0;i<res.owners.length;i++){
                                var field_name = res.owners[i];
                                const index = this.foto_kavling_rows.push(field_name) - 1;
                                this.$nextTick(()=>{
                                    this.initGcFileUploader(index);
                                });
                            }
                        }
                    }
                },'json');
            },
            addFotoKavlingRow(){
                var field_name = 'foto_kavling_'+uuidv4().split('-')[0];
                const index = this.foto_kavling_rows.push(field_name) - 1;
                this.$nextTick(()=>{
                    this.initGcFileUploader(index);
                });
                return index;
            },
            delFotoKavlingRow(index){
                // console.log(index)
                let field_name = $('div[row_index='+index+'] input.hidden-upload-input').attr('name');
                let filename = $('div[row_index='+index+'] input.hidden-upload-input').val();
                if(filename.length>0){
                    // //////////////////////
                    swal({
                          title: "Konfirmasi",
                          text: "Apakah anda yakin ingin menghapus file "+filename,
                          type: "warning",
                          showCancelButton: true,
                          cancelButtonText:'Batal',
                          confirmButtonClass: "btn-danger",
                          confirmButtonText: "Ya",
                          closeOnConfirm: true
                        },
                        ()=>{
                            let url = site_url()+`pengaturan/grup-proyek/foto_kavling_upload/delete_file/${field_name}/${filename}`;

                            $.ajax({
                                url: url,
                                cache: false,
                                success:(res)=>{
                                    // if(res)
                                    // console.log(index)
                                 // this.foto_kavling_rows.splice(index, 1);
                                 // $(`foto_kavling_${index}`).remove(); 
                                 $('div[row_index='+index+'] input.hidden-upload-input').closest(`div[row_index=${index}]`).remove()   
                                }
                            });
                        });
                         
                 

                    // //////////////////////
                }else{
                    this.foto_kavling_rows.splice(index, 1);
                    $(`foto_kavling_${index}`).remove();
                }
                
            },
            initFileUpload(foto_kavling){
                /**************************************************************/
                let _self = this;
                let extra_cls = typeof foto_kavling == "boolean" ? '.foto_kavling-upload':'';
                $('.gc-file-upload'+extra_cls).each(function(a,b){
                    var hasFileUpload = $(this).attr('hasFileUpload') == 'yes';
                    if(hasFileUpload){
                        // console.log('skipping gc-file-upload '+a)
                        return;
                    }
                    $(this).attr('hasFileUpload','yes'); 
                    var unique_id   = $(this).attr('id');
                    var uploader_url = $(this).attr('rel');
                    var uploader_element = $(this);
                    var x_field_name = uploader_element.next().attr('name');
                    var delete_url  = $('#delete_url_'+unique_id).attr('href');
                    eval("var file_upload_info = upload_info_"+unique_id+"");
                    eval("var string_upload_file = string_upload_file_"+unique_id+"");
                    eval("var string_progress = string_progress_"+unique_id+"");
                    eval("var error_on_uploading = error_on_uploading_"+unique_id+"");
                    eval("var message_prompt_delete_file = message_prompt_delete_file_"+unique_id+"");
                    eval("var error_max_number_of_files = error_max_number_of_files_"+unique_id+"");
                    eval("var error_accept_file_types = error_accept_file_types_"+unique_id+"");
                    eval("var error_max_file_size = error_max_file_size_"+unique_id+"");
                    eval("var error_min_file_size = error_min_file_size_"+unique_id+"");
                    eval("var base_url = base_url_"+unique_id+"");
                    eval("var upload_a_file_string = upload_a_file_string_"+unique_id+"");
                    eval("var string_delete_file = string_delete_file_"+unique_id+"");
                    // console.log(unique_id,uploader_url,uploader_element,delete_url,file_upload_info,x_field_name)
                    $(this).fileupload({
                        dataType: 'json',
                        url: uploader_url,
                        cache: false,
                        acceptFileTypes:  file_upload_info.accepted_file_types,
                        beforeSend: function(){
                            let verror = Object.assign({},_self.$data.verror);
                                verror[x_field_name] = false;
                                _self.$data.verror = verror;
                            $('#upload-state-message-'+unique_id).html(string_upload_file);
                            $("#loading-"+unique_id).show();
                            $("#upload-button-"+unique_id).slideUp("fast");
                        },
                        limitMultiFileUploads: 1,
                        maxFileSize: file_upload_info.max_file_size,            
                        send: function (e, data) {                      
                            
                            var errors = '';
                            
                            if (data.files.length > 1) {
                                errors += error_max_number_of_files + "\n" ;
                            }
                            
                            $.each(data.files,function(index, file){
                                if (!(data.acceptFileTypes.test(file.type) || data.acceptFileTypes.test(file.name))) {
                                    errors += error_accept_file_types + "\n";
                                }
                                if (data.maxFileSize && file.size > data.maxFileSize) {
                                    errors +=  error_max_file_size + "\n";
                                }
                                if (typeof file.size === 'number' && file.size < data.minFileSize) {
                                    errors += error_min_file_size + "\n";
                                }                           
                            }); 
                            
                            if(errors != '')
                            {
                                // swal(errors);
                                let verror = Object.assign({},_self.$data.verror);
                                verror[x_field_name] = errors;
                                _self.$data.verror = verror;
                                return false;
                            }
                            
                            return true;
                        },
                        done: function (e, data) {
                            if(typeof data.result.success != 'undefined' && data.result.success)
                            {
                                $("#loading-"+unique_id).hide();
                                $("#progress-"+unique_id).html('');
                                $.each(data.result.files, function (index, file) {
                                    $('#upload-state-message-'+unique_id).html('');
                                    var field_name = uploader_element.attr('name');
                                    $("input[rel="+field_name+"]").val(file.name);
                                    $("input[rel="+field_name+"]").trigger('change');
                                    _self.$data[x_field_name] = file.name;
                                    // console.log(file.name);
                                    var file_name = file.name;
                                    
                                    var is_image = (file_name.substr(-4) == '.jpg'  
                                                        || file_name.substr(-4) == '.png' 
                                                        || file_name.substr(-5) == '.jpeg' 
                                                        || file_name.substr(-4) == '.gif' 
                                                        || file_name.substr(-5) == '.tiff')
                                        ? true : false;
                                    if(is_image)
                                    {
                                        $('#file_'+unique_id).addClass('image-thumbnail');
                                        load_fancybox($('#file_'+unique_id));
                                        $('#file_'+unique_id).html('<img src="'+fixFileUrl(file.url)+'" height="50" />');
                                    }
                                    else
                                    {
                                        $('#file_'+unique_id).removeClass('image-thumbnail');
                                        $('#file_'+unique_id).unbind("click");
                                        $('#file_'+unique_id).html(file_name);
                                    }
                                    
                                    $('#file_'+unique_id).attr('href',file.url);
                                    $('#hidden_'+unique_id).val(file_name);

                                    $('#success_'+unique_id).fadeIn('slow');
                                    $('#delete_url_'+unique_id).attr('rel',file_name);
                                    $('#upload-button-'+unique_id).slideUp('fast');
                                });
                            }
                            else if(typeof data.result.message != 'undefined')
                            {
                                swal(data.result.message);
                                show_upload_button(unique_id, uploader_element);
                            }
                            else
                            {
                                swal(error_on_uploading);
                                show_upload_button(unique_id, uploader_element);
                            }
                        },
                        autoUpload: true,
                        error: function()
                        {
                            swal(error_on_uploading);
                            show_upload_button(unique_id, uploader_element);
                        },
                        fail: function(e, data)
                        {
                            // data.errorThrown
                            // data.textStatus;
                            // data.jqXHR;              
                            swal(error_on_uploading);
                            show_upload_button(unique_id, uploader_element);
                        },          
                        progress: function (e, data) {
                            $("#progress-"+unique_id).html(string_progress + parseInt(data.loaded / data.total * 100, 10) + '%');
                        }           
                    });
                    $('#delete_'+unique_id).click(function(){
                        swal({
                          title: "Konfirmasi",
                          text: message_prompt_delete_file,
                          type: "warning",
                          showCancelButton: true,
                          cancelButtonText:'Batal',
                          confirmButtonClass: "btn-danger",
                          confirmButtonText: "Ya",
                          closeOnConfirm: true
                        },
                        function(){
                            var file_name = $('#delete_url_'+unique_id).attr('rel');
                            $.ajax({
                                url: delete_url+"/"+file_name,
                                cache: false,
                                success:function(){
                                    show_upload_button(unique_id, uploader_element);
                                    _self.$data[x_field_name] = '';
                                },
                                beforeSend: function(){
                                    $('#upload-state-message-'+unique_id).html(string_delete_file);
                                    $('#success_'+unique_id).hide();
                                    $("#loading-"+unique_id).show();
                                    $("#upload-button-"+unique_id).slideUp("fast");
                                }
                            });
                        });
                         
                 
                        return false;
                    });         
                    
                });
                /**************************************************************/
            },
            initGcFileUploader(index){
                var spans = $('#foto_kavling_form span[replace=true]');
                // console.log(spans);
                spans.each((a,_el)=>{
                    console.log(a,_el);
                    var field_name = $(_el).attr('field_name');
                    var parent_id = $(_el).attr('parent_id');
                    var url = site_url()+`pengaturan/grup-proyek/foto_kavling_upload_file_input/${field_name}/${parent_id}`
                    $.post(url,{},(res)=>{
                        // console.log(res);
                        $(_el).replaceWith(res.input);
                        eval(res.js);
                        setTimeout(()=>{
                        this.initFileUpload(true);

                    },100);
                    },'json');
                });
            }
        }
	});
};
$(document).ready(()=>{
	let selector = '#field-foto_kavling';
	$(selector).hide();
	let id_klausul_sppr =  document.location.href.match(/\/edit\/(\d*)/)[1];
	$.get(site_url()+`pengaturan/grup_proyek/foto_kavling_form/${id_klausul_sppr}`,(r)=>{
		// console.log(r);
		$(selector).closest('.form-group').after(r.tpl);
		init_foto_kavling_form(r.parent_id);
	},'json');
    // console.log($('#field-foto_kavling').val())
})