function getParameterByName(name, url = window.location.href) {
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}
function formatRupiah(angka){
    angka=`${angka}`;
      let prefix= 'Rp.';  
      var number_string = angka.replace(/[^,\d]/g, '').toString();
      // Hapus 0 jika 0 pertama
      if(number_string.length > 1){
        while(number_string.charAt(0) === '0')
        {
        number_string = number_string.substr(1);
        }
      }
            var split           = number_string.split(','),
            sisa            = split[0].length % 3,
            rupiah          = split[0].substr(0, sisa),
            ribuan          = split[0].substr(sisa).match(/\d{3}/gi);
 
            // tambahkan titik jika yang di input sudah menjadi angka ribuan
            if(ribuan){
                separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
            }
 
            rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
            return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }
    
    function rupiahToInt(rupiah) {
    if(typeof rupiah != 'string')
        rupiah = `${rupiah}`
      var hasil = rupiah.replace(/[^,\d]/g, '').toString();
      let _value = parseInt(hasil);
      if(isNaN(_value)){
        _value = 0;
      }
      return _value;
    }

    function angka(angka) {
      var hasil = angka.replace(/[^,\d]/g, '').toString();
      return parseInt(hasil);
    }
Vue.component('alert-box', {
  template: `
    <div class="demo-alert-box">
      <strong>Error!</strong>
      <slot></slot>
    </div>
  `
});
Vue.component('dokumen-legal', {
  props: ['items','dokumen_legal_list'],

  template: `
    <div class="dokumen-legal">
      <div class="uploader row form-group" v-for="(item,index) in items">
      <div class="col-md-2" style="width:10px;padding-top:2px;">
        {{index+1}}
      </div>
      <div class="col-md-4">
          <select class="form-control" :index="index" @change="onDokChange(index)">
            <option :value="d.value" v-for="d in dokumen_legal_list" :selected="items[index].nama_dokumen==d.value">{{d.label}}</option>
          </select>
      </div>
        
      <div class="col-md-4" v-if="(item.path==''||item.path=='null') && items[index].nama_dokumen != ''">
        <a href="javascript:;" class="btn btn-default" style="width:100%;text-align:left">
            <i class="fa fa-upload"></i> <span>Upload File</span>
            <div class="progress" :idx="index" style="display:none">
                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="">
                    <span class="sr-only"></span>
                </div>
            </div>
            <input @change="doUpload(index)" class="hidden-upload" type="file" :name="'upload_file_'+index"/>
        </a> 
      </div>
      <div class="col-md-4" v-else>
      <a class="fancybox" target="_blank" :href="site_url()+'uploads/dokumen_legal_unit/'+item.path">{{item.path}}</a>
      </div>
      <div class="col-md-2">
        <a href="javascript:;" class="btn btn-sm btn-danger" @click=trash(index)><i class="fa fa-trash"></i></a>
      </div>

      </div>
      <a style="float: right;clear: both;position: absolute;right: 215px;margin-top: -4.1em;" href="javascript:;" class="btn btn-default btn-sm btn-success" @click="addItem()"><i class="fa fa-plus"></i> Tambah</a>
    </div>
  `,
  methods:{
    addItem(){
        console.log('addItem')
       
            this.items.push({
                nama_dokumen: '',
                path: ''
            });

            $('#field-dokumen_legal_unit').val(JSON.stringify(this.items))
    },
    trash(index){
        let row = this.items[index];
        if(row.path != 'null' && row.path != ''){
            if(confirm(`Hapus ${row.path} ?`)){
                $.get(site_url()+`pengaturan/unit_perumahan/hapus_dok_legal?filename=${row.path}`,(res)=>{
                    this.items.splice(index,1);
                    $('#field-dokumen_legal_unit').val(JSON.stringify(this.items));
                });
                
            }
        }else{
            this.items.splice(index,1);
            $('#field-dokumen_legal_unit').val(JSON.stringify(this.items));
        }
        

    },
    onDokChange(index){
        this.items[index].nama_dokumen = event.target.value;
        $('#field-dokumen_legal_unit').val(JSON.stringify(this.items))
    },
    doUpload(index){
        let input_selector = `input[name=upload_file_${index}]`;
        let file = $(input_selector)[0].files[0];
        let formData = new FormData();

        formData.append('file', file);

        $.ajax({
            xhr: function() {
                var xhr = new window.XMLHttpRequest();

                xhr.upload.addEventListener("progress", function(evt) {
                  if (evt.lengthComputable) {
                    var percentComplete = evt.loaded / evt.total;
                    percentComplete = parseInt(percentComplete * 100);
                    $('div.progress[idx='+index+']').show();
                    $('div.progress[idx='+index+']').attr('aria-valuenow',percentComplete);
                    $('div.progress[idx='+index+']').css('width',percentComplete+'%');
                    $('div.progress[idx='+index+'] .sr-only').text(percentComplete+' % Complete');
                    // console.log(percentComplete);

                    if (percentComplete === 100) {
                        $('div.progress[idx='+index+']').hide();

                    }

                  }
                }, false);

                return xhr;
            },
            url : site_url() + `pengaturan/unit_perumahan/upload_dok_legal`,
            type :'post',
            data: formData,
            contentType: false,
            processData: false,
            beforeSend: (a,b)=>{
                // console.log(a)
            },
            success: (data)=>{
                let result = JSON.parse(data);
                // console.log(result);
                if(typeof result.error != 'undefined'){
                    swal($(result.error).text());
                    $(input_selector).val('');
                }
                if(typeof result.upload_data == 'object'){
                    let data = result.upload_data;
                    this.items[index].path = data.file_name;
                    // console.log(index,result.upload_data);

                    $('#field-dokumen_legal_unit').val(JSON.stringify(this.items));


                }
            }
        }) 
    }
  }
});
displayDokumenLegal = (id)=>{
    // console.log(id);
};
$(document).ready(()=>{
   gc.__onShowForm = (form,state,ajax_url,e) =>{
        
        $('a.btn.btn-success.open-file').fancybox();
        $('#field-harga_jual').closest('.panel-body')
                              .append($('span[x_field_name=foto_kavling]').closest('.form-group'));
        $('#field-dokumen_legal_unit').closest('.panel-body').append('<dokumen-legal v-bind:items="items" v-bind:dokumen_legal_list="dokumen_legal_list"></dokumen-legal>');
        $('#field-dokumen_legal_unit').closest('.form-group').css({'opacity':'0',position: 'absolute',bottom: '0px',right: '-1px',width: '50%'})
        // .attr('v-model','rawText')
                                      // .attr('v-on:change','onTextChange()');
        let node = $('#field-dokumen_legal_unit').closest('.panel-body').parent().get(0);
        let id = node.id;
        node.id = `vue_${id}`;
        id = node.id;

        console.log(id);

        // alertBox.$mount(`#${id}`)

        new Vue({
            el : `#${id}`,
            data:{
                rawText:'',
                items:[],
                dokumen_legal_list:[]
            },
            mounted(){
                this.fetchDokumenLegal();
                let id_grup_proyek = getParameterByName('id_grup_proyek');
                if(typeof id_grup_proyek == "string"){
                    $(`select#field-id_grup_proyek option[value=${id_grup_proyek}]`).attr('selected',true)
                }
                try{
                    this.items = JSON.parse($('#field-dokumen_legal_unit').val());
                }catch(e){
                    this.items = [];
                }
                
                this.$nextTick(()=>{
                    setTimeout(()=>{
                        $('a.fancybox').each(function(a,b){
                            if(b.href.toLowerCase().match(/jpg|jpeg|png/)){
                                $(b).fancybox({
                                    'transitionIn'  :   'elastic',
                                    'transitionOut' :   'elastic',
                                    'speedIn'       :   600,
                                    'speedOut'      :   200,
                                    'overlayShow'   :   false
                                });
                            }
                        });
                        this.fixCurrenCyField();
                        // 
                    },1000)
                });
                
            },
            methods:{
                fixCurrenCyField(){
                    let nd_harga_jual = $('#field-harga_jual');
                    let nd_uang_muka = $('#field-uang_muka');

                    let harga_jual_rp = formatRupiah(nd_harga_jual.val());
                    let uang_muka_rp = formatRupiah(nd_uang_muka.val());

                    nd_harga_jual.val(harga_jual_rp);
                    nd_uang_muka.val(uang_muka_rp);

                    nd_harga_jual.keyup(function(){
                        this.value = formatRupiah(rupiahToInt(this.value));
                    });

                    nd_uang_muka.keyup(function(){
                        this.value = formatRupiah(rupiahToInt(this.value));
                    });

                    nd_harga_jual.change(function(){
                        this.value = formatRupiah(rupiahToInt(this.value));
                    });

                    nd_uang_muka.change(function(){
                        this.value = formatRupiah(rupiahToInt(this.value));
                    });
                },
                onTextChange(){
                    console.log(this.rawText)
                },
                fetchDokumenLegal(){
                    axios.get(site_url()+`data/dd_dok_legal`).then((rs)=>{
                        let list = rs.data;
                        this.dokumen_legal_list = list.data;                       
                    });
                }
            }
        });

   } ;
})