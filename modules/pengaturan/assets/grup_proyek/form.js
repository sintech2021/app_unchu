const triggerSaveButton=(event,el)=>{
    // console.log(event,el);
    let mdl_content = $(el).closest('.modal-content').find('.modal-body');
    let iframe_izin_proyek = mdl_content.find('iframe#iframe_izin_proyek');
    let iframe_klausul_sppr = mdl_content.find('iframe#iframe_klausul_sppr');

    let btn_save_1 = iframe_izin_proyek[0].contentWindow.$('#form-button-save');
    let btn_save_2 = iframe_klausul_sppr[0].contentWindow.$('#form-button-save');
    let btn_save_0 = $(el).prev();

    setTimeout(()=>{btn_save_1.click();},1000);
    setTimeout(()=>{btn_save_2.click();},1500);
    window.my_btn_save = btn_save_0;
    // setTimeout(()=>{btn_save_0.click();},2000);
    // console.log(btn_save_1,btn_save_2,btn_save_0);
};
window.my_success_count = 0;
window.my_btn_save = null;
window.my_success_message=(r)=>{
    // console.log(error);
    window.my_success_count += 1;

    if(window.my_success_count == 2){
        window.my_success_count = 0;
        window.my_btn_save.click();
    }
};
$(document).ready(()=>{
   gc.__onShowForm = (form,state,ajax_url,e) =>{
        // console.log(form, state, ajax_url, e);
        
        $('input#field-propinsi').after('<select class="form-control" id="field-s_kd_prop"></select>');
        $('input#field-kota').after('<select class="form-control" id="field-s_kd_kab"></select>');
        $('input#field-kecamatan').after('<select class="form-control" id="field-s_kd_kec"></select>');
        $('input#field-kelurahan').after('<select class="form-control" id="field-s_kd_kel"></select>');

        $('#field-luas_tanah').numeric();

        if(state == 'edit'){

        	axios.get(site_url()+`data/dd_propinsi`).then((r)=>{
	        	// console.log(r.data.data);
	        	opts = r.data.data.map((k,v)=>{return `<option value="${k.value}">${k.label}</option>`});
	        	$('select#field-s_kd_prop').html(opts);
	        	let kd_prop = $('input#field-propinsi').val();
	        	$(`select#field-s_kd_prop > option[value=${kd_prop}]`).attr('selected',true);
	        	$('select#field-s_kd_prop').change();
	        });
        	$('select#field-s_kd_prop').change(()=>{
        		let value = $('select#field-s_kd_prop').val();
        		// console.log(value);

        		$.post(site_url()+`data/dd_kabupaten_kota`, {
				    parent_id: value,
				},(rs)=>{
					// console.log(response.data);
					opts = rs.data.map((k,v)=>{return `<option value="${k.value}">${k.label}</option>`});
        			$('select#field-s_kd_kab').html(opts);
        			let kd_kab = $('input#field-kota').val();
		        	$(`select#field-s_kd_kab > option[value=${kd_kab}]`).attr('selected',true);
		        	$('select#field-s_kd_kab').change();
				},'json');
				   
        	});

        	$('select#field-s_kd_kab').change(()=>{
        		let value = $('select#field-s_kd_kab').val();
        		// console.log(value);

        		$.post(site_url()+`data/dd_kecamatan`, {
				    parent_id: value,
				},(rs)=>{
					// console.log(response.data);
					opts = rs.data.map((k,v)=>{return `<option value="${k.value}">${k.label}</option>`});
        			$('select#field-s_kd_kec').html(opts);

        			let kd_kec = $('input#field-kecamatan').val();
		        	$(`select#field-s_kd_kec > option[value=${kd_kec}]`).attr('selected',true);
		        	$('select#field-s_kd_kec').change();
				},'json');
        	});

			$('select#field-s_kd_kec').change(()=>{
				let value = $('select#field-s_kd_kec').val();
        		// console.log(value);

        		$.post(site_url()+`data/dd_kelurahan`, {
				    parent_id: value,
				},(rs)=>{
					// console.log(response.data);
					opts = rs.data.map((k,v)=>{return `<option value="${k.value}">${k.label}</option>`});
        			$('select#field-s_kd_kel').html(opts);

        			let kd_kel = $('input#field-kelurahan').val();
		        	$(`select#field-s_kd_kel > option[value=${kd_kel}]`).attr('selected',true);
		        	// $('select#field-s_kd_kel').change();
				},'json');
        	});        	

        	$('select#field-s_kd_kel').change(()=>{
        		let kd_kel = $('select#field-s_kd_kel').val();
        		$('input#field-kd_kel').val(kd_kel);
        	});

        	// $('a.btn.btn-success.open-file').fancybox();
            let id_izin_proyek = $('#field-id_izin_proyek').val();
            let id_klausul_sppr = $('#field-id_klausul_sppr').val();

            let url_izin_proyek = site_url() + `pengaturan/grup_proyek/izin_proyek/edit/${id_izin_proyek}`;
            let url_klausul_sppr = site_url() + `pengaturan/grup_proyek/klausul_sppr/edit/${id_klausul_sppr}`;
            let iframe_izin_proyek  = `<iframe id="iframe_izin_proyek" class="form-iframe" src="${url_izin_proyek}"></iframe>`;
            let iframe_klausul_sppr = `<iframe id="iframe_klausul_sppr" class="form-iframe" src="${url_klausul_sppr}"></iframe>`;

            $('#field-id_izin_proyek').closest('.form-group').hide().after(iframe_izin_proyek);
            $('#field-id_klausul_sppr').closest('.form-group').hide().after(iframe_klausul_sppr);
            let btn_save_3 = '<a href="javascript:;" onclick="triggerSaveButton(event,this)" id="form-button-save-3" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</a>';
            $('#form-button-save').hide().after(btn_save_3)
        }
   } ;
})