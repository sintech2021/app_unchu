<?php  defined('BASEPATH') OR exit('No direct script access allowed');
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Exception\ParseException;
class Grup_user extends Theme_Controller {
	public $_page_title = 'Grup Pengguna';
	public function custom_grid_data()
    {
        $this->load->model('m_grup_user','model');
        $list = $this->model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $deletable = $field->count_user == 0;
            $action = "<td class=\"actions\"><div class='btn-group'> 
               <button onclick=\"gotoURL('".site_url('pengaturan/hak-akses?pk='.$field->id_grup_user)."&grup_user=".slugify($field->nama_grup)."')\" class=\"btn btn-sm btn-icon btn-pure btn-warning\" role=\"button\"> <i class=\"fa fa-key\" aria-hidden=\"true\"></i> 
            </button>
               <button href=\"".site_url('pengaturan/grup_user/index/edit/'.$field->id_grup_user)."/".slugify($field->nama_grup)."\" class=\"gc-bt-edit edit_button btn btn-sm btn-icon btn-pure btn-info\" role=\"button\"> <i class=\"fa fa-edit\" aria-hidden=\"true\"></i> 
            </button> ".($deletable?"
                        <button onclick=\"javascript: return delete_row('".site_url('pengaturan/grup_user/index/delete/'.$field->id_grup_user)."',-1,this)\" href=\"javascript:;\" class=\"gc-bt-delete delete_button btn btn-sm btn-icon btn-pure btn-danger\" role=\"button\"> <i class=\"fa fa-trash\" aria-hidden=\"true\"></i> 
                        </button>":'')."</div>
            </td>";
            $no++;
            $row = array();
            $row[] = $no.'.';
            $row[] = $field->nama_grup; 
            $row[] = $field->keterangan; 
            $row[] = $field->status; 
            $row[] = $action; 
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->model->count_all(),
            "recordsFiltered" => $this->model->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }
    public function custom_grid_ajax()
    {
        $tdata = ['add_url' => site_url('pengaturan/grup_user/index/add')];
        $tdata['is_admin'] = $this->cms_user_group() == 'admin';
        $this->load->view('_pengaturan/dt_grup_user.php',$tdata);
    }
    private function _customGrid(){
        $dump = file_get_contents(APP . '/config/gc.yml');
        $data = Yaml::parse($dump);
        $tdata = ['add_url' => site_url('pengaturan/grup_user/index/add')];
        $data['output'] = $this->load->view('_pengaturan/dt_grup_user.php',$tdata,true);
        $data['unique_hash'] = md5(date('YmdHis-Unit'));
        $data['is_admin']  = $this->cms_user_group()=='admin';
        $this->view('_pengaturan/grup_user.php', $data );
    }
    public function index()
    {
        $target_yaml = APP . '/form/tb_grup_user.yml';
        $buffer = file_get_contents($target_yaml);
        $_SERVER['FORM_DEF'] = Yaml::parse($buffer);
        $crud = $this->new_crud();
        $crud->set_subject('Grup');
        $state = $crud->getState();
        switch ($state) {
            case 'list':
                $args = func_get_args();
                return call_user_func_array([$this,'_customGrid'], $args);
                break;
            default:
                # code...
                break;
        }
        $crud->unset_jquery();
    
        if (! $this->input->is_ajax_request()) {
            $crud->set_theme_config(['crud_paging' => true ]);
        }
        $crud->set_table('tb_grup_user');
		$crud->set_model('m_grup_user');
		$crud->set_theme('datatables');
        $crud->field_type('tgl_entry','hidden');
        $crud->field_type('tgl_update','hidden');
        $crud->field_type('keterangan','text');
        $crud->field_type('role','hidden');
        $crud->set_rules('nama_grup','Nama Grup','required');
        $crud->set_rules('status','Status','required');
        $crud->set_rules('keterangan','Status','required');
        $crud->fields('nama_grup','keterangan','tgl_entry','tgl_update','role','status');
        $crud->unset_texteditor('keterangan');
        $crud->display_as('nama_grup','Nama Grup');

    	$id_user = $this->cms_user_id();
        
        $crud->callback_before_insert(array($this,'_set_role'));
        $crud->callback_before_update(array($this,'_set_tgl_update')); 

		$state = $crud->getState();
    	$state_info = $crud->getStateInfo();
    	$id_user = $this->cms_user_id();
        $data = $crud->render();
		$data->is_admin  = $this->cms_user_group()=='admin';
        

		$this->view('_pengaturan/grup_user.php',$data);
    }
    function _set_role($post_array){
        $post_array['role'] = underscorize($post_array['nama_grup']);
        $dt = date('Y-m-d H:i:s');
        $post_array['tgl_entry'] = $dt;
        $post_array['tgl_update'] = $dt;
        return $post_array;
    }
    
    function _set_tgl_update($post_array,$pk){
    
        $dt = date('Y-m-d H:i:s');
        $post_array['tgl_update'] = $dt;
        $post_array['role'] = underscorize($post_array['nama_grup']);
        return $post_array;
    }
}
