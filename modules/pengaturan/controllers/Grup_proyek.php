<?php  defined('BASEPATH') OR exit('No direct script access allowed');
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Exception\ParseException;
class Grup_proyek extends Theme_Controller {
	public $_page_title = 'Daftar Grup Proyek';
	public function custom_grid_data()
    {
        $this->load->model('m_grup_proyek','model');
        $list = $this->model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $action = "<td class=\"actions\"> 
            <button style=\"display:none\" href=\"".site_url('pengaturan/grup_proyek/hpp/'.$field->id_grup_proyek)."/".slugify($field->nama_proyek)."\" class=\"gc-bt-edit edit_button btn btn-sm btn-icon btn-pure btn-info\" role=\"button\"> HPP a
            </button><div style='height:20px'></div> <a href=\"".site_url('pengaturan/unit-perumahan?id_grup_proyek='.$field->id_grup_proyek)."&nama_proyek=".slugify($field->nama_proyek)."\" class=\" btn btn-sm btn-icon btn-pure btn-info\" role=\"button\"> <i class=\"fa fa-home\"></i>
            </a> <br/><div style='height:20px'></div>"."
            <button href=\"".site_url('pengaturan/grup_proyek/index/edit/'.$field->id_grup_proyek)."/".slugify($field->nama_proyek)."\" class=\"gc-bt-edit edit_button btn btn-sm btn-icon btn-pure btn-info\" role=\"button\"> <i class=\"fa fa-edit\"></i></button>
            </button>
            </td>";
            $no++;
            $row = array();
            $row[] = $no.'.';
            $row[] = $field->nama_proyek;
            $siteplan_path = 'uploads/tb_izin_proyek_siteplan/'.$field->siteplan;
            $siteplan_url = site_url($siteplan_path);
            $field->siteplan_link = '<a class="image-thumbnail" target="_blank" href="'.$siteplan_url.'">Lihat Siteplan</a>';
            if(!is_file($siteplan_path)){
                $field->siteplan_link = '-';
            }    

            $logo_path = 'uploads/tb_klausul_sppr_logo/'.$field->logo;
            $logo_url = site_url($logo_path);
            $field->logo_link = '<a class="image-thumbnail" target="_blank" href="'.$logo_url.'">Lihat Logo</a>';
            if(!is_file($logo_path)){
                $field->logo_link = '-';
            } 
            
            

            $no_imb_path = 'uploads/tb_izin_proyek_file_imb/'.$field->file_imb;
            $no_imb_url = site_url($no_imb_path);
            $field->no_imb_link = '<a target="_blank" href="'.$no_imb_url.'">Lihat</a>';
            if(!is_file($no_imb_path)){
                $field->no_imb_link = '-';
            } 

            $izin_lokasi_path = 'uploads/tb_izin_proyek_file_izin_lokasi/'.$field->file_izin_lokasi;
            $izin_lokasi_url = site_url($izin_lokasi_path);
            $field->izin_lokasi_link = '<a target="_blank" href="'.$izin_lokasi_url.'">Lihat</a>';
            if(!is_file($izin_lokasi_path)){
                $field->izin_lokasi_link = '-';
            }
            
         

             
            $pengesahan_path = 'uploads/tb_izin_proyek_file_pengesahan/'.$field->file_pengesahan;
            $pengesahan_url = site_url($pengesahan_path);
            $field->no_pengesahan_link = '<a target="_blank" href="'.$pengesahan_url.'">Lihat</a>';
            if(!is_file($pengesahan_path)){
                $field->no_pengesahan_link = '-';
            }
            $shgb_induk_path = 'uploads/tb_izin_proyek_file_shgb_induk/'.$field->file_shgb_induk;
            $shgb_induk_url = site_url($shgb_induk_path);
            $field->no_shgb_link = '<a target="_blank" href="'.$shgb_induk_url.'">Lihat</a>';
            if(!is_file($shgb_induk_path)){
                $field->no_shgb_link = '-';
            }
            $klausul_sppr_url = site_url('pengaturan/grup_proyek/view_klausul_sppr/'.$field->id_klausul_sppr);
            $field->klausul_sppr_link = '<a onclick="displayDetail(event,this,\'Klausul SPPR\')" target="_blank" href="'.$klausul_sppr_url.'">Lihat Klausul SPPR</a>';
            
            $foto_kavling_url = site_url('pengaturan/grup_proyek/view_foto_kavling/'.$field->id_grup_proyek);
            $field->foto_kavling_link = '<a onclick="displayDetail(event,this,\'Foto Kavling\')" target="_blank" href="'.$foto_kavling_url.'">Lihat Foto Kavling</a>';

            $row[] = $this->load->view('_grup_proyek/row_grup_proyek',$field, TRUE);
            $row[] = $this->load->view('_grup_proyek/row_alamat_proyek',$field, TRUE);
            $row[] =  $this->load->view('_grup_proyek/row_izin_proyek',$field, TRUE);
            $row[] = $this->load->view('_grup_proyek/row_klausul_sppr',$field, TRUE);
            $row[] = $action; 
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->model->count_all(),
            "recordsFiltered" => $this->model->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }
    public function custom_grid_ajax()
    {
        $tdata = ['add_url' => site_url('pengaturan/grup_proyek/index/add')];
        $tdata['is_admin'] = $this->cms_user_group() == 'admin';
        $this->load->view('_pengaturan/dt_grup_proyek.php',$tdata);
    }
    private function _customGrid(){
        $dump = file_get_contents(APP . '/config/gc.yml');
        $data = Yaml::parse($dump);
        $tdata = ['add_url' => site_url('pengaturan/grup_proyek/index/add')];
        $data['output'] = $this->load->view('_pengaturan/dt_grup_proyek.php',$tdata,true);
        $data['unique_hash'] = md5(date('YmdHis-Unit'));
        $data['is_admin']  = $this->cms_user_group()=='admin';
        $data['crud_izin_proyek'] = $this->izin_proyek(true);
        $data['crud_klausul_sppr']  = $this->klausul_sppr(true);
        // print_r($data->crud_izin_proyek);
        // exit();
        $this->view('_pengaturan/grup_proyek.php', $data );
    }
    public function index()
    {
        $tmp_session_key = $this->session->userdata('foto_kavling_tmp_session_key');
        if(empty($tmp_session_key)){
            $this->session->set_userdata('foto_kavling_tmp_session_key',md5(microtime()));

        }

        $target_yaml = APP . '/form/tb_grup_proyek.yml';
        $buffer = file_get_contents($target_yaml);
        $_SERVER['FORM_DEF'] = Yaml::parse($buffer);
        $crud = $this->new_crud();
        $crud->set_subject('Grup Proyek');
        $state = $crud->getState();
        switch ($state) {
            case 'list':
                $args = func_get_args();
                return call_user_func_array([$this,'_customGrid'], $args);
                break;
            default:
                # code...
                break;
        }
        $crud->unset_jquery();
        // $crud->unset_export();
        if (! $this->input->is_ajax_request()) {
            $crud->set_theme_config(['crud_paging' => true ]);
        }
        $crud->set_table('tb_grup_proyek');
		$crud->set_model('m_grup_proyek');
		$crud->set_theme('datatables');
        $crud->field_type('tgl_entry','hidden');
        $crud->field_type('tgl_update','hidden');
        $crud->field_type('id_user','hidden');
        // $crud->field_type('id_izin_proyek','hidden');
        // $crud->field_type('id_klausul_sppr','hidden');
        // $crud->field_type('foto_kavling','hidden');

        $crud->display_as('id_izin_proyek','Izin Proyek');
        $crud->display_as('id_klausul_sppr','Klausul Sppr');
        $crud->display_as('nama_proyek','Nama Proyek');
        $crud->display_as('alamat_proyek','Alamat Proyek');
        $crud->display_as('kelurahan','Kelurahan');
        $crud->display_as('kecamatan','Kecamatan');
        $crud->display_as('kota','Kota');
        $crud->display_as('propinsi','Propinsi');
        $crud->display_as('luas_tanah','Luas Tanah');
        $crud->display_as('jml_unit','Jumlah Unit');
        $crud->display_as('klasifikasi','Klasifikasi');
        $crud->display_as('konsep','Konsep');
        $crud->display_as('segmentasi','Segmentasi');

        foreach (['propinsi','kota','kecamatan','kelurahan','nama_proyek','alamat_proyek','propinsi','kota','kecamatan','kelurahan', 'luas_tanah', 'jml_unit', 'klasifikasi', 'konsep', 'segmentasi'] as $field) {
            $caption = title_case(str_replace('_',' ',$field));
            if($field=='jml_unit'){
                $caption = 'Jumlah Unit';
            }
            $crud->set_rules($field,$caption,'trim|required'.($field=='nama_proyek'?'|callback_nama_proyek_check':''));
        }
        $crud->callback_after_insert([$this,'_cbAfterInsertGrupProyek']);
    	$id_user = $this->cms_user_id();
		$state = $crud->getState();
    	$state_info = $crud->getStateInfo();
    	$id_user = $this->cms_user_id();
        $data = $crud->render();
		$data->is_admin  = $this->cms_user_group()=='admin';

        $data->crud_izin_proyek  = $this->izin_proyek(true);
        $data->crud_klausul_sppr  = $this->klausul_sppr(true);
		$this->view('_pengaturan/grup_proyek.php',$data);
    }
    function foto_kavling_check(){
        $tmp_session_key = $this->session->userdata('foto_kavling_tmp_session_key');
         
        $num_row = $this->db->where('parent_id',$tmp_session_key)->get('uploads')->num_rows();
        if ($num_row > 0){
            return true;
        }
        else{
             $this->form_validation->set_message('foto_kavling_check', 'Foto Kavling harus diisi ');
            return false;
        }
    }
    public function klausul_sppr($return=false)
    {
        // if($return == 'edit')
        //     $this->__site_layout = 'full_single';

        $target_yaml = APP . '/form/tb_klausul_sppr.yml';
        $buffer      = file_get_contents($target_yaml);
        $conf        = Yaml::parse($buffer);
        $crud = $this->new_crud($conf);
        $field_upload_info_logo = [
            'file_upload_allow_file_types' => 'jpeg|jpg|png',
            'file_upload_max_file_size'=> '2MB'
        ];
        $crud->set_subject('Klausul SPPR');
        $crud->set_field_upload_info('logo',$field_upload_info_logo);
        
        // $dd_rekening = [];
        $this->load->model('pengaturan/m_rekening_bank');
        $rekening_list = $this->m_rekening_bank->get_dd();  
        // print_r($rekening_list);
        // exit();  
        $crud->field_type('no_rekening','dropdown',$rekening_list);
        
        if($return === true){
            $crud->set_crud_url_path(site_url('pengaturan/grup_proyek/klausul_sppr'));
            return $crud; 
        }else{
            // $crud->callback_before_insert([$this,'_cbBeforeInsertKlausul']);
         
            if (! $this->input->is_ajax_request()) {
                $crud->set_theme_config(['crud_paging' => true ]);
            }

            $data = $crud->render();
            $data->state = $state;
        }
        
        $this->view('_grup_proyek/gc_klausul_sppr',$data,['layout'=>'full_single']);
    }
    public function izin_proyek($return=false)
    {
        // if($return == 'edit')
        //     $this->__site_layout = 'full_single';

        $target_yaml = APP . '/form/tb_izin_proyek.yml';
        $buffer      = file_get_contents($target_yaml);
        $conf        = Yaml::parse($buffer);
        $crud        = $this->new_crud($conf);
        $state       = $crud->getState();
        $data = new stdClass();
        // $this->config->set_item('grocery_crud_file_upload_allow_file_types','jpeg|jpg|png|pdf');
        // $this->config->set_item('grocery_crud_file_upload_max_file_size','2MB');
        $field_upload_info = [
            'file_upload_allow_file_types' => 'jpeg|jpg|png|pdf',
            'file_upload_max_file_size'=> '2MB'
        ];
        $field_upload_info_siteplan = [
            'file_upload_allow_file_types' => 'jpeg|jpg|png',
            'file_upload_max_file_size'=> '2MB'
        ];
        $crud->set_field_upload_info('file_imb',$field_upload_info);
        $crud->set_field_upload_info('file_pengesahan',$field_upload_info);
        $crud->set_field_upload_info('file_shgb_induk',$field_upload_info);
        $crud->set_field_upload_info('file_izin_lokasi',$field_upload_info);
        $crud->set_field_upload_info('siteplan',$field_upload_info_siteplan);
        
        $crud->set_subject('Izin Proyek');

        if($return === true){
            $crud->set_crud_url_path(site_url('pengaturan/grup_proyek/izin_proyek'));
            return $crud; 
        }else{
            $crud->callback_before_insert([$this,'_cbBeforeInsertIzin']);
         
            if (! $this->input->is_ajax_request()) {
                $crud->set_theme_config(['crud_paging' => true ]);
            }

            $data = $crud->render();
            $data->state = $state;
        }
        $this->view('_grup_proyek/gc_izin_proyek',$data,['layout'=>'full_single']);
    }
    function _cbAfterInsertGrupProyek($post_array,$pk){
        $tmp_session_key = $this->session->userdata('foto_kavling_tmp_session_key');
        if(is_string($tmp_session_key)){
            $data=[
                'is_tmp'=>0,
                'parent_id'=>$pk,
                
            ];
            $this->db->where('parent_id',$tmp_session_key)
                ->update('uploads',$data);
        }
        $this->session->set_userdata('foto_kavling_tmp_session_key',md5(microtime()));
    }
    function nama_proyek_check($str){
        $id_grup_proyek = $this->uri->segment(5);
        if(!empty($id_grup_proyek) && is_numeric($id_grup_proyek)){
            $nama_proyek_old = $this->db->where("id_grup_proyek",$id_grup_proyek)->get('tb_grup_proyek')->row()->nama_proyek;
            $this->db->where("nama_proyek !=",$nama_proyek_old);
        }
        $num_row = $this->db->where('nama_proyek',$str)->get('tb_grup_proyek')->num_rows();
        if ($num_row >= 1){
            $this->form_validation->set_message('nama_proyek_check', 'Nama Proyek tidak tersedia atau sudah digunakan');
            return false;
        }
        else{
            return true;
        }
    }
    function _cbBeforeInsertIzin($post_array){
        // unset($post_array['file_imb']);
        // unset($post_array['file_izin_lokasi']);
        // unset($post_array['file_pengesahan']);
        // unset($post_array['file_shgb_induk']);
        // unset($post_array['siteplan']);
        return $post_array;
    }
    function _cbBeforeInsertKlausul($post_array){
        // unset($post_array['logo']);
        // unset($post_array['foto_kavling']);
        return $post_array;

    }

    function foto_kavling_upload($cmd,$parent_id=""){

        $tmp_session_key = $this->session->userdata('foto_kavling_tmp_session_key');
        // if(empty($parent_id)){
        //     $parent_id = -1;
        // }
        $config['upload_path'] = 'uploads/foto_kavling/';
        $config['allowed_types'] = 'jpg|jpeg|png|JPG|JPEG|PNG';
    
        $this->load->library('upload', $config);
        
        $field_name =   's'.substr(md5($cmd), 0, 8);;
        if($cmd=='delete_file'){
            $owner = $parent_id;
            $row = $this->db->where('owner',$owner)
                            ->where('rules','foto_kavling')
                            ->get('uploads')->row();

            if(is_object($row)){
                @unlink(BASE .'/'. $row->file_path);
                $this->db->where('owner',$owner)
                         ->where('rules','foto_kavling')
                         ->delete('uploads');
            }
            echo json_encode(['success'=>true,'data'=>$row]);
            return;
        }
        $owner = $cmd;
        // echo $field_name."\n".$cmd;
        // die($field_name);
        if ( ! $this->upload->do_upload($field_name))
        {
            $msg =  $this->upload->display_errors() ;
            echo json_encode(['success'=>false,'message'=>$msg]);
            return false;
        }

        $uploaded_file =(object) $this->upload->data();
        $uploaded_file->success = true;

        $row = $this->db->where('owner',$owner)
                            ->where('rules','foto_kavling')
                            ->get('uploads')->row();

            if(is_object($row)){
                @unlink(BASE .'/'. $row->file_path);
                $this->db->where('owner',$owner)
                         ->where('rules','foto_kavling')
                         ->delete('uploads');
            }
            $upload = [
                'rules' => 'foto_kavling',
                'desc'=>'foto_kavling',
                'owner' => $owner,
                'user_id' => $this->cms_user_id(),
                'file_name' => $uploaded_file->file_name,
                'file_type' => $uploaded_file->file_type,
                'file_ext' => $uploaded_file->file_ext,
                'is_image' => 1,
                'file_size' => $uploaded_file->file_size,
                'image_width'=>$uploaded_file->image_width,
                'image_height'=>$uploaded_file->image_height,
                'creation_date'=> date('Y-m-d H:i:s'),
                'file_path' => 'uploads/foto_kavling/'.$uploaded_file->file_name
            ];
            if(empty($parent_id)){
                $upload['is_tmp'] = 1;
                $upload['parent_id'] = $tmp_session_key;
            }else{
                $upload['is_tmp'] = 0;
                $upload['parent_id'] = $parent_id;
            }
            $this->db->insert('uploads',$upload);

            $upload['id'] = $this->db->insert_id();
            $file = [
                'name' => $upload['file_name'],
                'size' => $upload['file_size'],
                'type' => $upload['file_type'],
                'url'  => site_url($upload['file_path'])
            ];
        echo json_encode(['success'=>true,'files'=>[$file],'id'=>$upload['id']]);
    }
    function foto_kavling_upload_session_check($parent_id=""){
        $owner = $parent_id;
        if(empty($parent_id)){
            $parent_id = $this->session->userdata('foto_kavling_tmp_session_key');
        }
        $value = "";
        $results = $this->db->select('owner')->where('parent_id',$parent_id)
                            ->where('rules','foto_kavling')
                            ->get('uploads')->result();
        $owners = [];
        foreach ($results as $row) {
            $owners[] = $row->owner;
        }

        echo json_encode(['success'=>true,'owners'=>$owners,'parent_id'=>$parent_id]);
    }
    function foto_kavling_upload_file_input($field_name, $parent_id="")
    {

        header('Content-Type:application/json');

        $upload_path = 'uploads/foto_kavling';
        $upload_url = site_url('pengaturan/grup-proyek/foto_kavling_upload').'/';
        $delete_url = $upload_url.'delete_file/';
        $field_info = new stdClass();
        $field_info->name = $field_name;
        $field_info->extras = new stdClass();
        $field_info->extras->upload_path = $upload_path;
        // $this->load_js_uploader();
        $owner = $field_name;
        if(empty($parent_id)){
            $parent_id = $this->session->userdata('foto_kavling_tmp_session_key');
        }

        $value = "";
        $row = $this->db->where('owner',$owner)
                            ->where('parent_id',$parent_id)
                            ->where('rules','foto_kavling')
                            ->get('uploads')->row();
        if(is_object($row)){
            $value = $row->file_name;
        }
        // //Fancybox
        // $this->load_js_fancybox();

        // $this->set_js_config($this->default_javascript_path.'/jquery_plugins/config/jquery.fancybox.config.js');

        $unique = uniqid();

        $allowed_files = 'jpg|png|jpeg';
        $allowed_files_ui = '.'.str_replace('|', ',.', $allowed_files);
        $max_file_size_ui = '2MB';
        $max_file_size_bytes = convert_bytes_ui_to_bytes($max_file_size_ui);
        $have_individual_config = true;

        
        $inline_js = ('
            window.upload_info_'.$unique.' = {
                accepted_file_types: /(\\.|\\/)('.$allowed_files.')$/i,
                accepted_file_types_ui : "'.$allowed_files_ui.'",
                max_file_size: '.$max_file_size_bytes.',
                max_file_size_ui: "'.$max_file_size_ui.'"
            };

            window.string_upload_file'.($have_individual_config?'_'.$unique:'').'  =  "Unggah berkas";
            window.string_delete_file'.($have_individual_config?'_'.$unique:'').'  = "Menghapus file";
            window.string_progress'.($have_individual_config?'_'.$unique:'').'             ="Status: ";
            window.error_on_uploading'.($have_individual_config?'_'.$unique:'').'          = "Terdapat kesalahan pada saat upload.";
            window.message_prompt_delete_file'.($have_individual_config?'_'.$unique:'').'  = "Apakah anda yakin ingin menghapus file ini?";

            window.error_max_number_of_files'.($have_individual_config?'_'.$unique:'').'   = "Anda hanya dapat mengunggah 1 (satu) file pada satu waktu.";
            window.error_accept_file_types'.($have_individual_config?'_'.$unique:'').'    = "Anda tidak dapat mengunggah file dengan jenis ini.";
            window.error_max_file_size'.($have_individual_config?'_'.$unique:'').'         = "'.str_replace("{max_file_size}", $max_file_size_ui, "Ukuran file melebihi {max_file_size}.").'";
            window.error_min_file_size'.($have_individual_config?'_'.$unique:'').'         = "Anda tidak dapat mengunggah file kosong.";

            window.base_url'.($have_individual_config?'_'.$unique:'').' = "'.base_url().'";
            window.upload_a_file_string'.($have_individual_config?'_'.$unique:'').' = "Unggah berkas";
        ');

        $uploader_display_none  = empty($value) ? "" : "display:none;";
        $file_display_none      = empty($value) ?  "display:none;" : "";

        $is_image = !empty($value) &&
                        ( substr($value, -4) == '.jpg'
                                || substr($value, -4) == '.png'
                                || substr($value, -5) == '.jpeg'
                                || substr($value, -4) == '.gif'
                                || substr($value, -5) == '.tiff')
                    ? true : false;

        $image_class = $is_image ? 'image-thumbnail' : '';
        $unique_field_name = 's'.substr(md5($field_info->name), 0, 8);
         // die($parent_id);
        $input = '<span x_field_name="'.$field_info->name.'" class="btn btn-info fileinput-button qq-upload-button" id="upload-button-'.$unique.'" style="'.$uploader_display_none.'">
            <span><i class="fa fa-file-picture-o"></i> Unggah File</span>
            <input type="file" name="'.$unique_field_name.'" class="gc-file-upload foto_kavling-upload form-control" rel="'.$upload_url.($field_info->name).(!empty($parent_id)?'/'.$parent_id:'').'" id="'.$unique.'">
            <input class="hidden-upload-input" type="hidden" name="'.$field_info->name.'" value="'.$value.'" rel="'.$unique_field_name.'" />
        </span>';
        
        // $this->set_css($this->default_css_path.'/jquery_plugins/file_upload/fileuploader.css');

        $file_url = base_url().$field_info->extras->upload_path.'/'.$value;
        // $file_url = json_encode($value);
        // dir($file_url);
        $input .= "<div id='uploader_$unique' rel='$unique' class='grocery-crud-uploader' style='$uploader_display_none'></div>";
        $input .= "<div id='success_$unique' class='upload-success-url' style='$file_display_none padding-top:7px;'>";
        $input .= "<a target='_blank' href='".$file_url."' id='file_$unique' class='btn btn-success open-file";
        $input .= $is_image ? " $image_class'><img src='".$file_url."' height='50px'>" : "' target='_blank'>".($is_image?$value:'<i class="fa fa-download"></i> '.preg_replace('/^[0-9a-f]{5}/', '', $value));
        $input .= "</a> ";
        $input .= "<a href='javascript:void(0)' id='delete_$unique' class='btn btn-danger delete-anchor'>".'<i class="icon wb-trash"></i>'."</a> ";
        $input .= "</div><div style='clear:both'></div>";
        $input .= "<div id='loading-$unique' style='display:none'><span id='upload-state-message-$unique'></span> <span class='qq-upload-spinner'></span> <span id='progress-$unique'></span></div>";
        $input .= "<div style='display:none'><a href='".$upload_url.($field_info->name).(!empty($parent_id)?'/'.$parent_id:'')."' id='url_$unique'></a></div>";
        $input .= "<div style='display:none'><a class='x-del-url' href='".$delete_url . ($field_info->name)."' id='delete_url_$unique' rel='$value' ></a></div>";

        echo json_encode(['js'=>$inline_js,'input'=>$input,'value'=>$value]) ;
    }

    function view_klausul_sppr($id_klausul_sppr){
        $klausul_sppr = $this->db->where('id_klausul_sppr', $id_klausul_sppr)->get('tb_klausul_sppr')->row();
        $data = [
            'klausul_sppr' => $klausul_sppr
        ];

        $this->load->view('_grup_proyek/view_klausul_sppr',$data);
    }

    function view_foto_kavling($id_grup_proyek){
        $foto_kavling = $this->db->where('parent_id',$id_grup_proyek)
                                    ->where('rules','foto_kavling')
                                    ->get('uploads')
                                    ->result();
        if(empty($foto_kavling)){
            $foto_kavling = [
                // (object)['file_path'=>'uploads/foto_kavling/default.png']
            ];
        }
        $data = [
            'foto_kavling' => $foto_kavling
        ];
        $this->load->view('_grup_proyek/view_foto_kavling',$data);
        
    }
    function foto_kavling_form($id_klausul_sppr){
        $gp = $this->db->where('id_klausul_sppr',$id_klausul_sppr)->get('tb_grup_proyek')->row();
         $foto_kavling = $this->db->where('parent_id',$gp->id_grup_proyek)
                                    ->where('rules','foto_kavling')
                                    ->get('uploads')
                                    ->result();
        
        $data=[
            'foto_kavling' => $foto_kavling
        ];

        $response = [
            'tpl' => $this->load->view('_pengaturan/foto_kavling_form',$data,TRUE),
            'parent_id'=>$gp->id_grup_proyek,
            'foto_kavling' => $foto_kavling,
            'success'=> true
        ];
        
        echo json_encode($response);        
    }
}
