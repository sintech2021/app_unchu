<?php  defined('BASEPATH') OR exit('No direct script access allowed');
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Exception\ParseException;
class Harga_perumahan extends Theme_Controller {
	public $_page_title = 'Harga Perumahan';
	public function custom_grid_data()
    {
        $this->load->model('account/m_login');
        $this->load->model('m_harga_perumahan','model');
        $list = $this->model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $action = "<td class=\"actions\"> 
            <div class='btn-group'>  
            <button href=\"".site_url('pengaturan/harga_perumahan/index/edit/'.$field->id)."/".slugify($field->tipe)."\" class=\"gc-bt-edit edit_button btn btn-sm btn-icon btn-pure btn-info\" role=\"button\"> <i class=\"fa fa-edit\" aria-hidden=\"true\"></i> 
            </button> "."
            <button onclick=\"javascript: return delete_row('".site_url('pengaturan/harga_perumahan/index/delete/'.$field->id)."',-1,this)\" href=\"javascript:;\" class=\"gc-bt-delete delete_button btn btn-sm btn-icon btn-pure btn-danger\" role=\"button\"> <i class=\"fa fa-trash\" aria-hidden=\"true\"></i> 
            </button>
            </div>
            </td>";
            $no++;
            $row = array();
            // $row[] = $no;        
            $row[]=$field->nama_proyek;

            $row[] = $field->tipe;
      
		$row[]=$field->luas_bangunan;
		$row[]=$field->luas_tanah;
		$row[]=$field->metode_pembelian;
		$row[]=$field->harga_jual_standar;
		// $row[]=$field->uang_muka_standar;
		// $row[]=$field->diskon;
		// $row[]=$field->biaya_tambahan;
		// $row[]=$field->kelebihan_tanah_hook;
		// $row[]=$field->harga_per_m2;
		// $row[]=$field->pilih_masa_indent;
		;

            // $user = $this->m_login->get_by_id($field->id_user);
            // $row[] =date('d-m-Y',strtotime($field->tgl_entry));
            // $row[] = date('d-m-Y',strtotime($field->tgl_update));
            // $row[] = $user->name;
            $row[] = $action; 
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->model->count_all(),
            "recordsFiltered" => $this->model->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }
    public function custom_grid_ajax()
    {
        $tdata = ['add_url' => site_url('pengaturan/harga_perumahan/index/add')];
        $tdata['is_admin'] = $this->cms_user_group() == 'admin';
        $this->load->view('_pengaturan/dt_harga_perumahan.php',$tdata);
    }
    private function _customGrid(){
        $dump = file_get_contents(APP . '/config/gc.yml');
        $data = Yaml::parse($dump);
        $tdata = ['add_url' => site_url('pengaturan/harga_perumahan/index/add')];
        $data['output'] = $this->load->view('_pengaturan/dt_harga_perumahan.php',$tdata,true);
        $data['unique_hash'] = md5(date('YmdHis-Unit'));
        $data['is_admin']  = $this->cms_user_group()=='admin';
        $this->view('_pengaturan/harga_perumahan.php', $data );
    }
    public function index()
    {
        $target_yaml = APP . '/form/tb_harga_perumahan.yml';
        $buffer = file_get_contents($target_yaml);
        $_SERVER['FORM_DEF'] = Yaml::parse($buffer);
        $crud = $this->new_crud();
        $crud->set_subject('Harga');
        $state = $crud->getState();
        switch ($state) {
            case 'list':
                $args = func_get_args();
                return call_user_func_array([$this,'_customGrid'], $args);
                break;
            default:
                # code...
                break;
        }
        $crud->unset_jquery();
        // $crud->unset_export();
        if (! $this->input->is_ajax_request()) {
            $crud->set_theme_config(['crud_paging' => true ]);
        }
        $crud->set_table('tb_harga_perumahan');
		$crud->set_model('m_harga_perumahan');
		$crud->set_theme('datatables');
        $crud->field_type('tgl_entry','hidden');
        $crud->field_type('tgl_update','hidden');
        $crud->field_type('id_user','hidden');
        $crud->callback_before_insert(array($this,'_set_tgl_entry'));
        $crud->callback_before_update(array($this,'_set_tgl_update'));



        $crud->display_as('id_grup_proyek','Grup Proyek');
        $crud->display_as('tipe','Tipe');
        $crud->display_as('luas_bangunan','Luas Bangunan');
        $crud->display_as('luas_tanah','Luas Tanah');
        $crud->display_as('metode_pembelian','Metode Pembelian');
        $crud->display_as('harga_jual_standar','Harga Jual Standar');
        $crud->display_as('uang_muka_standar','Uang Muka Standar');
        $crud->display_as('diskon','Diskon');
        $crud->display_as('biaya_tambahan','Biaya Tambahan');
        $crud->display_as('kelebihan_tanah_hook','Kelebihan Tanah Hook');
        $crud->display_as('harga_per_m2','Harga Per M2');
        $crud->display_as('pilih_masa_indent','Pilih Masa Indent');

        $crud->set_rules('id_grup_proyek','Grup Proyek','trim|required|numeric');
        $crud->set_rules('tipe','Tipe','trim|required');
        $crud->set_rules('luas_bangunan','Luas Bangunan','trim|required');
        $crud->set_rules('luas_tanah','Luas Tanah','trim|required');
        $crud->set_rules('metode_pembelian','Metode Pembelian','trim|required');
        $crud->set_rules('harga_jual_standar','Harga Jual Standar','trim|required');
        $crud->set_rules('uang_muka_standar','Uang Muka Standar','trim|required');
        // $crud->set_rules('diskon','Diskon','trim|required');
        // $crud->set_rules('biaya_tambahan','Biaya Tambahan','trim|required');
        // $crud->set_rules('kelebihan_tanah_hook','Kelebihan Tanah Hook','trim|required');
        // $crud->set_rules('harga_per_m2','Harga Per M2','trim|required');
        // $crud->set_rules('pilih_masa_indent','Pilih Masa Indent','trim|required');

        $crud->set_relation('id_grup_proyek','tb_grup_proyek','nama_proyek');
        $crud->set_relation('pilih_masa_indent','tb_master_masa_indent','nama');
        $crud->set_relation('metode_pembelian','tb_master_metode_pembelian','nama');
    	
        $id_user = $this->cms_user_id();
		$state = $crud->getState();
    	$state_info = $crud->getStateInfo();
    	$id_user = $this->cms_user_id();
        $data = $crud->render();
		$data->is_admin  = $this->cms_user_group()=='admin';
		$this->view('_pengaturan/harga_perumahan.php',$data);
    }
    function _set_tgl_entry($post_array){
        $dt = date('Y-m-d H:i:s');
        $post_array['tgl_entry'] = $dt;
        $post_array['tgl_update'] = $dt;
        $post_array['id_user'] = $this->cms_user_id();
        return $post_array;
    }
    function _set_tgl_update($post_array,$pk){
        $dt = date('Y-m-d H:i:s');
        $post_array['tgl_update'] = $dt;
        return $post_array;
    } 
    public function list_data_biaya_tambahan($fk_id)
    {
        $param_64  = $this->input->get('param');
        $param_str = base64_decode($param_64);
        $param     = json_decode($param_str);
        $results   = [
            'success' => false,
            'data'=> []
        ];
        // print_r($param);
        $cmd = $this->input->get('cmd');
        $post_keys = ['id','nama','nominal','parent_id'];
        $post_data = [];

        foreach ($post_keys as $name) {
            $post_data[$name] = $this->input->post($name);
        }
        $is_temporal = $this->input->get('temporal') == 'true';
        switch ($cmd) {
            case 'add':
                
                if($is_temporal){
                    $post_data['tmp_id'] = $this->session->userdata('tb_master_biaya_tambahan_tmp_fk');
                    $post_data['is_tmp'] = 1;
                    $post_data['parent_id'] = -1;
                }
                
                $results['success'] = true;
                $this->db->insert('tb_master_biaya_tambahan',$post_data);
                $results['data'] = $post_data;
                $results['data']['id'] = $this->db->insert_id();
                break;
            case 'edit':
                $post_data['is_tmp'] = 0;
                $results['success'] = true;
                $pk = $this->input->post('id');
                $this->db->where('id',$pk)->update('tb_master_biaya_tambahan',$post_data);
                $results['data'] = $post_data;
                $results['data']['id'] = $pk;
                break;
            case 'delete': 
                $results['success'] = true;
                $pk = $this->input->post('parent_id');
                if($is_temporal){
                    $tmp_id = $this->session->userdata('tb_master_biaya_tambahan_tmp_fk');
                    $this->db->where('parent_id',$pk)->where('tmp_id',$tmp_id)->where('is_tmp',1)->delete('tb_master_biaya_tambahan');
                }else{
                    
                    $this->db->where('id',$pk)->delete('tb_master_biaya_tambahan');
                }

                break;    
            case 'list':
                $results['success'] = true;
                if($is_temporal){
                    $tmp_id = $this->session->userdata('tb_master_biaya_tambahan_tmp_fk');

                    $results['data'] = $this->db->where('tmp_id',$tmp_id)
                                            ->where('is_tmp',1)
                                            ->get('tb_master_biaya_tambahan')
                                            ->result_array();
                }else{
                    $results['data'] = $this->db->where('parent_id',$fk_id)
                                            ->get('tb_master_biaya_tambahan')
                                            ->result_array();
                }
                
                $results['sql'] = $this->db->last_query();                            
                break;
        }
        echo json_encode($results);
    }
}
