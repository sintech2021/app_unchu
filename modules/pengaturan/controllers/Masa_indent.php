<?php  defined('BASEPATH') OR exit('No direct script access allowed');
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Exception\ParseException;
class Masa_indent extends Theme_Controller {
	public $_page_title = 'Masa Indent';
	public function custom_grid_data()
    {
        $this->load->model('account/m_login');
        $this->load->model('m_masa_indent','model');
        $list = $this->model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $is_active = is_dates_in_between($field->tgl_berlaku,$field->tgl_berakhir,date('Y-m-d'));
            $active_cls = $is_active ? 'fa-check' : 'fa-exclamation-circle';
            $action = " 
            <div class='btn-group'> 
            <button style='border:none' class=\"btn btn-sm btn-icon btn-pure btn-default\"><i class='fa {$active_cls}'></i></button> 
            <button href=\"".site_url('pengaturan/masa_indent/index/edit/'.$field->id)."/".slugify($field->nominal_indent)."\" class=\"gc-bt-edit edit_button btn btn-sm btn-icon btn-pure btn-info\" role=\"button\"> <i class=\"fa fa-edit\" aria-hidden=\"true\"></i> 
            </button> "."
            <button onclick=\"javascript: return delete_row('".site_url('pengaturan/masa_indent/index/delete/'.$field->id)."',-1,this)\" href=\"javascript:;\" class=\"gc-bt-delete delete_button btn btn-sm btn-icon btn-pure btn-danger\" role=\"button\"> <i class=\"fa fa-trash\" aria-hidden=\"true\"></i> 
            </button>
            </div>
            ";
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = rupiah($field->nominal_indent);
    		$row[] = date('d-m-Y',strtotime($field->tgl_berlaku));
    		$row[] = date('d-m-Y',strtotime($field->tgl_berakhir));
            $user  = $this->m_login->get_by_id($field->id_user);
            $row[] = date('d-m-Y',strtotime($field->tgl_entry));
            $row[] = date('d-m-Y',strtotime($field->tgl_update));
            $row[] = $user->name;
            $row[] = $action; 
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->model->count_all(),
            "recordsFiltered" => $this->model->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }
    public function custom_grid_ajax()
    {
        $tdata = ['add_url' => site_url('pengaturan/masa_indent/index/add')];
        $tdata['is_admin'] = $this->cms_user_group() == 'admin';
        $this->load->view('_master/dt_masa_indent.php',$tdata);
    }
    private function _customGrid(){
        $dump = file_get_contents(APP . '/config/gc.yml');
        $data = Yaml::parse($dump);
        $tdata = ['add_url' => site_url('pengaturan/masa_indent/index/add')];
        $data['output'] = $this->load->view('_master/dt_masa_indent.php',$tdata,true);
        $data['unique_hash'] = md5(date('YmdHis-Unit'));
        $data['is_admin']  = $this->cms_user_group()=='admin';
        $this->view('_master/masa_indent.php', $data );
    }
    public function index()
    {
        $target_yaml = APP . '/form/ms_indent.yml';
        $buffer = file_get_contents($target_yaml);
        $_SERVER['FORM_DEF'] = Yaml::parse($buffer);
        $crud = $this->new_crud();
        $crud->set_subject('Masa Indent');
        $state = $crud->getState();
        switch ($state) {
            case 'list':
                $args = func_get_args();
                return call_user_func_array([$this,'_customGrid'], $args);
                break;
            default:
                # code...
                break;
        }
        $crud->unset_jquery();
        $crud->display_as('tgl_berlaku','Tanggal Berlaku');
        $crud->display_as('tgl_berakhir','Tanggal Berakhir');
        $crud->display_as('nominal_indent','Nominal Indent');
        if (! $this->input->is_ajax_request()) {
            $crud->set_theme_config(['crud_paging' => true ]);
        }
        $crud->set_rules('nominal_indent','Nominal Indent','trim|required');
        $crud->set_rules('tgl_berlaku','Tanggal Berlaku','trim|required');
        $crud->set_rules('tgl_berakhir','Tanggal Berakhir','trim|required');
        $crud->set_table('ms_indent');
		$crud->set_model('m_masa_indent');
		$crud->set_theme('datatables');
        $crud->field_type('tgl_entry','hidden');
        $crud->field_type('tgl_update','hidden');
        $crud->field_type('id_user','hidden');
        $crud->callback_before_insert(array($this,'_set_tgl_entry'));
        $crud->callback_before_update(array($this,'_set_tgl_update'));
    	$id_user = $this->cms_user_id();
		$state = $crud->getState();
    	$state_info = $crud->getStateInfo();
    	$id_user = $this->cms_user_id();
        $data = $crud->render();
		$data->is_admin  = $this->cms_user_group()=='admin';
		$this->view('_master/masa_indent.php',$data);
    }
    function _set_tgl_entry($post_array){
        $dt = date('Y-m-d H:i:s');
        $post_array['tgl_entry'] = $dt;
        $post_array['tgl_update'] = $dt;
        $post_array['nominal_indent'] = rupiah_to_int($post_array['nominal_indent']);
        $post_array['id_user'] = $this->cms_user_id();
        return $post_array;
    }
    function _set_tgl_update($post_array,$pk){
        $dt = date('Y-m-d H:i:s');
        $post_array['tgl_update'] = $dt;
        $post_array['nominal_indent'] = rupiah_to_int($post_array['nominal_indent']);

        return $post_array;
    } 
}
