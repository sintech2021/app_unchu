<?php  defined('BASEPATH') OR exit('No direct script access allowed');
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Exception\ParseException;
class Unit_perumahan extends Theme_Controller {
	public $_page_title = 'Unit Perumahan';
	public function custom_grid_data()
    {
        // $this->load->model('account/m_login');
        $this->load->model('m_unit_perumahan','model');
        $list = $this->model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $action = "<td class=\"actions\"> 
            <div class='btn-group'>  
            <button href=\"".site_url('pengaturan/unit_perumahan/index/edit/'.$field->id_unit)."/".slugify($field->tipe)."\" class=\"gc-bt-edit edit_button btn btn-sm btn-icon btn-pure btn-info\" role=\"button\"> <i class=\"fa fa-edit\" aria-hidden=\"true\"></i> 
            </button> "."
            <button onclick=\"javascript: return delete_row('".site_url('pengaturan/unit_perumahan/index/delete/'.$field->id_unit)."',-1,this)\" href=\"javascript:;\" class=\"gc-bt-delete delete_button btn btn-sm btn-icon btn-pure btn-danger\" role=\"button\"> <i class=\"fa fa-trash\" aria-hidden=\"true\"></i> 
            </button>
            </div>
            </td>";
            $no++;
            $row = array();
            $row[] = $no;
            $row[]=$field->perumahan;
            // $row[]=$field->perumahan;
            $field->legal_link = '<a href="'.site_url('pengaturan/unit_perumahan/view_dokumen_legal_unit/'.$field->id_unit).'" onclick="displayDetail(event,this, \'Dokumen Legal Unit\')">Lihat Legal</a>';
            $field->foto_kavling_link = '<a class="image-thumbnail" target="_blank" href="'.site_url('uploads/foto_kavling/'.$field->foto_kavling).'">Lihat Foto Kavling</a>';
            
            $field->active_status = is_dates_in_between($field->tanggal_berlaku,$field->tanggal_berakhir,date('Y-m-d')) ? 'Aktif' : 'Tidak Aktif';

            $row[] = $this->load->view('_unit_perumahan/perumahan', $field, TRUE);
            $row[] = $this->load->view('_unit_perumahan/alamat', $field, TRUE);
            $row[] = $this->load->view('_unit_perumahan/harga_jual_uang_muka', $field, TRUE);

            $row[] = $action; 
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->model->count_all(),
            "recordsFiltered" => $this->model->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }
    public function custom_grid_ajax()
    {
        $tdata = ['add_url' => site_url('pengaturan/unit_perumahan/index/add')];
        $tdata['is_admin'] = $this->cms_user_group() == 'admin';
        $this->load->view('_pengaturan/dt_unit_perumahan.php',$tdata);
    }
    private function _customGrid(){
        $dump = file_get_contents(APP . '/config/gc.yml');
        $data = Yaml::parse($dump);
        $tdata = ['add_url' => site_url('pengaturan/unit_perumahan/index/add')];
        $data['output'] = $this->load->view('_pengaturan/dt_unit_perumahan.php',$tdata,true);
        $data['unique_hash'] = md5(date('YmdHis-Unit'));
        $data['is_admin']  = $this->cms_user_group()=='admin';
        $this->view('_pengaturan/unit_perumahan.php', $data );
    }
    public function index()
    {
        $target_yaml = APP . '/form/tb_unit_perumahan.yml';
        $buffer = file_get_contents($target_yaml);
        $_SERVER['FORM_DEF'] = Yaml::parse($buffer);
        $crud = $this->new_crud();
        $crud->set_subject('Unit Perumahan');
        $state = $crud->getState();
        // if(!empty($id_grup_proyek)){
        //     $crud->where('id_grup_proyek', $id_grup_proyek);
        // }
        switch ($state) {
            case 'list':
                $args = func_get_args();
                // if(!empty($id_grup_proyek)){
                //     $args['id_grup_proyek'] = $id_grup_proyek;
                // }
                return call_user_func_array([$this,'_customGrid'], $args);
                break;
            default:
                # code...
                break;
        }
        $crud->unset_jquery();
        // $crud->unset_export();
        if (! $this->input->is_ajax_request()) {
            $crud->set_theme_config(['crud_paging' => true ]);
        }
        $crud->set_table('tb_unit_perumahan');
		$crud->set_model('m_unit_perumahan');
		$crud->set_theme('datatables');
        $crud->field_type('tgl_entry','hidden');
        $crud->field_type('tgl_update','hidden');
        $crud->field_type('id_user','hidden');
        $crud->field_type('alamat','text');
        $crud->field_type('dokumen_legal_unit','text');
        // $crud->field_type('foto_kavling','text');
        $crud->field_type('status','dropdown',['0'=>'Tidak Aktif','1'=>'Aktif']);
        // $crud->unset_texteditor('alamat');
        $crud->unset_texteditor('dokumen_legal_unit');
        // $crud->unset_texteditor('foto_kavling');
        $crud->callback_before_insert(array($this,'_set_tgl_entry'));
        $crud->callback_before_update(array($this,'_set_tgl_update'));

        $crud->set_rules('id_grup_proyek','Perumahan','trim|numeric|required');
        $crud->set_rules('nomor','Nomor','trim|required');
        $crud->set_rules('harga_jual','Harga Jual','trim|required');
        $crud->set_rules('uang_muka','Uang Muka','trim|required');
        $crud->set_rules('tanggal_berlaku','Tanggal Berlaku','trim|required');
        $crud->set_rules('tanggal_berakhir','Tanggal Berakhir','trim|required');
        $crud->set_rules('foto_kavling','Foto Kavling','trim|required');
        $crud->set_rules('nama_blok_kavling','Nama Blok Kavling','trim|required');
        $crud->set_rules('alamat','Alamat','trim|required');
        $crud->set_rules('tipe','Tipe','trim|required');
        $crud->set_rules('luas_tanah','Luas Tanah','trim|required');
        $crud->set_rules('luas_bangunan','Luas Bangunan','trim|required');
        $crud->set_rules('status','Status','trim|required');
        // $crud->set_rules('dokumen_legal_unit','Dokumen Legal Unit','trim|required');
        // $crud->set_rules('foto_kavling','Foto Kavling','trim|required');

        $crud->display_as('nama_blok_kavling','Nama Blok Kavling');
        $crud->display_as('alamat','Alamat');
        $crud->display_as('tipe','Tipe');
        $crud->display_as('luas_tanah','Luas Tanah');
        $crud->display_as('luas_bangunan','Luas Bangunan');
        $crud->display_as('status','Status');
        $crud->display_as('dokumen_legal_unit','Dokumen Legal Unit');
        $crud->display_as('foto_kavling','Foto Kavling');
        $crud->display_as('id_grup_proyek','Perumahan');
        $crud->display_as('kd_prop','Provinsi');
        $crud->display_as('kd_kab','Kota/Kabupaten');
        $crud->display_as('kd_kec','Kecamatan');
        $crud->display_as('kd_kel','Kelurahan');

        $crud->set_field_upload('foto_kavling','foto_kavling');

        // $crud->field_type('kd_prop','dropdown',[''=>'-- Pilih Provinsi --']);
        // $crud->field_type('kd_kab','dropdown',[''=>'-- Pilih Kabupaten --']);
        // $crud->field_type('kd_kec','dropdown',[''=>'-- Pilih Kecamatan --']);
        // $crud->field_type('kd_kel','dropdown',[''=>'-- Pilih Kelurahan --']);

        $crud->set_relation('id_grup_proyek','tb_grup_proyek','nama_proyek');

        $crud->callback_before_insert(array($this,'_befinsert_callback'));
        $crud->callback_before_update(array($this,'_befinsert_callback'));
        // $crud->fields

    	$id_user = $this->cms_user_id();
		$state = $crud->getState();
    	$state_info = $crud->getStateInfo();
    	$id_user = $this->cms_user_id();
        $data = $crud->render();
		$data->is_admin  = $this->cms_user_group()=='admin';
		$this->view('_pengaturan/unit_perumahan.php',$data);
    }
    function _befinsert_callback($post_array,$pk="") {
        $post_array['harga_jual'] = str_replace('Rp.', '', $post_array['harga_jual']);
        $post_array['harga_jual'] = str_replace('.', '', $post_array['harga_jual']);
     
        $post_array['uang_muka'] = str_replace('Rp.', '', $post_array['uang_muka']);
        $post_array['uang_muka'] = str_replace('.', '', $post_array['uang_muka']);
        return $post_array;
    }  
    function _set_tgl_entry($post_array){
        $dt = date('Y-m-d H:i:s');
        $post_array['tgl_entry'] = $dt;
        $post_array['tgl_update'] = $dt;
        $post_array['id_user'] = $this->cms_user_id();
        return $post_array;
    }
    function _set_tgl_update($post_array,$pk){
        $dt = date('Y-m-d H:i:s');
        $post_array['tgl_update'] = $dt;
        return $post_array;
    } 
    function upload_dok_legal(){
        // $filename = $_FILES['file']['name'];
        // $location = 'uploads/dokumen_legal_unit/'.slugify($filename);

        // if(move_uploaded_file($_FILES['file']['tmp_name'], $location)){
        //     echo $location;
        // }else{
        //     echo 0;
        // }
        $config['upload_path']          = './uploads/dokumen_legal_unit';
        $config['allowed_types']        = 'gif|jpg|png|jpeg|pdf';
        $config['max_size']             = 9048;
     

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('file'))
        {
                $error = array('error' => $this->upload->display_errors());

                // $this->load->view('upload_form', $error);
                echo json_encode($error);
        }
        else
        {
                $data = array('upload_data' => $this->upload->data());

                // $this->load->view('upload_success', $data);
                echo json_encode($data);
        }
    }

    function hapus_dok_legal(){
        $filename = $this->input->get('filename');
        $path = './uploads/dokumen_legal_unit/'.$filename;

        if(file_exists($path)){
            @unlink($path);
        }
    }

    function view_dokumen_legal_unit($id_unit){
        $dok_legal_list = $this->db->get('tb_dok_legal')->result_array();
        $dd = [];

        foreach ($dok_legal_list as $row) {
            $dd[$row['nama_dokumen']] =$row['keterangan'];
        }

        $unit = $this->db->where('id_unit', $id_unit)->get('tb_unit_perumahan')->row();
        $dokumen_legal_unit = json_decode($unit->dokumen_legal_unit);
        if(!is_array($dokumen_legal_unit)){
            $dokumen_legal_unit = [];
        }
        // print_r($dokumen_legal_unit);
        $data = [
            'unit' => $unit,
            'dd' => $dd,
            'dokumen_legal_unit' => $dokumen_legal_unit
        ];
        $this->load->view('_unit_perumahan/view_dokumen_legal_unit', $data);
    }
}
