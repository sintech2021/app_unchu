<?php  defined('BASEPATH') OR exit('No direct script access allowed');
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Exception\ParseException;
class Profile_perusahaan extends Theme_Controller {
	public $_page_title = 'Profile Perusahaan';
	 
    public function custom_grid_ajax()
    {
        $tdata = ['add_url' => site_url('pengaturan/profile_perusahaan/index/add')];
        $tdata['is_admin'] = $this->cms_user_group() == 'admin';
        $this->load->view('_pengaturan/dt_profile_perusahaan.php',$tdata);
    }
    private function _customGrid(){
        $dump = file_get_contents(APP . '/config/gc.yml');
        $data = Yaml::parse($dump);
        $tdata = ['add_url' => site_url('pengaturan/profile_perusahaan/index/add')];
        $data['output'] = $this->load->view('_pengaturan/dt_profile_perusahaan.php',$tdata,true);
        $data['unique_hash'] = md5(date('YmdHis-Unit'));
        $data['is_admin']  = $this->cms_user_group()=='admin';
        $this->view('_pengaturan/profile_perusahaan.php', $data );
    }
    public function index()
    {
        $target_yaml = APP . '/form/tb_profile_perusahaan.yml';
        $buffer = file_get_contents($target_yaml);
        $_SERVER['FORM_DEF'] = Yaml::parse($buffer);
        $crud = $this->new_crud();
        $crud->set_subject('Profile');
        $state = $crud->getState();
        switch ($state) {
            case 'list':
                $args = func_get_args();
                return call_user_func_array([$this,'_customGrid'], $args);
                break;
            default:
                # code...
                break;
        }
        $crud->unset_jquery();
        // $crud->unset_export();
        if (! $this->input->is_ajax_request()) {
            $crud->set_theme_config(['crud_paging' => true ]);
        }
        $crud->set_table('tb_profile_perusahaan');
		$crud->set_model('m_profile_perusahaan');
		$crud->set_theme('datatables');
        $crud->field_type('tgl_entry','hidden');
        $crud->field_type('tgl_update','hidden');
        $crud->field_type('id_user','hidden');

        $crud->display_as('nama_perusahaan','Nama Perusahaan');
        $crud->display_as('no_telp','Nomor Telepon');
        $crud->display_as('nama_pimpinan','Nama Pimpinan');
        $crud->display_as('logo_perusahaan','Logo Perusahaan');
        $upload_dir = 'logo';
        if(!is_dir(BASE.'/uploads/'.$upload_dir)){
            @mkdir(BASE.'/uploads/'.$upload_dir);
        }
        $crud->set_field_upload('logo_perusahaan', $upload_dir);
        
        $crud->set_rules('nama_perusahaan','Nama Perusahaan','required');
        $crud->set_rules('alamat','Alamat','required');
        $crud->set_rules('no_telp','Nomor Telepon','required');
        $crud->set_rules('email','Email','required');
        $crud->set_rules('website','Website','required');
        $crud->set_rules('nama_pimpinan','Nama Pimpinan','required');
        $crud->set_rules('logo_perusahaan','Logo Perusahaan','required');

        $crud->callback_before_insert(array($this,'_set_tgl_entry'));
        $crud->callback_before_update(array($this,'_set_tgl_update'));
    	$id_user = $this->cms_user_id();
		$state = $crud->getState();
    	$state_info = $crud->getStateInfo();
    	$id_user = $this->cms_user_id();
        $data = $crud->render();
		$data->is_admin  = $this->cms_user_group()=='admin';
		$this->view('_pengaturan/profile_perusahaan.php',$data);
    }
    function _set_tgl_entry($post_array){
        $dt = date('Y-m-d H:i:s');
        $post_array['tgl_entry'] = $dt;
        $post_array['tgl_update'] = $dt;
        $post_array['id_user'] = $this->cms_user_id();
        return $post_array;
    }
    function _set_tgl_update($post_array,$pk){
        $dt = date('Y-m-d H:i:s');
        $post_array['tgl_update'] = $dt;
        return $post_array;
    } 
}
