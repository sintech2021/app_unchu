<?php
 
class Menu extends Theme_Controller
{
	
	public function index($parent_id="",$cmd='',$id='',$slug='',$mode='')
    {
    	if(is_numeric($parent_id)){
    		if (empty($cmd)) {
	           exit('No direct script access allowed');
	        }	
    	}
        
        $crud = $this->new_crud();
        $crud->set_subject('Menu');
        $crud->set_table('tb_menu');
        $crud->set_theme('datatables');

        $crud->set_rules('nama_menu','Nama Menu','required');
        $crud->set_rules('path','Path/URL','required');
        $crud->set_rules('icon','Font Icon CSS','required');

        $crud->columns('nama_menu','path','order','icon','tree_path','keterangan');
        $crud->fields('nama_menu','path','order','icon','tree_path','parent_id','keterangan','module');
    	$crud->field_type('tree_path','hidden');
    	$crud->field_type('parent_id','hidden');
    	$crud->field_type('order','hidden');
        $crud->display_as('nama_menu','Nama Menu');
        $crud->display_as('path','Path/URL');
        $crud->display_as('order','Urutan Ke');
        $crud->display_as('icon','Font Icon CSS');
        $crud->display_as('tree_path','Tree');
        $crud->display_as('Parent','Parent Menu');
         $crud->display_as('module','Modul');
        $crud->field_type('keterangan','text');
        $crud->unset_texteditor('keterangan');

        $crud->callback_before_insert(array($this,'_set_parent_id'));
        $crud->callback_before_update(array($this,'_set_tgl_update'));
        $crud->callback_after_insert(array($this,'_set_tree_path'));

        $data = $crud->render();

        // $this->view('gc',$data);
		$this->view('menu_index',$data);
	}
	function _set_parent_id($post_array){
        $this->load->model('m_menu');
        $parent_id =  $this->uri->segment(4);
        if(!is_numeric($parent_id) || empty($parent_id)){
            // exit();
        }else{
        	$post_array['parent_id'] = $parent_id;
        }
        
        $no = $this->m_menu->get_count_by_parent($parent_id);

        $this->m_menu->check_valid_order_n($no,$parent_id);
        $post_array['order'] = $no + 1;
      
        return $post_array;
    }
    function _set_tree_path($post_array,$primary_key){
     
        $this->load->model('m_menu');
        $dt = date('Y-m-d H:i:s');
        $data = [
        	'tgl_entry' 	=> $dt,
        	'tgl_update'	=> $dt,
        	'tree_path'		=> $this->m_menu->get_tree_path($post_array['parent_id'],$primary_key)
        ];
        $this->m_menu->update($primary_key,$data);
        return $post_array;
    }
    function _set_tgl_update($post_array,$pk){
        $dt = date('Y-m-d H:i:s');
        $post_array['tgl_update'] = $dt;
        return $post_array;
    }
	public function grid_data()
	{
		$this->load->library('mypdo');

		$SQLL = "SELECT t1.id_menu FROM tb_menu AS t1 LEFT JOIN tb_menu as t2 "
		." ON t1.id_menu = t2.parent_id WHERE t2.id_menu IS NULL";

		$sth = $this->pdo->prepare($SQLL);
		$sth->execute();
		// calculate the number of rows returned

		$leafnodes = array();

		while($rw = $sth->fetch(PDO::FETCH_ASSOC)) {
		   $leafnodes[$rw['id_menu']] = $rw['id_menu'];
		}

		// build the response
		$response = new stdClass();
		$response->page =1;
		$response->total =1;
		$response->records = 1;
		$response->rows = array();

		// Get parameters from the grid
		$node = filter_input(INPUT_POST, "nodeid", FILTER_SANITIZE_NUMBER_INT);
		$n_lvl = filter_input(INPUT_POST, "n_level", FILTER_SANITIZE_NUMBER_INT);

		if($node == null ) $node = 0;
		if($n_lvl == null ) $n_lvl = 0;

		if((int)$node > 0) { //check to see which node to load
		   $wh = 'parent_id='.$node; // parents
		   $n_lvl = $n_lvl+1; // we should ouput next level
		} else {
		   $wh = 'ISNULL(parent_id)'; // roots
		}

		$SQL = "SELECT id_menu, nama_menu, `path`, `order`, icon ico,tree_path, parent_id, keterangan FROM tb_menu WHERE ".$wh;
		$stm = $this->pdo->prepare($SQL);
		$stm->execute();

		while($row = $stm->fetch(PDO::FETCH_OBJ)) {
		    $row->level = $n_lvl;
		    $row->parent_id = !$row->parent_id ? 'NULL' : $row->parent_id;
		    $row->isLeaf = false;
		    if( isset($leafnodes[$row->id_menu]) && $row->id_menu == $leafnodes[$row->id_menu] ) {
		        $row->isLeaf = true;
		    }
		    $row->expanded = false;
		    $row->loaded = false;
		    $btn_add = '<button href="'.site_url().'pengaturan/menu/index/'.$row->id_menu.'/add" class="btn btn-sm btn-success add_button" onclick="addRow(this,'.$row->id_menu.','.$row->level.')"><i class="fa fa-plus"></i></button>';
		    $btn_edit = '<button href="'.site_url().'pengaturan/menu/index/'.(empty($row->parent_id)?'':$row->parent_id.'/').'edit/'.$row->id_menu.'" class="btn btn-sm btn-info edit_button" onclick="editRow(this,'.$row->id_menu.')"><i class="fa fa-edit"></i></button>';
		    $btn_delete = '<button href="'.site_url().'pengaturan/menu/index/delete/'.$row->id_menu.'" class="btn btn-sm btn-danger" onclick="deleteRow(this,'.$row->id_menu.','.$row->level.')"><i class="fa fa-trash"></i></button>';
		    $row->aksi = '<div class="btn-group">'.($row->level <= 2 ? $btn_add: '').$btn_edit.(!$row->isLeaf?'':$btn_delete).'</div>';
		    $response->rows[] = $row;

		}
		$stm->closeCursor();
		header("Content-type: text/x-json;charset=utf-8");
		// output the data to json
		echo json_encode($response);
	}
	public function grid_data_all()
	{
		$this->load->library('mypdo');
		$this->pdo->query("SET NAMES utf8");

		// First we need to determine the leaf nodes
		$SQLL = "SELECT t1.id_menu FROM tb_menu AS t1 LEFT JOIN tb_menu as t2 "
		." ON t1.id_menu = t2.parent_id WHERE t2.id_menu IS NULL";

		$sth = $this->pdo->prepare($SQLL);
		$sth->execute();
		// calculate the number of rows returned

		$leafnodes = array();

		while($rw = $sth->fetch(PDO::FETCH_ASSOC)) {
		   $leafnodes[$rw['id_menu']] = $rw['id_menu'];
		}

		// build the response
		$response = new stdClass();
		$response->page =1;
		$response->total =1;
		$response->records = 1;
		$response->rows = array();



		$this->_display_node('', 0, $leafnodes, $response);

		header("Content-type: text/x-json;charset=utf-8");
		// output the data to json
		echo json_encode($response);
	}
	function _display_node($parent, $level, &$leafnodes, &$response) {
	    // global $leafnodes, $response, $conn;
	    if($parent >0) {
	        $wh = 'parent_id='.$parent;
	    } else {
	        $wh = ' ISNULL(parent_id)';
	    }
	    $SQL = "SELECT id_menu, nama_menu, `path`, `order`, icon,tree_path, parent_id FROM tb_menu WHERE ".$wh;
	    $stm = $this->pdo->prepare($SQL);
	    $stm->execute();

	    while($row = $stm->fetch(PDO::FETCH_OBJ)) {
	        $row->level = $level;
	        $row->parent_id = !$row->parent_id ? 'NULL' : $row->parent_id;
	        $row->isLeaf = false;
	        if( isset($leafnodes[$row->id_menu]) && $row->id_menu == $leafnodes[$row->id_menu] ) {
	            $row->isLeaf = true;
	        }
	        $row->expanded = false;
	        $row->loaded = true;
	        $response->rows[] = $row;
	        $this->_display_node((integer)$row->id_menu, $level+1, $leafnodes, $response);
	    }
	    $stm->closeCursor();


	}
	
 
 
}