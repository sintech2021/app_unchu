<div class="form" id="form_penugasan_property">
	<div class="form-group">
		<div class="row">
			<label class="col-md-3">Nama</label>
			<div class="col-md-6">
				<input type="text" class="form-control" disabled value="<?=$pengguna['name']?>">
			</div>
		</div>
	</div>
	<div class="form-group">
		<div class="row">
			<label class="col-md-3">Username</label>
			<div class="col-md-6">
				<input type="text" class="form-control" disabled value="<?=$pengguna['username']?>">
			</div>
		</div>
	</div>
	<div class="form-group">
		<div class="row">
			<label class="col-md-3">Email</label>
			<div class="col-md-6">
				<input type="text" class="form-control" disabled value="<?=$pengguna['email']?>">
			</div>
		</div>
	</div>
	<div class="form-group">
		<div class="row">
			<label class="col-md-3">Nomor HP</label>
			<div class="col-md-6">
				<input type="text"  class="form-control"disabled value="<?=$pengguna['no_hp']?>">
			</div>
		</div>
	</div>
	<div class="form-group">
		<div class="row">
			<label class="col-md-3">Grup Pengguna</label>
			<div class="col-md-6">
				<input type="text" class="form-control" disabled value="<?=$pengguna['nama_grup']?>">
			</div>
		</div>
	</div>
	<div class="form-group">
		<div class="row">
			<label class="col-md-3">Grup Property</label>
			<div class="col-md-9">
				<div v-for="row,index in penugasan_property_list">
					<input class="ck_penugasan_property" type="checkbox" v-bind:index="index" :checked="row.reserved > 0" :disabled="row.reserved > 0 && row.id_pengguna != '<?=$id_pengguna?>'"/>
					<label class="control-label" v-text="row.nama_proyek"></label>

				</div>
			</div>
		</div>
	</div>
	<div class="form-group">
		<div class="row">
			<div class="col-md-6">
				
			</div>
			<div class="col-md-6" style="text-align: right;">
				<div class="btn-group">
					<button class="btn btn-default" @click="onBatal(this)"><i class="fa fa-close"></i> Batal</button>
					<button class="btn btn-info" @click="onSimpan(this)"><i class="fa fa-check"></i> Simpan</button>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	function paginate(array, page_size, page_number) {
	  return array.slice((page_number - 1) * page_size, page_number * page_size);
	}
	window.penugasan_property = new Vue({
		el:'#form_penugasan_property',
		data:{
			penugasan_property_list : <?=json_encode($penugasan_property_reserved)?>,
			penugasan_property_list_paged: [],
			penugasan_property_list_total_pages: 0, 
			penugasan_property_list_page: 1, 
		},
		mounted(){
			this.penugasan_property_list_total_pages =  Math.ceil(this.penugasan_property_list.length / 3);
			if(this.penugasan_property_list_total_pages < 1){
				this.penugasan_property_list_total_pages = 1;
			}
			console.log(this.penugasan_property_list)
		},
		methods:{
			getPenugasanProperty(){
				// if(this.penugasan_property_list_page <= this.penugasan_property_list_total_pages){
				// 	this.penugasan_property_list_paged = paginate(this.penugasan_property_list, 3, this.penugasan_property_list_page)
				// 	this.penugasan_property_list_page += 1;
				// 	return this.penugasan_property_list_paged;
				// }
			},
			onSimpan(){
				var els = $('input.ck_penugasan_property');
				var i = $(event.target).find('i');
				console.log(i)
				toggleCls(i.get(0),'fa-check','fa-spin fa-spinner');
				var postData = {};
				var list = [];
				for(var i =0; i < els.length;i++){
					var row = els[i];
					var checked = $(row).is(':checked');

					var index = $(row).attr('index');
					var obj = Object.assign({},this.penugasan_property_list[index]);
					obj.checked = checked;
					obj.id_pengguna = '<?=$id_pengguna?>';
					console.log(checked)
					if(	!$(row).is(':disabled'))
						list.push(obj)
				}
				setTimeout(()=>{
					postData.list = JSON.stringify(list); 
					$.post(site_url()+`pengaturan/pengguna/penugasan_property_save`,postData,(res)=>{
						$("#detail_pengguna").modal("hide");
					},'json');
				},1000);
				
			},
			onBatal(){
				$("#detail_pengguna").modal("hide");
			}
		}
	});
	$('#detail_pengguna span.title').text(`Penugasan Property : <?=$pengguna['name']?>(<?=$pengguna['username']?>)`)
</script>