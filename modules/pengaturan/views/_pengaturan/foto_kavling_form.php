<form id="foto_kavling_form">
                            
    <div class="row" v-for="label,index in foto_kavling_rows" :row_index="index" style="padding: 1em">
        <div class="col-md-6">
            <span replace="true" v-text="label" :parent_id="parent_id" :field_name="label" :row_index="index"></span>
                                                
        </div>
        <div class="col-md-6">
            <a class="btn btn-danger" :row_index="index" @click="delFotoKavlingRow(index)"><i class="fa fa-minus"></i></a>
            
        </div>
    </div>
    <a class="btn btn-primary" @click="addFotoKavlingRow()"><i class="fa fa-plus"></i> Tambah</a>
</form>

<style type="text/css">
    #foto_kavling_form{
        border: solid 1px #ddd;
        padding: .5em;
        text-align: right;
        margin-bottom: 1em;
    }
</style>