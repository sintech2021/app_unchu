<div class="content-tab" id="content_tab_progress_bangun">
    <ul class="nav nav-tabs">
        <li v-for="caption,url in tabs" v-bind:url="url" v-bind:class="{'active':(url==active_item)}">
            <a v-bind:href="'#'+caption" data-toggle="tab" @click="gotoUrl(url)"><span v-text="caption"></span> </a>
        </li>
    </ul>
</div>
<script type="text/javascript">
    $(document).ready(()=>{
        var _tab = new Vue({
            el : '#content_tab_progress_bangun',
            data :{
                tabs : {"pengaturan\/grup-proyek":"Perumahan","pengaturan\/dokumen-perumahan":"Dokumen Perumahan","pengaturan\/progress-bangun":"Kontraktor","pengaturan\/progress-bangun":"Progress Bangun"},
                active_item : window.breadcrumb.active_item
            } ,
            mounted(){
            },
            methods:{
                gotoUrl(url){
                    document.location.href = site_url()+url;
                }
            }
        });
    });
</script>
