<div class="content-tab" id="content_tab_kelompok_keuangan">
    <ul class="nav nav-tabs">
        <li v-for="caption,url in tabs" v-bind:url="url" v-bind:class="{'active':(url==active_item)}">
            <a v-bind:href="'#'+caption" data-toggle="tab" @click="gotoUrl(url)"><span v-text="caption"></span> </a>
        </li>
    </ul>
</div>
<script type="text/javascript">
    $(document).ready(()=>{
        var _tab = new Vue({
            el : '#content_tab_kelompok_keuangan',
            data :{
                tabs : {"pengaturan\/pos-keuangan":"Jenis Pos","pengaturan\/kelompok-keuangan":"Kelompok"},
                active_item : window.breadcrumb.active_item
            } ,
            mounted(){
            },
            methods:{
                gotoUrl(url){
                    document.location.href = site_url()+url;
                }
            }
        });
    });
</script>
