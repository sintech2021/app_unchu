<?$this->view('_pengaturan/tab_profile_perusahaan')?>
<h4><i class="fa fa fa-briefcase"></i> Profile Perusahaan</h4>
<div id="alert_cnt" style="padding: 2px;"></div>
<div id="form_detail_profile_perusahaan" href="{{ site_url }}pengaturan/profile-perusahaan/index/edit/1">
    <div class="form-content"></div>
</div>
<?php 
foreach($css_files as $file): ?>
    <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<?php foreach($js_files as $file): ?>
    <script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>
<script type="text/javascript" src="{{ site_url }}/pub/gc/js/common/list.js"></script>
<script type="text/javascript" src="{{ site_url }}/pub/gc/js/common/lazyload-min.js"></script>
<style type='text/css'>
body
{
   /* font-family: Arial;
    font-size: 14px;*/
}
a:hover
{
    text-decoration: underline;
}
th.action{ 
	width 200px !important;
}
span.select2{
	width: 100
}
table.table > thead > tr > th.text-center{
    vertical-align: top;
}
#alert_cnt i,#alert_cnt p {display: inline;}
</style>
<?php echo $output; ?>


<script type="text/javascript">
    function success_message(msg)
{
    // noty({
    //    text: success_message,
    //    type: 'success',
    //    dismissQueue: true,
    //    layout: 'top',
    //    callback: {
    //      afterShow: function() {

    //          setTimeout(function(){
    //              $.noty.closeAll();
    //          },7000);
    //      }
    //    }
    // });
    // $('#successMsg').attr('data-log-message',success_message);
    // $('#successMsg').trigger('click');

    App.alert({
        container: "#alert_cnt",
        place: 'prepend',
        type: 'success',
        message: msg,
        close: true,
        reset: true,
        focus: true,
        closeInSeconds: 0,
        icon: 'check'
    });
}
    // window.success_message = 'Profile Berhasil diupdate';
    gc.FormDef = <?=json_encode($_SERVER['FORM_DEF'])?>;
    function displayDetail(event,el) {
        // const el = event.target;
        console.log(el.href);
        const tr = $(el).closest('tr');
        console.log(tr)
        const url = el.href;
        $.post(url,(res)=>{
            console.log(res);
            $("#detail_profile_perusahaan").unbind('show.bs.modal');
            $("#detail_profile_perusahaan").on('show.bs.modal', function(){
                $('#lihat-profile_perusahaan').html(res);
            });
            $("#detail_profile_perusahaan").modal("show");
        });
        event.preventDefault();
        return false;
    }
    
    $(document).ready(function(){
        var form = $('#form_detail_profile_perusahaan');
        fnOpenEditFormInline(form)
    });
gc.table = 'tb_profile_perusahaan';  
 gc.__onShowForm = (form,state,ajax_url,e)=>{
    window.changeFormMode_form = form;
    window.changeFormMode = (mode)=>{
        if(mode=='0'){
            $(window.changeFormMode_form).attr('editing','0');
            $(window.changeFormMode_form).find('input[type=text]').attr('disabled',true);
            
            if($(window.changeFormMode_form).find('a.btn.delete-anchor').length > 0){
                $(window.changeFormMode_form).find('a.btn.delete-anchor').hide();

            }
            $(window.changeFormMode_form).find('.fileinput-button').attr('disabled',true);
            $(window.changeFormMode_form).find('.gc-file-upload').attr('disabled',true).css('opacity',0);

            $(window.changeFormMode_form).find('#cancel-button').attr('disabled',true);
            $(window.changeFormMode_form).find('#form-button-edit').show();
            $(window.changeFormMode_form).find('#form-button-save').hide();

        }else{
            $(window.changeFormMode_form).attr('editing',mode);
            $(window.changeFormMode_form).find('input[type=text]').attr('disabled',false);
            if($(window.changeFormMode_form).find('a.btn.delete-anchor').length > 0){
                $(window.changeFormMode_form).find('a.btn.delete-anchor').show();

            }
            $(window.changeFormMode_form).find('.fileinput-button,.gc-file-upload').attr('disabled',false);
            $(window.changeFormMode_form).find('.gc-file-upload').attr('disabled',false).css('opacity',0);

            $(window.changeFormMode_form).find('#form-button-save').show();
            $(window.changeFormMode_form).find('#form-button-edit').hide();
            $(window.changeFormMode_form).find('#cancel-button').attr('disabled',false);

        }
     }
    var btnEdit = `<a id="form-button-edit" onclick="changeFormMode('1')" class="btn btn-info"><i class="fa fa-edit"></i> Edit</a>`;
    $(form).find('#form-button-save').hide().after(btnEdit);
    $(form).find('#cancel-button').click(()=>{
        changeFormMode('0');
    });
    changeFormMode('0');
    
}; 
 
</script>
<script type="text/javascript" src="<?=site_url()?>pub/accordion_form/script.js"></script>
<link rel="stylesheet" type="text/css" href="<?=site_url()?>pub/accordion_form/style.css">
