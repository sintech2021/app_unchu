<?$this->view('_pengaturan/tab_pengguna')?>
<h4><i class="fa fa-user-plus"></i> Daftar Pengguna</h4>
<iframe src="" id="export_excel" style="display: none"></iframe> 
<div class="modal" tabindex="-1" id="detail_pengguna">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body" style="padding-top: 0">
                <div class="row">
                    <div class="col-md-12" style="background:#2b3643;padding:.5em  ">
                        <h4><a style="margin-right: 1em" data-dismiss="modal"><i class="fa fa-chevron-left"></i></a> <span style="color: #fff" class="title">Isi Pengguna</span></h4>
                    </div> 
                </div>
                <div class="row">
                    <div class="col-md-12">
                         <div id="lihat-pengguna" style="padding: 1em">
                            Memuat Detail 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php 
foreach($css_files as $file): ?>
    <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<?php foreach($js_files as $file): ?>
    <script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>
<style type='text/css'>
body
{
   /* font-family: Arial;
    font-size: 14px;*/
}
a:hover
{
    text-decoration: underline;
}
th.action{
	width 200px !important;
}
span.select2{
	width: 100
}
table.table > thead > tr > th.text-center{
    vertical-align: top;
}
</style>
<?php echo $output; ?>
<script type="text/javascript">
    gc.FormDef = <?=json_encode($_SERVER['FORM_DEF'])?>;
    function displayDetail(event,el) {
        // const el = event.target;
        console.log(el.href);
        const tr = $(el).closest('tr');
        console.log(tr)
        const url = el.href;
        $.post(url,(res)=>{
            // console.log(res);
            $("#detail_pengguna").unbind('show.bs.modal');
            $("#detail_pengguna").on('show.bs.modal', function(){
                $('#lihat-pengguna').html(res);
            });
            $("#detail_pengguna").modal("show");
        });
        event.preventDefault();
        return false;
    }
    $('button').click(()=>{
        ref = $('#pengguna_table').DataTable();
        ref.ajax.reload();
    })
    $(document).ready(function(){
        
    });
gc.table = 'tb_users';   
attachHref=(href)=>{
    const url = href;
        $.post(url,(res)=>{
            // console.log(res);
            $(".content-main-0").hide();
            $('.content-main-1').html(res);
            $('.content-main-1').show();
        });
        event.preventDefault();
        return false;
} 
</script>
<script type="text/javascript" src="<?=site_url()?>pub/accordion_form/script.js"></script>
<link rel="stylesheet" type="text/css" href="<?=site_url()?>pub/accordion_form/style.css">
