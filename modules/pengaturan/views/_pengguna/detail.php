<div class="profile-pengguna" id="content_tab">
	<div class="content-tab">
    <ul class="nav nav-tabs">
        <li v-for="caption,url in tabs" v-bind:url="url" v-bind:class="{'active':(url==active_item)}">
            <a v-bind:href="'#'+slugify(caption)" data-toggle="tab" @click="gotoUrl(url)"><i :class="icon[url]"></i> {{caption}} </a>
        </li>
    </ul>


	
	<div class="tab-content">
		<div class="tab-pane active" id="tab_profile">
			<h4>Profile Pengguna</h4>
			<div class="row">
				<div class="col-md-3">
					<div class="foto-pengguna" style="width: 211px;height:211px;overflow:hidden;margin: 0 auto;">
						<img class="thumbnail" src="<?=$foto?>" style="width:100%">
					</div>
				</div>
				<div class="col-md-6">
					<table class="table table-hover">
						<tbody>
							<tr>
								<th>Nama</th><td>: <?=$pengguna['name']?></td>
							</tr>
							<tr>
								<th>Username</th><td>: <?=$pengguna['username']?></td>
							</tr>
							<tr>
								<th>Email</th><td>: <?=$pengguna['email']?></td>
							</tr>
							<tr>
								<th>Nomor HP</th><td>: <?=$pengguna['no_hp']?></td>
							</tr>
							<tr>
								<th>Grup Pengguna</th><td>: <?=$pengguna['nama_grup']?></td>
							</tr>
							<tr>
								<th>Status Pengguna</th><td>: <?=$pengguna['status']==1?'Aktif':'Tidak Aktif'?></td>
							</tr>
							<tr>
								<th>Tipe Sales</th><td>: <?=$pengguna['tipe_sales']?></td>  
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
       new Vue({
            el : '#content_tab',
            data :{
                tabs : {tab_profile:"Profil",kembali:"Kembali"},
                icon : {tab_profile:'',kembali:'fa fa-arrow-left'},
                active_item : 'tab_profile'
            } ,
            mounted(){
            },
            methods:{
                gotoUrl(url){
                    if(url=='kembali'){
                    	$(".content-main-1").hide().html('');
            			$('.content-main-0').show();
                    }
                }
            }
        });
</script>
</div>