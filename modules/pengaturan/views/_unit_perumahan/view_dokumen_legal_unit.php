<!-- <h4>Dokumen Legal Unit</h4> -->

<!-- <textarea id="field-dokumen_legal_unit"><?=$unit->dokumen_legal_unit?></textarea> -->

<table class="table table-bordered">
	<thead>
		<tr>
			<th>No</th>
			<th>Nama Dokumen</th>
			<th>Keterangan</th>
			<th>Nama File</th>
		</tr>
	</thead>
	<tbody>
		<?if(count($dokumen_legal_unit) == 0):?>
		<tr>
			<td colspan="4">Tidak ada dokumen</td>
		</tr>
		<?else:?>
		<?foreach($dokumen_legal_unit as $index => $d):?>
		<tr>
			<td><?=$index+1?></td>
			<td><?=$d->nama_dokumen?></td>
			<td><?=$dd[$d->nama_dokumen]?></td>
			<td><a target="_blank" href="<?=site_url('uploads/dokumen_legal_unit/'.$d->path)?>"><?=$d->path?></a></td>
		</tr>
		<?endforeach?>
		<?endif?>
		
	</tbody>
</table>