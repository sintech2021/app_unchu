<table class="row-detail">
	<tbody>
		<tr>
			<td style="text-align: left;">Harga Jual Standar</td>
			<td style="display: block !important"> : </td>
			<td><?=rupiah($harga_jual)?></td>
		</tr>
		<tr>
			<td style="text-align: left;">Uang Muka Standar</td>
			<td style="display: block !important"> : </td>
			<td><?=rupiah($uang_muka)?></td>
		</tr>
		<tr>
			<td style="text-align: left;">Legal</td>
			<td style="display: block !important"> : </td>
			<td><?=$legal_link?></td>
		</tr>
		<tr>
			<td style="text-align: left;">Foto Kavling</td>
			<td style="display: block !important"> : </td>
			<td><?=$foto_kavling_link?></td>
		</tr>
		<tr>
			<td style="text-align: left;">Tanggal Berlaku</td>
			<td style="display: block !important"> : </td>
			<td><?=date('d-m-Y',strtotime($tanggal_berlaku))?></td>
		</tr>
		
		
	</tbody>
</table>