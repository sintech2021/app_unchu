<table class="row-detail">
	<tbody>
		<tr>
			<td style="text-align: left;">Blok/Kavling</td>
			<td style="display: block !important"> : </td>
			<td><?=$nama_blok_kavling?>/No. <?=$nomor?></td>
		</tr>
		<tr>
			<td style="text-align: left;">Tipe</td>
			<td style="display: block !important"> : </td>
			<td><?=$tipe?></td>
		</tr>
		<tr>
			<td style="text-align: left;">Luas Bangunan</td>
			<td style="display: block !important"> : </td>
			<td><?=$luas_bangunan?> M<sup>2</sup></td>
		</tr>
		<tr>
			<td style="text-align: left;">Luas Tanah</td>
			<td style="display: block !important"> : </td>
			<td><?=$luas_tanah?> M<sup>2</sup></td>
		</tr>
		<tr>
			<td style="text-align: left;">Status</td>
			<td style="display: block !important"> : </td>
			<td><?=$active_status?></td>
		</tr>
	</tbody>
</table>