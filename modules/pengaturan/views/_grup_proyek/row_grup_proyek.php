
<table class="tbl-detail">
<tbody>
	<tr><td style="text-align: left;">Luas Tanah</td><td>:</td><td style="text-align: left;"><?=format_angka($luas_tanah)?>M<sup>2</sup></td></tr>
	<tr><td style="text-align: left;">Jumlah Unit</td><td>:</td><td style="text-align: left;"><?=format_angka($jml_unit)?></sup></td></tr>
	<tr><td style="text-align: left;">Klasifikasi</td><td>:</td><td style="text-align: left;"><?=$klasifikasi?></td></tr>
	<tr><td style="text-align: left;">Konsep</td><td>:</td><td style="text-align: left;"><?=$konsep?></td></tr>
	<tr><td style="text-align: left;">Segmentasi</td><td>:</td><td style="text-align: left;"><?=$segmentasi?></td></tr>
</tbody>
</table>
