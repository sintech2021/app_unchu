<?foreach($css_files as $file): ?>
    <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<?php foreach($js_files as $file): ?>
    <script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>

<?php echo $output; ?>

<script type="text/javascript">
	success_message = function(r){
		if(typeof parent.my_success_message == 'function'){
			parent.my_success_message(r);
		}
	}
	$(document).ready(()=>{
		$('#modalForm').removeClass('modal fade');
		$('#modalForm .modal-dialog').removeClass('modal-dialog modal-lg');
	});
</script>
<script type="text/javascript" src="<?=site_url('modules/pengaturan/assets/klausul_sppr/form.js')?>"></script>
<style type="text/css">
	#modalForm .modal-header{
		display: none;
	}
	#modalForm #cancel-button,#modalForm #form-button-save,.modal-footer,#logger{
		display: none !important;
	}
	.modal-content{
		border: none !important;
	}
</style>