<link rel="stylesheet" type="text/css" href="{{ site_url }}pub/css/jqgrid/ui.jqgrid-bootstrap.css">
<script type="text/javascript" src="{{ site_url }}pub/js/jqgrid/jquery.jqGrid.min.js"></script>
<script type="text/javascript" src="{{ site_url }}pub/js/jqgrid/grid.locale-id.js"></script>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h4><i class="fa fa-gg"></i> Pengaturan Menu</h4>
			<div id="dt">
				<?php 
				foreach($css_files as $file): ?>
				    <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
				<?php endforeach; ?>
				<?php foreach($js_files as $file): ?>
				    <script src="<?php echo $file; ?>"></script>
				<?php endforeach; ?>
				</style>
				<?php echo $output; ?>
			</div>
			<div id="grid_container">
				
			<table id="treegrid"></table>
			<div id="ptreegrid"></div>
			</div>

				
		</div>
	</div>
</div>

<script type="text/javascript">
 $(document).ready(()=>{


$.jgrid.defaults.width = $('.page-content').width();	
jQuery("#treegrid").jqGrid({
  caption: "MENU",
  treeGrid: true,
  treeGrid_bigData : true, styleUI : 'Bootstrap',
  treeGridModel: 'adjacency',
  ExpandColumn : 'nama_menu',
  url:  '{{ site_url }}pengaturan/menu/grid_data',
  datatype: "json",
  jsonReader : {
    repeatitems : false
  },
  // parent fields need to be configured
  treeReader : {
   parent_id_field: "parent_id"
  },
  mtype: "POST",
  colNames:["ID","Nama","Path", "Icon", "Order","Parent","Tree","Keterangan",'Aksi'],
  colModel:[
    {name:'id_menu',index:'id_menu', width:1,hidden:true,key:true},
    {name:'nama_menu',index:'nama_menu', width:80},
    {name:'path',index:'path', width:100, align:"left"},
    {name:'ico',index:'ico', width:80, align:"left"},      
    {name:'order',index:'order',width:1,hidden:true,align:"center"},      
    {name:'parent_id',index:'parent_id',width:1,hidden:true ,align:"center"},      
    {name:'tree_path',index:'tree_path', width:1,hidden:true,align:"right"},      
    {name:'keterangan',index:'keterangan', width:120,align:"left"},      
    {name:'aksi',index:'aksi', width:80,align:"center"}      
  ],
  height:'auto',
  pager : "#ptreegrid",
  rowNum : 10,
  rowList : [10,20,30],
  loadComplete: function(data) {
	   add_edit_button_listener();
	},
});
$(window).resize(()=>{
	console.log('resized');
	setTimeout(()=>{
		 $('#treegrid').jqGrid('setGridWidth',  $('.page-bar').width()-30);
	},1000);
}).resize();

	setTimeout(()=>{
		var refreshBtn = $('<button class="refresh_grid_menu btn btn-sm btn-circle btn-success" onclick="refreshGridMenu()"><i class="fa fa-refresh"></i></button>');
		$('.datatables-add-button').append(refreshBtn);
	},1000);
 });
 gc._DONT_REFRESH_CLICK = true;
 gc._AFTER_SUCCESS_FORM = (data)=>{
 	console.log(data);
 	refreshGridMenu();
 };
 gc._AFTER_SUCCESS_DELETE = (data)=>{
 	console.log(data);
 	refreshGridMenu();
 };
 addRow = (el,id_menu,level)=>{
 	console.log(el,id_menu,level)
 };
 deleteRow = (el,id_menu) =>{
 	return delete_row(site_url()+`pengaturan/menu/index/delete/${id_menu}`, -1);
 };
 editRow = (el,id_menu) =>{

 };
 refreshGridMenu = ()=>{
 	$('#treegrid').trigger("reloadGrid");
 };
</script>

<style type="text/css">
	.page-content,
	.ui-jqgrid-bdiv{
		overflow-x: hidden !important;
	}
	.ui-jqgrid-titlebar {
		display: none
	}
	#jqgh_treegrid_aksi{
		text-align: center;
	}
	.dataTablesContainer{
		display: none;
		/*visibility: .8*/
	}
	.refresh_grid_menu{
		float: right;
	}
</style>