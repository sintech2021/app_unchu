<?php
require_once APPPATH . '/models/Grocery_CRUD_Model.php';
class M_unit_perumahan  extends Grocery_CRUD_Model  {
	var $table = 'tb_unit_perumahan'; 
    
    function get_detail($id_unit){
        $this->db->select("a.*,gp.nama_proyek perumahan,gp.alamat_proyek alamat,p.nama_propinsi propinsi,k.nama_kota kota_kabupaten,kc.nama_kecamatan kecamatan,kl.nama_kelurahan kelurahan")
                 ->from($this->table . ' a')
                 ->join('tb_grup_proyek gp','gp.id_grup_proyek=a.id_grup_proyek','left')
                 ->join('tb_propinsi p','p.id_propinsi=gp.propinsi','left')
                 ->join('tb_kota k','k.id_kota=gp.kota','left')
                 ->join('tb_kecamatan kc','kc.id_kecamatan=gp.kecamatan','left')
                 ->join('tb_kelurahan kl','kl.id_kelurahan=gp.kelurahan','left');
        return $this->db->where('a.id_unit',$id_unit)->get()->row();
    }
    function _get_datatables_query()
    {
        $this->db->select("a.*,gp.nama_proyek perumahan,gp.alamat_proyek alamat,p.nama_propinsi propinsi,k.nama_kota kota_kabupaten,kc.nama_kecamatan kecamatan,kl.nama_kelurahan kelurahan")
                 ->from($this->table . ' a')
                 ->join('tb_grup_proyek gp','gp.id_grup_proyek=a.id_grup_proyek','left')
                 ->join('tb_propinsi p','p.id_propinsi=gp.propinsi','left')
                 ->join('tb_kota k','k.id_kota=gp.kota','left')
                 ->join('tb_kecamatan kc','kc.id_kecamatan=gp.kecamatan','left')
                 ->join('tb_kelurahan kl','kl.id_kelurahan=gp.kelurahan','left');
        $i = 0;
        $search_queries = $_POST['search']['value'];
        $search_queries = explode(',', $search_queries);

        $id_grup_proyek = $this->input->get('id_grup_proyek');

        if(is_numeric($id_grup_proyek)){
            $this->db->where('a.id_grup_proyek',$id_grup_proyek);
        }

        if(count($search_queries) > 0){
            foreach ($this->column_search as $item) // looping awal
            {
                if($_POST['search']['value']) // jika datatable mengirimkan pencarian dengan metode POST
                {
                    if($i===0) // looping awal
                    {
                        $this->db->group_start(); 
                        $this->db->like($item, $_POST['search']['value']);
                    }
                    else
                    {
                        $this->db->or_like($item, $_POST['search']['value']);
                    }
                    if(count($this->column_search) - 1 == $i) 
                        $this->db->group_end(); 
                }
                $i++;
            }
        }
        if(isset($_POST['order'])) 
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
}
