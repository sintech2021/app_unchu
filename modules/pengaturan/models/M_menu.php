<?php
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Exception\ParseException; 
class M_menu extends CI_Model
{
	var $table = 'tb_menu';

	public function update($pk,$data)
	{
		$this->db->where('id_menu',$pk)->update($this->table,$data);
	}
	public function get_count_by_parent($parent_id){
		if(!empty($parent_id)){
            $this->db->where('a.parent_id',$parent_id);
        }else{
            $this->db->where(' a.parent_id ',null,false);

        }
        return $this->db->select("COUNT(a.id_menu) _cx")
                        ->get($this->table.' a')
                        ->row()->_cx + 0; 
	}
	public function check_valid_order_n($a,$parent_id){
		if(!empty($parent_id)){
            $this->db->where('a.parent_id',$parent_id);
        }else{
            $this->db->where('a.parent_id',null,false);

        }
		$record_exists = $this->db->where('a.order',$a)
                 ->get($this->table .' a')
                 ->num_rows() == 1;
        if(!$record_exists){
        	if(!empty($parent_id)){
	            $this->db->where('a.parent_id',$parent_id);
	        }else{
	            $this->db->where('a.parent_id',null,false);

	        }
            $record_orders = $this->db->select('a.id_menu')
                                      ->order_by('a.order','asc')
                                      ->get($this->table .' a')->result_array();
            foreach ($record_orders as $new_order => $row) {
                $this->db->where('id_menu',$row['id_menu'])
                         ->update($this->table ,['order'=>$new_order+1]);
            }                          
        } 
        return $record_exists;  
	}
	public function get_tree_path($parent_id, $pk)
	{
		if(empty($parent_id)){
			return $pk . '*';
		}else{
			return $this->db->where('id_menu',$parent_id)->select('tree_path')->get($this->table)->row()->tree_path . $pk . '*';
		}
		
	}
	public function get_dropdown_tree()
	{
		$yml_cache = BASE .'/cache/menu.yml';

		if(file_exists($yml_cache)){
			return Yaml::parse(file_get_contents($yml_cache));
		}

		$level_0 = $this->db->select('a.id_menu,a.nama_menu')
				 ->where('a.parent_id',null,false)
				 ->order_by('order','asc')
				 ->get($this->table.' a')
				 ->result_array();
		$menu = [];


		foreach ($level_0 as &$p) {
			$menu[$p['id_menu']] = $p['nama_menu'];

			$level_1 = $this->db->select('a.id_menu,a.nama_menu')
				 ->where('a.parent_id',$p['id_menu'])
				 ->order_by('order','asc')
				 ->get($this->table.' a')
				 ->result_array();
			 if(!empty($level_1)){
			 	foreach ($level_1 as &$p2) {
				 
				 	$menu[$p2['id_menu']] = " ---- ".$p2['nama_menu'];
					$level_2 = $this->db->select('a.id_menu,a.nama_menu')
						 ->where('a.parent_id',$p2['id_menu'])
						 ->order_by('order','asc')
						 ->get($this->table.' a')
						 ->result_array();
					if(!empty($level_2)){
						foreach ($level_2 as &$p3) {
							$menu[$p3['id_menu']] =" -------- " . $p3['nama_menu'];
						}	
					}		 
					 
				}
			 }
				 

		}
		
		file_put_contents($yml_cache, Yaml::dump($menu));
		return $menu;		 
	}
	 
	public function get_by_id($id_menu){
		return $this->db->where('id_menu',$id_menu)->get($this->table)->row();
	}
}