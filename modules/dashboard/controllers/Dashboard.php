<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Theme_Controller {
	public $_page_title = 'Dashboard';
	
	public function index()
	{
		$dd_perumahan = [];
		$dd_perumahan_tmp = $this->db->select("id_grup_proyek,nama_proyek")->get('tb_grup_proyek')->result();

		foreach ($dd_perumahan_tmp as $row) {
			$dd_perumahan[$row->id_grup_proyek]=$row->nama_proyek;
		}
 		$data= [
			'display_name' => $this->cms_display_name(),
			'dd_perumahan'=>$dd_perumahan,
			'selected_id_grup_proyek' => $this->input->get('id_grup_proyek')
		];
		// print_r($data);
		// exit();
		$this->view('_dashboard',$data);
	}

	public function set_toggle_session_menu($state)
	{
		$this->session->set_userdata('toggle_session_menu',$state);
	}
}
