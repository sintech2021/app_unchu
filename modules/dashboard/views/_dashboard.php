<div id="dashboard">
<!-- Gambar -->
<div class="modal" tabindex="-1" id="detail_event">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-body" style="padding-top: 0">
               	<div class="row">
               		<div class="col-md-12" style="background:#2b3643;padding:.5em  ">
               			<h4><a style="margin-right: 1em" data-dismiss="modal"><i class="fa fa-chevron-left"></i></a> <span style="color: #fff">Detail Survey</span></h4>
               		</div> 
               	</div>
                <div class="row">
                	<div class="col-md-12">
                		 <div id="lihat-event" style="padding: 1em">
		                	Memuat Detail Event
		                </div>
               
                	</div>
                	 
                </div>
            </div>
            
        </div>
    </div>
</div>
<!-- @@@@ -->
<div class="row">
  <div class="col-md-12">
    <h4>Selamat Datang {{ display_name }},  </h4>

   </div>
</div>
<div class="row">
  <div class="col-md-12">
    <form action="#" class="form-horizontal form-bordered">
      <div class="form-group">
        <div class="row">
          <div class="col-md-6" style="">
            <div class="row">
              <label class="control-label col-md-4" style="width:125px;text-align: left;padding-left: 2em">Pilih Tanggal</label>
              <div class="col-md-8" >
                  <div class="input-group  date-picker input-daterange" data-date="10/11/2012" data-date-format="mm/dd/yyyy">
                      <span class="input-group-btn">
                          <button class="btn default" type="button">
                              <i class="fa fa-calendar"></i>
                          </button>
                      </span>
                      <input type="text" class="form-control" name="dt_min">
                      <span class="input-group-addon" style="border:none;background: none"> s.d </span>
                      <span class="input-group-btn">
                          <button class="btn default" type="button">
                              <i class="fa fa-calendar"></i>
                          </button>
                      </span>
                      <input type="text" class="form-control" name="dt_max"> </div>
              </div>
            </div>
          </div>
          <div class="col-md-6" style="">
             <div class="row">
            <label class="control-label col-md-2" style="text-align: left;width: 115px">Pilih Proyek</label>
            <div class="col-md-5">
                <select class="form-control" id="pilih_id_grup_proyek">
                  <!-- <option id_grup_proyek="1">Al Akutsar Moslem City</option> -->
                  <?foreach($dd_perumahan as $id_grup_proyek => $nama_proyek):?>
                  <option value="<?=$id_grup_proyek?>"><?=$nama_proyek?></option>
                  <?endforeach?>
                </select>
            </div>  
            <div class="col-md-3">
              <button class="btn btn-circle btn-success"><i class="fa fa-file-excel-o"></i> Expor Excel</button>
            </div>
            </div> 
          </div>
        </div>
      </div>        
    </form>
  </div>
</div>

<script type="text/javascript">
  function slugify(str,limitWordCount) {
  if(typeof str=='undefined')
    return;
    str = str.replace(/^\s+|\s+$/g, ''); // trim
    str = str.toLowerCase();
    
    // remove accents, swap ñ for n, etc
    var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
    var to   = "aaaaeeeeiiiioooouuuunc------";
    for (var i=0, l=from.length ; i<l ; i++) {
      str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
    }
  
    str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
      .replace(/\s+/g, '-') // collapse whitespace and replace by -
      .replace(/-+/g, '-'); // collapse dashes
    if(typeof limitWordCount != 'undefined'){
      const words = str.split('-');
      let slugs = [];
      if(limitWordCount > words.length){
        limitWordCount = words.length;
      }
      for(let i = 0 ; i < limitWordCount;i++){
        slugs.push(words[i]);
      }
      return slugs.join('-')
    }
    return str;
  }
  toMysqlDateStr = (id_date_str) =>{
    // dd/mm/yyyy
    let tmp_str = id_date_str.split('/');
    const [dd,mm,yyyy] = tmp_str;
    
    return `${yyyy}-${mm}-${dd}`;
  };
  getMysqlDate = (inputSelector) =>{
    let value = $(inputSelector).val();
    return toMysqlDateStr(value);
  };

  getFilterParameter = () =>{
    let dt_min = getMysqlDate('input[name=dt_min]');
    let dt_max = getMysqlDate('input[name=dt_max]');
    let id_grup_proyek = $(`select#pilih_id_grup_proyek`).val();

    console.log(dt_min,dt_max,id_grup_proyek);
  };
  $(document).ready(()=>{
    setTimeout(()=>{
      let id_grup_proyek = '<?=$selected_id_grup_proyek?>';
      if(id_grup_proyek != ''){
        return;
      
      $(`select#pilih_id_grup_proyek > option[value=${id_grup_proyek}]`).attr('selected',true)
      console.log(id_grup_proyek)
      }// select id grup proyek handler
      $(`select#pilih_id_grup_proyek`).change(()=>{
        // console.log(this.value)
        const id_grup_proyek = $(`select#pilih_id_grup_proyek`).val();
        let opt = `select#pilih_id_grup_proyek > option[value=${id_grup_proyek}]`;
        let slug = slugify($(opt).text());
        let url = `dashboard?id_grup_proyek=${id_grup_proyek}&grup_proyek=${slug}`
        
        setTimeout(()=>{ document.location.href = url},1000);
      });

      // set dt_min/max value

      var today = new Date();
      var dd = today.getDate();
      var mm = today.getMonth()+1; //January is 0!
      var yyyy = today.getFullYear();
       if(dd<10){
              dd='0'+dd
          } 
          if(mm<10){
              mm='0'+mm
          } 

      today = dd+'/'+mm+'/'+yyyy;
      var d = new Date(yyyy, 12, 0);
      $('input[name=dt_min]').val(`01/01/${yyyy}`);
      $('input[name=dt_max]').val(`${d.getDate()}/12/${yyyy}`);

      // $(`select#pilih_id_grup_proyek`).change()
      getFilterParameter();
    },1000);
  });
</script>