<?php  defined('BASEPATH') OR exit('No direct script access allowed');
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Exception\ParseException;
class Data extends Theme_Controller 
{
    public function dd_no_rekening()
    {
        $list = $this->db->select('rek.id_rekening_bank pk,bank.nama_bank,bank.kode_bank,rek.no_rekening,rek.atas_nama,rek.cabang')
                         ->where('rek.status','AKTIF')
                         ->join('tb_daftar_bank bank','bank.id_bank=rek.id_bank','left')   
                         ->get('tb_rekening_bank rek')->result_array();
        $dd = [['label'=>'-- Pilih Nomor Rekening --','value'=>'']];

        foreach ($list as $row) {
            $info_rekening = sprintf("%s Cabang %s (%s a.n %s)",$row['nama_bank'],$row['cabang'],$row['no_rekening'],$row['atas_nama']);
            $dd[] = ['label'=>$info_rekening,'value'=>$row['pk']];
        }

        echo json_encode(['data'=>$dd, 'success'=>true]);
    }
	public function dd_propinsi()
    {
    	$list = $this->db->get('tb_propinsi')->result_array();
    	$dd = [['label'=>'-- Pilih Propinsi --','value'=>'']];

    	foreach ($list as $row) {
    		$dd[] = ['label'=>$row['nama_propinsi'],'value'=>$row['id_propinsi']];
    	}

    	echo json_encode(['data'=>$dd, 'success'=>true]);
    }
    public function dd_kabupaten_kota()
    {
    	$id_prop = $this->input->post('parent_id');

    	$list = $this->db->where('id_propinsi',$id_prop)->get('tb_kota')->result_array();
    	$dd = [['label'=>'-- Pilih Kota/Kabupaten --','value'=>'']];

    	foreach ($list as $row) {
    		$dd[] = ['label'=>$row['nama_kota'],'value'=>$row['id_kota']];
    	}

    	echo json_encode(['data'=>$dd, 'success'=>true]);
    }
	public function dd_kecamatan()
    {
    	$id_kota_kab = $this->input->post('parent_id');

    	$list = $this->db->where('id_kota',$id_kota_kab)->get('tb_kecamatan')->result_array();
    	$dd = [['label'=>'-- Pilih Kecamatan --','value'=>'']];

    	foreach ($list as $row) {
    		$dd[] = ['label'=>$row['nama_kecamatan'],'value'=>$row['id_kecamatan']];
    	}

    	echo json_encode(['data'=>$dd, 'success'=>true]);
    }
    
    public function dd_kelurahan()
    {
    	$id_kec = $this->input->post('parent_id');
 

    	$list = $this->db->where('id_kecamatan',$id_kec)->get('tb_kelurahan')->result_array();
    	$dd = [['label'=>'-- Pilih Kelurahan --','value'=>'']];

    	foreach ($list as $row) {
    		$dd[] = ['label'=>$row['nama_kelurahan'],'value'=>$row['id_kelurahan']];
    	}

    	echo json_encode(['data'=>$dd, 'success'=>true]);
    }

    public function dd_dok_legal()
    {
        $list = $this->db->where('status','AKTIF')->get('tb_dok_legal')->result_array();
        $dd = [['label'=>'-- Pilih Dokumen --','value'=>'']];

        foreach ($list as $row) {
            $dd[] = ['label'=>$row['keterangan'],'value'=>$row['nama_dokumen']];
        }

        echo json_encode(['data'=>$dd, 'success'=>true]);
    }

    public function dd_unit_perumahan($id_grup_proyek="")
    {
        $list = $this->db->where('status','1')->where('id_grup_proyek',$id_grup_proyek)->get('tb_unit_perumahan')->result_array();
        $dd = [['label'=>'-- Pilih Unit --','value'=>'']];

        foreach ($list as $row) {
            $dd[] = ['label'=>'Blok '.$row['nama_blok_kavling'].'/No. '.$row['nomor'],'value'=> $row['id_unit']];
        }

        echo json_encode(['data'=>$dd, 'success'=>true]);
    }
    public function dd_grup_proyek()
    {
        $list = $this->db->get('tb_grup_proyek')->result_array();
        $dd = [['label'=>'-- Pilih Perumahan --','value'=>'']];

        foreach ($list as $row) {
            $dd[] = ['label'=>$row['nama_proyek'],'value'=>$row['id_grup_proyek']];
        }

        echo json_encode(['data'=>$dd, 'success'=>true]);
    }
    public function ac_konsumen()
    {
        $param  = $this->input->get('param'); 
        // $field = str_replace('ttd_', '', $param) ;
        $query = $this->input->get('term');
        $this->load->model('app/m_konsumen');
        $data = $this->m_konsumen->get_ac($query,$param);

        echo json_encode($data);
    }
    function detail_unit_perumahan($id_unit=''){
        if(empty($id_unit)){
            echo json_encode(['data'=>[], 'success'=>true]);

            return ;
        }
        $this->load->model('pengaturan/m_unit_perumahan');
        $detail = $this->m_unit_perumahan->get_detail($id_unit);
        echo json_encode(['data'=>$detail, 'success'=>true]);

    }
}