<?php

class Upload extends CI_Controller
{
	protected $_valid_auth = false;
	function __construct()
	{
		parent::__construct();

		$api_key 	= $this->input->post('X-API-KEY');
		$api_app_id = $this->input->post('X-APP-ID');
		
		$valid_api_keys = $this->db->where('key',$api_key)
								   ->where('app_id',$api_app_id)
								   ->get('api_keys')
								   ->num_rows() > 0;

		if($valid_api_keys){
			$this->_valid_auth = true;
		}
	}

	public function photoProfile()
	{
		$owner  = $this->input->post('owner');
		$rules  = $this->input->post('rules');

		$config['upload_path'] = BASE.'/uploads/avatar';
		$config['allowed_types'] = 'jpg|jpeg|png|JPG|JPEG|PNG';
		$this->load->library('upload', $config);
    	
    	$field_name = "file";
        
        $response['success'] = false;	
        $response['msg'] = 'Upload Success';	

        $response['owner'] = $owner;	
        // $response['log'] = $_POST;

        $uploaded_file = json_decode( $_POST['uploaded_file'] );
        $uploaded_file->file_path = 'uploads/avatar/'.$uploaded_file->file_name;
        $uploaded_file->full_path = 'uploads/avatar/'.$uploaded_file->file_name;

        $response['uploaded_file'] = $uploaded_file;
        // $response['file_path'] = 'uploads/avatar/'.$uploaded_file->file_name;
        // $file_path = 'uploads/avatar/'.$uploaded_file['file_name'];

        file_put_contents($uploaded_file->file_path.'', base64_decode($_POST['file']));
		if(true)
		{
			 

			// DELETE prev

			$row = $this->db->where('owner',$owner)
							->where('rules',$rules)
							->get('uploads')->row();

			if(is_object($row)){
				@unlink(BASE .'/'. $row->file_path);
				$this->db->where('owner',$owner)
						 ->where('rules',$rules)
						 ->delete('uploads');
			}
			$upload = [
				'rules' => $rules,
				'desc'=>$rules,
				'owner' => $owner,
				'user_id' => $owner,
				'file_name' => $uploaded_file->file_name,
				'file_type' => $uploaded_file->file_type,
				'file_ext' => $uploaded_file->file_ext,
				'is_image' => 1,
				'file_size' => $uploaded_file->file_size,
				'image_width'=>$uploaded_file->image_width,
				'image_height'=>$uploaded_file->image_height,
				'creation_date'=> date('Y-m-d H:i:s'),
				'file_path' => $uploaded_file->full_path
			];
			$this->db->insert('uploads',$upload);

			$upload['id'] = $this->db->insert_id();
			$response['upload'] = $upload;
        	$response['success'] = true;	

		}else{
			$response['msg'] =  $this->upload->display_errors() ;

		}

		echo json_encode($response);
	}
	public function userPhoto()
	{
		$owner  = $this->input->post('owner');
		$rules  = $this->input->post('rules');
		$field_name  = $this->input->post('field_name');

		$config['upload_path'] = BASE.'/uploads';
		$config['allowed_types'] = 'jpg|jpeg|png|JPG|JPEG|PNG';
		$this->load->library('upload', $config);
    	
    	$field_name = "file";
        
        $response['success'] = false;	
        $response['msg'] = 'Upload Success';	

        $response['owner'] = $owner;	
        // $response['log'] = $_POST;

        $uploaded_file = json_decode( $_POST['uploaded_file'] );
        $uploaded_file->file_path = 'uploads/avatar/'.$uploaded_file->file_name;
        $uploaded_file->full_path = 'uploads/avatar/'.$uploaded_file->file_name;

        $response['uploaded_file'] = $uploaded_file;
        // $response['file_path'] = 'uploads/avatar/'.$uploaded_file->file_name;
        // $file_path = 'uploads/avatar/'.$uploaded_file['file_name'];

        file_put_contents($uploaded_file->file_path.'', base64_decode($_POST['file']));
		if(true)
		{
			 

			// DELETE prev

			$row = $this->db->where('owner',$owner)
							->where('rules',$rules)
							->get('uploads')->row();

			if(is_object($row)){
				@unlink(BASE .'/'. $row->file_path);
				$this->db->where('owner',$owner)
						 ->where('rules',$rules)
						 ->delete('uploads');
			}
			$upload = [
				'rules' => $rules,
				'desc'=>$rules,
				'owner' => $owner,
				'user_id' => $owner,
				'file_name' => $uploaded_file->file_name,
				'file_type' => $uploaded_file->file_type,
				'file_ext' => $uploaded_file->file_ext,
				'is_image' => 1,
				'file_size' => $uploaded_file->file_size,
				'image_width'=>$uploaded_file->image_width,
				'image_height'=>$uploaded_file->image_height,
				'creation_date'=> date('Y-m-d H:i:s'),
				'file_path' => $uploaded_file->full_path
			];
			$this->db->insert('uploads',$upload);

			$upload['id'] = $this->db->insert_id();
			$response['upload'] = $upload;
        	$response['success'] = true;	

		}else{
			$response['msg'] =  $this->upload->display_errors() ;

		}

		echo json_encode($response);
	}

	public function photoPelanggan(){
		$id_foto 	= $this->input->post('id_foto');
		$key 		= $this->input->post('key');
		$filename 	= $this->input->post('filename');
		$content    = $this->input->post('content');

		$file_buffer= base64_decode($content);
		$ext        = array_pop(explode('.', $filename));
		$filename   = str_replace('.'.$ext, '', $file_name);
		$filename   = slugify($filename, '_');
		$filename   = md5(microtime()).'_'.$filename.'.'.$ext;

		$file_path  = BASE . '/uploads/pelanggan/' . $filename;
		
		if(!file_exists($file_path)){
			file_put_contents($file_path, $file_buffer);
			$this->db->where('id', $id_foto)
					 ->update('m_foto_pelanggan', [$key => $filename]);
		} 

		echo json_encode([$key => $filename]);
	}
}