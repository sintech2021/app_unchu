<?php
require_once APPPATH . '/models/Grocery_CRUD_Model.php';
class M_tanda_jadi  extends Grocery_CRUD_Model  {
    var $table = 'pmb_tanda_jadi'; 
	var $table_name = 'pmb_tanda_jadi';
    function create_from_tr($tr,$pk){
        $record = $this->db->where('id_tr_sppr',$pk)
                          ->get($this->table)
                          ->row();
        $record_exists = is_object($record);
        if(!$record_exists){
            // create new
            $new_row = [
                'id_tr_sppr' => $pk,
                'jml_dana'   => $tr['tanda_jadi'],
                'tgl_entry'  => date('Y-m-d H:i:s'),
                'tgl_update' => date('Y-m-d H:i:s'),
            ];
            $this->db->insert($this->table,$new_row);    
        }else{
            $row = [
                'jml_dana'   => $tr['tanda_jadi'],
                'tgl_update' => date('Y-m-d H:i:s'),
            ];
            // update if not lunas
            if($record->status_pmb != 'lunas'){
                $this->db->where('id', $record->id)
                         ->update($this->table, $row);
            }
        }
        
    }
    function get_edit_values($pk){
        return $this->db->select("a.*,
                           tr.tanda_jadi tr_tanda_jadi, 
                           k.nama_lengkap nama_konsumen,
                           k.identitas nik,
                           k.no_hp nomor_hp,
                           k.email,
                           gp.nama_proyek perumahan,
                           u.nomor unit_nomor,
                           u.nama_blok_kavling unit_blok_kavling,
                           db.nama_bank")
                 ->join('tb_rekening_bank rb','rb.id_rekening_bank=a.bank_tujuan','left')
                 ->join('tb_daftar_bank db','db.id_bank=rb.id_bank','left')
                 ->join('tr_sppr tr','tr.id=a.id_tr_sppr','left')
                 ->join('tb_unit_perumahan u','u.id_unit=tr.id_unit','left')
                 ->join('tb_grup_proyek gp','gp.id_grup_proyek=tr.id_grup_proyek','left')
                 ->join('tb_data_konsumen k','k.id_data_konsumen=tr.id_konsumen','left')
                 ->where('a.id',$pk)->from($this->table . ' a')->get()->row();
    } 
    private function _get_datatables_query()
    {
        $this->db->select("a.*,
                           tr.tanda_jadi tr_tanda_jadi,
                           k.nama_lengkap nama_konsumen,
                           k.identitas nik,
                           k.no_hp nomor_hp,
                           k.email,
                           gp.nama_proyek perumahan,
                           u.nomor unit_nomor,
                           u.nama_blok_kavling unit_blok_kavling,
                           db.nama_bank")
                 ->join('tb_rekening_bank rb','rb.id_rekening_bank=a.bank_tujuan','left')
                 ->join('tb_daftar_bank db','db.id_bank=rb.id_bank','left')
                 ->join('tr_sppr tr','tr.id=a.id_tr_sppr','left')
                 ->join('tb_unit_perumahan u','u.id_unit=tr.id_unit','left')
                 ->join('tb_grup_proyek gp','gp.id_grup_proyek=tr.id_grup_proyek','left')
                 ->join('tb_data_konsumen k','k.id_data_konsumen=tr.id_konsumen','left')
                 ->from($this->table . ' a');
        $i = 0;
        $search_queries = $_POST['search']['value'];
        $search_queries = explode(',', $search_queries);
        if(count($search_queries) > 0){
            foreach ($this->column_search as $item) // looping awal
            {
                if($_POST['search']['value']) // jika datatable mengirimkan pencarian dengan metode POST
                {
                    if($i===0) // looping awal
                    {
                        $this->db->group_start(); 
                        $this->db->like($item, $_POST['search']['value']);
                    }
                    else
                    {
                        $this->db->or_like($item, $_POST['search']['value']);
                    }
                    if(count($this->column_search) - 1 == $i) 
                        $this->db->group_end(); 
                }
                $i++;
            }
        }
        if(isset($_POST['order'])) 
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
}
