<?php  defined('BASEPATH') OR exit('No direct script access allowed');
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Exception\ParseException;
class Tanda_jadi extends Theme_Controller {
    public $_page_title = 'Pembayaran Tanda Jadi';
    public function custom_grid_data(){
            
        $this->load->model('account/m_login');
        $this->load->model('m_tanda_jadi','model');
        $list = $this->model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $belum_bayar = $field->status_pmb != 'lunas';
            $lunas = !$belum_bayar;
            $action = "<td class=\"actions\"> 
            <div class='btn-group'>". ($belum_bayar?" 
            <button href=\"".site_url('pembayaran/tanda_jadi/index/edit/'.$field->id)."/".slugify($field->unit_blok_kavling)."\" class=\"gc-bt-edit edit_button btn btn-sm btn-icon btn-pure btn-info\" role=\"button\"> <i class=\"fa fa-money\" aria-hidden=\"true\"></i> 
            </button> ":""). ($lunas?" 
            <button href=\"".site_url('pembayaran/tanda_jadi/index/edit/'.$field->id)."/".slugify($field->unit_blok_kavling)."\" class=\"gc-bt-edit edit_button btn btn-sm btn-icon btn-pure btn-info\" role=\"button\"> <i class=\"fa fa-edit\" aria-hidden=\"true\"></i> 
            </button> ":""). ($lunas?" 
            <button href=\"".site_url('pembayaran/tanda_jadi/print/'.$field->id)."/".slugify($field->unit_blok_kavling)."\" class=\"gc-bt-edit btn btn-sm btn-icon btn-pure btn-info\" role=\"button\"> <i class=\"fa fa-print\" aria-hidden=\"true\"></i> 
            </button> ":"")."
            
            </div>
            </td>";
            $no++;
            $row = array();
            
            $row[] = $no;
            $row[] = $field->nama_konsumen;
            minus_if_empty($field,'metode_pmb');
            minus_if_empty($field,'nama_bank');
            $row[] = $this->load->view('_booking/row_data_konsumen',$field,true);
            $row[] = $this->load->view('_booking/row_perumahan',$field,true);
            $row[] = $this->load->view('_booking/row_booking',$field,true);
            $row[] = $this->load->view('_booking/row_realisasi_pmb',$field,true);

            $row[] = $action; 
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->model->count_all(),
            "recordsFiltered" => $this->model->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }
    public function custom_grid_ajax()
    {
        $tdata = ['add_url' => site_url('pembayaran/tanda_jadi/index/add')];
        $tdata['is_admin'] = $this->cms_user_group() == 'admin';
        $this->load->view('_pembayaran/dt_tanda_jadi.php',$tdata);
    }
    private function _customGrid(){
        $dump = file_get_contents(APP . '/config/gc.yml');
        $data = Yaml::parse($dump);
        $tdata = ['add_url' => site_url('pembayaran/tanda_jadi/index/add')];
        $data['output'] = $this->load->view('_pembayaran/dt_tanda_jadi.php',$tdata,true);
        $data['unique_hash'] = md5(date('YmdHis-Unit'));
        $data['is_admin']  = $this->cms_user_group()=='admin';
        $this->view('_pembayaran/tanda_jadi.php', $data );
    }
    public function index()
    {
        $target_yaml = dirname(__FILE__) . '/../config/form.yml';
        $buffer      = file_get_contents($target_yaml);
        $conf        = Yaml::parse($buffer);
        $crud        = $this->new_crud($conf); 

        $_SERVER['FORM_DEF'] = $conf;
        $crud->set_subject('Pembayaran Tanda Jadi');
        $state = $crud->getState();
        switch ($state) {
            case 'list':
                $args = func_get_args();
                return call_user_func_array([$this,'_customGrid'], $args);
                break;
            case 'add':
                die('ERR_ACCESS_DENIED');    
                break;  
            default:
                # code...
                break;
        }
        $metode_pmb = $this->session->userdata('set_metode_pmb_sess');
        if($metode_pmb == 'transfer'){
            $crud->set_rules('upload_bukti_transfer','Upload Bukti Transfer','required');
        }
        $crud->unset_jquery();
        $crud->unset_export();
        $crud->unset_add();
        if (! $this->input->is_ajax_request()) {
            $crud->set_theme_config(['crud_paging' => true ]);
        }
        $crud->fields('id_tr_sppr','tgl_realisasi','metode_pmb','jml_dana','bank_tujuan','upload_bukti_transfer','status_pmb','tgl_update');
        $this->load->model('pengaturan/m_bank');
        $dd_bank = $this->m_bank->get_dd_array();
        // print_r($dd_bank);
        $crud->field_type('bank_tujuan','dropdown',$dd_bank);
		$crud->set_model('m_tanda_jadi');
		$crud->set_theme('datatables');
        $crud->field_type('status_pmb','hidden');
        $crud->field_type('tgl_entry','hidden');
        $crud->field_type('tgl_update','hidden');
        $crud->field_type('id_user','hidden');
        $crud->callback_before_insert(array($this,'_set_tgl_entry'));
        $crud->callback_before_update(array($this,'_set_tgl_update'));
    	$id_user = $this->cms_user_id();
		$state = $crud->getState();
    	$state_info = $crud->getStateInfo();
    	$id_user = $this->cms_user_id();
        $data = $crud->render();
		$data->is_admin  = $this->cms_user_group()=='admin';
		$this->view('_pembayaran/tanda_jadi.php',$data);
    }
    function _set_tgl_entry($post_array){
        $dt = date('Y-m-d H:i:s');
        $post_array['tgl_entry'] = $dt;
        $post_array['tgl_update'] = $dt;
        $post_array['id_user'] = $this->cms_user_id();
        $post_array['jumlah_dana'] = rupiah_to_int($post_array['jumlah_dana']);
        unset($post_array['jml_dana']);

        return $post_array;
    }
    function _set_tgl_update($post_array,$pk){
        $dt = date('Y-m-d H:i:s');
        $post_array['tgl_update'] = $dt;
        $post_array['status_pmb'] = 'lunas';
        // $post_array['jumlah_dana'] = rupiah_to_int($post_array['jumlah_dana']);
        unset($post_array['jml_dana']);

        return $post_array;
    } 

    function set_metode_pmb_sess(){
        $mode = $this->input->post('mode');

        $this->session->set_userdata('set_metode_pmb_sess',$mode);
        echo json_encode(['success'=>true]);
    }
}
