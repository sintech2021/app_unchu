<div class="customDTContainer">
<script type="text/javascript">
    var dialog_forms = '1';
</script>
<div id="modalForm"></div>
<script type="text/javascript">
    var base_url = '{{ base_url }}';
    var subject = 'Pembayaran Tanda Jadi';
    var unique_hash = '<?= $unique_hash ?>';
    var displaying_paging_string = "Halaman _START_ - _END_ dari _TOTAL_ total";
    var filtered_from_string    = "(filtered from _MAX_ total entries)";
    var show_entries_string     = "Tampil _MENU_";
    var search_string           = "Cari";
    var list_no_items           = "Tidak ada item";
    var list_zero_entries       = "Ditampilkan 0 - 0 dari 0 item";
    var list_loading            = "Mohon Tunggu";
    var paging_first    = "Awal";
    var paging_previous = '<i class="icon wb-chevron-left-mini"></i>';
    var paging_next     = '<i class="icon wb-chevron-right-mini"></i>';
    var paging_last     = "Terakhir";
    var message_alert_delete = "Apakah anda yakin ingin menghapus data?";
    var default_per_page = 10;
    var unset_export = false;
    var unset_print = false;
    var export_text = 'Ekspor';
    var print_text = 'Cetak';
    var datatables_aaSorting = [[ 0, "asc" ]];
</script>
<div id="alert_cnt" style="padding: 2px;"></div>
<div id='list-report-error' class='report-div error report-list'></div>
<div id='list-report-success' class='report-div success report-list' <?php if ($success_message !== null) {
    ?>style="display:block"<?php
}?>><?php
if ($success_message !== null) {?>
    <p><?php echo $success_message; ?></p>
<?php }
?></div>

<div style='height:10px;'></div>
<style type="text/css">
    button.refresh-data,button#successMsg {
        position: absolute;
        margin: -100000px;
        z-index: -1;
    }
    .datatables-pager{
        display: block;
    }
    .dataTables_length{
        float: left;
        width: 200px;
        display: block;
    }
    #alert_cnt p{
        display: inline;
    }
</style>
<div class="dataTablesContainer">
   <table id="tanda_jadi_table" class="table table-bordered table-hover table-striped groceryCrudTable">
        <thead>
            <tr>
                <tr>
	<th class="no" >No</th>
    <th class="nama-konsumen" >Nama</th>
	<th class="data-konsumen" >Data Konsumen</th>
	<th class="perumhan" >Perumahan</th>
	<th class="tanda-jadi" >Tanda Jadi</th>
	<th class="realisasi-pembayaran" >Realisasi Pembayaran</th>

                <th class="actions">Aksi</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    <button data-url="{{ base_url }}pembayaran/tanda_jadi/custom_grid_ajax" class="btn btn-primary refresh-data"><i class="icon wb-refresh"></i>Refresh</button>
<button style="opacity: 0" type="button" class="btn btn-primary" id="successMsg" data-plugin="alertify" data-type="log" data-delay="10000" data-log-message="Berhasil dengan sukses">Save Success</button>
<script type="text/javascript">
    $(document).ready(function(){
        $('.dataTables_length').parent().addClass('datatables-pager');
    });
</script>
<script type="text/javascript">
    var default_javascript_path = '{{ base_url }}public/assets/gc/js';
    var default_css_path = '{{ base_url }}public/assets/gc/css';
    var default_texteditor_path = '{{ base_url }}public/assets/gc/texteditor';
    var default_theme_path = '{{ base_url }}public/assets/gc/themes';
    var base_url = '{{ base_url }}';
</script>
<script type="text/javascript">
    var default_javascript_path = '{{ base_url }}public/assets/gc/js';
    var default_css_path = '{{ base_url }}public/assets/gc/css';
    var default_texteditor_path = '{{ base_url }}public/assets/gc/texteditor';
    var default_theme_path = '{{ base_url }}public/assets/gc/themes';
    var base_url = '{{ base_url }}';
</script>
<!-- <script type="text/javascript" src="{{ base_url }}public/assets/gc/js/jquery_plugins/jquery.noty.js"></script>
<script type="text/javascript" src="{{ base_url }}public/assets/gc/js/jquery_plugins/config/jquery.noty.config.js"></script><script type="text/javascript" src="{{ base_url }}public/assets/gc/js/common/lazyload-min.js"></script>
<script type="text/javascript" src="{{ base_url }}public/assets/gc/js/common/list.js"></script>
<script type="text/javascript" src="{{ base_url }}public/assets/gc/js/jquery_plugins/jquery.fancybox-1.3.4.js"></script>
<script type="text/javascript" src="{{ base_url }}public/assets/gc/js/jquery_plugins/jquery.easing-1.3.pack.js"></script>
<script type="text/javascript" src="{{ base_url }}public/assets/gc/themes/datatables/js/datatables.js"></script>  --> 
    </div>
<script type="text/javascript">
    var table;
    var DONT_INIT_DT = true;
    $(document).ready(function() {
        //datatables
        table = $('#tanda_jadi_table').DataTable({ 
            "ordering": false,
            "processing": true, 
            "serverSide": true, 
            "order": [], 
            "ajax": {
                "url": "<?=site_url('pembayaran/tanda_jadi/custom_grid_data')?>",
                "type": "POST"
            },
            "columnDefs": [
            { 
                "targets": [ 0 ], 
                "orderable": false, 
            },
            ],
        responsive: 0,
            
        "iDisplayLength": default_per_page,
        "oLanguage":{
            "sProcessing":   list_loading,
            "sLengthMenu":   show_entries_string,
            "sZeroRecords":  list_no_items,
            "sInfo":         displaying_paging_string,
            "sInfoEmpty":   list_zero_entries,
            "sInfoFiltered": filtered_from_string,
            "sSearch":       search_string+":",
            "oPaginate": {
                "sFirst":    '',
                "sPrevious": '<',
                "sNext":     '>',
                "sLast":     ''
            }
        },
        "bDestory": true,
        "bRetrieve": true,
        "fnDrawCallback": function() {
            // $('.image-thumbnail').fancybox({
            //     'transitionIn'  :   'elastic',
            //     'transitionOut' :   'elastic',
            //     'speedIn'       :   600,
            //     'speedOut'      :   200,
            //     'overlayShow'   :   false
            // });
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;
 
            api.column(1, {page:'current'} ).data().each( function ( group, i ) {
                if ( last !== group ) {
                    $(rows).eq( i ).before(
                        '<tr class="group"><td colspan="7">'+group+'</td></tr>'
                    );
 
                    last = group;
                }
            } );
            add_edit_button_listener();
        },
        "sDom": 'T<"clear"><"H"lfr>t<"F"ip>',
        "oTableTools": {
            "aButtons": aButtons,
            "sSwfPath": base_url+"public/assets/gc/themes/datatables/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"
            }
        });
        loadListenersForDatatables();
    });
</script>
 <style type="text/css">
     .dataTablesContainer table tr td:first-child{
        text-align:right;
     }
     .dataTablesContainer table tr.group td:first-child{
        text-align:left;
        font-weight: bold;
     }
     .dataTablesContainer table tr th:first-child + th,
     .dataTablesContainer table tr td:first-child + td{
        display: none !important;
     }
     .dataTablesContainer table tr td:first-child + td + td + td + td + td + td{
        text-align: center;
     }
      button.refresh-data,button#successMsg {
        position: absolute;
        margin: -100000px;
        z-index: -1;
    }
    .datatables-pager{
        display: block;
        /*width: 200px;
        float: left;*/
    }
    .dataTables_length{
        float: left;
        width: 200px;
        display: block;
    }
    #alert_cnt p{
        display: inline;
    }
    td.status-pmb{font-style: italic;font-weight: bold;}
    td.status-pmb.lunas{color: green}
    td.status-pmb.belum_lunas{color: red}
    table.tbl-detail{
        width: 100%;
    }
    table.tbl-detail td{
        background-color: transparent !important;
        padding: 0;
        text-align: left !important;
    } 
    .table td, .table th {
        font-size: 12px;
    }
 </style>
 </div>
<script type="text/javascript">
$(document).ready(()=>{
   const onCurrencyChanged = (el)=>{
        const new_val = formatRupiah(el.value);
        el.value = formatRupiah(el.value);
   }; 
   const attachCurrencyField = (field_name)=>{
        const nd_currency = $(`#field-${field_name}`);
        nd_currency.attr('placeholder','Rp. 0');
        nd_currency.attr('maxlength','');
        nd_currency.change(function(){
          onCurrencyChanged(this);
        });
        nd_currency.keyup(function(){
          onCurrencyChanged(this);
        });

        nd_currency.change();
   }; 
   gc.__onShowForm = (form,state,ajax_url,e) =>{
        attachCurrencyField('jml_dana');
        $('#field-jml_dana').attr('disabled',1);
        $('#field-metode_pmb').change(function(){
            const val = $(this).val();
            console.log(val);
            $('span[x_field_name=upload_bukti_transfer]')
                .closest('.form-group')[val=='transfer'?'show':'hide']();
            if(val.length > 0){
                $.post(site_url()+`pembayaran/booking/set_metode_pmb_sess`,{mode:val},(r)=>{

                },'json');
            }
        }).change();
   };
    
});
</script>