<?
// print_r($crud);
// exit();
$field_upload_kvs= [
    'pass_foto' => 'uploads/tb_data_konsumen_pass_foto',
    'doc_identitas' => 'uploads/tb_data_konsumen_doc_identitas',
]; 

foreach ($field_upload_kvs as $field_name => $upload_path) {
    $fo = new stdClass();
    $fo->name = $field_name;
    $fo->extras = new stdClass();
    $fo->extras->upload_path = $upload_path;
    $var_name = "upload_input_{$field_name}";
    $$var_name = $crud->call_protected_method('get_upload_file_input',$fo,'');
}


$views_as_string =  $crud->call_protected_method('get_views_as_string');

$field_upload_kvs= [
    'pass_foto_ps' => 'uploads/tb_data_pasangan_konsumen_pass_foto',
    'doc_identitas_ps' => 'uploads/tb_data_pasangan_konsumen_doc_identitas',
]; 

foreach ($field_upload_kvs as $field_name => $upload_path) {
    $fo = new stdClass();
    $fo->name = $field_name;
    $fo->extras = new stdClass();
    $fo->extras->upload_path = $upload_path;
    $var_name = "upload_input_{$field_name}";
    $$var_name = $crud_ps->call_protected_method('get_upload_file_input',$fo,'');
}


$views_as_string .=  $crud_ps->call_protected_method('get_views_as_string');
echo $views_as_string;
?>
<?php 
foreach($crud->get_protected_property_value('css_files') as $file): ?>
    <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<?php foreach($crud->get_protected_property_value('js_files') as $file): ?>
    <?if(!preg_match('/jquery\.fileupload\.config\.js/', $file)):?>
    <script src="<?php echo $file; ?>"></script>
    <?endif?>
<?php endforeach; ?>
<section id="form_add_konsumen" style="display: none;">
 <div class="form-konsumen">
    <div class="content-tab">
        
        <div class="tab-content">
            <div class="tab-pane active" id="tab_grup_proyek">
                <h4>Tambah Data Konsumen</h4>
                <div class="wizard">
                    <div class="wizard-inner">
                        <div class="connecting-line"></div>
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Data Pemohon">
                                    <span class="round-tab">
                                        <i class="fa fa-th"></i>
                                    </span>
                                </a>
                            </li>
                            <li role="presentation" class="disabled">
                                <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Data Pekerjaan & Penghasilan">
                                    <span class="round-tab">
                                        <i class="fa fa-file"></i>
                                    </span>
                                </a>
                            </li>
                            <li role="presentation" class="disabled">
                                <a href="#step3" data-toggle="tab" aria-controls="step3" role="tab" title="Data Pinjaman & Aset/Kekayaan">
                                    <span class="round-tab">
                                        <i class="fa fa-tasks"></i>
                                    </span>
                                </a>
                            </li>
                            <li role="presentation" class="disabled">
                                <a href="#complete" data-toggle="tab" aria-controls="complete" role="tab" title="Selesai">
                                    <span class="round-tab">
                                        <i class="icon-check"></i>
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    
                    <div id="form_items">
                    	<form role="form" id="form_konsumen" enctype="multipart/form-data" action="{{ site_url }}app/konsumen/insert">
	                   		<div class="tab-content">
	                   			 <div class="tab-pane active" role="tabpanel" id="step1">
	                   			 	<div class="form-body">
                                        <h3 class="form-section" style="padding: .5em;padding-left: .6em">Data Pribadi Pemohon</h3>
                                    	<div class="row">
                                    		<div class="col-md-6">
                                    			<div class="form-group" :class="{'has-error':verror.nama_lengkap}">
                                    				<label class="control-label">Nama Lengkap</label>
                                    				<input type="text" name="nama_lengkap" v-model="nama_lengkap" class="form-control" placeholder="Nama Lengkap" />
                                    				<span class="help-block" v-if="verror.nama_lengkap"> {{verror.nama_lengkap}} </span>
                                    			
                                    			</div>
                                    		</div>
                                    		<div class="col-md-6">
                                    			<div class="form-group" :class="{'has-error':verror.identitas}">
                                    				<label class="control-label">Identitas</label>
                                    				<div class="input-group">
                                    				<span class="input-group-addon" style="padding: 0;border: none">
                                    					<select name="jenis_identitas" v-model="jenis_identitas" class="form-control" style="width: 130px;margin-right: 4px;">
                                    						<option value="">-- PILIH --</option>
                                    						<option value="KTP">KTP</option>
                                    						<option value="SIM">SIM</option>
                                    						<option value="PASSPORT">PASSPOR</option>
                                    					</select>
                                    					</span><input type="text" name="identitas"  v-model="identitas" class="form-control" placeholder="Identitas" />
                                    				
                                    				</div>
                                    				<span class="help-block" v-if="verror.identitas"> {{verror.identitas}} </span>
                                    			
                                    			</div>
                                    			
                                    		</div>
                                    	</div>
                                    	<div class="row">
                                    		<div class="col-md-6">
                                    			<div class="form-group" :class="{'has-error':verror.tempat_lahir}">
                                    				<label class="control-label">Tempat Lahir</label>
                                    				<input type="text" name="tempat_lahir" v-model="tempat_lahir" class="form-control" placeholder="Tempat Lahir" />
                                    				<span class="help-block" v-if="verror.tempat_lahir"> {{verror.tempat_lahir}} </span>
                                    			
                                    			</div>
                                    		</div>
                                    		<div class="col-md-6">
                                    			<div class="form-group" :class="{'has-error':verror.tanggal_lahir}">
                                    				<label class="control-label">Tanggal Lahir</label>
                                    				<input type="text" name="tanggal_lahir" class="form-control datepicker-input" placeholder="Tanggal Lahir" />
                                    				<span class="help-block" v-if="verror.tanggal_lahir"> {{verror.tanggal_lahir}} </span>
                                    			</div>
                                    			
                                    		</div>
                                    	</div>
                                    	<div class="row">
                                    		<div class="col-md-6">
                                    			<div class="form-group" :class="{'has-error':verror.npwp}">
                                    				<label class="control-label">NPWP</label>
                                    				<input type="text" name="npwp" v-model="npwp" class="form-control" placeholder="NPWP" />
                                    				<span class="help-block" v-if="verror.npwp"> {{verror.npwp}} </span>
                                    			
                                    			</div>
                                    		</div>
                                    		<div class="col-md-6">
                                    			<div class="form-group" :class="{'has-error':verror.email}">
                                    				<label class="control-label">Email</label>
                                    				<input type="email" name="email" v-model="email" class="form-control" placeholder="Email" />
                                    				<span class="help-block" v-if="verror.email"> {{verror.email}} </span>
                                    			
                                    			</div>
                                    		</div>
                                    	</div>
                                    	<div class="row">
                                    		<div class="col-md-6">
                                    			<div class="form-group" :class="{'has-error':verror.jml_anak}">
                                    				<label class="control-label">Jumlah Anak</label>
                                    				<input type="number" name="jml_anak" v-model="jml_anak" class="form-control" placeholder="Jumlah Anak" />
                                    				<span class="help-block" v-if="verror.jml_anak"> {{verror.jml_anak}} </span>
                                    			
                                    			</div>
                                                <div class="form-group" :class="{'has-error':verror.status_pernikahan}">
                                                    <label class="control-label">Status Pernikahan</label>
                                                    <select name="status_pernikahan" v-model="status_pernikahan" class="form-control" placeholder="status_pernikahan">
                                                        <option value="">-- PILIH --</option>
                                                        <option value="MENIKAH">MENIKAH</option>
                                                        <option value="BELUM MENIKAH">BELUM MENIKAH</option>
                                                        <option value="JANDA">JANDA</option>
                                                        <option value="DUDA">DUDA</option>
                                                    </select>
                                                    <span class="help-block" v-if="verror.status_pernikahan"> {{verror.status_pernikahan}} </span>
                                                </div>
                                                <div class="form-group" :class="{'has-error':verror.pilih_alamat_penagihan}">
                                                    <label class="control-label">Pilihan Alamat Penagihan</label>
                                                    <select name="pilih_alamat_penagihan" v-model="pilih_alamat_penagihan" class="form-control">
                                                        <option value="">--- PILIH ---</option>
                                                        <option value="ya">Sama Dengan Alamat Rumah</option>
                                                        <option value="tidak">Tidak Sama Dengan Alamat Rumah</option>
                                                    </select>
                                                    <span class="help-block" v-if="verror.pilih_alamat_penagihan"> {{verror.pilih_alamat_penagihan}} </span>
                                                
                                                </div>
                                    		</div>
                                    		<div class="col-md-6">
                                    			<div class="form-group" :class="{'has-error':verror.no_hp}">
                                    				<label class="control-label">No. HP</label>
                                    				<input type="text" name="no_hp" v-model="no_hp" class="form-control" placeholder="No. HP 1" />
                                    				<span class="help-block" v-if="verror.no_hp"> {{verror.no_hp}} </span>
                                    			
                                    			</div>
                                                <div class="form-group" :class="{'has-error':verror.no_hp_2}">
                                                    <label class="control-label">No. HP 2</label>
                                                    <input type="text" name="no_hp_2" v-model="no_hp_2" class="form-control" placeholder="No. HP 2" />
                                                    <span class="help-block" v-if="verror.no_hp_2"> {{verror.no_hp_2}} </span>
                                                
                                                </div>
                                                 <div class="form-group" :class="{'has-error':verror.no_hp_3}">
                                                    <label class="control-label">No. HP 3</label>
                                                    <input type="text" name="no_hp_3" v-model="no_hp_3" class="form-control" placeholder="No. HP 2" />
                                                    <span class="help-block" v-if="verror.no_hp_3"> {{verror.no_hp_3}} </span>
                                                
                                                </div>
                                    		</div>
                                    	</div>

                                    	<div class="row">
                                    		
                                    		<div class="col-md-6">
                                    			
                                    		</div>
                                    		<div class="col-md-6">
                                    			
                                    		</div>
                                    	</div>

                                    	<div class="row">
                                    		
                                    		<div class="col-md-6">
                                    			<div class="form-group" :class="{'has-error':verror.alamat_rumah}">
                                    				<label class="control-label">Alamat Rumah</label>
                                    				<input type="text" name="alamat_rumah"  v-model="alamat_rumah" class="form-control" placeholder="Alamat Rumah" />
                                    				<span class="help-block" v-if="verror.alamat_rumah"> {{verror.alamat_rumah}} </span>
                                    			
                                    			</div>
                                    		</div>
                                    		<div class="col-md-6">
                                    			<div class="form-group" :class="{'has-error':verror.alamat_penagihan}">
                                    				<label class="control-label">Alamat Penagihan</label>
                                    				<input :disabled="pilih_alamat_penagihan=='ya'" type="text" name="alamat_penagihan" v-model="alamat_penagihan" class="form-control" placeholder="Alamat Penagihan" />
                                    				<span class="help-block" v-if="verror.alamat_penagihan"> {{verror.alamat_penagihan}} </span>
                                    			
                                    			</div>
                                    		</div>
                                    	</div>
                                    	<div class="row">
                                    		
                                    		<div class="col-md-6">
                                    			<div class="form-group" :class="{'has-error':verror.propinsi}">
                                    				<label class="control-label">Propinsi</label>
                                    				<select name="propinsi" v-model="propinsi" class="form-control" placeholder="Propinsi" @change="onSelectChange('propinsi')">
                                    					<option v-for="row in dd_propinsi" :value="row.value" v-text="row.label"></option>
                                    				</select>
                                    				<span class="help-block" v-if="verror.propinsi"> {{verror.propinsi}} </span>
                                    			
                                    			</div>
                                    		</div>
                                    		<div class="col-md-6">
                                    			<div class="form-group" :class="{'has-error':verror.propinsi_penagihan}">
                                    				<label class="control-label">Propinsi</label>
                                    				<select :disabled="pilih_alamat_penagihan=='ya'"  name="propinsi_penagihan" v-model="propinsi_penagihan" class="form-control" placeholder="Propinsi" @change="onSelectChange('propinsi_penagihan')">
                                    					<option v-for="row in dd_propinsi_penagihan" :value="row.value" v-text="row.label"></option>
                                    					
                                    				</select>
                                    				<span class="help-block" v-if="verror.propinsi_penagihan"> {{verror.propinsi_penagihan}} </span>
                                    			
                                    			</div>
                                    		</div>
                                    	</div>
                                    	<div class="row">
                                    		
                                    		<div class="col-md-6">
                                    			<div class="form-group" :class="{'has-error':verror.kota}">
                                    				<label class="control-label">Kota/Kabupaten</label>
                                    				<select name="kota" v-model="kota" class="form-control" placeholder="Kota" @change="onSelectChange('kota')">
                                    					<option v-for="row in dd_kota" :value="row.value" v-text="row.label"></option>
                                    				</select>
                                    				<span class="help-block" v-if="verror.kota"> {{verror.kota}} </span>
                                    			
                                    			</div>
                                    		</div>
                                    		<div class="col-md-6">
                                    			<div class="form-group" :class="{'has-error':verror.kota_penagihan}">
                                    				<label class="control-label">Kota/Kabupaten</label>
                                    				<select :disabled="pilih_alamat_penagihan=='ya'"  name="kota_penagihan" v-model="kota_penagihan" class="form-control" placeholder="Kota Penagihan" @change="onSelectChange('kota_penagihan')">
                                    					<option v-for="row in dd_kota_penagihan" :value="row.value" v-text="row.label"></option>
                                    				</select>
                                    				<span class="help-block" v-if="verror.kota_penagihan"> {{verror.kota_penagihan}} </span>
                                    			
                                    			</div>
                                    		</div>
                                    	</div>
                                    	<div class="row">
                                    		
                                    		<div class="col-md-6">
                                    			<div class="form-group" :class="{'has-error':verror.kecamatan}">
                                    				<label class="control-label">Kecamatan</label>
                                    				<select name="kecamatan" v-model="kecamatan" class="form-control" placeholder="Kecamatan" @change="onSelectChange('kecamatan')">
                                    					<option v-for="row in dd_kecamatan" :value="row.value" v-text="row.label"></option>
                                    				</select>
                                    				<span class="help-block" v-if="verror.kecamatan"> {{verror.kecamatan}} </span>
                                    			
                                    			</div>
                                    		</div>
                                    		<div class="col-md-6">
                                    			<div class="form-group" :class="{'has-error':verror.kecamatan_penagihan}">
                                    				<label class="control-label">Kecamatan</label>
                                    				<select :disabled="pilih_alamat_penagihan=='ya'"  name="kecamatan_penagihan" v-model="kecamatan_penagihan" class="form-control" placeholder="Kecamatan" @change="onSelectChange('kecamatan_penagihan')">
                                    					<option v-for="row in dd_kecamatan_penagihan" :value="row.value" v-text="row.label"></option>
                                    				</select>
                                    				<span class="help-block" v-if="verror.kecamatan_penagihan"> {{verror.kecamatan_penagihan}} </span>
                                    			
                                    			</div>
                                    		</div>
                                    	</div>
                                    	<div class="row">
                                    		
                                    		<div class="col-md-6">
                                    			<div class="form-group" :class="{'has-error':verror.kelurahan}">
                                    				<label class="control-label">Kelurahan</label>
                                    				<select  name="kelurahan" v-model="kelurahan" class="form-control" placeholder="Kelurahan">
                                    					<option v-for="row in dd_kelurahan" :value="row.value" v-text="row.label"></option>
                                    				</select>
                                    				<span class="help-block" v-if="verror.kelurahan"> {{verror.kelurahan}} </span>
                                    			
                                    			</div>
                                    		</div>
                                    		<div class="col-md-6">
                                    			<div class="form-group" :class="{'has-error':verror.kelurahan_penagihan}">
                                    				<label class="control-label">Kelurahan</label>
                                    				<select :disabled="pilih_alamat_penagihan=='ya'" name="kelurahan_penagihan" v-model="kelurahan_penagihan" class="form-control" placeholder="Kelurahan">
                                    					<option v-for="row in dd_kelurahan_penagihan" :value="row.value" v-text="row.label"></option>
                                    				</select>
                                    				<span class="help-block" v-if="verror.kelurahan_penagihan"> {{verror.kelurahan_penagihan}} </span>
                                    			
                                    			</div>
                                    		</div>
                                    	</div>
                                    	<div class="row">
                                    		
                                    		<div class="col-md-6">
                                    			<div class="form-group" :class="{'has-error':verror.pass_foto}">
                                    				<label class="control-label">Upload Pass Foto</label>
                                    				<div class="form-control" style="border: none;padding: 0">
                                    					<?=$upload_input_pass_foto?> 
                                    				</div>
                                    				
                                    				<span class="help-block" style="position:relative;"  v-if="verror.pass_foto"> {{verror.pass_foto}} </span>
                                    			
                                    			</div>
                                    		</div>
                                    		<div class="col-md-6">
                                    			<div class="form-group" :class="{'has-error':verror.doc_identitas}">
                                    				<label class="control-label">Upload Identitas</label>
                                    				<div class="form-control" style="border: none;padding: 0">
                                    					<?=$upload_input_doc_identitas?> 
                                    				</div> 
                                    				<span class="help-block" style="position:relative;"  v-if="verror.doc_identitas"> {{verror.doc_identitas}} </span>
                                    			
                                    			</div>
                                    		</div>
                                    	</div>
                                    	<div class="row" style="height: 50px;border: solid 0px #ddd">
                                    		
                                    		
                                    		<div class="col-md-6">
                                    			
                                    		</div>
                                    		<div class="col-md-6">
                                    			 
                                    		</div>
                                    	</div>
                                    	<h3 class="form-section" style="padding: .5em;padding-left: .6em">Data Pribadi Pasangan</h3>
                                    	<div class="row">
                                    		<div class="col-md-6">
                                    			<div class="form-group" :class="{'has-error':verror.ps_nama_lengkap}">
                                    				<label class="control-label">Nama Lengkap</label>
                                    				<input type="text" name="ps_nama_lengkap" v-model="ps_nama_lengkap" class="form-control" placeholder="Nama Lengkap" />
                                    				<span class="help-block" v-if="verror.ps_nama_lengkap"> {{verror.ps_nama_lengkap}} </span>
                                    			
                                    			</div>
                                    		</div>
                                    		<div class="col-md-6">
                                    			<div class="form-group" :class="{'has-error':verror.ps_identitas}">
                                    				<label class="control-label">Identitas</label>
                                    				<div class="input-group">
                                    				<span class="input-group-addon" style="padding: 0;border: none">
                                    					<select name="ps_jenis_identitas" v-model="ps_jenis_identitas" class="form-control" style="width: 130px;margin-right: 4px;">
                                    						<option value="">-- PILIH --</option>
                                    						<option value="KTP">KTP</option>
                                    						<option value="SIM">SIM</option>
                                    						<option value="PASSPORT">PASSPOR</option>
                                    					</select>
                                    					</span><input type="text" name="ps_identitas" v-model="ps_identitas" class="form-control" placeholder="Identitas" />
                                    				
                                    				</div>
                                    				<span class="help-block" v-if="verror.ps_identitas"> {{verror.ps_identitas}} </span>
                                    			
                                    			</div>
                                    			
                                    		</div>
                                    	</div>
                                    	<div class="row">
                                    		<div class="col-md-6">
                                    			<div class="form-group" :class="{'has-error':verror.ps_tempat_lahir}">
                                    				<label class="control-label">Tempat Lahir</label>
                                    				<input type="text" name="ps_tempat_lahir"  v-model="ps_tempat_lahir" class="form-control" placeholder="Tempat Lahir" />
                                    				<span class="help-block" v-if="verror.ps_tempat_lahir"> {{verror.ps_tempat_lahir}} </span>
                                    			
                                    			</div>
                                    		</div>
                                    		<div class="col-md-6">
                                    			<div class="form-group" :class="{'has-error':verror.ps_tanggal_lahir}">
                                    				<label class="control-label">Tanggal Lahir</label>
                                    				<input type="text" name="ps_tanggal_lahir"  class="form-control datepicker-input" placeholder="Tanggal Lahir" />
                                    				<span class="help-block" v-if="verror.ps_tanggal_lahir"> {{verror.ps_tanggal_lahir}} </span>
                                    			
                                    			</div>
                                    		</div>
                                    	</div>
                                    	<div class="row">
                                    		
                                    		<div class="col-md-6">
                                    			<div class="form-group" :class="{'has-error':verror.ps_no_hp}">
                                    				<label class="control-label">No. HP</label>
                                    				<input type="text" name="ps_no_hp" v-model="ps_no_hp" class="form-control" placeholder="No. HP" />
                                    				<span class="help-block" v-if="verror.ps_no_hp"> {{verror.ps_no_hp}} </span>
                                    			
                                    			</div>
                                    		</div>
                                    		<div class="col-md-6">
                                    			
                                    		</div>
                                    	</div>
                                    	<div class="row">
                                    		
                                    		<div class="col-md-6">
                                    			<div class="form-group" :class="{'has-error':verror.ps_pass_foto}">
                                    				<label class="control-label">Upload Pass Foto</label>
                                    				<div class="form-control" style="border: none;padding: 0">
                                    					<?=$upload_input_pass_foto_ps?> 
                                    				</div> 
                                    				<span class="help-block" style="position:relative;" v-if="verror.ps_pass_foto"> {{verror.ps_pass_foto}} </span>
                                    			
                                    			</div>
                                    		</div>
                                    		<div class="col-md-6">
                                    			<div class="form-group" :class="{'has-error':verror.ps_doc_identitas}">
                                    				<label class="control-label">Upload Identitas</label>
                                    				<div class="form-control" style="border: none;padding: 0">
                                    					<?=$upload_input_doc_identitas_ps?> 
                                    				</div> 
                                    				<span class="help-block" style="position:relative;"  v-if="verror.ps_doc_identitas"> {{verror.ps_doc_identitas}} </span>
                                    			
                                    			</div>
                                    		</div>
                                    	</div>
                                    	<div class="row" style="height: 50px;border: solid 0px #ddd;">
                                    	</div>
                                    	<h3 class="form-section" style="padding: .5em;padding-left: .6em">Data Keluarga Terdekat</h3>
                                    	<div class="row">
                                    		<div class="col-md-6">
                                    			<div class="form-group" :class="{'has-error':verror.kl_nama_lengkap}">
                                    				<label class="control-label">Nama Lengkap</label>
                                    				<input type="text" name="kl_nama_lengkap" v-model="kl_nama_lengkap" class="form-control" placeholder="Nama Lengkap" />
                                    				<span class="help-block" v-if="verror.kl_nama_lengkap"> {{verror.kl_nama_lengkap}} </span>
                                    			
                                    			</div>
                                    		</div>
                                    		<div class="col-md-6">
                                    			<div class="form-group" :class="{'has-error':verror.kl_no_hp}">
                                    				<label class="control-label">No. HP</label>
                                    				<input type="text" name="kl_no_hp" v-model="kl_no_hp" class="form-control" placeholder="No. HP" />
                                    				<span class="help-block" v-if="verror.no_hp"> {{verror.kl_no_hp}} </span>
                                    			
                                    			</div>
                                    		</div>
                                    	</div>
                                    	<div class="row">
                                    		
                                    		<div class="col-md-6">
                                    			<div class="form-group" :class="{'has-error':verror.kl_hubungan}">
                                    				<label class="control-label">Hubungan Dengan Pemohon</label>
                                    				<select name="kl_hubungan" v-model="kl_hubungan" class="form-control">
                                    						<option value="">-- PILIH --</option>
                                    						<option value="ORANG TUA">ORANG TUA</option>
                                    						<option value="ANAK">ANAK</option>
                                    						<option value="PAMAN">PAMAN</option>
                                    						<option value="BIBI">BIBI</option>
                                    						<option value="SAUDARA KANDUNG">SAUDARA KANDUNG</option>
                                    						<option value="LAINNYA">LAINNYA</option>
                                    					</select>
                                    				<span class="help-block" v-if="verror.kl_hubungan"> {{verror.kl_hubungan}} </span>
                                    			
                                    			</div>
                                    		</div>
                                    		<div class="col-md-6">
                                    			<div class="form-group" :class="{'has-error':verror.kl_alamat}">
                                    				<label class="control-label">Alamat</label>
                                    				<textarea name="kl_alamat"  v-model="kl_alamat" class="form-control" placeholder="Alamat"></textarea> 
                                    				<span class="help-block" v-if="verror.kl_alamat"> {{verror.kl_alamat}} </span>
                                    			
                                    			</div>
                                    		</div>
                                    	</div>
                                    	<div class="row" style="padding: 2em 0">
                                            <div class="col-md-6">
                                                <a class="btn" @click="cancelForm()">Batal</a>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6" style="text-align: right;padding-right: 2em">
                                                <button type="button" class="btn btn-primary next-step" @click="validateSelf()">Selanjutnya <i class="fa fa-chevron-right"> </i></button>
                                            </div>
                                            <!--/span-->
                                        </div>
                                    </div>
	                   			 </div>
	                   			 <div class="tab-pane" role="tabpanel" id="step2">
                                    <div class="form-body">
                                        <h3 class="form-section" style="padding: .5em;padding-left: .6em">Data Pekerjaan Pemohon</h3>
                                    	<div class="row">
                                    		<div class="col-md-6">
                                    			<div class="form-group" :class="{'has-error':verror.pekerjaan}">
                                    				<label class="control-label">Pekerjaan</label>
                                    				<select name="pekerjaan" v-model="pekerjaan" class="form-control" placeholder="Pekerjaan">
                                    					<option v-for="label,val in dd_pekerjaan" :value="val" v-text="label"></option>
                                    				</select>
                                    				<span class="help-block" v-if="verror.pekerjaan"> {{verror.pekerjaan}} </span>
                                    			
                                    			</div>
                                    		</div>
                                    		<div class="col-md-6">
                                    			<div class="form-group" :class="{'has-error':verror.bentuk_usaha}">
                                    				<label class="control-label">Bentuk Usaha</label>
                                    				<input type="text" name="bentuk_usaha" v-model="bentuk_usaha" class="form-control" placeholder="Bentuk Usaha" />
                                    				<span class="help-block" v-if="verror.bentuk_usaha"> {{verror.bentuk_usaha}} </span>
                                    			</div>
                                    		</div>
                                    		
                                    	</div>
                                    	<div class="row">
                                    		<div class="col-md-6">
                                    			<div class="form-group" :class="{'has-error':verror.nama_perusahaan}">
                                    				<label class="control-label">Nama Perusahaan</label>
                                    				<input type="text" name="nama_perusahaan" v-model="nama_perusahaan" class="form-control" placeholder="Nama Perusahaan" />
                                    				<span class="help-block" v-if="verror.nama_perusahaan"> {{verror.nama_perusahaan}} </span>
                                    			</div>
                                    		</div>
                                    		<div class="col-md-6">
                                    			 <div class="form-group" :class="{'has-error':verror.bidang_usaha}">
                                    				<label class="control-label">Bidang Usaha</label>
                                    				<input type="text" name="bidang_usaha" v-model="bidang_usaha" class="form-control" placeholder="Bidang Usaha" />
                                    				<span class="help-block" v-if="verror.bidang_usaha"> {{verror.bidang_usaha}} </span>
                                    			</div>
                                    		</div>
                                    		
                                    	</div>
                                    	<div class="row">
                                    		<div class="col-md-6">
                                    			 <div class="form-group" :class="{'has-error':verror.alamat_perusahaan}">
                                    				<label class="control-label">Alamat Perusahaan</label>
                                    				<textarea name="alamat_perusahaan" v-model="alamat_perusahaan" class="form-control" placeholder="Alamat Perusahaan"></textarea>
                                    				<span class="help-block" v-if="verror.alamat_perusahaan"> {{verror.alamat_perusahaan}} </span>
                                    			
                                    			</div>
                                    		</div>
                                    		<div class="col-md-6">
                                    			<div class="form-group" :class="{'has-error':verror.jabatan_perusahaan}">
                                    				<label class="control-label">Jabatan</label>
                                    				<input type="text" name="jabatan_perusahaan" v-model="jabatan_perusahaan" class="form-control" placeholder="Jabatan" />
                                    				<span class="help-block" v-if="verror.jabatan_perusahaan"> {{verror.jabatan_perusahaan}} </span>
                                    			</div> 
                                    		</div>
                                    		
                                    	</div>
                                    	<div class="row">
                                    		<div class="col-md-6">
                                    			 <div class="form-group" :class="{'has-error':verror.no_telp_perusahaan}">
                                    				<label class="control-label">Nomor Telepon</label>
                                    				<input type="text" name="no_telp_perusahaan" v-model="no_telp_perusahaan" class="form-control" placeholder="Nomor Telepon" />
                                    				<span class="help-block" v-if="verror.no_telp_perusahaan"> {{verror.no_telp_perusahaan}} </span>
                                    			
                                    			</div>
                                    		</div>
                                    		<div class="col-md-6">
                                    			 <div class="form-group" :class="{'has-error':verror.masa_kerja_perusahaan}">
                                    				<label class="control-label">Masa Kerja</label>
                                    				<div class="input-group">
                                    				<input type="text" name="masa_kerja_perusahaan" v-model="masa_kerja_perusahaan" class="form-control" placeholder="Masa Kerja" />
                                    				<span class="input-group-addon">
															Tahun
														</span>
													</div>
                                    				<span class="help-block" v-if="verror.masa_kerja_perusahaan"> {{verror.masa_kerja_perusahaan}} </span>
                                    			</div> 
                                    		</div>
                                    		
                                    	</div>
                                    	<h3 class="form-section" style="padding: .5em;padding-left: .6em">Data Pekerjaan Pasangan</h3>
										<div class="row">
											<div class="col-md-6">
												<div class="form-group" :class="{'has-error':verror.ps_pekerjaan}">
													<label class="control-label">Pekerjaan</label>
													<select name="ps_pekerjaan" v-model="ps_pekerjaan" class="form-control" placeholder="Pekerjaan">
														<option v-for="label,val in dd_pekerjaan" :value="val" v-text="label"></option>
													</select>
													<span class="help-block" v-if="verror.ps_pekerjaan"> {{verror.ps_pekerjaan}} </span>
												
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group" :class="{'has-error':verror.ps_bentuk_usaha}">
													<label class="control-label">Bentuk Usaha</label>
													<input type="text" name="ps_bentuk_usaha" v-model="ps_bentuk_usaha" class="form-control" placeholder="Bentuk Usaha" />
													<span class="help-block" v-if="verror.ps_bentuk_usaha"> {{verror.ps_bentuk_usaha}} </span>
												</div>
											</div>
											
										</div>
										<div class="row">
											<div class="col-md-6">
												<div class="form-group" :class="{'has-error':verror.ps_nama_perusahaan}">
													<label class="control-label">Nama Perusahaan</label>
													<input type="text" name="ps_nama_perusahaan" v-model="ps_nama_perusahaan" class="form-control" placeholder="Nama Perusahaan" />
													<span class="help-block" v-if="verror.ps_nama_perusahaan"> {{verror.ps_nama_perusahaan}} </span>
												</div>
											</div>
											<div class="col-md-6">
												 <div class="form-group" :class="{'has-error':verror.ps_bidang_usaha}">
													<label class="control-label">Bidang Usaha</label>
													<input type="text" name="ps_bidang_usaha" v-model="ps_bidang_usaha" class="form-control" placeholder="Bidang Usaha" />
													<span class="help-block" v-if="verror.ps_bidang_usaha"> {{verror.ps_bidang_usaha}} </span>
												</div>
											</div>
											
										</div>
										<div class="row">
											<div class="col-md-6">
												 <div class="form-group" :class="{'has-error':verror.ps_alamat_perusahaan}">
													<label class="control-label">Alamat Perusahaan</label>
													<textarea name="ps_alamat_perusahaan" v-model="ps_alamat_perusahaan" class="form-control" placeholder="Alamat Perusahaan"></textarea>
													<span class="help-block" v-if="verror.ps_alamat_perusahaan"> {{verror.ps_alamat_perusahaan}} </span>
												
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group" :class="{'has-error':verror.ps_jabatan_perusahaan}">
													<label class="control-label">Jabatan</label>
													<input type="text" name="ps_jabatan_perusahaan" v-model="ps_jabatan_perusahaan" class="form-control" placeholder="Jabatan" />
													<span class="help-block" v-if="verror.ps_jabatan_perusahaan"> {{verror.ps_jabatan_perusahaan}} </span>
												</div> 
											</div>
											
										</div>
										<div class="row">
											<div class="col-md-6">
												 <div class="form-group" :class="{'has-error':verror.ps_no_telp_perusahaan}">
													<label class="control-label">Nomor Telepon</label>
													<input type="text" name="ps_no_telp_perusahaan" v-model="ps_no_telp_perusahaan" class="form-control" placeholder="Nomor Telepon" />
													<span class="help-block" v-if="verror.ps_no_telp_perusahaan"> {{verror.ps_no_telp_perusahaan}} </span>
												
												</div>
											</div>
											<div class="col-md-6">
												 <div class="form-group" :class="{'has-error':verror.ps_masa_kerja_perusahaan}">
													<label class="control-label">Masa Kerja</label>
													<div class="input-group">
														<input type="text" name="ps_masa_kerja_perusahaan" v-model="ps_masa_kerja_perusahaan" class="form-control" placeholder="Masa Kerja" />
														<span class="input-group-addon">
															Tahun
														</span>
													</div>
													<span class="help-block" v-if="verror.ps_masa_kerja_perusahaan"> {{verror.ps_masa_kerja_perusahaan}} </span>
													
												</div> 
											</div>
											
										</div>
										<h3 class="form-section" style="padding: .5em;padding-left: .6em">Data Penghasilan Dan Pengeluaran</h3>
										<div class="row">
											<div class="col-md-6">
												<div class="form-group" :class="{'has-error':verror.penghasilan_utama}">
													<label class="control-label">Penghasilan Utama</label>
													<input @change="calculateTotalPenghasilan()" @keyup="formatRupiahAndCalculate()" style="text-align: right;" type="text" name="penghasilan_utama" v-model="penghasilan_utama" class="form-control" placeholder="Penghasilan Utama" />
													<span class="help-block" v-if="verror.penghasilan_utama"> {{verror.penghasilan_utama}} </span>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group" :class="{'has-error':verror.penghasilan_pasangan}">
													<label class="control-label">Penghasilan Pasangan</label>
													<input @change="calculateTotalPenghasilan()" @keyup="formatRupiahAndCalculate()" style="text-align: right;" type="text" name="penghasilan_pasangan" v-model="penghasilan_pasangan" class="form-control" placeholder="Penghasilan Pasangan" />
													<span class="help-block" v-if="verror.penghasilan_pasangan"> {{verror.penghasilan_pasangan}} </span>
												</div>
											</div>
											
										</div>
										<div class="row">
											<div class="col-md-6">
												<div class="form-group" :class="{'has-error':verror.penghasilan_tambahan}">
													<label class="control-label">Penghasilan Tambahan</label>
													<input @change="calculateTotalPenghasilan()" @keyup="formatRupiahAndCalculate()" style="text-align: right;" type="text" name="penghasilan_tambahan" v-model="penghasilan_tambahan" class="form-control" placeholder="Penghasilan Tambahan" />
													<span class="help-block" v-if="verror.penghasilan_tambahan"> {{verror.penghasilan_tambahan}} </span>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group" :class="{'has-error':verror.penghasilan_tambahan_pasangan}">
													<label class="control-label">Penghasilan Tambahan Pasangan</label>
													<input @change="calculateTotalPenghasilan()" @keyup="formatRupiahAndCalculate()" style="text-align: right;" type="text" name="penghasilan_tambahan_pasangan" v-model="penghasilan_tambahan_pasangan" class="form-control" placeholder="Penghasilan Tambahan Pasangan" />
													<span class="help-block" v-if="verror.penghasilan_tambahan_pasangan"> {{verror.penghasilan_tambahan_pasangan}} </span>
												</div>
											</div>
											
										</div>
										<div class="row">
											<div class="col-md-6">
												
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label">Total Penghasilan</label>
													<span style="text-align: right;background: #eee" type="number" name="total_penghasilan" v-text="total_penghasilan" class="form-control" placeholder="Total Penghasilan"></span>
												</div>
											</div>
											
										</div>
										<div class="row" style="padding: 2em 0">
                                            <div class="col-md-6">
                                                <button type="button" class="btn btn-default prev-step"  @click="prevFirst()"><i class="fa fa-chevron-left"></i> Sebelumnya </button>
                                                
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6" style="text-align: right;padding-right: 2em;">
                                                <button type="button" class="btn btn-primary next-step" @click="validateFirst()">Selanjutnya <i class="fa fa-chevron-right"> </i></button>
                                            </div>
                                            <!--/span-->
                                        </div>
                                    </div>
                                 </div>
                                 <div class="tab-pane" role="tabpanel" id="step3">
                                    <div class="form-body">
                                        <h3 class="form-section" style="padding: .5em;padding-left: .6em">Data Pinjaman &amp; Aset/Kekayaaan</h3>
                                        <h4 class="form-section" style="padding: .5em;padding-left: .6em">Data Pinjaman</h4>
                                        <div id="form_pinjaman"></div>

                                        <h4 class="form-section" style="padding: .5em;padding-left: .6em">Data Aset/Kekayaaan</h4>
                                    	<div class="row form-group" :class="{'has-error':verror.tabungan_bank||verror.tabungan_nilai}">
                            				<label class="col-md-2 control-label">Tabungan</label>
                            				<div class="cold-md-8">
                            				<div class="input-group">
                            				<span class="input-group-addon" style="padding: 0;border: none">
                            					<input type="text" name="tabungan_bank" v-model="tabungan_bank" class="form-control" style="width: 400px;margin-right: 4px;" placeholder="Nama Bank" />
                            					 
                            					</span>
                            					<input type="text" style="text-align: right;" name="tabungan_nilai"  v-model="tabungan_nilai" class="form-control" placeholder="Nilai" />
                            				
                            				</div>
                            				<span class="help-block" v-if="verror.tabungan_bank"> {{verror.tabungan_bank}} </span>
                            				<span class="help-block" v-if="verror.tabungan_nilai"> {{verror.tabungan_nilai}} </span>
                            				</div>
                            			</div>
                            			<div class="row form-group" :class="{'has-error':verror.deposito_bank||verror.deposito_nilai}">
                            				<label class="col-md-2 control-label">Deposito</label>
                            				<div class="cold-md-8">
                            				<div class="input-group">
                            				<span class="input-group-addon" style="padding: 0;border: none">
                            					<input type="text" name="deposito_bank" v-model="deposito_bank" class="form-control" style="width: 400px;margin-right: 4px;" placeholder="Nama Bank" />
                            					 
                            					</span>
                            					<input type="text" style="text-align: right;" name="tabungan_nilai"  v-model="tabungan_nilai" class="form-control" placeholder="Nilai" />
                            				
                            				</div>
                            				<span class="help-block" v-if="verror.deposito_bank"> {{verror.deposito_bank}} </span>
                            				<span class="help-block" v-if="verror.deposito_nilai"> {{verror.deposito_nilai}} </span>
                            				</div>
                            			</div>
                            			
                            			<div class="row form-group" :class="{'has-error':verror.rumah_an||verror.rumah_nilai}">
                            				<label class="col-md-2 control-label">Rumah</label>
                            				<div class="cold-md-8">
                            				<div class="input-group">
                            				<span class="input-group-addon" style="padding: 0;border: none">
                            					<input type="text" name="rumah_an" v-model="rumah_an" class="form-control" style="width: 400px;margin-right: 4px;" placeholder="Atas Nama" />
                            					 
                            					</span>
                            					<input type="text" style="text-align: right;" name="rumah_nilai"  v-model="rumah_nilai" class="form-control" placeholder="Nilai" />
                            				
                            				</div>
                            				<span class="help-block" v-if="verror.rumah_an"> {{verror.rumah_an}} </span>
                            				<span class="help-block" v-if="verror.rumah_nilai"> {{verror.rumah_nilai}} </span>
                            				</div>
                            			</div>
                            			<div class="row form-group" :class="{'has-error':verror.kendaraan_an||verror.kendaraan_nilai}">
                            				<label class="col-md-2 control-label">Kendaraan</label>
                            				<div class="cold-md-8">
                            				<div class="input-group">
                            				<span class="input-group-addon" style="padding: 0;border: none">
                            					<input type="text" name="kendaraan_an" v-model="kendaraan_an" class="form-control" style="width: 400px;margin-right: 4px;" placeholder="Atas Nama" />
                            					 
                            					</span>
                            					<input type="text" style="text-align: right;" name="kendaraan_nilai"  v-model="kendaraan_nilai" class="form-control" placeholder="Nilai" />
                            				
                            				</div>
                            				<span class="help-block" v-if="verror.kendaraan_an"> {{verror.kendaraan_an}} </span>
                            				<span class="help-block" v-if="verror.kendaraan_nilai"> {{verror.kendaraan_nilai}} </span>
                            				</div>
                            			</div>
                            			<div class="row"style="padding-top: 2em">
	                                        <div class="col-md-6">
                                                <button type="button" class="btn btn-default prev-step" @click="prevSecond()"><i class="fa fa-chevron-left"></i> Sebelumnya </button>
                                            </div>
                                            <div class="col-md-6" style="text-align: right;padding-right: 0">
                                                <button type="button" class="btn btn-primary next-step" @click="saveForm()">Finish <i class="fa fa-chevron-right"> </i></button>
                                            </div>
                                    	</div>
                                    </div>
                                </div>
                                  <div class="tab-pane" role="tabpanel" id="complete">
                                    <div class="form-body">
                                    	<div id="insert_progress">
					                    	
					                    </div>
                                    </div>
                                  </div>
	                   		</div>
                   		</form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
<script type="text/javascript" src="{{ site_url }}modules/app/assets/konsumen/form_add_wizard.js"></script>
<link rel="stylesheet" type="text/css" href="{{ site_url }}modules/app/assets/konsumen/form_add_wizard.css">