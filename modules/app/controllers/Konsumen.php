<?php  defined('BASEPATH') OR exit('No direct script access allowed');
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Exception\ParseException;
class Konsumen extends Theme_Controller {
	public $_page_title = 'Daftar Konsumen';
	public function custom_grid_data()
    {
        $this->load->model('account/m_login');
        $this->load->model('m_konsumen','model');
        $list = $this->model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $action = "<td class=\"actions\"> 
            <div class='btn-group'>  
            <button href=\"".site_url('app/konsumen/index/edit/'.$field->id_data_konsumen)."/".slugify($field->nama_lengkap)."\" class=\"gc-bt-edit edit_button btn btn-sm btn-icon btn-pure btn-info\" role=\"button\"> <i class=\"fa fa-edit\" aria-hidden=\"true\"></i> 
            </button> "."
            <button onclick=\"javascript: return delete_row('".site_url('app/konsumen/index/delete/'.$field->id_data_konsumen)."',-1,this)\" href=\"javascript:;\" class=\"gc-bt-delete delete_button btn btn-sm btn-icon btn-pure btn-danger\" role=\"button\"> <i class=\"fa fa-trash\" aria-hidden=\"true\"></i> 
            </button>
            </div>
            </td>";
            $no++;
            $row = array();
            $row[] = $no;

            $row[] = '<table class="tb-detail" width="300" border="0">
                                              <tr>
                                                <td width="87" align="left">Nama</td>
                                                <td width="10" align="left">:</td>
                                                <td width="150" align="left">'.$field->nama_lengkap.'</sup></td>
                                              </tr>
                                              <tr>
                                                <td align="left">Alamat</td>
                                                <td align="left">:</td>
                                                <td align="left">'.$field->alamat_rumah.'</td>
                                              </tr>
                                              <tr>
                                                <td align="left">No. Hp</td>
                                                <td align="left">:</td>
                                                <td align="left">'.$field->no_hp.'</td>
                                              </tr>
                                              <tr>
                                                <td align="left">Email</td>
                                                <td align="left">:</td>
                                                <td align="left">'.$field->email.'</td>
                                              </tr>
                                            </table>';
            $row[] = '<table class="tb-detail" width="300" border="0">
                                              <tr>
                                                <td width="87" align="left">Nama</td>
                                                <td width="10" align="left">:</td>
                                                <td width="150" align="left">'.$field->nama_pasangan.'</td>
                                              </tr>
                                              
                                              <tr>
                                                <td align="left">No. Hp</td>
                                                <td align="left">:</td>
                                                <td align="left">'.$field->no_hp_pasangan.'</td>
                                              </tr>
                                              
                                            </table>';
            $row[] = '<table class="tb-detail" width="300" border="0">
                                              <tr>
                                                <td width="87" align="left">Nama</td>
                                                <td width="10" align="left">:</td>
                                                <td width="150" align="left">'.$field->nama_keluarga_terdekat.'</sup></td>
                                              </tr>
                                              <tr>
                                                <td align="left">Alamat</td>
                                                <td align="left">:</td>
                                                <td align="left">'.$field->alamat_keluarga_terdekat.'</td>
                                              </tr>
                                              <tr>
                                                <td align="left">No. Hp</td>
                                                <td align="left">:</td>
                                                <td align="left">'.$field->no_hp_keluarga_terdekat.'</td>
                                              </tr>
                                              <tr>
                                                <td align="left">Hubungan</td>
                                                <td align="left">:</td>
                                                <td align="left">'.$field->hubungan.'</td>
                                              </tr>
                                            </table>';

            $row[] = '<table class="tb-detail" width="300" border="0">
                                              <tr>
                                                <td width="87" align="left">SPPR</td>
                                                <td width="10" align="left">:</td>
                                                <td width="150" align="left">   </td>
                                              </tr>
                                              <tr>
                                                <td align="left">Pindah / Ubah</td>
                                                <td align="left">:</td>
                                                <td align="left"></td>
                                              </tr>
                                              <tr>
                                                <td align="left">Batal</td>
                                                <td align="left">:</td>
                                                <td align="left"></td>
                                              </tr>
                                            </table>';
            $row[] = '<a href="" class="btn btn-primary btn-sm" title="Edit Data"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp;<a href="" class="btn btn-danger btn-sm" title="Hapus Data"><i class="fa fa-trash"></i></a>&nbsp;&nbsp;<a href="" class="btn btn-warning btn-sm" title="SPPR"><i class="fa fa-file"></i></a>';
          
            // $row[] = 'HPP<br><br><a href="'.base_url('konsumen/load_sppr/'.$field->id_konsumen.'""').'" class="btn btn-default btn-sm"><i class="fa fa-pencil"></i></a>';
            $row[] = $action; 
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->model->count_all(),
            "recordsFiltered" => $this->model->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }
    public function data_pasangan($return=false)
    {
        $target_yaml = APP . '/form/tb_data_pasangan_konsumen.yml';
        $buffer      = file_get_contents($target_yaml);
        $conf        = Yaml::parse($buffer);
        $crud = $this->new_crud($conf);
        $field_upload_info_pass_foto = [
            'file_upload_allow_file_types' => 'jpeg|jpg|png',
            'file_upload_max_file_size'=> '2MB'
        ];
        $crud->set_field_upload_info('pass_foto_ps',$field_upload_info_pass_foto);
        $crud->set_field_upload_info('doc_identitas_ps',$field_upload_info_pass_foto);
        if($return === true){
            $crud->set_crud_url_path(site_url('app/konsumen/data_pasangan'));
            return $crud; 
        }else{
            // $crud->callback_before_insert([$this,'_cbBeforeInsertKlausul']);
         
            if (! $this->input->is_ajax_request()) {
                $crud->set_theme_config(['crud_paging' => true ]);
            }

            $data = $crud->render();
            $data->state = $state;
        }
        
        $this->view('_konsumen/gc_data_pasangan',$data);
    }
    public function data_keluarga_dekat($return=false)
    {
        $target_yaml = APP . '/form/tb_data_keluarga_terdekat.yml';
        $buffer      = file_get_contents($target_yaml);
        $conf        = Yaml::parse($buffer);
        $crud = $this->new_crud($conf); 
        if($return === true){
            $crud->set_crud_url_path(site_url('app/konsumen/data_pasangan'));
            return $crud; 
        }else{
 
         
            if (! $this->input->is_ajax_request()) {
                $crud->set_theme_config(['crud_paging' => true ]);
            }

            $data = $crud->render();
            $data->state = $state;
        }
        
        $this->view('_konsumen/gc_data_keluarga_dekat',$data);
    }
    public function data_pekerjaan($return=false)
    {
        $target_yaml = APP . '/form/tb_data_pekerjaan_konsumen.yml';
        $buffer      = file_get_contents($target_yaml);
        $conf        = Yaml::parse($buffer);
        $crud = $this->new_crud($conf); 
        if($return === true){
            $crud->set_crud_url_path(site_url('app/konsumen/data_pekerjaan'));
            return $crud; 
        }else{
 
         
            if (! $this->input->is_ajax_request()) {
                $crud->set_theme_config(['crud_paging' => true ]);
            }

            $data = $crud->render();
            $data->state = $state;
        }
        
        $this->view('_konsumen/gc_data_pekerjaan',$data);
    }
    public function data_pekerjaan_pasangan($return=false)
    {
        $target_yaml = APP . '/form/tb_data_pekerjaan_pasangan.yml';
        $buffer      = file_get_contents($target_yaml);
        $conf        = Yaml::parse($buffer);
        $crud = $this->new_crud($conf); 
        if($return === true){
            $crud->set_crud_url_path(site_url('app/konsumen/data_pekerjaan_pasangan'));
            return $crud; 
        }else{
 
         
            if (! $this->input->is_ajax_request()) {
                $crud->set_theme_config(['crud_paging' => true ]);
            }

            $data = $crud->render();
            $data->state = $state;
        }
        
        $this->view('_konsumen/gc_data_pekerjaan_pasangan',$data);
    }
    public function data_pinjaman($return=false)
    {
        $target_yaml = APP . '/form/tb_data_pinjaman.yml';
        $buffer      = file_get_contents($target_yaml);
        $conf        = Yaml::parse($buffer);
        $crud = $this->new_crud($conf); 
        if($return === true){
            $crud->set_crud_url_path(site_url('app/konsumen/data_pinjaman'));
            return $crud; 
        }else{
 
         
            if (! $this->input->is_ajax_request()) {
                $crud->set_theme_config(['crud_paging' => true ]);
            }

            $data = $crud->render();
            $data->state = $state;
        }
        
        $this->view('_konsumen/gc_data_pinjaman',$data);
    }
    public function data_aset($return=false)
    {
        $target_yaml = APP . '/form/tb_data_aset.yml';
        $buffer      = file_get_contents($target_yaml);
        $conf        = Yaml::parse($buffer);
        $crud = $this->new_crud($conf); 
        if($return === true){
            $crud->set_crud_url_path(site_url('app/konsumen/data_aset'));
            return $crud; 
        }else{
 
         
            if (! $this->input->is_ajax_request()) {
                $crud->set_theme_config(['crud_paging' => true ]);
            }

            $data = $crud->render();
            $data->state = $state;
        }
        
        $this->view('_konsumen/gc_data_aset',$data);
    }
    public function data_penghasilan_pengeluaran($return=false)
    {
        $target_yaml = APP . '/form/tb_data_penghasilan_pengeluaran.yml';
        $buffer      = file_get_contents($target_yaml);
        $conf        = Yaml::parse($buffer);
        $crud = $this->new_crud($conf); 
        if($return === true){
            $crud->set_crud_url_path(site_url('app/konsumen/data_penghasilan_pengeluaran'));
            return $crud; 
        }else{
 
         
            if (! $this->input->is_ajax_request()) {
                $crud->set_theme_config(['crud_paging' => true ]);
            }

            $data = $crud->render();
            $data->state = $state;
        }
        
        $this->view('_konsumen/gc_data_penghasilan_pengeluaran',$data);
    }
    public function custom_grid_ajax()
    {
        $tdata = ['add_url' => site_url('app/konsumen/index/add')];
        $tdata['is_admin'] = $this->cms_user_group() == 'admin';
        $this->load->view('_app/dt_konsumen.php',$tdata);
    }
    private function _customGrid(){
        $args = func_get_args();
        // print_r($args[1]);
        $dump = file_get_contents(APP . '/config/gc.yml');
        $data = Yaml::parse($dump);
        $tdata = ['add_url' => site_url('app/konsumen/index/add')];
        $data['output'] = $this->load->view('_app/dt_konsumen.php',$tdata,true);
        $data['unique_hash'] = md5(date('YmdHis-Unit'));
        $data['is_admin']  = $this->cms_user_group()=='admin';
        $data['crud']  = $args[0];
        $data['crud_ps']  = $args[1];
        $data['crud_kl']  = $args[2];
        $data['crud_pekerjaan']  = $args[3];
        $data['crud_pekerjaan_pasangan']  = $args[4];
        $data['crud_penghasilan_pengeluaran']  = $args[5];
        $this->view('_app/konsumen.php', $data );
    }
    public function index()
    {
        // for data pinjaman
        $tmp_id = $this->session->userdata('tb_data_pinjaman_tmp_fk');
        if(empty($tmp_id)){
            $this->session->set_userdata('tb_data_pinjaman_tmp_fk',md5(microtime()));
        } 
        $target_yaml = APP . '/form/tb_data_konsumen.yml';
        $buffer = file_get_contents($target_yaml);
        $_SERVER['FORM_DEF'] = Yaml::parse($buffer);
        $crud = $this->new_crud();
        $crud->set_subject('Konsumen');
        $state = $crud->getState();
        $crud->set_crud_url_path(site_url('app/konsumen/index'));
        $crud->unset_jquery();
        // $crud->unset_export();
        if (! $this->input->is_ajax_request()) {
            $crud->set_theme_config(['crud_paging' => true ]);
        }
        $crud->set_table('tb_data_konsumen');
		$crud->set_model('m_konsumen');
		$crud->set_theme('datatables');
        $crud->field_type('tgl_entry','hidden');
        $crud->field_type('tgl_update','hidden');
        $crud->field_type('id_user','hidden');
        $crud->callback_before_insert(array($this,'_set_tgl_entry'));
        $crud->callback_before_update(array($this,'_set_tgl_update'));
        $crud->callback_after_insert(array($this, '_fix_data_pinjaman'));

        $crud->display_as('nama_lengkap','Nama Lengkap');
        $crud->display_as('tempat_lahir','Tempat Lahir');
        $crud->display_as('tanggal_lahir','Tanggal Lahir');
        $crud->display_as('jenis_identitas','Jenis Identitas');
        $crud->display_as('identitas','Identitas');
        $crud->display_as('alamat_rumah','Alamat Rumah');
        $crud->display_as('kelurahan','Kelurahan');
        $crud->display_as('kecamatan','Kecamatan');
        $crud->display_as('kota','Kota');
        $crud->display_as('propinsi','Propinsi');
        $crud->display_as('status_pernikahan','Status Pernikahan');
        $crud->display_as('pass_foto','Pass Foto');
        $crud->display_as('doc_identitas','Doc Identitas');
        $crud->display_as('no_hp','Nomor HP');
        $crud->display_as('email','Email');
        $crud->display_as('npwp','Npwp');
        $crud->display_as('jml_anak','Jumlah Anak');
        $crud->display_as('alamat_penagihan','Alamat Penagihan');
        $crud->display_as('kelurahan_penagihan','Kelurahan Penagihan');
        $crud->display_as('kecamatan_penagihan','Kecamatan Penagihan');
        $crud->display_as('kota_penagihan','Kota Penagihan');
        $crud->display_as('propinsi_penagihan','Propinsi Penagihan');

        $crud->set_rules('nama_lengkap','Nama Lengkap','trim|required');
        $crud->set_rules('tempat_lahir','Tempat Lahir','trim|required');
        $crud->set_rules('tanggal_lahir','Tanggal Lahir','trim|required');
        $crud->set_rules('jenis_identitas','Jenis Identitas','trim|required');
        $crud->set_rules('identitas','Identitas','trim|required|callback_identitas_check');
        $crud->set_rules('alamat_rumah','Alamat Rumah','trim|required');
        $crud->set_rules('kelurahan','Kelurahan','trim|required');
        $crud->set_rules('kecamatan','Kecamatan','trim|required');
        $crud->set_rules('kota','Kota','trim|required');
        $crud->set_rules('propinsi','Propinsi','trim|required');
        $crud->set_rules('status_pernikahan','Status Pernikahan','trim|required');
        // $crud->set_rules('pass_foto','Upload Pass Foto','trim|required');
        // $crud->set_rules('doc_identitas','Upload Identitas','trim|required');
        $crud->set_rules('no_hp','Nomor HP','trim|required|callback_no_hp_check');
        $crud->set_rules('email','Email','trim|required|valid_email|callback_email_check');
        $crud->set_rules('npwp','NPWP','trim|required|callback_npwp_check');
        $crud->set_rules('jml_anak','Jumlah Anak','trim|required');
        // $crud->set_rules('alamat_penagihan','Alamat Penagihan','trim|required');
        // $crud->set_rules('kelurahan_penagihan','Kelurahan Penagihan','trim|required');
        // $crud->set_rules('kecamatan_penagihan','Kecamatan Penagihan','trim|required');
        // $crud->set_rules('kota_penagihan','Kota Penagihan','trim|required');
        // $crud->set_rules('propinsi_penagihan','Propinsi Penagihan','trim|required');


    	$id_user = $this->cms_user_id();
		$state = $crud->getState();
    	$state_info = $crud->getStateInfo();
    	$id_user = $this->cms_user_id();

        $crud->set_field_upload('pass_foto','tb_data_konsumen_pass_foto');
        $crud->set_field_upload('doc_identitas','tb_data_konsumen_doc_identitas');
        $field_upload_info_pass_foto = [
            'file_upload_allow_file_types' => 'jpeg|jpg|png',
            'file_upload_max_file_size'=> '2MB'
        ];
        $crud->set_field_upload_info('pass_foto',$field_upload_info_pass_foto);
        $crud->set_field_upload_info('doc_identitas',$field_upload_info_pass_foto);
        $crud_ps  = $this->data_pasangan(true);
        $crud_kl  = $this->data_keluarga_dekat(true);
        $crud_pekerjaan = $this->data_pekerjaan(true);
        $crud_pekerjaan_pasangan = $this->data_pekerjaan_pasangan(true);
        $crud_penghasilan_pengeluaran = $this->data_penghasilan_pengeluaran(true);
        switch ($state) {
            case 'list':
                $args = func_get_args();
                // print_r($args);
                $args['crud'] = $crud;
                $args['crud_ps'] = $crud_ps;
                $args['crud_kl'] = $crud_kl;
                $args['crud_pekerjaan'] = $crud_pekerjaan;
                $args['crud_pekerjaan_pasangan'] = $crud_pekerjaan_pasangan;
                $args['crud_penghasilan_pengeluaran'] = $crud_penghasilan_pengeluaran;
                return call_user_func_array([$this,'_customGrid'], $args);
                break;
            default:
                # code...
                break;
        }
        $data = $crud->render();
        $data->crud = $crud;
        $data->crud_ps = $crud_ps;
        $data->crud_pekerjaan = $crud_pekerjaan;
        $data->crud_pekerjaan_pasangan = $crud_pekerjaan_pasangan;
        $data->crud_penghasilan_pengeluaran = $crud_penghasilan_pengeluaran;
		$data->is_admin  = $this->cms_user_group()=='admin';
		$this->view('_app/konsumen.php',$data);
    }
    function _set_tgl_entry($post_array){
        $dt = date('Y-m-d H:i:s');
        $post_array['tgl_entry'] = $dt;
        $post_array['tgl_update'] = $dt;
        $post_array['id_user'] = $this->cms_user_id();
        return $post_array;
    }
    function _set_tgl_update($post_array,$pk){
        $dt = date('Y-m-d H:i:s');
        $post_array['tgl_update'] = $dt;
        return $post_array;
    } 
    function _fix_data_pinjaman($post_array,$primary_key){
        $dt = date('Y-m-d H:i:s');
        $data_update = [

            'id_data_konsumen' => $primary_key,
            'tmp_id' => '',
            'is_tmp' => 0,
            'id_user' =>$this->cms_user_id(),
            'tgl_entry'=> $dt,
            'tgl_update' => $dt
        ];
        $tmp_id = $this->session->userdata('tb_data_pinjaman_tmp_fk');
        $this->db->where('tmp_id', $tmp_id)
                 ->where('is_tmp',1)
                 ->update('tb_data_pinjaman',$data_update);
    }
    public function list_data_pinjaman($fk_id)
    {
        $param_64  = $this->input->get('param');
        $param_str = base64_decode($param_64);
        $param     = json_decode($param_str);
        $results   = [
            'success' => false,
            'data'=> []
        ];
        // print_r($param);
        $cmd = $this->input->get('cmd');
        $post_keys = ['id_data_konsumen','nama_peminjam','jenis_pinjaman','jumlah_pinjaman','jumlah_angsuran_perbulan','sisa_pinjaman'];
        $post_data = [];

        foreach ($post_keys as $name) {
            $post_data[$name] = $this->input->post($name);
        }
        $is_temporal = $this->input->get('temporal') == 'true';
        switch ($cmd) {
            case 'add':
                
                if($is_temporal){
                    $post_data['tmp_id'] = $this->session->userdata('tb_data_pinjaman_tmp_fk');
                    $post_data['is_tmp'] = 1;
                    $post_data['id_data_konsumen'] = -1;
                }
                
                $results['success'] = true;
                $this->db->insert('tb_data_pinjaman',$post_data);
                $results['data'] = $post_data;
                $results['data']['id'] = $this->db->insert_id();
                break;
            case 'edit':
                $post_data['is_tmp'] = 0;
                $results['success'] = true;
                $pk = $this->input->post('id');
                $this->db->where('id',$pk)->update('tb_data_pinjaman',$post_data);
                $results['data'] = $post_data;
                $results['data']['id'] = $pk;
                break;
            case 'delete': 
                $results['success'] = true;
                $pk = $this->input->post('id_data_pinjaman');
                if($is_temporal){
                    $tmp_id = $this->session->userdata('tb_data_pinjaman_tmp_fk');
                    $this->db->where('id_data_pinjaman',$pk)->where('tmp_id',$tmp_id)->where('is_tmp',1)->delete('tb_data_pinjaman');
                }else{
                    
                    $this->db->where('id_data_pinjaman',$pk)->delete('tb_data_pinjaman');
                }

                break;    
            case 'list':
                $results['success'] = true;
                if($is_temporal){
                    $tmp_id = $this->session->userdata('tb_data_pinjaman_tmp_fk');

                    $results['data'] = $this->db->where('tmp_id',$tmp_id)
                                            ->where('is_tmp',1)
                                            ->get('tb_data_pinjaman')
                                            ->result_array();
                }else{
                    $results['data'] = $this->db->where('id_data_konsumen',$fk_id)
                                            ->get('tb_data_pinjaman')
                                            ->result_array();
                }
                
                $results['sql'] = $this->db->last_query();                            
                break;
        }
        echo json_encode($results);
    }

    function email_check($str){
        $id_data_konsumen = $this->uri->segment(5);
        if(!empty($id_data_konsumen) && is_numeric($id_data_konsumen)){
            $email_old = $this->db->where("id_data_konsumen",$id_data_konsumen)->get('tb_data_konsumen')->row()->email;
            $this->db->where("email !=",$email_old);
        }
        $num_row = $this->db->where('email',$str)->get('tb_data_konsumen')->num_rows();
        if ($num_row >= 1){
            $this->form_validation->set_message('email_check', 'Email tidak tersedia atau sudah digunakan');
            return false;
        }
        else{
            return true;
        }
    }
    function npwp_check($str){
        $id_data_konsumen = $this->uri->segment(5);
        if(!empty($id_data_konsumen) && is_numeric($id_data_konsumen)){
            $npwp_old = $this->db->where("id_data_konsumen",$id_data_konsumen)->get('tb_data_konsumen')->row()->npwp;
            $this->db->where("npwp !=",$npwp_old);
        }
        $num_row = $this->db->where('npwp',$str)->get('tb_data_konsumen')->num_rows();
        if ($num_row >= 1){
            $this->form_validation->set_message('npwp_check', 'NPWP tidak tersedia atau sudah digunakan');
            return false;
        }
        else{
            return true;
        }
    }

    function identitas_check($str){
        $id_data_konsumen = $this->uri->segment(5);
        if(!empty($id_data_konsumen) && is_numeric($id_data_konsumen)){
            $identitas_old = $this->db->where("id_data_konsumen",$id_data_konsumen)->get('tb_data_konsumen')->row()->identitas;
            $this->db->where("identitas !=",$identitas_old);
        }
        $num_row = $this->db->where('identitas',$str)->get('tb_data_konsumen')->num_rows();
        if ($num_row >= 1){
            $this->form_validation->set_message('identitas_check', 'Identitas tidak tersedia atau sudah digunakan');
            return false;
        }
        else{
            return true;
        }
    }

    function no_hp_check($str){
        $id_data_konsumen = $this->uri->segment(5);
        if(!empty($id_data_konsumen) && is_numeric($id_data_konsumen)){
            $no_hp_old = $this->db->where("id_data_konsumen",$id_data_konsumen)->get('tb_data_konsumen')->row()->no_hp;
            $this->db->where("no_hp !=",$no_hp_old);
        }
        $num_row = $this->db->where('no_hp',$str)->get('tb_data_konsumen')->num_rows();
        if ($num_row >= 1){
            $this->form_validation->set_message('no_hp_check', 'No. HP tidak tersedia atau sudah digunakan');
            return false;
        }
        else{
            return true;
        }
    }
}
