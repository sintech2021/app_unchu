toMysqlDateStr = (id_date_str) =>{
    // dd/mm/yyyy
    let tmp_str = id_date_str.split('-');
    const [dd,mm,yyyy] = tmp_str;
    if(typeof dd == 'undefined' || typeof mm == 'undefined' || typeof yyyy=='undefined')
        return '';
    return `${yyyy}-${mm}-${dd}`;
};
$(document).ready(function () {
    //Initialize tooltips
    $('.nav-tabs > li a[title]').tooltip();
    
    //Wizard
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

        var $target = $(e.target);
    
        if ($target.parent().hasClass('disabled')) {
            return false;
        }
    });

    $(".next-step").click(function (e) {
       /* do_remote_validation();
        var $active = $('.wizard .nav-tabs li.active');
        $active.next().removeClass('disabled');
        nextTab($active);
        */
    });
function create_inline_table_cell(item,state,primary_key){
    const id = 'vue_inline_table_cell_'+MD5('form_pinjaman');
    const selector = `#${id}`;
    const columnDef = item.columnDef;
    const tableHeader = item.tableHeader;
    const url = site_url()+item.url;
    const fk_parent = item.fk_parent; 
    const fk = item.fk; 
    const pk = item.pk; 
    const table_cls = item.table_cls;
    let fk_value = primary_key;
    let is_temporal = false;
    if(fk_value == '' || fk_value == null){
        fk_value = Math.floor(Date.now() / 1000);
        is_temporal = true;
    }
     

    parentHtml = `
        <div class="row">
            <div class="col-md-12 general" id="${id}">
                <table class="table table-bordered table-list ${table_cls}">
                    ${tableHeader.html}
                    <tbody>
                    <tr v-for="row,index in json" v-bind:${pk}="json.${pk}">
                        <td style="text-align:center;width:10px">{{index+1}}.</td>
                        <td v-bind:style="{width:c.width}" v-if="c.type != 'hidden'" v-for="c,k in colDefParsed">
                            <input v-if="c.type=='text'||c.type=='number'" v-bind:okey="k" @click="onInputClick(index)" @change="onInputChanged(row,index)" @keyup="onInputKeyUp(row,index)" class="form-control" v-bind:type="c.type" v-bind:maxlength="c.length" v-model="row[k]"/>
                            <textarea v-if="c.type=='textarea'" v-bind:okey="k" @click="onInputClick(index)" @change="onInputChanged(row,index)" @keyup="onInputKeyUp(row,index)" class="form-control" v-bind:type="c.type" v-bind:maxlength="c.length" v-model="row[k]"></textarea>
                        </td>
                        <td class="act" style="text-align:center">
                            <a @click="saveRow(row,index)" v-if="isDirty(index)" class="btn btn-sm btn-success" href="javascript:;"><i class="fa fa-check"></i></a>
                            <a @click="delRow(row,index)" v-if="!isDirty(index)" class="btn btn-sm btn-danger" href="javascript:;"><i class="fa fa-trash"></i></a>
                            <a @click="cancelRow(row,index)" v-if="!row.${pk}" class="btn btn-sm btn-warning" href="javascript:;"><i class="fa fa-close"></i></a>
                        </td>
                    </tr>
                    <tr v-if="json.length==0">
                        <td :colspan="columnCount">Tidak ada data.</td>
                    </tr>
                    <tr>
                        <td style="text-align:right" :colspan="columnCount">
                            <a @click="addRow()" class="btn btn-sm btn-info" href="javascript:;"><i class="fa fa-plus"></i> Tambah </a>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div> 
        </div> 
    `;
    parentNode = $(parentHtml);

    rightNode = parentNode.find('.general');
    
    // createFormAccordion(form, title, parentNode);

    $('#form_pinjaman').append(parentNode);

    setTimeout(()=>{
        window[id] = new Vue({
        el : selector,
        data:{
            fk_value: fk_value,
            fk: fk,
            pk: pk,
            state: state,
            fk_parent: fk_parent,
            columnDef:columnDef,
            colDefParsed: {},
            url: url,
            tableHeader: tableHeader,
            json:[],
            newColumn:{},
            columnCount: 2,
            dirtyRows:[],
            tmpJson:{},
            is_temporal : is_temporal
        },
        methods:{
            addRow(){
                const newColumn = Object.assign({},this.newColumn);
                const index = this.json.push(newColumn) - 1;
                this.$nextTick(()=>{
                    $(selector).find('tbody tr:last').prev().find('input:first').focus();
                });
                this.getTmpJson(index);
                return index;
            },
            delRow(row,index){
                const el = event.target;
                const orig_cls = 'fa-trash';
                const loading_cls = 'fa-spin fa-spinner';
                const i = $(el).find('i.fa').get(0);
                swal({
                  title: "Konfirmasi",
                  text: 'Hapus baris ?',
                  type: "warning",
                  showCancelButton: true,
                  cancelButtonText:'Batal',
                  confirmButtonClass: "btn-danger",
                  confirmButtonText: "Ya",
                  closeOnConfirm: true
                },
                ()=>{
                   const postData = {};
                    postData[this.pk] = this.json[index][this.pk];
                    if($(el).attr('loading') == 'true'){
                        return;
                    }
                    $(el).attr('loading','true');
                    toggleCls(i,orig_cls,loading_cls);
                    const is_temporal = this.is_temporal ? '&temporal=true' : '';
                    $.post(`${this.url}/${this.fk_value}?cmd=delete${is_temporal}`,postData,(res)=>{
                        console.log(res);
                        if(res.success){
                            this.json.splice(index, 1);
                            this.unsetTmpJson(index);
                            this.unsetDirty(index);
                        }
                        $(el).attr('loading',false);
                        toggleCls(i,loading_cls,orig_cls);
                    },'json'); 
                });
               
            },
            cancelRow(row,index){
                this.json.splice(index, 1);
                this.unsetTmpJson(index);
                this.unsetDirty(index);
            },
            saveRow(row,index){
                const el = event.target;
                const orig_cls = 'fa-check';
                const loading_cls = 'fa-spin fa-spinner';
                const i = $(el).find('i.fa').get(0);
                if($(el).attr('loading') == 'true'){
                    return;
                }
                $(el).attr('loading','true');
                toggleCls(i,orig_cls,loading_cls);

                let newRow    = Object.assign({},this.json[index]);
                for(field_name in newRow){
                    if(field_name == 'jumlah_pinjaman' || field_name == 'jumlah_angsuran_perbulan' || field_name =='sisa_pinjaman'){
                        newRow[field_name] = rupiahToInt(newRow[field_name]);
                    }
                }
                const postData  = newRow;
                postData[this.fk] = this.fk_value;
                const is_temporal = this.is_temporal ? '&temporal=true' : '';
                const oper = !this.is_temporal ? 'edit':'add';

                $.post(`${this.url}/${this.fk_value}?cmd=${oper}${is_temporal}`,postData,(res)=>{
                    console.log(res);
                    if(res.success){
                        try{
                            if(res.data[this.pk]){
                                // newRow[this.pk] = formatRupiah(res.data[this.pk]);
                            }
                        }catch(e){
                            console.log(e)
                        }
                        
                        
                        // this.json[index] = newRow;
                        this.unsetDirty(index);
                        this.unsetTmpJson(index);
                        this.getTmpJson(index);
                    }
                    $(el).attr('loading',false);
                    toggleCls(i,loading_cls,orig_cls);
                },'json');
            },
            isDirty(index){
                return $.inArray(index,this.dirtyRows) != -1;
            },
            unsetDirty(index){
                const dirty_idx = $.inArray(index,this.dirtyRows);
                console.log(dirty_idx)
                if(dirty_idx != -1){
                    this.dirtyRows.splice(dirty_idx, 1);
                }
                console.log(this.dirtyRows)
            },
            unsetTmpJson(index){
                const tmpJsonKey = MD5(index);
                if(typeof this.tmpJson[tmpJsonKey] == 'object'){
                    this.tmpJson[tmpJsonKey] = null;
                }
            },
            compareRow(index){
                const tmpJson = this.getTmpJson(index);
                let isDirty = false;
                for( key in this.columnDef){
                    if(this.json[index][key] != tmpJson[key]){
                        isDirty = true;
                        break;
                    }
                };
                if(!isDirty){
                    this.unsetDirty(index);
                }else{
                    if(!this.isDirty(index)){
                        this.dirtyRows.push(index);
                    }
                    
                }
            },
            onInputClick(index){
                const tmpJson = this.getTmpJson(index);
                this.compareRow(index);
            },
            onInputChanged(row,index){ 

                this.compareRow(index);
            },
            getTmpJson(index){
                const tmpJsonKey = MD5(index.toString());
                if(typeof this.tmpJson[tmpJsonKey] != 'object'){
                    this.tmpJson[tmpJsonKey] = Object.assign({},this.json[index]);
                }
                return this.tmpJson[tmpJsonKey];
            },
            onInputKeyUp(row,index){ 
                console.log(event.target)
                const field_name = $(event.target).attr('okey');
                console.log(field_name);
                setTimeout(()=>{
                    if(field_name == 'jumlah_pinjaman' || field_name == 'jumlah_angsuran_perbulan' || field_name =='sisa_pinjaman'){
                        this.$data.json[index][field_name] = formatRupiah(this.$data.json[index][field_name]);
                    }
                    
                },500);
                this.compareRow(index);
            },
            getList(){
                this.parseColumnDef();
                const is_temporal = this.is_temporal ? '&temporal=true' : '';

                $.get(`${this.url}/${this.fk_value}?cmd=list${is_temporal}`,(res)=>{
                    // console.log(res);
                    res.data.forEach((item,index)=>{
                        for(var i in item){
                            if(i == 'jumlah_pinjaman' || i == 'jumlah_angsuran_perbulan' || i == 'sisa_pinjaman'){
                                res.data[index][i] = formatRupiah(res.data[index][i])
                            }
                        }
                    });
                    this.json = res.data;

                },'json');
            },
            parseColumnDef(){
                let colDefParsed = {};
                let columnCount = 0;
                $.each(this.columnDef,(key,val)=>{
                    columnCount += 1;
                    // console.log(key,value);
                    const valueArr = val.split('|');
                    const value = {
                        type: valueArr[0],
                        length: valueArr[1],
                        label: valueArr[2],
                        width: typeof valueArr[3] != 'undefined'? `${valueArr[3]}px`:'auto'
                    }; 
                    colDefParsed[key] = value;

                });
                this.colDefParsed = colDefParsed;
                this.columnCount = columnCount+2;
            }
            
        },
        mounted(){
            this.getList();
            // $(form).find('button[data-dismiss=modal]').click(()=>{
            //     if(this.state=='add'){
            //         $.post(`${this.url}/${this.fk_value}?cmd=delete&temporal=true`,{},(res)=>{
            //                 console.log(res);
                             
            //         },'json'); 
            //     }
                
            // });
            this.$nextTick(()=>{
                // let  numeric_inputs = $('table.table_data_pinjaman').find('input[okey=jumlah_pinjaman],input[okey=jumlah_angsuran_perbulan],input[okey=sisa_pinjaman]');
                // numeric_inputs.each((a,b)=>{
                //     console.log(a,b)
                // });
            });
             
        }
    }) ;
    },500);

    return  window[id];
}
function nextTab(elem) {
    var e = $(elem).next().find('a[data-toggle="tab"]');
  
        e.click();
   
}
function prevTab(elem) {
    var e = $(elem).prev().find('a[data-toggle="tab"]');//;
     
        e.click();
} 
function progressHandlingFunction(e){
    if(e.lengthComputable){
        $('progress').attr({value:e.loaded, max:e.total});
        // reset progress on complete
        if (e.loaded == e.total) {
            $('progress').attr('value','0.0');
        }
    }
} 
function show_upload_button(unique_id, uploader_element)
{
    $('#upload-state-message-'+unique_id).html('');
    $("#loading-"+unique_id).hide();

    $('#upload-button-'+unique_id).slideDown('fast');
    $("input[rel="+uploader_element.attr('name')+"]").val('');
    $('#success_'+unique_id).slideUp('fast');   
}

function load_fancybox(elem)
{
    elem.fancybox({
        'transitionIn'  :   'elastic',
        'transitionOut' :   'elastic',
        'speedIn'       :   600, 
        'speedOut'      :   200, 
        'overlayShow'   :   false
    });     
}
function formatRupiah(angka){
    angka=`${angka}`;
      let prefix= 'Rp.';  
      var number_string = angka.replace(/[^,\d]/g, '').toString();
      // Hapus 0 jika 0 pertama
      if(number_string.length > 1){
        while(number_string.charAt(0) === '0')
        {
        number_string = number_string.substr(1);
        }
      }
            var split           = number_string.split(','),
            sisa            = split[0].length % 3,
            rupiah          = split[0].substr(0, sisa),
            ribuan          = split[0].substr(sisa).match(/\d{3}/gi);
 
            // tambahkan titik jika yang di input sudah menjadi angka ribuan
            if(ribuan){
                separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
            }
 
            rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
            return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }
    
    function rupiahToInt(rupiah) {
    if(typeof rupiah != 'string')
        rupiah = `${rupiah}`
      var hasil = rupiah.replace(/[^,\d]/g, '').toString();
      let _value = parseInt(hasil);
      if(isNaN(_value)){
        _value = 0;
      }
      return _value;
    }

    function angka(angka) {
      var hasil = angka.replace(/[^,\d]/g, '').toString();
      return parseInt(hasil);
    }
$(document).ready(()=>{
    //--------------------------------------
    window.______FORM = new Vue({
        el: '#form_items',
        data:{
            formMode : '',
            id_data_konsumen : '',
            nama_lengkap:'',
            tempat_lahir:'',
            tanggal_lahir:'',
            jenis_identitas:'',
            identitas:'',
            alamat_rumah:'',
            kelurahan:'',
            kecamatan:'',
            kota:'',
            propinsi:'',
            status_pernikahan:'',
            pass_foto:'',
            doc_identitas:'',
            no_hp:'',
            email:'',
            npwp:'',
            jml_anak:0,
            alamat_penagihan:'',
            kelurahan_penagihan:'',
            kecamatan_penagihan:'',
            kota_penagihan:'',
            propinsi_penagihan:'',
            pilih_alamat_penagihan:'',

            //pribadi pasangan
            ps_nama_lengkap:'',
            ps_tempat_lahir:'',
            ps_tanggal_lahir:'',
            ps_no_hp:'',
            ps_jenis_identitas:'',
            ps_identitas:'',
            ps_pass_foto:'',
            ps_doc_identitas:'',

            //keluarga dekat
            kl_nama_lengkap:'',
            kl_no_hp:'',
            kl_hubungan:'',
            kl_alamat:'',

            verror :{},
            no_hp:'',
            no_hp_2:'',
            no_hp_3:'',
            dd_propinsi: [],
            dd_kecamatan: [],
            dd_kota:[],
            dd_kelurahan:[],

            dd_propinsi_penagihan: [],
            dd_kecamatan_penagihan: [],
            dd_kota_penagihan:[],
            dd_kelurahan_penagihan:[],
            valid_first_stage: 0,
            dd_pekerjaan:{
                '':'-- Pilih Pekerjaan --',
                'PNS': 'PNS',
                'Karyawan Swasta': 'Karyawan Swasta',
                'Karyawan BUMN': 'Karyawan BUMN',
                'Karyawan BUMD':'Karyawan BUMD',
                'Wiraswasta':'Wiraswasta',
                'Pensiunan PNS':'Pensiunan PNS',
                'Pensiunan Swasta':'Pensiunan Swasta',
                'Pensiunan BUMN':'Pensiunan BUMN',
                'Pensiunan BUMD':'Pensiunan BUMD',
                'Pensiunan TNI':'Pensiunan TNI',
                'Pensiunan POLRI':'Pensiunan POLRI',
                'PNI':'PNI',
                'POLRI':'POLRI',
                'Profesional':'Profesional',
                'Ibu Rumah Tangga':'Ibu Rumah Tangga',
                'Pelajar':'Pelajar',
                'Lainya':'Lainya'
            },

            // data pekerjaan

            pekerjaan:'',
            nama_perusahaan:'',
            alamat_perusahaan: '',
            no_telp_perusahaan: '',
            
            bentuk_usaha:'-',
            bidang_usaha:'-',
            jabatan_perusahaan:'',
            masa_kerja_perusahaan:'',

            // data pekerjaan pasangan
            ps_pekerjaan:'',
            ps_nama_perusahaan: '',
            ps_alamat_perusahaan:  '',
            ps_no_telp_perusahaan:  '',
            ps_bentuk_usaha:'-',
            ps_bidang_usaha:'-',
            ps_jabatan_perusahaan:'',
            ps_masa_kerja_perusahaan:'',

            // data penghasilan pengeluaran
            penghasilan_utama: '',
            penghasilan_tambahan:'',
            penghasilan_pasangan:'',
            penghasilan_tambahan_pasangan:'',
            total_penghasilan: '',
            valid_second_stage: 0,
            // data aset
            tabungan_bank:'',
            tabungan_nilai:'',
            deposito_bank:'',
            deposito_nilai:'',
            rumah_an:'',
            rumah_nilai:'',
            kendaraan_an:'',
            kendaraan_nilai:'',

            // Pinjaman FormDef
            data_pinjaman_form_def : gc.FormDef.tb_data_konsumen.data_pinjaman_form_def,
            data_pinjaman_form_vue : null,
            submit_counter :0
 
        },
        mounted(){
            setTimeout(()=>{
                this.loadDD('propinsi','');
                this.loadDD('kota',this.propinsi);
                this.loadDD('kecamatan',this.kota);
                this.loadDD('kelurahan',this.kecamatan);

                this.loadDD('propinsi_penagihan','');
                this.loadDD('kota_penagihan',this.propinsi_penagihan);
                this.loadDD('kecamatan_penagihan',this.kota_penagihan);
                this.loadDD('kelurahan_penagihan',this.kecamatan_penagihan);

                $('input[name=no_hp]').numeric();
                $('input[name=ps_no_hp]').numeric();
                $('input[name=kl_no_hp]').numeric();
                $('input[name=identitas]').numeric();
                $('input[name=ps_identitas]').numeric();
                $('input[name=penghasilan_utama]').numeric();
                $('input[name=penghasilan_tambahan]').numeric();
                $('input[name=penghasilan_pasangan]').numeric();
                $('input[name=penghasilan_tambahan_pasangan]').numeric();
                $('input[name=no_telp_perusahaan]').numeric();
                $('input[name=ps_no_telp_perusahaan]').numeric();
                $('input[name=masa_kerja_perusahaan]').numeric();
                $('input[name=ps_masa_kerja_perusahaan]').numeric();

                this.initFileUpload();
                $('.datepicker-input').datepicker({
                    dateFormat: "dd-mm-yy",
                    // container: container,
                    format:'dd-mm-yyyy',
                    language:'id',
                    todayHighlight: true,
                    autoclose: true,
                });
                const formMode = this.$data.mode;
                const primary_key = this.$data.id_data_konsumen;

                this.$data.data_pinjaman_form_vue = create_inline_table_cell(gc.FormDef.tb_data_konsumen.data_pinjaman_form_def,formMode,primary_key);
                this.calculateTotalPenghasilan();
            },500);
        },
        watch:{

        },
        methods:{
            //
            chekSubmitComplete(){
                if(this.submit_counter == 7){
                    // console.log('submit complete');
                    this.submit_counter = 0;
                    this.resetFormData();
                    $('button.refresh-data').click();
                    var firstTab = $('ul[role=tablist] li[role=presentation]').attr('class','disabled').get(0);
                    $(firstTab).attr('class','active').find('a[data-toggle=tab]').click();

                    document.location.href = site_url()+'konsumen?_rand='+(new Date()).toString()
                }
                // console.log(this.submit_counter);
            },
            saveForm(){
                this.submit_counter = 0;
                var $active = $('.wizard .nav-tabs li.active');
                $active.next().removeClass('disabled');
                nextTab($active);

                //1 tb_data_konsumen
                var data_konsumen = new FormData();
                var fields = ['nama_lengkap','tempat_lahir','tanggal_lahir','jenis_identitas','identitas','alamat_rumah','kelurahan','kecamatan','kota','propinsi','status_pernikahan','pass_foto','doc_identitas','no_hp','email','npwp','jml_anak','alamat_penagihan','kelurahan_penagihan','kecamatan_penagihan','kota_penagihan','propinsi_penagihan'] ;
                
                fields.forEach((field,b)=>{
                    if(typeof this.$data[field] == 'undefined')
                        this.$data[field] = '';
                    // if(field == 'tanggal_lahir'){
                    //     if(this.$data[field].match(/undefined/));
                    //     this.$data[field]='';
                    // }
                    
                    data_konsumen.append(field,this.$data[field]);
                });

                //2 tb_data_pasangan
                var data_pasangan = new FormData();
                var fields = ['nama_lengkap','tempat_lahir','tanggal_lahir','jenis_identitas','identitas','pass_foto','doc_identitas','no_hp'] ;
                fields.forEach((field,b)=>{
                    if(typeof this.$data[field] == 'undefined')
                        this.$data[field] = '';
                    let form_key = 'ps_'+field;
                    
                    if(field == 'pass_foto' || field == 'doc_identitas'){
                        field += '_ps';
                        this.$data[form_key] = $(`input[name=${field}]`).val();
                    }
                    data_pasangan.append(field,this.$data[form_key]);
                });
                //3 tb_keluarga_dekat
                var data_keluarga_dekat = new FormData();
                var fields = ['nama_lengkap','alamat','no_hp','hubungan'] ;
                fields.forEach((field,b)=>{
                    if(typeof this.$data[field] == 'undefined')
                        this.$data[field] = '';
                    data_keluarga_dekat.append(field,this.$data['kl_'+field]);
                });

                //4 tb_data_pekerjaan
                var data_pekerjaan = new FormData();
                var fields = ['pekerjaan','nama_perusahaan','alamat_perusahaan','no_telp','bentuk_usaha','bidang_usaha','jabatan','masa_kerja'] ;
                fields.forEach((field,b)=>{
                    let xfield = field;
                    if(field == 'jabatan'){
                        xfield = 'jabatan_perusahaan';
                    }
                    if(field == 'masa_kerja'){
                        xfield = 'masa_kerja_perusahaan';
                    }
                    if(field == 'no_telp'){
                        xfield = 'no_telp_perusahaan';
                    }
                    if(typeof this.$data[field] == 'undefined')
                        this.$data[field] = '';

                    data_pekerjaan.append(field,this.$data[xfield]);
                });

                //5 tb_data_pekerjaan_pasangan
                var data_pekerjaan_pasangan = new FormData();
                var fields = ['pekerjaan','nama_perusahaan','alamat_perusahaan','no_telp','bentuk_usaha','bidang_usaha','jabatan','masa_kerja'] ;
                fields.forEach((field,b)=>{
                    let xxfield = field;
                    field = 'ps_' + field;
                    let xfield = field;
                    if(xxfield == 'jabatan'){
                        xfield = 'ps_jabatan_perusahaan';
                    }
                    if(xxfield == 'masa_kerja'){
                        xfield = 'ps_masa_kerja_perusahaan';
                    }
                    if(xxfield == 'no_telp'){
                        xfield = 'ps_no_telp_perusahaan';
                    }
                    if(typeof this.$data[xfield] == 'undefined')
                        this.$data[xfield] = '';

                    data_pekerjaan_pasangan.append(xxfield,this.$data[xfield]);
                });

                //6 tb_penghasilan_pengeluaran
                var data_penghasilan_pengeluaran = new FormData();
                var fields = ['penghasilan_utama','penghasilan_tambahan','penghasilan_pasangan','penghasilan_tambahan_pasangan','total_penghasilan'] ;
                fields.forEach((field,b)=>{
                  
                    data_penghasilan_pengeluaran.append(field,rupiahToInt(this.$data[field]));
                });

                // 7 fix is tmp data pinjaman

                // 8 tb_data aset 
                var data_aset = new FormData();
                var fields = ['tabungan_bank','nilai_tabungan_bank','deposito_bank','nilai_deposito_bank','rumah_atas_nama','nilai_rumah','kendaraan_atas_nama','nilai_kendaraan'];
                fields.forEach((field,b)=>{
                    let xfield = field;
                    if(field == 'nilai_tabungan_bank'){
                        xfield = 'tabungan_nilai';
                    }
                    if(field == 'nilai_deposito_bank'){
                        xfield = 'deposito_nilai';
                    }
                    if(field == 'rumah_atas_nama'){
                        xfield = 'rumah_an';
                    }
                    if(field == 'nilai_rumah'){
                        xfield = 'rumah_nilai';
                    }
                    if(field == 'kendaraan_atas_nama'){
                        xfield = 'kendaraan_an';
                    }
                    if(field == 'nilai_kendaraan'){
                        xfield = 'kendaraan_nilai';
                    }
                    let _value = '';
                    if(xfield.match(/nilai/)){
                        _value = rupiahToInt(this.$data[xfield]);
                        
                    }else{
                        _value = this.$data[xfield];
                    }
                    data_aset.append(field,_value);
                });

                const data_konsumen_url                 = site_url() + `app/konsumen/index/insert?is_ajax=true`;
                const data_pasangan_url                 = site_url() + `app/konsumen/data_pasangan/insert?is_ajax=true`;
                const data_keluarga_dekat_url           = site_url() + `app/konsumen/data_keluarga_dekat/insert?is_ajax=true`;
                const data_pekerjaan_url                = site_url() + `app/konsumen/data_pekerjaan/insert?is_ajax=true`;
                const data_pekerjaan_pasangan_url       = site_url() + `app/konsumen/data_pekerjaan_pasangan/insert?is_ajax=true`;
                const data_penghasilan_pengeluaran_url  = site_url() + `app/konsumen/data_penghasilan_pengeluaran/insert?is_ajax=true`;
                // const data_pinjaman_url                 = site_url() + `app/konsumen/data_pinjaman_fix`;
                const data_aset_url                     = site_url() + `app/konsumen/data_aset/insert?is_ajax=true`;

                this.submitFormAjax(data_konsumen_url, data_konsumen, (res)=>{
                    if(res.success){


                        // console.log(res);
                        const loadingTag = `<div><i class="fa fa-check"></i> Menyimpan data konsumen</div>`;
                        $('#insert_progress').html(loadingTag);

                        const id_data_konsumen = res.insert_primary_key;

                        data_pasangan.append('id_data_konsumen',id_data_konsumen);
                        data_keluarga_dekat.append('id_data_konsumen',id_data_konsumen);
                        data_pekerjaan.append('id_data_konsumen',id_data_konsumen);
                        data_pekerjaan_pasangan.append('id_data_konsumen',id_data_konsumen);
                        data_penghasilan_pengeluaran.append('id_data_konsumen',id_data_konsumen);
                        data_aset.append('id_data_konsumen',id_data_konsumen);

                        this.submit_counter += 1;
                        /////////////////////////
                         this.submitFormAjax(data_pasangan_url, data_pasangan, (res)=>{
                            if(res.success){
                                // console.log(res);
                                const loadingTag = `<div><i class="fa fa-check"></i> Menyimpan data pasangan</div>`;
                                $('#insert_progress #data_pasangan_tag').html(loadingTag);  
                                
                                this.submit_counter += 1;
                                this.chekSubmitComplete();
                            }
                        },()=>{
                            const loadingTag = `<div id="data_pasangan_tag"><i class="fa fa-spin fa-spinner"></i> Menyimpan data pasangan</div>`;
                            $('#insert_progress').append(loadingTag);
                        });

                       
                         this.submitFormAjax(data_keluarga_dekat_url, data_keluarga_dekat, (res)=>{
                            if(res.success){
                                // console.log(res);
                                const loadingTag = `<div><i class="fa fa-check"></i> Menyimpan data keluarga dekat</div>`;
                                $('#insert_progress #data_keluarga_dekat_tag').html(loadingTag);

                                this.submit_counter += 1;
                                this.chekSubmitComplete();
                            }
                        },()=>{
                            const loadingTag = `<div id="data_keluarga_dekat_tag"><i class="fa fa-spin fa-spinner"></i> Menyimpan data pasangan</div>`;
                            $('#insert_progress').append(loadingTag);
                        });

                         this.submitFormAjax(data_pekerjaan_url, data_pekerjaan, (res)=>{
                            if(res.success){
                                // console.log(res);
                                const loadingTag = `<div><i class="fa fa-check"></i> Menyimpan data pekerjaan</div>`;
                                $('#insert_progress #data_pekerjaan_tag').html(loadingTag);

                                this.submit_counter += 1;
                                this.chekSubmitComplete();
                            }
                        },()=>{
                            const loadingTag = `<div id="data_pekerjaan_tag"><i class="fa fa-spin fa-spinner"></i> Menyimpan data pekerjaan</div>`;
                            $('#insert_progress').append(loadingTag);
                        });

                         this.submitFormAjax(data_pekerjaan_pasangan_url, data_pekerjaan_pasangan, (res)=>{
                            if(res.success){
                                // console.log(res);
                                const loadingTag = `<div><i class="fa fa-check"></i> Menyimpan data pekerjaan pasangan</div>`;
                                $('#insert_progress #data_pekerjaan_pasangan_tag').html(loadingTag);

                                this.submit_counter += 1;
                                this.chekSubmitComplete();
                            }
                        },()=>{
                            const loadingTag = `<div id="data_pekerjaan_pasangan_tag"><i class="fa fa-spin fa-spinner"></i> Menyimpan data pekerjaan pasangan</div>`;
                            $('#insert_progress').append(loadingTag);
                        });
                         this.submitFormAjax(data_penghasilan_pengeluaran_url, data_penghasilan_pengeluaran, (res)=>{
                            if(res.success){
                                // console.log(res);
                                const loadingTag = `<div><i class="fa fa-check"></i> Menyimpan data penghasilan & pengeluaran</div>`;
                                $('#insert_progress #data_penghasilan_pengeluaran_tag').html(loadingTag);

                                this.submit_counter += 1;
                                this.chekSubmitComplete();
                            }
                        },()=>{
                            const loadingTag = `<div id="data_penghasilan_pengeluaran_tag"><i class="fa fa-spin fa-spinner"></i> Menyimpan data penghasilan & pengeluaran</div>`;
                            $('#insert_progress').append(loadingTag);
                        });

                        this.submitFormAjax(data_aset_url, data_aset, (res)=>{
                            if(res.success){
                                // console.log(res);
                                const loadingTag = `<div><i class="fa fa-check"></i> Menyimpan data aset</div>`;
                                $('#insert_progress #data_aset_tag').html(loadingTag);

                                this.submit_counter += 1;
                                this.chekSubmitComplete();
                            }
                        },()=>{
                            const loadingTag = `<div id="data_aset_tag"><i class="fa fa-spin fa-spinner"></i> Menyimpan data aset</div>`;
                            $('#insert_progress').append(loadingTag);
                        });
                        //////////////////////////////////

                    }
                },()=>{
                    const loadingTag = `<div><i class="fa fa-spin fa-spinner"></i> Menyimpan data konsumen</div>`;
                    $('#insert_progress').html(loadingTag);
                });
            },
            formComplete(){

            },
            submitFormAjax: async (url,data,cbSuccess,cbBeforeSend,type_)=>{
                $.ajax({
                        url: url,
                        data: data,
                        cache: false,
                        contentType: false,
                        processData: false,
                        method: typeof type_ == 'undefined' ? 'POST':'GET',
                        // async:false,
                        // contentType: 'multipart/form-data',
                        type: typeof type_ == 'undefined' ? 'POST':'GET', // For jQuery < 1.9                        
                        beforeSend:()=>{App.startPageLoading({animate:!0});if(typeof cbBeforeSend == 'function'){cbBeforeSend()}},
                        success: (res)=>{
                            if(typeof res=='object'){
                                return cbSuccess(res);
                            }
                            res = JSON.parse($(res).text());
                            cbSuccess(res);
                        },     
                        complete:()=>{App.stopPageLoading()}
                    });
            },
            calculateTotalPenghasilan(){
               let total_penghasilan = rupiahToInt(this.penghasilan_utama) + rupiahToInt(this.penghasilan_tambahan) + rupiahToInt(this.penghasilan_pasangan) +rupiahToInt( this.penghasilan_tambahan_pasangan);
                // console.log(total_penghasilan)
                total_penghasilan = isNaN(total_penghasilan) ? 0 : total_penghasilan;
                this.total_penghasilan = formatRupiah(total_penghasilan);
            },
            formatRupiahAndCalculate(){
               let key = event.target.name;
               this.$data[key] = formatRupiah(this.$data[key]);
               setTimeout(()=>{
                this.calculateTotalPenghasilan();
               },1000);
            },
            onSelectChange(key){
                let dd_key = '';
                let parent_id = '';
                if(key == 'propinsi'){
                    dd_key = 'kota';
                    parent_id = this.propinsi;
                }
                if(key == 'kota'){
                    dd_key = 'kecamatan';
                    parent_id = this.kota;

                }
                if(key == 'kecamatan'){
                    dd_key = 'kelurahan';
                    parent_id = this.kecamatan;

                }
                if(key == 'propinsi_penagihan'){
                    dd_key = 'kota_penagihan';
                    parent_id = this.propinsi_penagihan;
                }
                if(key == 'kota_penagihan'){
                    dd_key = 'kecamatan_penagihan';
                    parent_id = this.kota_penagihan;

                }
                if(key == 'kecamatan_penagihan'){
                    dd_key = 'kelurahan_penagihan';
                    parent_id = this.kecamatan_penagihan;

                }
                if(key != 'kelurahan' && key != 'kelurahan_penagihan')
                this.loadDD(dd_key,parent_id,`dd_${dd_key}`,(res,_key,_parent_id)=>{
                    if(_key=='kabupaten_kota'){
                        _key = 'kota';
                    }
                   
                    // console.log(_key)
                    var select = $(`select[name=${_key}]`);
                    this.$data[_key]='';
                    select.change();
                });
            },
            loadDD(key,parent_id,dd_key,callback){
                
                if(typeof dd_key=='undefined'){
                    dd_key = `dd_${key}`;
                }
                let url = site_url()+`data/${dd_key}`;
                if(url.match(/penagihan/)){
                    url = url.replace(/_penagihan/,'');
                }
                if(url.match(/dd_kota/)){
                    url = url.replace(/dd_kota/,'dd_kabupaten_kota');
                }
                let postData = {parent_id:parent_id};

                $.post(url,postData,(res)=>{
                    if(res.success){
                        if(dd_key == 'dd_kabupaten_kota'){
                            dd_key = 'dd_kota';
                        }
                        // if(dd_key.match(/_penagihan/)){
                        //     dd_key +=  '_penagihan' ;
                        // }
                        this.$data[dd_key] = res.data;
                        
                        if(typeof callback == 'function'){
                            callback(res,key,parent_id);
                        }
                    }
                },'json');
            },
            prevFirst(){
                this.goto1stForm();
            },
            prevSecond(){
                var $active = $('.wizard .nav-tabs li.active');
                prevTab($active);
            },
            goto2ndForm(){
                var $active = $('.wizard .nav-tabs li.active');
                $active.next().removeClass('disabled');
                nextTab($active);
            },
            goto3rdForm(){
                this.goto2ndForm();
            },

            goto1stForm(){
                var $active = $('.wizard .nav-tabs li.active');
                prevTab($active);
            },
            setError(data,prefix,clear){
                let verror = Object.assign({},this.$data.verror);
                if(clear){
                    verror = {};
                }
                for(var k in data){
                    let kk = k;
                    if(prefix != ''){
                        let replace = `_${prefix}`;
                        let rgx = new RegExp(replace,"g");
                        kk = kk.replace(rgx, "");
                        // console.log(kk)
                    }
                    const _k = prefix != '' ? prefix+'_'+kk : kk;
                    verror[_k] = data[k];
                }
                // console.log(verror)
                this.$data.verror = verror;
            },

            validateFirstPpl(){
                var data = new FormData();
                var fields = ['penghasilan_utama','penghasilan_tambahan','penghasilan_pasangan','penghasilan_tambahan_pasangan','total_penghasilan'] ;
                fields.forEach((field,b)=>{
                  
                    data.append(field,rupiahToInt(this.$data[field]));
                });

                $.ajax({
                    url: site_url() + `app/konsumen/data_penghasilan_pengeluaran/insert_validation?is_ajax=true`,
                    data: data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    method: 'POST',
                    beforeSend:()=>{App.startPageLoading({animate:!0})},
                    type: 'POST', // For jQuery < 1.9
                    success: (res)=>{
                        res = JSON.parse($(res).text());
                            // console.log(res);

                        // this.validateFirstPs(res.success);
                            
                        if(res.success){
                            this.valid_second_stage += 1;
                            if(this.valid_second_stage==3){
                                this.goto3rdForm();
                            }
                        }else{
                            
                            this.setError(res.error_fields,'',false);
                            
                        } 
                    },
                    complete:()=>{App.stopPageLoading()}
                }); 

                event.preventDefault();
                return false;
            },
            validateFirstPs(){
                var data = new FormData();
                var fields = ['pekerjaan','nama_perusahaan','alamat_perusahaan','no_telp','bentuk_usaha','bidang_usaha','jabatan','masa_kerja'] ;
                fields.forEach((field,b)=>{
                    let xxfield = field;
                    field = 'ps_' + field;
                    let xfield = field;
                    if(xxfield == 'jabatan'){
                        xfield = 'ps_jabatan_perusahaan';
                    }
                    if(xxfield == 'masa_kerja'){
                        xfield = 'ps_masa_kerja_perusahaan';
                    }
                    if(xxfield == 'no_telp'){
                        xfield = 'ps_no_telp_perusahaan';
                    }
                    if(typeof this.$data[xfield] == 'undefined')
                        this.$data[xfield] = '';

                    data.append(xxfield,this.$data[xfield]);
                });

                $.ajax({
                    url: site_url() + `app/konsumen/data_pekerjaan_pasangan/insert_validation?is_ajax=true`,
                    data: data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    method: 'POST',
                    beforeSend:()=>{App.startPageLoading({animate:!0})},
                    type: 'POST', // For jQuery < 1.9
                    success: (res)=>{
                        res = JSON.parse($(res).text());
                            // console.log(res);

                        // this.validateFirstPs(res.success);
                        this.validateFirstPpl(res.success)
                        if(res.success){
                            this.valid_second_stage += 1;
                        }else{
                            for(k in res.error_fields){
                                 
                                if(k == 'jabatan'){
                                    res.error_fields.jabatan_perusahaan = res.error_fields[k];
                                }
                                if(k == 'masa_kerja'){
                                    res.error_fields.masa_kerja_perusahaan = res.error_fields[k];
                                }
                                if(k == 'no_telp'){
                                    res.error_fields.no_telp_perusahaan = res.error_fields[k];
                                }
                            }
                            this.setError(res.error_fields,'ps',false);
                            
                        } 
                    },
                    complete:()=>{App.stopPageLoading()}
                }); 

                event.preventDefault();
                return false;
            },
            validateFirst(){
                this.verror={}
                this.valid_second_stage = 0;

                var data = new FormData();
                var fields = ['pekerjaan','nama_perusahaan','alamat_perusahaan','no_telp','bentuk_usaha','bidang_usaha','jabatan','masa_kerja'] ;
                fields.forEach((field,b)=>{
                    let xfield = field;
                    if(field == 'jabatan'){
                        xfield = 'jabatan_perusahaan';
                    }
                    if(field == 'masa_kerja'){
                        xfield = 'masa_kerja_perusahaan';
                    }
                    if(field == 'no_telp'){
                        xfield = 'no_telp_perusahaan';
                    }
                    if(typeof this.$data[field] == 'undefined')
                        this.$data[field] = '';

                    data.append(field,this.$data[xfield]);
                });

                $.ajax({
                    url: site_url() + `app/konsumen/data_pekerjaan/insert_validation?is_ajax=true`,
                    data: data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    method: 'POST',
                    beforeSend:()=>{App.startPageLoading({animate:!0})},
                    type: 'POST', // For jQuery < 1.9
                    success: (res)=>{
                        res = JSON.parse($(res).text());
                            // console.log(res);

                        this.validateFirstPs(res.success);
                            
                        if(res.success){
                            this.valid_second_stage += 1;
                        }else{
                            for(k in res.error_fields){
                                 
                                if(k == 'jabatan'){
                                    res.error_fields.jabatan_perusahaan = res.error_fields[k];
                                }
                                if(k == 'masa_kerja'){
                                    res.error_fields.masa_kerja_perusahaan = res.error_fields[k];
                                }
                                if(k == 'no_telp'){
                                    res.error_fields.no_telp_perusahaan = res.error_fields[k];
                                }
                            }
                            this.setError(res.error_fields,'',true);
                            
                        } 
                    },
                    complete:()=>{App.stopPageLoading()}
                }); 

                event.preventDefault();
                return false;
            },

            validateSelf(){
                this.verror={}
                this.valid_first_stage = 0;

                var data = new FormData();
                var fields = ['nama_lengkap','tempat_lahir','tanggal_lahir','jenis_identitas','identitas','alamat_rumah','kelurahan','kecamatan','kota','propinsi','status_pernikahan','pass_foto','doc_identitas','no_hp','email','npwp','jml_anak','alamat_penagihan','kelurahan_penagihan','kecamatan_penagihan','kota_penagihan','propinsi_penagihan'] ;
                this.$data.tanggal_lahir =  $('input[name=tanggal_lahir]').val() ;
                fields.forEach((field,b)=>{
                    if(typeof this.$data[field] == 'undefined')
                        this.$data[field] = '';
                    if(field=='tanggal_lahir'){
                        data.append(field,toMysqlDateStr(this.$data[field]));

                    }else{
                        data.append(field,this.$data[field]);

                    }
                });
                
                // data.append('tanggal_lahir',)
                console.log();

                $.ajax({
                    url: site_url() + `app/konsumen/index/insert_validation?is_ajax=true`,
                    data: data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    method: 'POST',
                    beforeSend:()=>{App.startPageLoading({animate:!0})},
                    type: 'POST', // For jQuery < 1.9
                    success: (res)=>{
                        res = JSON.parse($(res).text());
                            // console.log(res);

                        this.validatePs(res.success);
                            
                        if(res.success){
                            this.valid_first_stage += 1;
                        }else{
                            this.setError(res.error_fields,'',true);
                            
                        } 
                    },
                    complete:()=>{App.stopPageLoading()}
                }); 

                event.preventDefault();
                return false;
            },
            cancelForm(){
                // document.location.href = site_url() + `konsumen`
                $('#form_add_konsumen').fadeOut();
                $('.customDTContainer,.datatables-add-button,.form_subtitle').fadeIn();
            },
            validatePs(){
                // this.verror={}
                   
                var data = new FormData();
                var fields = ['nama_lengkap','tempat_lahir','tanggal_lahir','jenis_identitas','identitas','pass_foto','doc_identitas','no_hp'] ;
                fields.forEach((field,b)=>{
                    if(typeof this.$data[field] == 'undefined')
                        this.$data[field] = '';
                    let form_key = 'ps_'+field;
                    if(field == 'pass_foto' || field == 'doc_identitas'){
                        field += '_ps';
                        this.$data[form_key] = $(`input[name=${field}]`).val();
                    }
                    if(field=='tanggal_lahir'){
                        let ps_tanggal_lahir = $('input[name=ps_tanggal_lahir]').val();
                        data.append(field,toMysqlDateStr(ps_tanggal_lahir));

                    }else{
                        data.append(field,this.$data[field]);

                    }
                });

                $.ajax({
                    url: site_url() + `app/konsumen/data_pasangan/insert_validation?is_ajax=true`,
                    data: data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    method: 'POST',
                    beforeSend:()=>{App.startPageLoading({animate:!0})},
                    type: 'POST', // For jQuery < 1.9
                    success: (res)=>{
                        res = JSON.parse($(res).text());
                            // console.log(res);
                        this.validateKl(res.success);
                        if(res.success){
                           this.valid_first_stage += 1;
                        }else{
                            
                            this.setError(res.error_fields,'ps',false);
                        } 
                    },
                    complete:()=>{App.stopPageLoading()}
                }); 

                event.preventDefault();
                return false;
            },
            validateKl(){
                // this.verror={}
                   
                var data = new FormData();
                var fields = ['nama_lengkap','alamat','no_hp','hubungan'] ;
                fields.forEach((field,b)=>{
                    if(typeof this.$data[field] == 'undefined')
                        this.$data[field] = '';
                    data.append(field,this.$data['kl_'+field]);
                });

                $.ajax({
                    url: site_url() + `app/konsumen/data_keluarga_dekat/insert_validation?is_ajax=true`,
                    data: data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    method: 'POST',
                    beforeSend:()=>{App.startPageLoading({animate:!0})},
                    type: 'POST',  
                    success: (res)=>{
                        res = JSON.parse($(res).text());
                            // console.log(res);
                        if(res.success){
                            this.valid_first_stage += 1;
                            if(this.valid_first_stage==3){
                                this.goto2ndForm();
                            }
                        }else{
                            
                            this.setError(res.error_fields,'kl',false);
                        } 
                    },
                    complete:()=>{App.stopPageLoading()}
                }); 

                event.preventDefault();
                return false;
            },
            initFileUpload( ){
                /**************************************************************/
                let _self = this;
                let extra_cls =  '';
                $('.gc-file-upload'+extra_cls).each(function(a,b){
                    var hasFileUpload = $(this).attr('hasFileUpload') == 'yes';
                    if(hasFileUpload){
                        // console.log('skipping gc-file-upload '+a)
                        return;
                    }
                    $(this).attr('hasFileUpload','yes'); 
                    var unique_id   = $(this).attr('id');
                    var uploader_url = $(this).attr('rel');
                    var uploader_element = $(this);
                    var x_field_name = uploader_element.next().attr('name');
                    var delete_url  = $('#delete_url_'+unique_id).attr('href');
                    eval("var file_upload_info = upload_info_"+unique_id+"");
                    eval("var string_upload_file = string_upload_file_"+unique_id+"");
                    eval("var string_progress = string_progress_"+unique_id+"");
                    eval("var error_on_uploading = error_on_uploading_"+unique_id+"");
                    eval("var message_prompt_delete_file = message_prompt_delete_file_"+unique_id+"");
                    eval("var error_max_number_of_files = error_max_number_of_files_"+unique_id+"");
                    eval("var error_accept_file_types = error_accept_file_types_"+unique_id+"");
                    eval("var error_max_file_size = error_max_file_size_"+unique_id+"");
                    eval("var error_min_file_size = error_min_file_size_"+unique_id+"");
                    eval("var base_url = base_url_"+unique_id+"");
                    eval("var upload_a_file_string = upload_a_file_string_"+unique_id+"");
                    eval("var string_delete_file = string_delete_file_"+unique_id+"");
                    // console.log(unique_id,uploader_url,uploader_element,delete_url,file_upload_info,x_field_name)
                    $(this).fileupload({
                        dataType: 'json',
                        url: uploader_url,
                        cache: false,
                        acceptFileTypes:  file_upload_info.accepted_file_types,
                        beforeSend: function(){
                            let verror = Object.assign({},_self.$data.verror);
                                if(x_field_name.match(/_ps$/)){
                                    x_field_name = x_field_name.replace(/(\w+)(_)(ps)$/,'$3$2$1');
                                }
                                verror[x_field_name] = false;
                                _self.$data.verror = verror;
                            $('#upload-state-message-'+unique_id).html(string_upload_file);
                            $("#loading-"+unique_id).show();
                            $("#upload-button-"+unique_id).slideUp("fast");
                        },
                        limitMultiFileUploads: 1,
                        maxFileSize: file_upload_info.max_file_size,            
                        send: function (e, data) {                      
                            
                            var errors = '';
                            
                            if (data.files.length > 1) {
                                errors += error_max_number_of_files + "\n" ;
                            }
                            
                            $.each(data.files,function(index, file){
                                if (!(data.acceptFileTypes.test(file.type) || data.acceptFileTypes.test(file.name))) {
                                    errors += error_accept_file_types + "\n";
                                }
                                if (data.maxFileSize && file.size > data.maxFileSize) {
                                    errors +=  error_max_file_size + "\n";
                                }
                                if (typeof file.size === 'number' && file.size < data.minFileSize) {
                                    errors += error_min_file_size + "\n";
                                }                           
                            }); 
                            
                            if(errors != '')
                            {
                                // swal(errors);
                                let verror = Object.assign({},_self.$data.verror);
                                verror[x_field_name] = errors;
                                _self.$data.verror = verror;
                                return false;
                            }
                            
                            return true;
                        },
                        done: function (e, data) {
                            if(typeof data.result.success != 'undefined' && data.result.success)
                            {
                                $("#loading-"+unique_id).hide();
                                $("#progress-"+unique_id).html('');
                                $.each(data.result.files, function (index, file) {
                                    $('#upload-state-message-'+unique_id).html('');
                                    var field_name = uploader_element.attr('name');
                                    $("input[rel="+field_name+"]").val(file.name);
                                    $("input[rel="+field_name+"]").trigger('change');
                                    _self.$data[x_field_name] = file.name;
                                    // console.log(file.name);
                                    var file_name = file.name;
                                    
                                    var is_image = (file_name.substr(-4) == '.jpg'  
                                                        || file_name.substr(-4) == '.png' 
                                                        || file_name.substr(-5) == '.jpeg' 
                                                        || file_name.substr(-4) == '.gif' 
                                                        || file_name.substr(-5) == '.tiff')
                                        ? true : false;
                                    if(is_image)
                                    {
                                        $('#file_'+unique_id).addClass('image-thumbnail');
                                        load_fancybox($('#file_'+unique_id));
                                        $('#file_'+unique_id).html('<img src="'+fixFileUrl(file.url)+'" height="50" />');
                                    }
                                    else
                                    {
                                        $('#file_'+unique_id).removeClass('image-thumbnail');
                                        $('#file_'+unique_id).unbind("click");
                                        $('#file_'+unique_id).html(file_name);
                                    }
                                    
                                    $('#file_'+unique_id).attr('href',file.url);
                                    $('#hidden_'+unique_id).val(file_name);

                                    $('#success_'+unique_id).fadeIn('slow');
                                    $('#delete_url_'+unique_id).attr('rel',file_name);
                                    $('#upload-button-'+unique_id).slideUp('fast');
                                });
                            }
                            else if(typeof data.result.message != 'undefined')
                            {
                                swal(data.result.message);
                                show_upload_button(unique_id, uploader_element);
                            }
                            else
                            {
                                swal(error_on_uploading);
                                show_upload_button(unique_id, uploader_element);
                            }
                        },
                        autoUpload: true,
                        error: function()
                        {
                            swal(error_on_uploading);
                            show_upload_button(unique_id, uploader_element);
                        },
                        fail: function(e, data)
                        {
                            // data.errorThrown
                            // data.textStatus;
                            // data.jqXHR;              
                            swal(error_on_uploading);
                            show_upload_button(unique_id, uploader_element);
                        },          
                        progress: function (e, data) {
                            $("#progress-"+unique_id).html(string_progress + parseInt(data.loaded / data.total * 100, 10) + '%');
                        }           
                    });
                    $('#delete_'+unique_id).click(function(){
                        swal({
                          title: "Konfirmasi",
                          text: message_prompt_delete_file,
                          type: "warning",
                          showCancelButton: true,
                          cancelButtonText:'Batal',
                          confirmButtonClass: "btn-danger",
                          confirmButtonText: "Ya",
                          closeOnConfirm: true
                        },
                        function(){
                            var file_name = $('#delete_url_'+unique_id).attr('rel');
                            $.ajax({
                                url: delete_url+"/"+file_name,
                                cache: false,
                                success:function(){
                                    show_upload_button(unique_id, uploader_element);
                                    _self.$data[x_field_name] = '';
                                },
                                beforeSend: function(){
                                    $('#upload-state-message-'+unique_id).html(string_delete_file);
                                    $('#success_'+unique_id).hide();
                                    $("#loading-"+unique_id).show();
                                    $("#upload-button-"+unique_id).slideUp("fast");
                                }
                            });
                        });
                         
                 
                        return false;
                    });         
                    
                });
                /**************************************************************/
            },
            resetFormData(){
                //"id_data_konsumen",
                this.$data.verror={};
                const dd = ["dd_propinsi","dd_kecamatan","dd_kota","dd_kelurahan","dd_propinsi_penagihan",
                "dd_kecamatan_penagihan","dd_kota_penagihan","dd_kelurahan_penagihan"];

                dd.forEach((item,index)=>{
                    this.$data[item] = [];
                });
                this.$data.pilih_alamat_penagihan = 'ya';
                this.$data.valid_first_stage = 0;
                this.$data.valid_second_stage = 0;

                // Constant Values 
                // "dd_pekerjaan"
         
                
                // Empty String Value
                const str_values = ["nama_lengkap","tempat_lahir","tanggal_lahir","jenis_identitas","identitas","alamat_rumah",
                "kelurahan","kecamatan","kota","propinsi","status_pernikahan","pass_foto","doc_identitas","no_hp",
                "email","npwp","alamat_penagihan","kelurahan_penagihan","kecamatan_penagihan",
                "kota_penagihan","propinsi_penagihan","ps_nama_lengkap","ps_tempat_lahir","ps_tanggal_lahir","ps_no_hp","ps_jenis_identitas","ps_identitas",
                "ps_pass_foto","ps_doc_identitas","kl_nama_lengkap","kl_no_hp","kl_hubungan","kl_alamat","pekerjaan","nama_perusahaan","alamat_perusahaan",
                "no_telp_perusahaan","bentuk_usaha","bidang_usaha","jabatan_perusahaan","masa_kerja_perusahaan","ps_pekerjaan","ps_nama_perusahaan",
                "ps_alamat_perusahaan","ps_no_telp_perusahaan","ps_bentuk_usaha","ps_bidang_usaha","ps_jabatan_perusahaan","ps_masa_kerja_perusahaan","tabungan_bank","deposito_bank","rumah_an","rumah_nilai","kendaraan_an"];
                
                str_values.forEach((item,index)=>{
                    this.$data[item] = '';
                });
                // Integer Value
                const int_values = ["jml_anak","kendaraan_nilai","penghasilan_utama","penghasilan_tambahan","penghasilan_pasangan","penghasilan_tambahan_pasangan","total_penghasilan","tabungan_nilai","deposito_nilai"];
                int_values.forEach((item,index)=>{
                    this.$data[item] = 0;
                });
            },
        }
    });
});
})