<?php
require_once APPPATH . '/models/Grocery_CRUD_Model.php';
class M_sppr  extends Grocery_CRUD_Model  {
    var $table = 'tr_sppr'; 
    var $table_name = 'tr_sppr'; 
    public $_currency_fields  = ['bt_jalan_utama','bt_fasum_fasos','total_bt',
                              'klb_tanah_harga_per_m2','total_bhook',
                              'tanda_jadi','total_harga_jual',
                              'dp_um_1','dp_um_2','dp_um_3','dp_um_4','total_uang_muka',
                              'nilai_cicilan_indent','total_cicilan_indent',
                              'sh_kewajiban','sh_total'];
    function get_currency_fields(){
        return $this->_currency_fields;
    }
    function get_edit_values($primary_key_value)
    {
        $primary_key_field = $this->get_primary_key();
        $this->db->where('a.'.$primary_key_field,$primary_key_value);
                                     // gp.nama_proyek perumahan,
                                    // u.nama_blok_kavling,u.nomor u_nomor,u.tipe,u.status,
        $result = $this->db->select("a.*,
                                     
                                     k.nama_lengkap konsumen,
                                     k.identitas nik,
                                     k.no_hp nomor_hp,
                                     kp.pekerjaan,
                                     k.status_pernikahan,
                                     k.email,
                                     k.alamat_rumah alamat,
                                     gp.alamat_proyek alamat_perumahan,
                                     gp.nama_proyek perumahan,
                                     u.tipe unit_tipe,
                                     u.nama_blok_kavling unit_blok_kavling,
                                     u.luas_tanah unit_luas_tanah,
                                     u.luas_bangunan unit_luas_bangunan,
                                     u.harga_jual unit_harga_jual,
                                     u.uang_muka unit_uang_muka,
                                     ptj.status_pmb ptj_status_pmb,
                                     pum.status_pmb pum_status_pmb,
                                     pc.status_pmb pc_status_pmb,
                                     ks.uang_administrasi biaya_adm
                                     ")
                 // ->join('tb_users usr','usr.id_user=a.id_user','left')
                 ->join('tb_unit_perumahan u','u.id_unit=a.id_unit','left')
                 ->join('tb_grup_proyek gp','gp.id_grup_proyek=a.id_grup_proyek','left')
                 ->join('tb_klausul_sppr ks','ks.id_klausul_sppr=gp.id_klausul_sppr','left')
                 ->join('tb_data_konsumen k','k.id_data_konsumen=a.id_konsumen','left')
                 ->join('tb_data_pekerjaan_konsumen kp','kp.id_data_konsumen=a.id_konsumen','left')
                 ->join('pmb_tanda_jadi ptj','ptj.id_tr_sppr=a.id','left')
                 ->join('pmb_uang_muka pum','pum.id_tr_sppr=a.id','left')
                 ->join('pmb_cicilan pc','pc.id_tr_sppr=a.id','left')
                 ->get($this->table_name.' a')->row();
        return $result;
    }
    function get_active_masa_indent(){
        $current_dt = date('Y-m-d');
        $row = $this->db->select('id,nominal_indent,tgl_berlaku,tgl_berakhir,tgl_entry tgl_buat')
                                   ->where("DATE('{$current_dt}') >= tgl_berlaku  AND DATE('{$current_dt}') <= tgl_berakhir ")
                                   ->order_by('tgl_entry','desc')->get('ms_indent')
                                   
                                   ->row();
        return $row;
    }
    private function _get_datatables_query()
    {
        $this->db->select("a.*,gp.nama_proyek perumahan,
                           
                           u.nama_blok_kavling,u.nomor u_nomor,u.tipe,u.status_available,
                           k.nama_lengkap,k.identitas,k.no_hp nomor_hp,
                           usr.name dibuat_oleh,
                           k.email,k.status_pernikahan,kp.pekerjaan")
                 ->join('tb_users usr','usr.id_user=a.id_user','left')
                 ->join('tb_unit_perumahan u','u.id_unit=a.id_unit','left')
                 ->join('tb_grup_proyek gp','gp.id_grup_proyek=a.id_grup_proyek','left')
                 ->join('tb_data_konsumen k','k.id_data_konsumen=a.id_konsumen','left')
                 ->join('tb_data_pekerjaan_konsumen kp','kp.id_data_konsumen=a.id_konsumen','left')
                 ->join('pmb_tanda_jadi ptj','ptj.id_tr_sppr=a.id','left')
                 ->join('pmb_uang_muka pum','pum.id_tr_sppr=a.id','left')
                 ->join('pmb_cicilan pc','pc.id_tr_sppr=a.id','left')
                 ->from($this->table . ' a');
        $i = 0;
        $search_queries = $_POST['search']['value'];
        $search_queries = explode(',', $search_queries);
        if(count($search_queries) > 0){
            foreach ($this->column_search as $item) // looping awal
            {
                if($_POST['search']['value']) // jika datatable mengirimkan pencarian dengan metode POST
                {
                    if($i===0) // looping awal
                    {
                        $this->db->group_start(); 
                        $this->db->like($item, $_POST['search']['value']);
                    }
                    else
                    {
                        $this->db->or_like($item, $_POST['search']['value']);
                    }
                    if(count($this->column_search) - 1 == $i) 
                        $this->db->group_end(); 
                }
                $i++;
            }
        }
        if(isset($_POST['order'])) 
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
}
