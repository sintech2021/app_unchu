<?php  defined('BASEPATH') OR exit('No direct script access allowed');
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Exception\ParseException;
class Sppr extends Theme_Controller {
	public $_page_title = 'Daftar SPPR';
    public $_crud = null;
	public function custom_grid_data()
    {
        $this->load->model('account/m_login');
        $this->load->model('m_sppr','model');
        $list = $this->model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $action = "<td class=\"actions\"> 
            <div class='btn-group'>  
            <a href=\"".site_url('transaksi/sppr/index/edit/'.$field->id)."/".slugify($field->nama_blok_kavling)."\" class=\"gc-bt-edit btn btn-sm btn-icon btn-pure btn-info\" role=\"button\"> <i class=\"fa fa-edit\" aria-hidden=\"true\"></i> 
            </a> 
            <a href=\"".site_url('transaksi/sppr/print/'.$field->id)."/".slugify($field->nama_blok_kavling)."\" class=\"gc-bt-edit btn btn-sm btn-icon btn-pure btn-info\" role=\"button\"> <i class=\"fa fa-print\" aria-hidden=\"true\"></i> 
            </a>"."
            <button onclick=\"javascript: return delete_row('".site_url('transaksi/sppr/index/delete/'.$field->id)."',-1,this)\" href=\"javascript:;\" class=\"gc-bt-delete delete_button btn btn-sm btn-icon btn-pure btn-danger\" role=\"button\"> <i class=\"fa fa-trash\" aria-hidden=\"true\"></i> 
            </button>
            </div>
            </td>";
            $no++;
            $row = array();
            $field->nomor_sppr  = sprintf('%05d',$field->id);
            $row[] = $no;
            $row[] = $field->nama_lengkap;
            $row[] = $this->load->view('_sppr/row_data_konsumen',$field,true);
    		$row[] = $this->load->view('_sppr/row_perumahan',$field,true);
    		$row[] = $this->load->view('_sppr/row_sppr_konsumen',$field,true);
    		$row[] = $this->load->view('_sppr/row_sppr_rtb',$field,true);
            $row[] = $action; 
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->model->count_all(),
            "recordsFiltered" => $this->model->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }
    public function custom_grid_ajax()
    {
        $tdata = ['add_url' => site_url('transaksi/sppr/index/add')];
        $tdata['is_admin'] = $this->cms_user_group() == 'admin';
        $this->load->view('_transaksi/dt_sppr.php',$tdata);
    }
    private function _customGrid(){
        $dump = file_get_contents(APP . '/config/gc.yml');
        $data = Yaml::parse($dump);
        $tdata = ['add_url' => site_url('transaksi/sppr/index/add')];
        $data['output'] = $this->load->view('_transaksi/dt_sppr.php',$tdata,true);
        $data['unique_hash'] = md5(date('YmdHis-Unit'));
        $data['is_admin']  = $this->cms_user_group()=='admin';
        $this->view('_transaksi/sppr.php', $data );
    }
    public function index()
    {
        $jadwal_uang_muka_session = '';
        $form_mode_session = $this->_create_form_mode_session($jadwal_uang_muka_session);

        $target_yaml = APP . '/form/tr_sppr.yml';
        $buffer = file_get_contents($target_yaml);
        $_SERVER['FORM_DEF'] = Yaml::parse($buffer);
        $crud = $this->new_crud();
        $crud->set_subject('SPPR');
        $state = $crud->getState();
        switch ($state) {
            case 'list':
                $args = func_get_args();
                return call_user_func_array([$this,'_customGrid'], $args);
                break;
            default:
                # code...
                break;
        }
        $crud->unset_jquery();
        // $crud->unset_export();
        if (! $this->input->is_ajax_request()) {
            $crud->set_theme_config(['crud_paging' => true ]);
        }
        $crud->set_table('tr_sppr');
		$crud->set_model('m_sppr');
		$crud->set_theme('datatables');
        $crud->field_type('tgl_entry','hidden');
        $crud->field_type('tgl_update','hidden');
        $crud->field_type('user_id','hidden');
        $crud->callback_before_insert(array($this,'_set_tgl_entry'));
        $crud->callback_before_update(array($this,'_set_tgl_update'));
    	$id_user = $this->cms_user_id();
		$state = $crud->getState();
    	$state_info = $crud->getStateInfo();
    	$id_user = $this->cms_user_id();
    	$crud->fields('nomor',
    		'konsumen','id_konsumen',
            'nik','nomor_hp','email','alamat',
    		'id_grup_proyek',
    		'id_unit',
            'alamat_perumahan','unit_tipe','unit_luas_tanah','unit_luas_bangunan','unit_harga_jual',
    		'diskon','metode_pembelian','mp_cicilan',
    		'bt_jalan_utama','bt_fasum_fasos','total_bt','total_bt_ro','klb_tanah_ukuran','klb_tanah_harga_per_m2',
    		'total_bhook','total_bhook_ro','tanggal_transaksi','termasuk_tj','tanda_jadi','total_harga_jual','total_harga_jual_ro','jadwal_uang_muka','dp_um_1','dp_um_2','dp_um_3','dp_um_4','total_uang_muka','total_uang_muka_ro','masa_indent','nilai_cicilan_indent','total_cicilan_indent','total_cicilan_indent_ro','sh_dibayar','sh_kewajiban','sh_total','sh_total_ro','rtb_tanda_jadi','rtb_uang_muka','rtb_cicilan','rtb_pelunasan','tgl_entry','tgl_update','id_user');
    	
        $crud->display_as('termasuk_tj','Termasuk Tanda Jadi');
        $crud->field_type('id_user','hidden');
    	$crud->field_type('alamat','text');
    	$crud->unset_texteditor('alamat');
    	$crud->field_type('alamat_perumahan','text');
    	$crud->unset_texteditor('alamat_perumahan');
    	$crud->display_as('nik','NIK/SIM/Passport');
    	$crud->display_as('nomor_hp','Nomor HP');
    	$crud->display_as('alamat_perumahan','Alamat');
    	$crud->display_as('unit_harga_jual','Harga Jual Standar');
    	$crud->display_as('unit_tipe','Tipe');
    	$crud->display_as('unit_luas_tanah','Luas Tanah');
    	$crud->display_as('unit_luas_bangunan','Luas Bangunan');
    	$crud->display_as('mp_cicilan','Jumlah Cicilan');


    	$crud->field_type('jadwal_uang_muka','input');
    	// $crud->field_type('masa_indent','dropdown',['< 1'=>'< 1 Bulan','6'=>'6 Bulan','12'=>'12 Bulan','18'=>'18 Bulan','24'=>'24 Bulan']);
    	$crud->field_type('masa_indent','dropdown',['12'=>'12','18'=>'18','24'=>'24','all_in'=>'All In']);
    	$crud->field_type('jadwal_uang_muka','dropdown',['1'=>'1','2'=>'2','3'=>'3','4'=>'4']);
    	$crud->field_type('metode_pembelian','dropdown',['cash_keras'=>'Cash Keras','cash_bertahap'=>'Cash Bertahap','cicilan_120_x'=>'Cicilan 120 x'/*, 'custom' => 'Cicilan ...'*/]);
    	$crud->set_v_model('metode_pembelian');
    	$crud->set_v_change('metode_pembelian', 'onMetodePembelianChange()');
    	$crud->set_v_model('mp_cicilan');
    	$crud->set_v_model('diskon');
    	$crud->set_v_model('bt_jalan_utama');
    	$crud->set_v_model('bt_fasum_fasos');
    	$crud->set_v_change('bt_jalan_utama','calculateBt()');
    	$crud->set_v_change('bt_fasum_fasos','calculateBt()');
    	$crud->set_v_keyup('bt_jalan_utama','calculateBt()');
    	$crud->set_v_keyup('bt_fasum_fasos','calculateBt()');

    	$crud->set_v_model('total_bt');
    	$crud->set_v_model('total_bt_ro','total_bt');
    	$crud->set_v_model('total_bhook');
    	$crud->set_v_model('total_bhook_ro','total_bhook');

    	$crud->set_v_model('klb_tanah_ukuran');
    	$crud->set_v_model('klb_tanah_harga_per_m2');
    	$crud->set_v_change('klb_tanah_ukuran','calculateBHook()');
    	$crud->set_v_change('klb_tanah_harga_per_m2','calculateBHook()');
    	$crud->set_v_keyup('klb_tanah_ukuran','calculateBHook()');
    	$crud->set_v_keyup('klb_tanah_harga_per_m2','calculateBHook()');


    	$crud->display_as('dp_um_1','DP 1');
    	$crud->display_as('dp_um_2','DP 2');
        $crud->display_as('dp_um_3','DP 3');
    	$crud->display_as('dp_um_4','DP 4');
    	$crud->display_as('nomor','Nomor SPPR');
		$crud->display_as('id_konsumen','ID Konsumen');
		$crud->display_as('id_grup_proyek','Perumahan');
		$crud->display_as('id_unit','Unit');
		$crud->display_as('diskon','Diskon(Jika Ada)');
		$crud->display_as('metode_pembelian','Metode Pembelian');
		$crud->display_as('bt_jalan_utama','Depan Jalan Utama');
		$crud->display_as('bt_fasum_fasos','Depan Fasum/Fasos');

		$crud->display_as('total_bt','Total Biaya Tambahan');
		$crud->display_as('total_bt_ro','Total Biaya Tambahan');
		
		$crud->display_as('klb_tanah_ukuran','Ukuran');
		$crud->display_as('klb_tanah_harga_per_m2','Harga Per M2');

		$crud->display_as('total_bhook','Total Biaya Hook');
		$crud->display_as('total_bhook_ro','Total Biaya Hook');
		
		$crud->display_as('tanggal_transaksi','Tanggal Transaksi');
		$crud->display_as('tanda_jadi','Tanda Jadi');
		
		$crud->display_as('total_harga_jual','Total Harga Jual');
		$crud->display_as('total_harga_jual_ro','Total Harga Jual');

		$crud->display_as('jadwal_uang_muka','Jadwal Uang Muka(Bulan)');
		$crud->display_as('masa_indent','Masa Indent');
		$crud->display_as('nilai_cicilan_indent','Nilai Cicilan Indent(Bulan)');
		$crud->display_as('total_cicilan_indent','Total Cicilan Indent');
		$crud->display_as('total_cicilan_indent_ro','Total Cicilan Indent');
		$crud->display_as('sh_dibayar','Dibayar');
		$crud->display_as('sh_kewajiban','Kewajiban/Bulan');
		
		$crud->display_as('sh_total','Total Sisa Hutang');
		$crud->display_as('sh_total_ro','Total Sisa Hutang');

		$crud->display_as('rtb_tanda_jadi','Tanda Jadi');
		$crud->display_as('rtb_uang_muka','Bayar Uang Muka');
		$crud->display_as('rtb_cicilan','Bayar Cicilan');
		$crud->display_as('rtb_cicilan','Bayar Cicilan');
		$crud->display_as('rtb_pelunasan','Bayar Pelunasan');
		$crud->display_as('dp_uang_muka','DP');

		$crud->display_as('total_uang_muka_ro','Total Uang Muka');
		$crud->display_as('total_uang_muka','Total Uang Muka');

		// $crud->set_rules('nomor','Nomor','trim|required');
        $jadwal_uang_muka_session_int = (int)$jadwal_uang_muka_session;
        // echo $jadwal_uang_muka_session_int;
        // exit();

        if($form_mode_session == 'cash_keras'){

        }else if($form_mode_session == 'cash_bertahap'){
            $crud->set_rules('sh_dibayar','Dibayar','trim|required');
            $crud->set_rules('sh_kewajiban','Kewajiban/Bulan','trim|required');
            $crud->set_rules('sh_total','Total Sisa Hutang','trim|required');

            $crud->set_rules('jadwal_uang_muka','Jadwal Uang Muka','trim|required');
            $crud->set_rules('total_uang_muka','Total Uang Muka','trim|required');
            
            for($i=1;$i<=$jadwal_uang_muka_session_int;$i++){
                $crud->set_rules( 'dp_um_'.$i , 'DP '.$i,'trim|required');
            }

            $crud->set_rules('rtb_uang_muka','Bayar Uang Muka','trim|required');
        }else if($form_mode_session == 'cicilan_120_x'){
            $crud->set_rules('jadwal_uang_muka','Jadwal Uang Muka','trim|required');
            $crud->set_rules('total_uang_muka','Total Uang Muka','trim|required');
            $crud->set_rules('masa_indent','Masa Indent','trim|required');
            $crud->set_rules('nilai_cicilan_indent','Nilai Cicilan Indent','trim|required');
            $crud->set_rules('total_cicilan_indent','Total Cicilan Indent','trim|required');
            $crud->set_rules('sh_dibayar','Dibayar','trim|required');
            $crud->set_rules('sh_kewajiban','Kewajiban/Bulan','trim|required');
            $crud->set_rules('sh_total','Total Sisa Hutang','trim|required');

            $crud->set_rules('rtb_uang_muka','Bayar Uang Muka','trim|required');
            for($i=1;$i<=$jadwal_uang_muka_session_int;$i++){
                $crud->set_rules( 'dp_um_'.$i , 'DP '.$i,'trim|required');
            }
        }else{
            $crud->set_rules('jadwal_uang_muka','Jadwal Uang Muka','trim|required');
            $crud->set_rules('total_uang_muka','Total Uang Muka','trim|required');
            $crud->set_rules('masa_indent','Masa Indent','trim|required');
            $crud->set_rules('nilai_cicilan_indent','Nilai Cicilan Indent','trim|required');
            $crud->set_rules('total_cicilan_indent','Total Cicilan Indent','trim|required');
            $crud->set_rules('sh_dibayar','Dibayar','trim|required');
            $crud->set_rules('sh_kewajiban','Kewajiban/Bulan','trim|required');
            $crud->set_rules('sh_total','Total Sisa Hutang','trim|required');
            $crud->set_rules('rtb_uang_muka','Bayar Uang Muka','trim|required');

            for($i=1;$i<=$jadwal_uang_muka_session_int;$i++){
                $crud->set_rules( 'dp_um_'.$i , 'DP '.$i,'trim|required');
            }

        }
		$crud->set_rules('id_konsumen','Konsumen','trim|required');
		$crud->set_rules('id_grup_proyek','Perumahan','trim|required');
		$crud->set_rules('id_unit','Unit','trim|required');
		$crud->set_rules('diskon','Diskon','trim|required');
		$crud->set_rules('metode_pembelian','Metode Pembelian','trim|required');
		$crud->set_rules('bt_jalan_utama','Depan Jalan Utama','trim|required');
		$crud->set_rules('bt_fasum_fasos','Depan Fasum/Fasos','trim|required');
		$crud->set_rules('total_bt','Total Biaya Tambahan','trim|required');
		$crud->set_rules('klb_tanah_ukuran','Ukuran','trim|required');
		$crud->set_rules('klb_tanah_harga_per_m2','Harga Per M2','trim|required');
		$crud->set_rules('total_bhook','Total Biaya Hook','trim|required');
		$crud->set_rules('tanggal_transaksi','Tanggal Transaksi','trim|required');
		$crud->set_rules('tanda_jadi','Tanda Jadi','trim|required');
		$crud->set_rules('total_harga_jual','Total Harga Jual','trim|required');

		$crud->set_rules('rtb_tanda_jadi','Tanda Jadi','trim|required');
		$crud->set_rules('rtb_cicilan','Bayar Cicilan','trim|required');
		$crud->set_rules('rtb_pelunasan','Bayar Pelunasan','trim|required');

		$crud->callback_after_insert(array($this, '_update_nomor'));

    	$crud->set_view_file_base('modules/transaksi/views/_gc_sppr');

    	$loops = ['tanda_jadi','total_harga_jual','jadwal_uang_muka','dp_um_1','dp_um_2','dp_um_3','dp_um_4','total_uang_muka','masa_indent','nilai_cicilan_indent','total_cicilan_indent','sh_dibayar','sh_kewajiban','sh_total',
            'nomor',
            // 'nik','nomor_hp','email','alamat'
        ];
    	$ro_fields = ['total_harga_jual',
				'total_bt','total_bhook','total_uang_muka',
				'total_cicilan_indent','sh_total',
                // ro fields

            ];
    	foreach ($loops as $field_name) {
    		$crud->set_v_model($field_name);
    		if(in_array($field_name, $ro_fields)){
    			$crud->set_v_model("{$field_name}_ro",$field_name);
    		}
    	}
    	
    	
    	foreach (['masa_indent','nilai_cicilan_indent','total_cicilan_indent'] as $field_name) {
    		$crud->set_v_change($field_name,'calculateTotalCicilanIndent()');
    		if($field_name != 'masa_indent'){
	    		$crud->set_v_keyup($field_name,'calculateTotalCicilanIndent()');
    		}
    	}
    	$crud->set_first_dropdown('jadwal_uang_muka',[''=>'-- Pilih Jadwal --']);
    	$crud->set_v_change('jadwal_uang_muka', 'onJadwalUmChange()');
    	foreach (['dp_um_1','dp_um_2','dp_um_3','dp_um_4'] as $field_name) {
    		$crud->set_v_change($field_name,'calculateTotalUm()');
	    	$crud->set_v_keyup($field_name,'calculateTotalUm()');
    	}
    	foreach (['tanda_jadi','diskon','mp_cicilan'] as $field_name) {
	    	$crud->set_v_change($field_name,'calculateTHJ()');
		    $crud->set_v_keyup($field_name,'calculateTHJ()');
    	}
    	foreach (['sh_dibayar','sh_kewajiban'] as $field_name) {
    		$crud->set_v_change($field_name,'calculateTSH()');
	    	$crud->set_v_keyup($field_name,'calculateTSH()');
    	}
    	foreach (['masa_indent'] as $field_name) {
            $crud->set_v_change($field_name,'calculateTotalCicilanIndent()');
        }
    	$crud->set_placeholder('konsumen','Ketik Nama Konsumen');
        
        $currency_fields = $this->get_currency_fields();
        $crud->set_currency_fields($currency_fields);

        $push_fields = [
            'jadwal_uang_muka',
        ];
        $crud->set_push_fields($push_fields);
        
        $data = $crud->render();
		$data->is_admin  = $this->cms_user_group()=='admin';
		$this->view('_transaksi/sppr.php',$data);
    }
    function get_currency_fields(){
        $this->load->model('m_sppr');
        return $this->m_sppr->get_currency_fields();
    }
    function _update_nomor($post_array, $pk){
    	$post_array['nomor'] = sprintf('%5d',$pk);
        $this->_init_record_pmb($post_array,$pk);

    	return $post_array;
    }
    function _set_tgl_entry($post_array){
        $dt = date('Y-m-d H:i:s');
        // $post_array['nomor'] = $dt;
        $post_array['tgl_entry'] = $dt;
        $post_array['tgl_update'] = $dt;
        $post_array['id_user'] = $this->cms_user_id();

        $this->fix_currency_fields($post_array);
        $this->_clear_form_mode_session();
        $this->_fix_dummy_fields($post_array);

        // SET status transaksi
        $post_array['status_transaksi'] = 'booking';
        $this->db->where('id_unit',$post_array['id_unit'])->update('tb_unit_perumahan',[
            'status_available' => '0',

        ]);
        return $post_array;
    }
    function fix_currency_fields(&$post_array){
        $currency_fields = $this->get_currency_fields();
        foreach ($currency_fields as $field_name) {
            $post_array[$field_name] = rupiah_to_int($post_array[$field_name]);
        }
    }
    function _set_tgl_update($post_array,$pk){
        $this->fix_currency_fields($post_array);
        
        $dt = date('Y-m-d H:i:s');
        $post_array['tgl_update'] = $dt;

        $this->_clear_form_mode_session();
        $this->_fix_dummy_fields($post_array);

        $this->_init_record_pmb($post_array,$pk);
        return $post_array;
    } 
    function _fix_dummy_fields(&$post_array){
        $dummy_fields = ['konsumen'];
        foreach ($dummy_fields as $field_name) {
            unset($post_array[$field_name]);
        }
    }
    function get_nomor_transaksi(){
    	$num_rows = $this->db->get('tr_sppr')->num_rows();
    	$nomor = sprintf('%05d',$num_rows+1);

    	echo json_encode(['success'=>true, 'nomor'=>$nomor]);
    }
    function get_active_masa_indent(){
        $this->load->model('m_sppr');
        $item = $this->m_sppr->get_active_masa_indent();
        echo json_encode(['success'=>true, 'item'=>$item]);

    }
    function set_form_mode_session($metode_pembelian=''){
        $this->session->set_userdata('sppr_form_mode_session',$metode_pembelian);
        $this->session->set_userdata('sppr_form_mode_session_jum',$this->input->get('jadwal_uang_muka'));
        echo json_encode(['success'=>true]);
    }
    function _create_form_mode_session(&$jadwal_uang_muka_session){
        $form_mode_session = $this->session->userdata('sppr_form_mode_session');
        $jadwal_uang_muka_session = $this->session->userdata('sppr_form_mode_session_jum');
        if(empty($form_mode_session)){
            $this->session->set_userdata('sppr_form_mode_session','');
            $form_mode_session = '';
        }
        if(empty($jadwal_uang_muka_session)){
            $this->session->set_userdata('sppr_form_mode_session_jum','');
            $jadwal_uang_muka_session = '';
        }
        return $form_mode_session;
    }
    function _clear_form_mode_session(){
        $this->session->unset_userdata('sppr_form_mode_session');
        $this->session->unset_userdata('sppr_form_mode_session_jum');

    }
    public function dd_unit_perumahan($id_grup_proyek="",$id_tr_sppr="")
    {

        $current_dt = date('Y-m-d');
        $this->db->where('a.id_grup_proyek',$id_grup_proyek);
        $where_date = "DATE('{$current_dt}') >= a.tanggal_berlaku  AND DATE('{$current_dt}') <= a.tanggal_berakhir";
        $where_not_in_use = "a.status_available = 1";
        if(!empty($id_tr_sppr)){
            $where_not_in_use = "({$where_not_in_use} OR b.id = '$id_tr_sppr')";

        }else{
            
        }
        $this->db->where($where_date)
                 ->where($where_not_in_use);
                 
        
        $list = $this->db->select("a.id_unit,a.nama_blok_kavling,a.nomor")
                         ->join('tr_sppr b','b.id_unit=a.id_unit','left')
                         ->get('tb_unit_perumahan a')->result_array();
        
        $last_query =  $this->db->last_query();
        // die($last_query);
        $dd = [['label'=>'-- Pilih Unit --','value'=>'']];

        foreach ($list as $row) {
            $dd[] = ['label'=>'Blok '.$row['nama_blok_kavling'].'/No. '.$row['nomor'],'value'=> $row['id_unit']];
        }

        echo json_encode(['data'=>$dd, 'success'=>true, 'sql'=> $last_query]);
    }

    function print($id){
       $this->load->model('m_sppr');
       $row = $this->m_sppr->get_edit_values($id);
       
       if(is_object($row)){
            $row->booking_um_standar = (int)$row->unit_uang_muka;
            $row->booking_fee_with_adm = (int)$row->tanda_jadi;
       
            set_um_date($row);
       }
       $data = [
            'sppr'=>$row
       ];
       $html = $this->load->view('_sppr/print_pdf',$data,true);
       // echo $html;
       // return;
       $dompdf = new \Dompdf\Dompdf();  //if you use namespaces you may use new \DOMPDF()
       $dompdf->set_paper('a4','portrait');
       $dompdf->load_html($html);
       $dompdf->render();
       $dompdf->stream("SPPR.pdf");
    }

    function _init_record_pmb($post_array,$primary_key){
        $this->_create_ptj_record($post_array,$primary_key);
        // $this->_create_pum_record($post_array,$primary_key);
        // $this->_create_pc_record($post_array,$primary_key);
    }
    function _create_ptj_record($post_array,$primary_key){
        $this->load->model('pembayaran/m_tanda_jadi');
        $this->m_tanda_jadi->create_from_tr($post_array,$primary_key);
    }
    function _create_pum_record($post_array,$primary_key){
        $this->load->model('pembayaran/m_uang_muka');
        $this->m_uang_muka->create_from_tr($post_array,$primary_key);
    }
    function _create_pc_record($post_array,$primary_key){
        $this->load->model('pembayaran/m_cicilan');
        $this->m_cicilan->create_from_tr($post_array,$primary_key);
    }
}
