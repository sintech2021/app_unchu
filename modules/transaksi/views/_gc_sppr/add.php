<?$form_title = 'Form Surat Permohonan Pembelian Rumah'?>
<div aria-labelledby="Transaksi_sppr" role="dialog" tabindex="-1" id="modalForm" mode="add">
  <div class="">
    <div class="">
      <div class="modal-header" style="border-bottom: none;">
        <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button> -->
        <h4 class="modal-title"><?=$form_title?></h4>
      </div>
      <div class="modal-body">
         <form class="form-horizontal" id="crudForm" enctype="multipart/form-data" action="<?=$insert_url?>">
      <?  foreach($fields as $field): ?>
      <?if($field->field_name == 'konsumen'):?>
      <div class="form-group" v-bind:class="{'has-error':verror.id_konsumen}">
      <?else:?>  
      <div class="form-group" v-bind:class="{'has-error':verror.<?=$field->field_name?>}">
      <?endif?>  
        <label class="col-sm-3 control-label"><?= $input_fields[$field->field_name]->display_as?> <?= ($input_fields[$field->field_name]->required)? "<span class='required'>*</span> " : ""?> </label>
        <div class="col-sm-9">
          <?if($field->field_name == 'id_unit'):?>
          <select @change="onUnitChange()" class="form-control" id="field-id_unit" name="id_unit" v-model="id_unit">
            <option v-for="row in dd_unit" :value="row.value" v-text="row.label"></option>
          </select>
          <?elseif($field->field_name == 'id_grup_proyek'):?>
          <select @change="onPerumahanChange()" class="form-control" id="field-id_grup_proyek" name="id_grup_proyek" v-model="id_grup_proyek">
            <option v-for="row in dd_grup_proyek" :value="row.value" v-text="row.label"></option>
          </select>
          <?elseif($field->field_name == 'termasuk_tj'):?>
          <input @change="onTermasukTjChange()" id="field-termasuk_tj" type="checkbox" name="termasuk_tj" v-model="termasuk_tj"/>
          <?else:?>
          <?= $input_fields[$field->field_name]->input ?>
          <?endif?>
          <?if($field->field_name == 'konsumen'):?>
          <small style="" class="help-block" v-if="verror.id_konsumen">{{verror.id_konsumen}}</small>
          <?else:?>
          <small style="" class="help-block" v-if="verror.<?=$field->field_name?>">{{verror.<?=$field->field_name?>}}</small>
          <?endif?>
        </div>
      </div>

      <?  endforeach  ?>
     
      <?  foreach($hidden_fields as $hidden_field): ?>
            <?= $hidden_field->input; ?>
      <?  endforeach ?>
      
      <?  if ($is_ajax):?>
        <input type="hidden" name="is_ajax" value="true" />
      <?endif?>

      <div id='report-error' class='report-div error'></div>
      <div id='report-success' class='report-div success'></div>
      <div class='form-button-box loading-box' style="display: none;">
        <div class='small-loading' id='FormLoading'><?= $this->l('form_update_loading'); ?></div>
      </div>
    </form>
      </div>
      <div class="modal-footer">
        <button @click="gotoParent()" type="button" class="btn btn-default margin-0" data-dismiss="modal" id="cancel-button"><i class="fa fa-times"></i> <?= $this->l('form_cancel'); ?></button>
        <button type="button" class="btn btn-primary" id="form-button-save"><i class="fa fa-save"></i> <?= $this->l('form_save'); ?></button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  var validation_url = '<?=  $validation_url?>';
  var list_url = '<?=  $list_url?>';

  var message_alert_add_form = "<?=  $this->l('alert_add_form')?>";
  var message_insert_error = "<?=  $this->l('insert_error')?>";

</script>
<?=gc_theme_script_tag('twitter-bootstrap/js/jquery.form.js');?>
<?=gc_theme_script_tag('datatables/js/datatables-add.js');?>
<?=gc_theme_script_tag('../../accordion_form/script.js');?>
<?=gc_theme_script_tag('../../../modules/transaksi/assets/sppr/form.js');?>
<?=gc_theme_script_tag('../../../modules/transaksi/assets/sppr/form_sppr.vue.js');?>


<script type="text/javascript">
$(document).ready(()=>{
  // setTimeout(()=>{
  $('.content-main-0 > h4').hide();
    loadScript(site_url()+'pub/gc/themes/datatables/js/datatables-edit.js');
  
  if(typeof gc.onShowForm == 'function'){
    let modal = $('#modalForm');
    let ajax_url = document.location.href;
    let e = event; 
    gc.onShowForm(modal,modal.attr('mode'),ajax_url,e);
  }
  // },1000);
});
</script>