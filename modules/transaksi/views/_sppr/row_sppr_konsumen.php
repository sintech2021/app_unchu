<table class="tbl-detail">
<tbody>
	<tr><td style="text-align: left;">Nomor SPPR</td><td>:</td><td style="text-align: left;"><?=$nomor_sppr?></td></tr>
	<tr><td style="text-align: left;">Total Harga Jual</td><td>:</td><td style="text-align: left;"><?=rupiah($total_harga_jual)?></td></tr>
	<tr><td style="text-align: left;">Tanggal Transaksi</td><td>:</td><td style="text-align: left;"><?=$tanggal_transaksi?></td></tr>
	<tr><td style="text-align: left;">Metode Pembelian</td><td>:</td><td style="text-align: left;"><?=title_case($metode_pembelian,1,'_')?></td></tr>
	<tr><td style="text-align: left;">Status Pembayaran</td><td>:</td><td style="text-align: left;" class="sppr-status <?=$status_pmb?>"><?=title_case($status_pmb,1,'_')?></td></tr>
</tbody>
</table>