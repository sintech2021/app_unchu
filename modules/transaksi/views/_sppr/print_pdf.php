
<code>
<?
	$td_width = 'width:160px';
	$td_width_r = 'width:220px';
	$t_center = 'text-align:center';
	$t_left = 'text-align:left';
	// print_r($sppr);
	$data = file_get_contents(BASE.'/modules/transaksi/assets/images/sppr_print_bg.png');
	$base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);


?>
</code>
<style type="text/css">
	ol.keterangan > li{
		text-align: justify;
		margin-bottom: .5em;
		line-height: .79em;
		font-size: 14px;
		font-family: "SegoeUI";
	}

	.table-printer{
		width: 100%;
		font-size: 14px;
		font-family: "SegoeUI";
		line-height: .79em;
	}
	#paper{
		
		/*background-*/
		padding: 3em;
		padding-left: 3.5em;
		padding-bottom: 0;
		height: 1050px;
		/*width:900px;*/
		/*border: solid 1px red;*/
	}
	#paper{
		background-image: url(<?=$base64?>); 
		background-repeat: no-repeat;
	}
	
	@page { margin: 0px; }
	#paper-2{
		padding: 2em;
		padding-right: 3em;
	}
	tr td{vertical-align: middle;}
	body { margin: 0px; }
	ol > li{ padding: 0;margin: 0 }
</style>
<div id="paper">
<table class="table-printer">
	<tbody>
		<?
$data = file_get_contents(BASE.'/modules/transaksi/assets/images/sppr_print_logo_sm.png');
	$base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
		?>
		<tr>
			<td colspan="4" style="<?=$t_left?>"><img style="width: 432px" src="<?=$base64?>"> <span style="margin-top: .5em;position: absolute;right: 10em">Nomor : <?=sprintf('%05d',$sppr->id)?></span></td>
		</tr>
		<tr>
			<td colspan="4" style="<?=$t_center?>;font-style: italic;">Bismillahirrahmanirrahim</td>
		</tr>
		<tr>
			<td colspan="4" style="<?=$t_center?>;font-weight: bold;padding-bottom: 10px">SURAT PERMOHONAN PEMBELIAN RUMAH</td>
		</tr>
		<tr>
			<td style="<?=$td_width?>">Nama</td>
			<td colspan="3">: <?=$sppr->konsumen?></td>
		</tr>
		<tr>
			<td style="<?=$td_width?>">No. Telp./HP</td>
			<td colspan="3">: <?=$sppr->nomor_hp?></td>
		</tr>
		<tr>
			<td style="<?=$td_width?>">Pekerjaan</td>
			<td colspan="3">: <?=$sppr->pekerjaan?></td>
		</tr>
		<tr>
			<td style="<?=$td_width?>">Status</td>
			<td colspan="3">: <?=$sppr->status_pernikahan?></td>
		</tr>
		<tr>
			<td colspan="4" style="<?=$t_left?>">Dengan ini mengajukan pembelian rumah, dan setuju untuk menggunakan metode pembelian syariah.</td>
		</tr>
		<tr>
			<td colspan="4" style="<?=$t_left?>">Data rumah yang saya mohonkan : </td>
		</tr>
		<tr> <td colspan="4" style="<?=$t_left?>">&nbsp;</td> </tr>

		<tr>
			<td style="<?=$td_width?>">- Nama Perumahan</td>
			<td colspan="3">: <?=$sppr->perumahan?></td>
		</tr>
		<tr>
			<td style="<?=$td_width?>">- Lokasi</td>
			<td colspan="3">: <?=$sppr->alamat_perumahan?></td>
		</tr>
		<tr>
			<td style="<?=$td_width?>">- Blok / Kavling</td>
			<td colspan="3">: <?=$sppr->unit_blok_kavling?></td>
		</tr>
		<tr>
			<td style="<?=$td_width?>">- Tipe</td>
			<td colspan="3">: <?=$sppr->unit_tipe?></td>
		</tr>
		<tr>
			<td style="<?=$td_width?>">- Luas</td>
			<td colspan="3">: LB : <?=$sppr->unit_luas_bangunan?> m<sup>2</sup> / LT : <?=$sppr->unit_luas_tanah?> m<sup>2</sup></td>
		</tr>
		<tr>
			<td style="<?=$td_width?>">- Metode Pembelian</td>
			<td colspan="3">: <?=title_case($sppr->metode_pembelian,1,'_')?></td>
		</tr>

		<tr> <td colspan="4" style="<?=$t_left?>">&nbsp;</td> </tr>
		<tr> <td colspan="4" style="<?=$t_left?>">Rincian harga jual dan pembayaran:</td> </tr>
		<tr>
			<td style="<?=$td_width?>">Harga Jual Standar</td>
			<td style="<?=$td_width_r?>">: <?=rupiah($sppr->unit_harga_jual)?></td>
			<td style="<?=$td_width?>">Kelebihan Tanah (Hook)</td>
			<td></td>
		</tr>
		<tr>
			<td style="<?=$td_width?>">Booking + UM Standar</td>
			<td style="<?=$td_width_r?>">: <?=rupiah($sppr->booking_um_standar)?></td>
			<td style="<?=$td_width?>">- Ukuran</td>
			<td>: <?= $sppr->klb_tanah_ukuran ?> m<sup>2</sup> </td>
		</tr>
		<tr>
			<td style="<?=$td_width?>">Discount(Jika Ada)</td>
			<td style="<?=$td_width_r?>">: <?=$sppr->diskon?> %</td>
			<td style="<?=$td_width?>">- Harga per m2</td>
			<td>: <?=rupiah($sppr->klb_tanah_harga_per_m2)?></td>
		</tr>
		<tr>
			<td style="<?=$td_width?>">Biaya Tambahan</td>
			<td></td>
			<td style="<?=$td_width?>">- Total Biaya Hook</td>
			<td>: <?=rupiah($sppr->total_bhook)?></td>
		</tr>
		<tr>
			<td style="<?=$td_width?>">- Depan Jalan Utama</td>
			<td style="<?=$td_width_r?>">: <?=rupiah($sppr->bt_jalan_utama)?></td>
			<td style="<?=$td_width?>"></td>
			<td></td>
		</tr>
		<tr>
			<td style="<?=$td_width?>">- Depan Fasum/Fasos</td>
			<td style="<?=$td_width_r?>">: <?=rupiah($sppr->bt_fasum_fasos)?></td>
			<td style="<?=$td_width?>;font-weight: bold;">TOTAL HARGA JUAL</td>
			<td>: <?=rupiah($sppr->total_harga_jual)?></td>
		</tr>
		<tr> <td colspan="4" style="<?=$t_left?>">&nbsp;</td> </tr>
		<tr>
			<td style="<?=$td_width?>;font-weight: bold;">TOTAL UANG MUKA</td>
			<td style="<?=$td_width_r?>">: <?=rupiah($sppr->total_uang_muka)?></td>
			<td style="<?=$td_width?>"></td>
			<td></td>
		</tr>
		<tr> <td colspan="4" style="<?=$t_left?>">&nbsp;</td> </tr>
		
		<tr> 
			<td style="<?=$td_width?>">Tanda Jadi <?=$i?></td>
			<td style="<?=$td_width_r?>">Tgl. <?=dt_indo($sppr->rtb_tanda_jadi)?></td>
			<td colspan="2" style="<?=$td_width?>">: <?=rupiah($sppr->tanda_jadi)?></td>
		</tr>

		<?for($i = 1; $i <= $sppr->jadwal_uang_muka;$i++):?>
		<?
			$prop = "dp_um_{$i}";
			$rtb_um = "rtb_um_{$i}";
		?>
		<tr> 
			<td style="<?=$td_width?>">DP <?=$i?></td>
			<td style="<?=$td_width_r?>">Tgl. <?=dt_indo($sppr->{$rtb_um})?></td>
			<td colspan="2" style="<?=$td_width?>">: <?=rupiah($sppr->{$prop})?></td>
		</tr>
		<?endfor?>
		<tr> <td colspan="4" style="<?=$t_left?>">&nbsp;</td> </tr>
		<tr>
			<td style="<?=$td_width?>">Pilihan Masa Indent</td>
			<td colspan="3">: <?=$sppr->masa_indent." Bulan"?></td>
		</tr>
		<tr>
			<td style="<?=$td_width?>">Total Cicilan Indent</td>
			<td style="<?=$td_width_r?>">: <?=rupiah($sppr->total_cicilan_indent)?></td>
			<td style="<?=$td_width?>">Sisa Hutang</td>
			<td>: <?=rupiah($sppr->sh_total)?></td>
		</tr>
		<tr>
			<td style="<?=$td_width?>">Nilai Cicilan Indent/bulan</td>
			<td style="<?=$td_width_r?>">: <?=rupiah($sppr->nilai_cicilan_indent)?></td>
			<td style="<?=$td_width?>">Dibayar</td>
			<td>: <?=rupiah($sppr->sh_dibayar)?> X</td>
		</tr>
		<tr>
			<td style="<?=$td_width?>"></td>
			<td style="<?=$td_width_r?>"></td>
			<td style="<?=$td_width?>">Kewajiban/bulan</td>
			<td>: <?=rupiah($sppr->sh_kewajiban)?></td>
		</tr>
		<tr>
			<td style="<?=$td_width?>;font-weight: bold;">Promo Khusus</td>
			<td colspan="3">: -</td>
		</tr>
		<tr>
			<td colspan="4">Demikian permohonan ini saya ajukan dan saya bersedia mematuhi ketentuan yang berlaku dari Developer.</td>
		</tr>
	</tbody>
</table>
<table class="table-printer"  style="margin-top: 0;">
	<tbody>
		<tr style="">
			<td style="<?=$t_center?>">
				<p>Pemohon</p>
			</td>
			<td style="<?=$t_center?>">
				Marketing
			</td>
			<td style="<?=$t_center?>">
				Manager
			</td>
			<td style="<?=$t_center?>">
				Direktur
			</td>
		</tr>
		<tr style="">
			<td style="<?=$t_center?>">
				<p>Materai 6000</p>
			</td>
			<td style="<?=$t_center?>">
			</td>
			<td style="<?=$t_center?>">
			</td>
			<td style="<?=$t_center?>">
			</td>
		</tr>
		<tr style="">
			<td style="<?=$t_center?>">
				<p>...........................</p>
			</td>
			<td style="<?=$t_center?>">
				<p>...........................</p>

			</td>
			<td style="<?=$t_center?>">
				<p>...........................</p>

			</td>
			<td style="<?=$t_center?>">
				<p>...........................</p>

			</td>
		</tr>
	</tbody>
</table>
<table class="table-printer" style="margin-top: 0;">
	<tbody>
		<tr>
			<td style="<?=$t_center?>">
				<p>Lembar 1(asli): Developer</p>
			</td>
			<td style="<?=$t_center?>">
				Lembar 2: Pemohon
			</td>
			<td style="<?=$t_center?>">
				Lembar 3: Marketing
			</td>
		</tr>
	</tbody>
</table>
</div>
<div id="paper-2">
<ol class="keterangan">
	<li>Pemohon wajib mengisi Surat Permohonan Pembelian Rumah (SPPR) dan Form Biru (Biodata) serta melunasi Uang Tanda jadi (Booking Fee).</li>	
	<li>Persetujuan SPPR akan diputuskan oleh Komite Persetujuan Kredit internal Developer paling lambat dalam 2 (dua) hari kerja dan persetujuan tersebut akan diinformasikan kepada pemohon.</li>	
	<li>Pemohon wajib menyerahkan kelengkapan data pemohon dan membayar Uang Muka pertama paling lambat 7 (tujuh) hari setelah SPPR disetujui oleh Developer.</li>	
	<li>Apabila pengajuan pemohon ditolak oleh Developer maka Developer akan mengembalikan Uang Tanda Jadi (Booking Fee) kepada pemohon paling lambat dalam waktu 14 (empat belas) hari kerja.</li>	
	<li>Developer berhak membatalkan proses pembelian dalam kondisi sebagai berikut :
<br/>(a) Pemohon tidak menyerahkan kelengkapan data atau tidak melunasi pembayaran uang muka pertama<br/>&nbsp;&nbsp;&nbsp;&nbsp; sesuai jadwal yang telah disepakati (wanprestasi).
<br/>(b) Pemohon terbukti memalsukan data pribadi maupun data penghasilan.</li>	
	<li>Apabila konsumen melakukan wanprestasi seperti tertulis pada pasal No. 5 poin (a) maupun poin (b) maka konsumen bersedia dikenakan biaya administrasi sebesar Uang Tanda Jadi (Booking Fee).</li>
	<li>Pembayaran Uang Muka selanjutnya dibayarkan paling lambat tanggal 10 (sepuluh) setiap bulannya.</li>
	<li>Proses penandatanganan Akad Istishna’ dilaksanakan setelah konsumen telah melunasi Uang Muka.</li>
	<li>Apabila konsumen melakukan pembatalan sepihak setelah konsumen membayar Uang Muka, maka konsumen bersedia dikenakan biaya sebesar Uang Tanda Jadi (Booking Fee) + administrasi sebesar <?=rupiah($sppr->biaya_adm)?> Sisa pembayaran akan dikembalikan dalam jangka waktu 6 (enam) bulan setelah ditandatanganinya Surat Permohonan Pembatalan.</li>
	<li>Pembatalan sepihak setelah proses Akad Istishna’ maka maka konsumen bersedia dikenakan biaya sebesar Uang Tanda Jadi (Booking Fee) + administrasi sebesar <?=rupiah($sppr->biaya_adm)?> Sisa pembayaran akan dikembalikan dalam jangka waktu 2 (dua) tahun atau setelah unit tersebut terjual kembali dan Developer telah menerima pembayaran sejumlah nilai pengembalian.</li> 
</ol>	
</div>




