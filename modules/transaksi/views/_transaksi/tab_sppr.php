<div class="content-tab" id="content_tab_persetujuan_pindah">
    <ul class="nav nav-tabs">
        <li v-for="caption,url in tabs" v-bind:url="url" v-bind:class="{'active':(url==active_item)}">
            <a v-bind:href="'#'+caption" data-toggle="tab" @click="gotoUrl(url)">{{caption}} </a>
        </li>
    </ul>
</div>
<script type="text/javascript">
    $(document).ready(()=>{
        var _tab = new Vue({
            el : '#content_tab_persetujuan_pindah',
            data :{
                tabs : {"transaksi\/sppr":"SPPR","transaksi\/persetujuan-pindah":"Persetujuan Pindah\/Ubah","transaksi\/pernyataan-batal":"Pernyataan Batal"},
                active_item : window.breadcrumb.active_item
            } ,
            mounted(){
            },
            methods:{
                gotoUrl(url){
                    document.location.href = site_url()+url;
                }
            }
        });
    });
</script>
