$(document).ready(()=>{
      window.success_message = ()=>{
        document.location.href = site_url() + 'transaksi/sppr';
      };
      
      gc.form_vm = new Vue({
      el:'#modalForm',
      data:{
        mode: 'add',
        id: -1,
        id_grup_proyek:'',
        id_unit:'',
        metode_pembelian: '',
        mp_cicilan: '',
        diskon: 0,
        bt_jalan_utama: 0,
        bt_fasum_fasos: 0,
        total_bt: 0,

        klb_tanah_ukuran: 0,
        klb_tanah_harga_per_m2: 0,
        total_bhook: 0,
          
        tanda_jadi:0,
        total_harga_jual:'Rp. 0',

        jadwal_uang_muka:'',
        dp_um_1:0,
        dp_um_2:0,
        dp_um_3:0,
        dp_um_4:0,
        total_uang_muka:0,

        masa_indent:'',
        nilai_cicilan_indent:0,
        total_cicilan_indent:0,
        
        sh_dibayar:0,
        sh_kewajiban:0,
        sh_total:0,

        unit:{},
        dd_unit:[{
          label: '-- Pilih Unit --',value:''
        }],
        dd_grup_proyek:[],
        currency_fields: ['bt_jalan_utama','bt_fasum_fasos','total_bt',
                              'klb_tanah_harga_per_m2','total_bhook',
                              'tanda_jadi','total_harga_jual',
                              'dp_um_1','dp_um_2','dp_um_3','dp_um_4','total_uang_muka',
                              'nilai_cicilan_indent','total_cicilan_indent',
                              'sh_kewajiban','sh_total'],
        verror:{},
        um_fields: ['jadwal_uang_muka','dp_um_1','dp_um_2','dp_um_3','dp_um_4','total_uang_muka'],
        mi_fields: ['masa_indent','nilai_cicilan_indent','total_cicilan_indent'],
        sh_fields: ['sh_dibayar','sh_kewajiban','sh_total'],
        active_ms_indent: null,
        termasuk_tj:false,
        // ro fields
        nomor:'',
        nik: '',nomor_hp:'',email:'',alamat:''
      },
      mounted(){
        this.$nextTick(()=>{
          this.populateForm(gc.detail,()=>{
            this.loadDD_perumahan();
            this.onMetodePembelianChange();
            this.attachCurrencyCallback(()=>{
              this.calculateBt();
              this.onJadwalUmChange();
            });
            this.reloadDatePicker();
            this.getActiveMsIndent();
            this.onTermasukTjChange();
          });
          
        });
      },
      methods:{
        populateForm(row,cb){
          if(typeof row == 'object'){
            console.log(row);
            for(let prop in row){
              if(typeof this.$data[prop] != 'undefined'){
                this.$data[prop] = $.inArray(prop,this.currency_fields) != -1 ? formatRupiah(row[prop]):row[prop];

                
              }
            }
            // if(typeof cb == 'function'){

              this.loadDD_perumahan(()=>{
                this.loadDD_unit(()=>{
                  this.onUnitChange();
                  this.onMetodePembelianChange();
                  this.attachCurrencyCallback(()=>{
                    this.calculateBt();
                    this.onJadwalUmChange();
                  });
                  this.reloadDatePicker();
                  this.getActiveMsIndent();
                  this.onTermasukTjChange();
                  
                });
              });

              // this.onPerumahanChange();
              // cb();
            // }
          }else{
            cb();
          }
        },
        gotoParent(){
          document.location.href = site_url() + `transaksi/sppr`;
        },
        onTermasukTjChange(){
          // console.log(this.termasuk_tj);
          // if(!this.termasuk_tj){
            // $('#field-tanda_jadi').closest('.form-group')[!this.termasuk_tj?'hide':'show']();//.attr('disabled',!this.termasuk_tj);
            const tj_toggler_checked_index = this.termasuk_tj ? 1 : 0;
            $(`input#tj_toggler_${tj_toggler_checked_index}`).attr('checked',true);
          // }
          this.calculateTHJ();
        },
        getActiveMsIndent(){
          $.post(site_url()+`transaksi/sppr/get_active_masa_indent`,{},(res)=>{
            if(res.success){
              console.log(res);
              if(typeof res.item == 'object'){
                this.active_ms_indent = res.item;
                this.calculateTotalCicilanIndent();
              }
            }            
          },'json');
        },
        reloadDatePicker(){
          setTimeout(()=>{
            $('.datepicker-input').datepicker({
                dateFormat: js_date_format,
                // container: container,
                format:js_date_format,
                language:'id',
                todayHighlight: true,
                autoclose: true,
                changeMonth: true,
                changeYear: true
            });
            $('.numeric').numeric();  
          },3000);
          
        },
        unsetVerror(key){
          let verror = {};
          for(let k in this.verror){
            if(k != key){
              verror[k] = this.verror[k];
            }
          }
          this.verror = verror;
        },
        calculateTHJ(){
          // setTimeout(()=>{
            let error_cnt = 0;
            if(parseInt(this.mp_cicilan) > 360){
              this.verror.mp_cicilan = 'Jumlah Cicilan tidak boleh melebihi 360';
              error_cnt += 1;
              this.unsetVerror('mp_cicilan');
            }else{
              this.verror.mp_cicilan = false;
            }
            if(parseInt(this.diskon) > 99){
              this.verror.diskon = 'Diskon tidak boleh melebihi 99%';
              error_cnt += 1;
            }else{
              this.unsetVerror('diskon');
            }

            if(error_cnt>0){
              this.total_harga_jual = '!Error';
              this.verror.total_harga_jual = 'Tidak dapat menghitung total';
              return;
            }else{
              this.unsetVerror('total_harga_jual');
            }
            let harga_jual_standar = 0;
            try{
              harga_jual_standar = parseInt(this.unit.harga_jual);
            }catch(e){
              harga_jual_standar = 0;
            }
            const diskon = parseInt(this.diskon)/100;
     
            let total_harga_jual = (harga_jual_standar - (harga_jual_standar * diskon)) + rupiahToInt(this.total_bt) + rupiahToInt(this.total_bhook) + (this.termasuk_tj? rupiahToInt(this.tanda_jadi):0);
            
            // console.log(total_harga_jual+0);
            if(isNaN(total_harga_jual)){
              total_harga_jual = 0;
            }

            this.total_harga_jual = formatRupiah(total_harga_jual);
            console.log('calculateTHJ');
            this.calculateTSH();
          // },250);
          
        },
        calculateTSH(){
          
          let total_sisa_hutang = 0;
          let kewajiban_per_bulan = 0;
          let sh_dibayar = parseInt(this.sh_dibayar);

          if(this.metode_pembelian == 'cash_bertahap'){
              total_sisa_hutang = rupiahToInt(this.total_harga_jual) - rupiahToInt(this.tanda_jadi);
              kewajiban_per_bulan = total_sisa_hutang / sh_dibayar;
          }
          else if(this.metode_pembelian == 'cicilan_120_x'){
              total_sisa_hutang = rupiahToInt(this.total_harga_jual) - rupiahToInt(this.tanda_jadi) - rupiahToInt(this.total_uang_muka) - rupiahToInt(this.total_cicilan_indent);
              kewajiban_per_bulan = total_sisa_hutang / sh_dibayar; 
          }else{
              total_sisa_hutang = 0;
              kewajiban_per_bulan = 0;
          }
          kewajiban_per_bulan = Math.round(kewajiban_per_bulan);
          this.sh_total = formatRupiah(total_sisa_hutang);
          this.sh_kewajiban = formatRupiah(kewajiban_per_bulan);
          console.log('calculateSH');
        },
        calculateTotalUm(){
          let total_um = rupiahToInt(this.dp_um_1) +  rupiahToInt(this.dp_um_2) +  rupiahToInt(this.dp_um_3) +  rupiahToInt(this.dp_um_4) ;
          this.total_uang_muka = formatRupiah(total_um);
          this.calculateTHJ();
        },
        calculateTotalCicilanIndent(){
          this.total_cicilan_indent = formatRupiah(this.active_ms_indent.nominal_indent);
          const nilai_cicilan_indent = Math.round(this.active_ms_indent.nominal_indent/parseInt(this.masa_indent));
          this.nilai_cicilan_indent = formatRupiah(nilai_cicilan_indent);
          console.log('calculateTotalCicilanIndent'); 
        },
        calculateBHook(){
          let total_bhook = this.klb_tanah_ukuran * rupiahToInt(this.klb_tanah_harga_per_m2);
          this.total_bhook = formatRupiah(total_bhook);
          this.calculateTHJ();
        },
        calculateBt(){
          let total_bt = rupiahToInt(this.bt_jalan_utama) + rupiahToInt(this.bt_fasum_fasos);
          this.total_bt = formatRupiah(total_bt);
          this.calculateTHJ();
        },
        attachCurrencyCallback(cb){
          const currencies  = this.currency_fields;
          const onCurrencyChanged = (el)=>{
            const new_val = formatRupiah(el.value);
            this.$data[el.name] = formatRupiah(el.value);
            // $(el).val(new_val);
          };
          currencies.forEach((field_name)=>{
            // console.log(field_name);
            const nd_currency = $(`#field-${field_name}`);
            nd_currency.attr('placeholder','Rp. 0');
            nd_currency.attr('maxlength','');
            nd_currency.change(function(){
              onCurrencyChanged(this);
            });
            nd_currency.keyup(function(){
              onCurrencyChanged(this);
            });

            nd_currency.change();
          });

          if(typeof cb == 'function'){
            cb();
          }
        },
        toggleUM(disabled, reset){
          const uang_muka_fields = this.um_fields;
            uang_muka_fields.forEach((field_name)=>{
              if(reset){
                this.$data[field_name] = formatRupiah('0');
              }
              
              const um_nd = $(`#field-${field_name}`);

              if(field_name == 'jadwal_uang_muka'){
                if(reset){
                  this.$data[field_name] = '';
                }
                // um_nd.change();
              }
              if(disabled){
                um_nd.attr('disabled',disabled);
              }else{
                um_nd.attr('disabled',disabled);
              }
              
            });
        },
        toggleMI(disabled, reset){
          const masa_indent_fields = this.mi_fields;
            masa_indent_fields.forEach((field_name)=>{
              if(reset){
                this.$data[field_name] = formatRupiah('0');
              }
              const mi_nd = $(`#field-${field_name}`);

              if(field_name == 'masa_indent'){
                if(reset){
                  this.$data[field_name] = '';
                }
                mi_nd.change();
              }
              if(disabled){
                mi_nd.attr('disabled',disabled);  
              }else{
                mi_nd.attr('disabled',disabled);
              }
              
            });
        },
        toggleSH(disabled, reset){
          const sh_fields = this.sh_fields;
            sh_fields.forEach((field_name)=>{
              if(reset){
                this.$data[field_name] = formatRupiah('0');
              }
              const sh_nd = $(`#field-${field_name}`);

              if(field_name == 'sh_dibayar'){
                if(reset){
                  this.$data[field_name] = '0';
                }
              }
              if(disabled){
                sh_nd.attr('disabled',disabled);  
              }else{
                sh_nd.attr('disabled',disabled);
              }
              
            });
        },
        toggleRTBInput(field_name, disabled,reset){
          const nd = $(`#field-${field_name}`);
          if(reset){
            nd.val('');
          }
          if(disabled){
            nd.attr('disabled',disabled);  
          }else{
            nd.attr('disabled',disabled);
          }

        },
        toggleMpInputs(){
          const nd_ip = $('#field-mp_cicilan');
          const nd_fg = nd_ip.closest('.form-group');
          // console.log(this.metode_pembelian);

          if(this.metode_pembelian == 'cash_keras'){
            // 1. semua kolom input uang muka 0
            this.toggleUM(true,true);

            // 2. semua kolom masa indent 0
            this.toggleMI(true,true);

            // 3. sh
            this.toggleSH(true,true);

            // 4. rtb bayar um
            this.toggleRTBInput('rtb_uang_muka',true,true);
            this.onJadwalUmChange();
          }
          else if(this.metode_pembelian == 'cash_bertahap'){
            // 1. semua kolom input uang muka 0
            this.toggleUM(false,false);

            // 2. semua kolom masa indent 0
            this.toggleMI(true,true);

            // 3. sh
            this.toggleSH(false,false);

            // 4. rtb bayar um
            this.toggleRTBInput('rtb_uang_muka',false,false);

          }
          else if(this.metode_pembelian == 'cicilan_120_x'){
            // 1. semua kolom input uang muka 0
            this.toggleUM(false,false);

            // 2. semua kolom masa indent 0
            this.toggleMI(false,false);

            // 3. sh
            this.toggleSH(false,false);

            // 4. rtb bayar um
            this.toggleRTBInput('rtb_uang_muka',false,false);
            this.getActiveMsIndent();
          }
          // else{
          //   this.mp_cicilan = this.metode_pembelian == 'cicilan_120_x' ? 120 : 0;
          //   
          // }
          nd_fg.hide();
        },
        onJadwalUmChange(){
          console.log('onJadwalUmChange');
          // const nd_um = $('#field-mp_cicilan').closest('.form-group');
          const jadwal_uang_muka = parseInt(this.jadwal_uang_muka);
          // console.log(jadwal_uang_muka);
          const max_jd_um = 4;
          let index = 1;
          let nd_um = '';
          if(jadwal_uang_muka>0){
            for(let i=1;i<=jadwal_uang_muka;i++){
              index = i;
              nd_um = $(`#field-dp_um_${index}`).closest('.form-group');
              nd_um.show();
            }
            index+=1;

          }
          if(index < max_jd_um){
           for(let i=index;i<=max_jd_um;i++){
              index = i;
              nd_um = $(`#field-dp_um_${index}`);
              const name = nd_um[0].name;
              this.$data[name] = formatRupiah('0');
              nd_um.closest('.form-group').hide();

            } 
          }
          this.calculateTotalUm();
          this.setFormModeSession();
        },
        onMetodePembelianChange(){
          this.toggleMpInputs();
          this.calculateTSH();
          this.calculateTHJ();
          this.setFormModeSession();
        },
        setFormModeSession(){
          const url = site_url() + `transaksi/sppr/set_form_mode_session/${this.metode_pembelian}?jadwal_uang_muka=${this.jadwal_uang_muka}`;
          $.get(url,(res)=>{
            if(res.success){
              //
            }
          },'json');
        },
        onPerumahanChange(){
          // console.log(this.id_grup_proyek);
          this.loadDD_unit();
        },
        onUnitChange(){
          // console.log(this.id_unit);
          const url = site_url() + `data/detail_unit_perumahan/${this.id_unit}`;
          $.post(url,{},(res)=>{
              const unit = res.data;
              $('#field-alamat_perumahan').val(unit.alamat);
              $('#field-unit_tipe').val(unit.tipe);
              $('#field-unit_luas_tanah').val(unit.luas_tanah);
              $('#field-unit_luas_bangunan').val(unit.luas_bangunan);
              $('#field-unit_harga_jual').val(formatRupiah(unit.harga_jual));
              // console.log(unit);
              this.unit = unit;
              this.calculateTHJ();
          },'json');
        },
        loadDD_perumahan(cb){
          // let postData = {parent_id:parent_id};
          const url = site_url() + `data/dd_grup_proyek`;
          $.post(url,{},(res)=>{
              this.dd_grup_proyek = res.data;
              if(typeof cb == 'function'){
                cb();
              }
          },'json');
        },
        loadDD_unit(cb){
          const url = site_url() + `transaksi/sppr/dd_unit_perumahan/${this.id_grup_proyek}/${this.id}`;
          $.post(url,{},(res)=>{
              this.dd_unit = res.data;
              if(typeof cb == 'function'){
                cb();
              }
          },'json');
        }
      }
    });
  });
  