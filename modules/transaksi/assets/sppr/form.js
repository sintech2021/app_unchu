$(document).ready(()=>{
   gc.__onShowForm = (form,state,ajax_url,e) =>{
   		document.title = (state=='edit'?'Edit':'Tambah') + ' Transaksi SPPR';
   		if(state == 'add'){
   			$.get(site_url()+`transaksi/sppr/get_nomor_transaksi`,(r)=>{
	   			console.log(r);
	   			let nd_nomor = form.find('#field-nomor');
	   			if(nd_nomor.val()==''){
	   				nd_nomor.val(r.nomor);
	   			}
	   			
	   		},'json');	
   		}
   		
		const style_ = {
			'font-size' : '18px',
			'font-weight' : 'bold',
			'text-transform' : 'uppercase'	
		};
		form.find('#field-total_harga_jual_ro').css(style_).closest('.form-group').find('label').css(style_);
		
		const disabled_ids = ['nomor','nik','nomor_hp','email','alamat','alamat_perumahan','unit_tipe',
		'unit_luas_tanah','unit_luas_bangunan','unit_harga_jual',

		];
		const ro_fields = ['total_harga_jual',
				'total_bt','total_bhook','total_uang_muka',
				'total_cicilan_indent','sh_total'];
		disabled_ids.forEach((field_)=>{
			form.find(`#field-${field_}`).attr('disabled',true);
		});
		ro_fields.forEach((field_)=>{
			form.find(`#field-${field_}_ro`).attr('disabled',true);
			form.find(`#field-${field_}`).closest('.form-group').hide();
		});

		// toggle tanda jadi
		// const tj_toggler_value = $('#field-termasuk_tj').val()
		const tj_toggler = `<div class="form-group">
								<div class="col-sm-3"></div>	
								<div class="col-sm-9">
									<div class="row">
										<div class="col-sm-6">
										<input class="tj_toggler" id="tj_toggler_1" name="tj_toggler" type="radio" value="1"/> <label for="tj_toggler_1" class="control-label">Termasuk Harga Jual 
										</label>	
										</div>
										<div class="col-sm-6">
										<input class="tj_toggler" id="tj_toggler_0" name="tj_toggler" type="radio" value="0"/> <label for="tj_toggler_0" class="control-label">Tidak Termasuk Harga Jual</label>	
										</div>
 									</div>
								</div>	
						   </div>`;
		const tj_toggler_changed = function(){
			const name = this.name;
			const val = this.value;
			console.log(val);
			gc.form_vm.termasuk_tj = val == '1';
			gc.form_vm.onTermasukTjChange();

		};				   
		$('#field-termasuk_tj').closest('.form-group').hide();
		$('#field-tanda_jadi').closest('.form-group').after(tj_toggler);
		$('input.tj_toggler').change(tj_toggler_changed).click(tj_toggler_changed);

		form.find('#field-id_konsumen').closest('.form-group').hide();
		let input_konsumen = form.find('#field-konsumen').get(0);
		let url_ac_konsumen = `data/ac_konsumen`;
		const displayKey_konsumen = 'nama_lengkap';
		createAcWithCb(input_konsumen, url_ac_konsumen,displayKey_konsumen,(e, d)=> {
            form.find('#field-id_konsumen').val(d.id_data_konsumen);
            form.find('#field-nik').val(d.identitas);
            form.find('#field-nomor_hp').val(d.no_hp);
            form.find('#field-email').val(d.email);
            form.find('#field-alamat').val(d.alamat_rumah);
        });
   };
});