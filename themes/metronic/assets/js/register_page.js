  window.messages = {
    nama_lengkap: {
        required: "Nama Lengkap harus di isi"
    },
    nik: {
        required: "NIK harus di isi"
    },
    nik_file: {
        required: "Anda harus memilih file KTP untuk diunggah"
    },
    ktp_selfi_file: {
        required: "Anda harus memilih file KTP Selfi untuk diunggah"
    },
    email: {
        required: "Email harus di isi dengan email yang valid"
    },
    nomor_hp: {
        required: "Nomor HP harus di isi"
    },
    password: {
        required: "Kata Sandi harus di isi"
    },
    repeat_password:{
        required: "Ulangi Kata Sandi harus sama dengan Kata Sandi Anda"
    },
};
 var Register = function() {

    var handleRegister = function() {

        $('form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                nama_lengkap: {
                    required: true
                },
                nik: {
                    required: true
                },
                nik_file: {
                    required: true
                },
                email: {
                    required: true,
                    email:true
                },
                nomor_hp: {
                    required: true
                },
                password: {
                    required: true
                },
                repeat_password:{
                    required: true
                },
            },

           messages: messages,

            invalidHandler: function(event, validator) { //display error alert on form submit   
                $('.alert-danger', $('form')).show();
            },

            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function(error, element) {
                error.insertAfter(element.closest('.input-icon'));
            },

            submitHandler: function(form) {
                if(APP_YML_ENABLE_GOGLE_CAPTCHA){
                    if(typeof grecaptcha != 'undefined'){
                        if(grecaptcha.getResponse() != ''){

                        if( $('input[name=password]').val() != $('input[name=repeat_password]').val() )  {
                            swal("Ulangi Kata Sandi harus sama dengan Kata Sandi Anda");
                            $('input[name=repeat_password]').focus();
                            return false;
                        }  

                        beginServerValidate(function(){
                            form.submit(); // form validation success, call ajax form submit

                        });


                        }else{
                            swal('Silahkan konfirmasi anda bukan Robot');
                        }
                    }else{
                        swal('Gagal memuat recaptcha , Silahkan refresh halaman ini');
                    }
                }else{
                    if( $('input[name=password]').val() != $('input[name=repeat_password]').val() )  {
                        swal("Ulangi Kata Sandi harus sama dengan Kata Sandi Anda");
                        $('input[name=repeat_password]').focus();
                        return false;
                    }  

                    beginServerValidate(function(){
                        form.submit(); // form validation success, call ajax form submit

                    });
                }
                
                
            }
        });

        $('form input').keypress(function(e) {
            if (e.which == 13) {
                if ($('form').validate().form()) {
                    $('form').submit(); //form validation success, call ajax form submit
                }
                return false;
            }
        });


        // $('#forget-password').click(function(){
        //     $('form').hide();
        //     $('.forget-form').show();
        // });

        // $('#back-btn').click(function(){
        //     $('.register-form').show();
        //     $('.forget-form').hide();
        // });
    }

 
  

    return {
        //main function to initiate the module
        init: function() {

            handleRegister();

            // init background slide images
            $('.login-bg').backstretch([
                site_url() + "themes/metronic/assets/pages/img/login/bg1.jpg",
                site_url() + "themes/metronic/assets/pages/img/login/bg2.jpg",
                site_url() + "themes/metronic/assets/pages/img/login/bg3.jpg"
                ], {
                  fade: 1000,
                  duration: 8000
                }
            );

            // $('.forget-form').hide();

        }

    };

}();

jQuery(document).ready(function() {
    $('button[type=submit]').removeAttr('disabled');
     Register.init();
    console.log('hello');

    $('form').submit(function(e){

        console.log(arguments);
        // $(this).validate();
        $(this).find('input').each(function(){
            let name = this.name;
            console.log(name)
            if(this.value == ''){
                swal(window.messages[name].required);
                e.preventDefault();
                return false;
            }
        });
        e.preventDefault();
        return false;
    });

    $('a#daftar_button').click(function(){
        //
        $('form').submit();
    });
});

function beginServerValidate(callback) {
    // Cek NIK
    let postData = {
        nik : $('input[name=nik]').val(),
        email : $('input[name=email]').val(),
        nomor_hp : $('input[name=nomor_hp]').val()
    };
    // Cek Email

    // Cek Nomor_HP

    $.post(site_url()+'register?unique_validation=true',postData,function(r){
        //
        if(r.success){
            if(typeof callback == 'function'){
                callback();
            }
        }else{
            swal(r.message);
        }
        
    },'json');

    
}
