<!DOCTYPE html>
 
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>Registrasi Berhasil</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <!-- <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" /> -->
        <link href="{{ theme_assets }}/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ theme_assets }}/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ theme_assets }}/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ theme_assets }}/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="{{ theme_assets }}/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="{{ theme_assets }}/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="{{ theme_assets }}/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ theme_assets }}/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="{{ theme_assets }}/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        
        <link rel="shortcut icon" href="{{ base_url }}pub/img/favicon.ico">
         
    </head>
    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-boxed">
        <div class="page-wrapper">
            <!-- BEGIN HEADER -->
            
            <!-- END HEADER -->
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->
            <div class="container">
                <!-- BEGIN CONTAINER -->
                <div class="page-container">
                    <!-- BEGIN SIDEBAR -->
                 
                    <!-- END SIDEBAR -->
                    <!-- BEGIN CONTENT -->
                    <div class="page-content-wrapper">
                        <!-- BEGIN CONTENT BODY -->
                        <div class="page-content" style="margin-left: 0">
                            <!-- BEGIN PAGE HEADER-->
                            <!-- BEGIN THEME PANEL -->
                                
                            <!-- END THEME PANEL -->
                            
                            <!-- BEGIN PAGE TITLE-->
                            <h1 class="page-title"> Tinggal Selangkah Lagi 
                                <small></small>
                            </h1>
                            <!-- END PAGE TITLE-->
                            <!-- END PAGE HEADER-->
                            <div class="note note-info">
                                <p> Silahkan periksa email anda dengan membuka link verifikasi di kotak masuk anda. </p>
                                <p> Klik tombol kirim verifikasi email berikut jika anda belum menerima email dari kami. </p>

                                <div style="padding: 1em">
                                    <button id="send_email_again" class="btn btn-success"><i class="fa fa-envelope"></i> KIRIM SAYA EMAIL LAGI </button>
                                </div>
                            </div>
                        </div>
                        <!-- END CONTENT BODY -->
                    </div>
                    <!-- END CONTENT -->
                    <!-- BEGIN QUICK SIDEBAR -->
                    
                    
                    <!-- END QUICK SIDEBAR -->
                <!-- BEGIN FOOTER -->
<div class="page-footer" style="text-align: center;background: #fff;">
    <div class="" style="line-height: 0.7"><?=APP_YML['app_name']?> &copy; <?=date('Y')?> PT Sintech Berkah Abadi - All Rights Reserved.
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>
                <!-- END FOOTER -->
                </div>
                <!-- END CONTAINER -->

            </div>
            
        </div>
        
        <!-- END QUICK NAV -->
        <!--[if lt IE 9]>
<script src="{{ theme_assets }}/global/plugins/respond.min.js"></script>
<script src="{{ theme_assets }}/global/plugins/excanvas.min.js"></script> 
<script src="{{ theme_assets }}/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
<script type="text/javascript" src="{{ base_url }}www_static/js/app_js"></script>

        <script src="{{ theme_assets }}/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="{{ theme_assets }}/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="{{ theme_assets }}/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="{{ theme_assets }}/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="{{ theme_assets }}/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="{{ theme_assets }}/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="{{ theme_assets }}/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="{{ theme_assets }}/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
        <script src="{{ theme_assets }}/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
        <script src="{{ theme_assets }}/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <script src="{{ theme_assets }}/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
        <script>
            $(document).ready(function()
            {
                $('#clickmewow').click(function()
                {
                    $('#radio1003').attr('checked', 'checked');
                });
                $('#send_email_again').click(function(){
                    $('#send_email_again').attr('disabled',true);
                    $.get('{{ site_url }}register?resend_email=true&owner=<?=$owner?>&data=<?=$data?>',function(r){
                        swal(r);
                         $('#send_email_again').attr('disabled',false);

                    });
                });
            })
        </script>
    </body>

</html>