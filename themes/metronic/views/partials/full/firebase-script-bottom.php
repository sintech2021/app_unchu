<!-- Insert these scripts at the bottom of the HTML, but before you use any Firebase services -->

  <!-- Firebase App (the core Firebase SDK) is always required and must be listed first -->
<?if(APP_YML['app_enable_firebase']):?>  
<script type="text/javascript" src="https://www.gstatic.com/firebasejs/7.14.0/firebase-app.js"></script>
<script>
function loadGFB(){
  // $.getScript("");
  $.getScript("https://www.gstatic.com/firebasejs/7.14.0/firebase-messaging.js",function(){
    const firebaseConfig = {
    apiKey: "AIzaSyDej5H_A9psXwejKlGdnAM0CTvNNXslXHc",
    authDomain: "ppsl-perumdamtkr.firebaseapp.com",
    databaseURL: "https://ppsl-perumdamtkr.firebaseio.com",
    projectId: "ppsl-perumdamtkr",
    storageBucket: "ppsl-perumdamtkr.appspot.com",
    messagingSenderId: "843835605412",
    appId: "1:843835605412:web:7cb192708975f3aaba0131",
    measurementId: "G-WHVNS2ZNBM"
  };

    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);

    const messaging = firebase.messaging();
    messaging.requestPermission()
    .then(function(){
      console.log('fcm:you have permission');
      return messaging.getToken();
    })
    .then(function(token){
      sendTokenToServer(token);
      
    })
    .catch(function(err){
      console.log('Terjadi Error');
    });

      $.post(site_url()+'data/notificationBars',function(data){
        // console.log(data);
        window.$vmph.notifications = data;
      },'json');

      messaging.onMessage(function(payload){
      console.log(payload);
      
      try{  //try???
        console.log('Message received. ', payload);

        noteTitle = payload.notification.title; 
        noteOptions = {
          body: payload.notification.body,
          icon: "perumdamtkr-icon.png", //this is my image in my public folder,
          // link: site_url()
        };

        console.log("title ",noteTitle, " ", payload.notification.body);
            //var notification = //examples include this, seems not needed

        let notif = new Notification(noteTitle, noteOptions);//This can be used to generate a local notification, without an incoming message. noteOptions has to be an object
         
        
      }
      catch(err){
        console.log('Caught error: ',err);
      }               

      // const {title, ...options} = payload.notification;
      // navigator.serviceWorker.ready.then(registration => {
      //     registration.showNotification(notification,title, {});
      // });
      $.post(site_url()+'data/notificationBars',function(data){
        // console.log(data);
        window.$vmph.notifications = data;
      },'json');
      
    });

    messaging.onTokenRefresh(() => {
    messaging.getToken().then((refreshedToken) => {
    // console.log('Token refreshed.');
    // Indicate that the new Instance ID token has not yet been sent to the
    // app server.
    setTokenSentToServer(false);
    // Send Instance ID token to app server.
    sendTokenToServer(refreshedToken);
    // ...
    }).catch((err) => {
      console.log('Unable to retrieve refreshed token ', err);
      showToken('Unable to retrieve refreshed token ', err);
    });
  });
  function sendTokenToServer(refreshedToken){
    // console.log(refreshedToken);
    $.post(site_url()+'data/saveToken',{token:refreshedToken},function(){
      $.post(site_url()+'data/notificationBars',function(data){
        // console.log(data);
        window.$vmph.notifications = data;
      },'json');
    });
  }
  });

   
} 
$(document).ready(function(){
	// TODO: Replace the following with your app's Firebase project configuration
 

});
setTimeout(()=>{loadGFB();},250)

	   
</script>
<?endif?>