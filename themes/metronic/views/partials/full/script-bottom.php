<!--[if lt IE 9]>
<script src="{{ theme_assets }}/global/plugins/respond.min.js"></script>
<script src="{{ theme_assets }}/global/plugins/excanvas.min.js"></script> 
<script src="{{ theme_assets }}/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
<div id="logger" class="btn btn-circle">
<div id="loading">
    <!-- loading.... -->
</div>        
<div class="log"></div>
</div>

<?=js_assets('bottom_js')?>
 
        
      


<style type="text/css">
.page-content-white .page-content .page-bar{
    border-bottom: none;
}
.page-content .content-tab{
    padding-top: .19em;
}    
.dataTables_wrapper .dataTables_paginate .paginate_button{
    display: inline-block;
    padding: 0 0 0 1px;
}            
#logger{
    position: fixed;
    bottom: 0;
    right: 0;
    background: #000;
    color: #fff;
    opacity: .8;
    padding: 2px;
    font-size: 10px;
}
.btn.refresh-data{
    display: none;
}
th.no{
    width: 10px;
    text-align: right;
}
th.actions{
    width: 105px;
    text-align: center;
}
td.actions{
    text-align: center;
}
.btn-menu{
    padding: .5em;
    color: #fff !important;
    font-weight: bold;
    height: 10px !important;
    line-height: 0 !important;
    margin-top: 10px;
}
.btn-menu:hover{
    border-radius: 12px;
    color: #fff !important;
    font-weight: bold;
    background: #666 !important;

}
th.actions.sorting,
th.no.sorting_asc,
th.no.sorting_desc{
    background: none !important;
}
</style>